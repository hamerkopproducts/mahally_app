import {
  /** Settings */
  GET_SETTINGS_CONTENT,
  RESET_GET_SETTINGS_CONTENT_ERROR,
  RESET_SETTINGS_TEMP_DATA,
} from "./types";

import apiHelper, { executeApiAction } from "../lib/apiHelper";


/** Fetch settings */
export const getSettingsContent = (data) => {
  return async (dispatch) => {
    dispatch(
      executeApiAction(
        "get",
        apiHelper.getSettingsAPI(),
        GET_SETTINGS_CONTENT,
        true,
        data
      )
    );
  };
};

export const resetSettingsError = () => {
  return {
    type: RESET_GET_SETTINGS_CONTENT_ERROR,
  };
};

export const resetSettingsTempData = () => {
  return {
    type: RESET_SETTINGS_TEMP_DATA,
  };
};
