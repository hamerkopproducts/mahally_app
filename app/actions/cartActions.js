import * as ActionTypes from './types';

// Actions
export const cartUpdateAction = (data) => ({
	type: ActionTypes.MODIFY_CART_DATA,
    data: data
});

export const selectRestaurantAction = (data) => ({
	type: ActionTypes.SELECT_RESTAURANT,
    data: data
});

export const applyCouponCodeAction = (data) => ({
	type: ActionTypes.COUPON_CODE,
    data: data
});

export const postScanItemToAddAction = (data) => ({
	type: ActionTypes.POST_SCAN_ADD_ITEM_DATA,
    data: data
});

// Dispatches
export function cartUpdate(data) {
	return async dispatch => {
		dispatch(cartUpdateAction(data));
	}
}

export function selectRestaurant(data) {
	return async dispatch => {
		dispatch(selectRestaurantAction(data));
	}
}

export function applyCouponCode(data) {
	return async dispatch => {
		dispatch(applyCouponCodeAction(data));
	}
}

export function postScanItemToAdd(data) {
	return async dispatch => {
		dispatch(postScanItemToAddAction(data));
	}
}