import * as ActionTypes from './types';
import apiHelper from "../lib/apiHelper"
import { defaultEditUserState } from "../reducers/editUserReducer";
import axios from 'react-native-axios';
import globals from "../lib/globals"
import { Alert } from 'react-native';

export const apiServiceActionLoading = () => ({
	type: ActionTypes.EDIT_USER_LOADING
});

export const apiServiceActionError = (error) => ({
	type: ActionTypes.EDIT_USER_ERROR,
	error: error
});

export function GetUserDetailsSuccess(responseData) {
	return {
		type: ActionTypes.EDIT_USER_SUCCESS,
		responseData: responseData
	};
}
export function UpdateUserDetailsSuccess(responseData) {
	return {
		type: ActionTypes.UPDATE_USER_SUCCESS,
		responseData: responseData
	};
}
export function LogoutSuccess(responseData) {
	return {
		type: ActionTypes.LOGOUT_SUCCESS,
		responseData: responseData
		
	};
}

export const resetLogoutData = () => ({
	type: ActionTypes.RESET_LOGOUT_DATA,
});
export const resetallData = () => ({
	type: ActionTypes.LOGOUT_SUCCESS,
});

export function doUserDetailsEdit(token) {
	return async dispatch => {
		dispatch(apiServiceActionLoading());
		await axios.get(apiHelper.getUserEditDetailsAPI(),apiHelper.getAPIHeader(token))
			.then(response => {
				dispatch(GetUserDetailsSuccess(response.data));
			}).catch(error => {
				dispatch(apiServiceActionError(error.response.data));
			});
	};
}

export function doLogout(token) {
	return async dispatch => {
		dispatch(apiServiceActionLoading());
		await axios.post(apiHelper.logoutApi(), {}, apiHelper.getAPIHeader(token))
		.then(response => {
			dispatch(LogoutSuccess(response.data));
		}).catch(error => {
			dispatch(apiServiceActionError(error.response.data));
		});
	};
}

export function LogoutClearData() {
	return async dispatch => {
		dispatch(LogoutSuccess({}))
	}
}


