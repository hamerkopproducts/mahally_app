import * as ActionTypes from './types';

export const localLocationSave = (location) => ({
    type: ActionTypes.GEO_LOCATION,
    data: location
});

export function setLocationLocally(location) {
    return async dispatch => {
	    dispatch(localLocationSave(location));
    }
}

export function getOtp(location) {
    return async dispatch => {
	    dispatch(apiServiceActionLoading(location));
    }
}