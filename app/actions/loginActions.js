import * as ActionTypes from './types';
import apiHelper from "../lib/apiHelper"
import { defaultLoginState } from "../reducers/loginReducer";
import axios from 'react-native-axios';
import globals from "../lib/globals"


export const guestLoginAction = (is_login) => ({
	type: ActionTypes.GUEST_LOGIN,
	data: is_login
});

// export function guestLoggedIn(is_login) {
// 	return async dispatch => {
// 		dispatch(guestLoginAction(is_login));
// 	}
// }

export const apiServiceActionLoading = () => ({
	type: ActionTypes.LOGIN_SERVICE_LOADING
});

export const apiServiceActionError = (error) => ({
	type: ActionTypes.LOGIN_SERVICE_ERROR,
	error: error
});

export const otpServiceError = (error) => ({
	type: ActionTypes.GET_OTP_SERVICE_ERROR,
	error: error
});
export const guestLoginn = (flag) => ({
	type: ActionTypes.GUEST_LOGIN,
	responseData: flag
});

export function loginServiceActionSuccess(responseData) {
	return {
		type: ActionTypes.LOGIN_SUCCESS,
		responseData: responseData
	};
}

export function resetSuccessmessageAction(data) {
	return {
		type: ActionTypes.LOGIN_SUCCESS,
		responseData: data
	};
}

export function resetSuccessmessage(data) {
	let _data = data;
	_data.success = null;
	return async dispatch => {
		dispatch(resetSuccessmessageAction(_data));
	}
}

export function resendOtpServiceActionSuccess(responseData) {
	return {
		type: ActionTypes.RESEND_OTP_SUCCESS,
		responseData: responseData
	};
}
export function loginServiceActionError(responseData) {
	return {
		type: ActionTypes.LOGIN_ERROR,
		responseData: responseData
	};
}
export function getOtpSuccess(responseData) {
	return {
		type: ActionTypes.GET_OTP_SUCCESS,
		responseData: responseData
	};
}
export function loginServiceOff() {
	return {
		type: ActionTypes.LOGIN
	};
}

export function languageSelectionSuccess(responseData) {
	return {
		type: ActionTypes.LANGUAGE_SELECTION_SUCCESS,
		responseData: responseData
	};
}

export function getOtp(params) {
	return async dispatch => {
		dispatch(apiServiceActionLoading());
		await axios.post(apiHelper.getOtpAPI(), params)
			.then(response => {
				dispatch(getOtpSuccess(response.data));
			}).catch(error => {
				dispatch(otpServiceError(error.response.data));
			});
	}
}

export function doLogin(params) {
	return async dispatch => {
		dispatch(apiServiceActionLoading());
		await axios.post(apiHelper.loginAPI(), params, apiHelper.getLoginAPIHeader())
			.then(response => {
				dispatch(loginServiceActionSuccess(response.data));
				dispatch(loginServiceOff());
			}).catch(error => {
				dispatch(loginServiceActionError(error.response.data));
			});
	}
}
export function resendOtp(params) {
	return async dispatch => {
		dispatch(apiServiceActionLoading());
		await axios.post(apiHelper.resendOtpAPI(), params)
			.then(response => {
				dispatch(resendOtpServiceActionSuccess(response.data));
			}).catch(error => {
				alert('error')
				//dispatch(apiServiceActionError(error.response.data.message));
			});
	}
}



export function saveSelectedLanguage(language) {
	return async dispatch => {
		//dispatch(apiServiceActionLoading());
		dispatch(languageSelectionSuccess(language));
	}
}

export function resetLoginErrorMeesage() {
	return dispatch => {
		return dispatch({
			type: ActionTypes.RESET_LOGIN_ERROR_MESSAGE
		});
	};
}

export function doLogout(resetNavigation: Function) {
	return dispatch => {
		dispatch({
			type: ActionTypes.LOGOUT_SUCCESS,
			...defaultLoginState,
			resetNavigation
		});
	};
}

export function enableLogin() {
	return dispatch => {
		return dispatch({
			type: ActionTypes.ENABLE_LOGIN_SUCCESS
		});
	};
}

