import * as ActionTypes from './types';
import apiHelper from "../lib/apiHelper"
import axios from 'react-native-axios';
import globals from "../lib/globals"

export const loyaltyApiServiceActionLoading = () => ({
	type: ActionTypes.LOYALTY_SERVICE_LOADING
});

export function loyaltyServiceActionSuccess(responseData) {
	return {
		type: ActionTypes.LOYALTY_SERVICE_SUCCESS,
		responseData: responseData
	};
}
export const loyaltyServiceActionError = (error) => ({
	type: ActionTypes.LOYALTY_SERVICE_ERROR,
	error: error
});

export function clearLoyaltyDetails() {
	return {
		type: ActionTypes.CLEAR_LOYALTY_DETAILS,
	};
}

export const menuDetailsServiceActionError = (error) => ({
	type: ActionTypes.MENU_DETAILS_ERROR,
	error: error
});

export function getLoyaltyData(params) {
	return async dispatch => {
		dispatch(loyaltyApiServiceActionLoading());
        await axios.post(apiHelper.getLoyaltyAPI(), params)
			.then(response => {
				dispatch(loyaltyServiceActionSuccess(response.data));
			}).catch(error => {
				dispatch(loyaltyServiceActionError(error.response.data));
			});
	}
}


