import * as ActionTypes from './types';
import apiHelper from "../lib/apiHelper"
import axios from 'react-native-axios';
import globals from "../lib/globals"

export const offersApiServiceActionLoading = () => ({
	type: ActionTypes.OFFERS_SERVICE_LOADING
});

export function offersServiceActionSuccess(responseData) {
	return {
		type: ActionTypes.OFFERS_SERVICE_SUCCESS,
		responseData: responseData
	};
}
export const offersServiceActionError = (error) => ({
	type: ActionTypes.OFFERS_SERVICE_ERROR,
	error: error
});

export const menuDetailsApiServiceActionLoading = () => ({
	type: ActionTypes.MENU_DETAILS_SERVICE_LOADING
});

export function menuDetailsServiceActionSuccess(responseData) {
	return {
		type: ActionTypes.MENU_DETAILS_SUCCESS,
		responseData: responseData
	};
}
export function clearMenuDetails() {
	return {
		type: ActionTypes.CLEAR_MENU_DETAILS,
	};
}

export const menuDetailsServiceActionError = (error) => ({
	type: ActionTypes.MENU_DETAILS_ERROR,
	error: error
});

export function getOffersData(params) {
	return async dispatch => {
		dispatch(offersApiServiceActionLoading());
        await axios.post(apiHelper.getOffersAPI(), params)
			.then(response => {
				dispatch(offersServiceActionSuccess(response.data));
			}).catch(error => {
				dispatch(offersServiceActionError(error.response.data));
			});
	}
}

export function getMenuDetailsData(id) {
	return async dispatch => {
		dispatch(menuDetailsApiServiceActionLoading());
        await axios.post(apiHelper.getMenuDetailsAPI(id))
			.then(response => {
				dispatch(menuDetailsServiceActionSuccess(response.data));
			}).catch(error => {
				dispatch(menuDetailsServiceActionError(error.response.data));
			});
	}
}


