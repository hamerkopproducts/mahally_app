import * as ActionTypes from './types';
import apiHelper from "../lib/apiHelper"
//import axios from 'react-native-axios';
import globals from "../lib/globals"

export const apiServiceActionLoading = () => ({
  type: ActionTypes.GET_COMPLETED_ORDER_LOADING
});

export const apiServiceActionError = (error) => ({
  type: ActionTypes.GET_COMPLETED_ORDER_ERROR,
  error: error
});

export function updateDeliveredOder(responseData) {
  return {
    type: ActionTypes.GET_DELIVERED_ORDER_SUCCESS,
    responseData: responseData
  };
}

export function updateReturnedOder(responseData) {
  return {
    type: ActionTypes.GET_RETURNED_ORDER_SUCCESS,
    responseData: responseData
  };
}

export function getDeliveredOrders(params) {
  let orderArray = [
    {
      "orderId": "ORD123457787",
      "scheduledOn": "12 July 2020, 2pm-5pm",
      "location": "Jedda, AlNameem",
      "status": 0,
      "customerName": "John Doe",
      "deliveryAdress": {
        "address": "Al Bassam",
        "place": "Al Naemia Street",
        "nearBy": "Near Al Naemia Juma Masjid",
        "po": "PO Box: 2211",
        "city": "Al Namem",
        "state": "Jeddah",
        "number": "+966 7489328930"
      },
    
      "orderDetails": [{
        "itemId": "1",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"Mark as Collected",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')"
      }, {
        "itemId": "2",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "staus":"Mark as Collected",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')"
      }, {
        "itemId": "3",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"Pending",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')"
      }, {
        "itemId": "4",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"In Warehouse",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')"
      }],
      "orderSummary": "SAR 2720.00",
      "subTotal": "SAR 160.00",
      "discount": "SAR 20.00",
      "deliveryCharge": "SAR 10.00",
      "totalWithTax": "SAR 172.00",
      "paymentMethod": "Credit Card",
      "note": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    }, {
      "orderId": "ORD123457788",
      "scheduledOn": "12 July 2020, 2pm-5pm",
      "location": "Jedda, AlNameem",
      "status": 0,
      "customerName": "John Doe",
      "deliveryAdress": {
        "address": "Al Bassam",
        "place": "Al Naemia Street",
        "nearBy": "Near Al Naemia Juma Masjid",
        "po": "PO Box: 2211",
        "city": "Al Namem",
        "state": "Jeddah",
        "number": "+966 7489328930"
      },
      "billingAdress": {
        "address": "Al Bassam",
        "place": "Al Naemia Street",
        "nearBy": "Near Al Naemia Juma Masjid",
        "po": "PO Box: 2211",
        "city": "Al Namem",
        "state": "Jeddah",
        "number": "+966 7489328930"
      },
      "orderDetails": [{
        "itemId": "1",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"Mark as Collected",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')"
      }, {
        "itemId": "2",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"Pending",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')"
      }, {
        "itemId": "3",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"Mark as Collected",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')"
      }, {
        "itemId": "4",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"In Warehouse",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')"
      }],
      "orderSummary": "SAR 2720.00",
      "subTotal": "SAR 160.00",
      "discount": "SAR 20.00",
      "deliveryCharge": "SAR 10.00",
      "totalWithTax": "SAR 172.00",
      "paymentMethod": "Credit Card",
      "note": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    },{
      "orderId": "ORD123457789",
      "scheduledOn": "12 July 2020, 2pm-5pm",
      "location": "Jedda, AlNameem",
      "status": 0,
      "customerName": "John Doe",
      "deliveryAdress": {
        "address": "Al Bassam",
        "place": "Al Naemia Street",
        "nearBy": "Near Al Naemia Juma Masjid",
        "po": "PO Box: 2211",
        "city": "Al Namem",
        "state": "Jeddah",
        "number": "+966 7489328930"
      },
      "billingAdress": {
        "address": "Al Bassam",
        "place": "Al Naemia Street",
        "nearBy": "Near Al Naemia Juma Masjid",
        "po": "PO Box: 2211",
        "city": "Al Namem",
        "state": "Jeddah",
        "number": "+966 7489328930"
      },
      "orderDetails": [{
        "itemId": "1",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status": "In Warehouse",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')"
      }, {
        "itemId": "2",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status": "Mark as Collected",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')"
      }, {
        "itemId": "3",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"Pending",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')"
      }, {
        "itemId": "4",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"In Warehouse",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')"
      }],
      "orderSummary": "SAR 2720.00",
      "subTotal": "SAR 160.00",
      "discount": "SAR 20.00",
      "deliveryCharge": "SAR 10.00",
      "totalWithTax": "SAR 172.00",
      "paymentMethod": "Credit Card",
      "note": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    }, {
      "orderId": "ORD123457790",
      "scheduledOn": "12 July 2020, 2pm-5pm",
      "location": "Jedda, AlNameem",
      "status": 0,
      "customerName": "John Doe",
      "deliveryAdress": {
        "address": "Al Bassam",
        "place": "Al Naemia Street",
        "nearBy": "Near Al Naemia Juma Masjid",
        "po": "PO Box: 2211",
        "city": "Al Namem",
        "state": "Jeddah",
        "number": "+966 7489328930"
      },
      "billingAdress": {
        "address": "Al Bassam",
        "place": "Al Naemia Street",
        "nearBy": "Near Al Naemia Juma Masjid",
        "po": "PO Box: 2211",
        "city": "Al Namem",
        "state": "Jeddah",
        "number": "+966 7489328930"
      },
      "orderDetails": [{
        "itemId": "1",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"Pending",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')"
      }, {
        "itemId": "2",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"In Warehouse",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')"
      }, {
        "itemId": "3",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "status":"Mark as Collected",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')"
      }, {
        "itemId": "4",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"In Warehouse",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')"
      }],
      "orderSummary": "SAR 2720.00",
      "subTotal": "SAR 160.00",
      "discount": "SAR 20.00",
      "deliveryCharge": "SAR 10.00",
      "totalWithTax": "SAR 172.00",
      "paymentMethod": "Credit Card",
      "note": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    }];
  return async dispatch => {
    //dispatch(apiServiceActionLoading());
    dispatch(updateDeliveredOder(orderArray));
  }
}

export function getReturnedOrders(params) {
  let orderArray = [
    {
    "orderId": "ORD123457786",
    "scheduledOn": "12 July 2020, 2pm-5pm",
    "location": "Jedda, AlNameem",
    "status": 0,
    "customerName": "John Doe",
    "deliveryAdress": {
      "address": "Al Bassam",
      "place": "Al Naemia Street",
      "nearBy": "Near Al Naemia Juma Masjid",
      "po": "PO Box: 2211",
      "city": "Al Namem",
      "state": "Jeddah",
      "number": "+966 7489328930"
    },
    "billingAdress": {
      "address": "Al Bassam",
      "place": "Al Naemia Street",
      "nearBy": "Near Al Naemia Juma Masjid",
      "po": "PO Box: 2211",
      "city": "Al Namem",
      "state": "Jeddah",
      "number": "+966 7489328930"
    },
    "orderDetails": [{
      "itemId": "1",
      "itemCategory": "Tasty- Pro Fitness Conscious",
      "itemName": "Edible Oil",
      "weight": "2",
      "unit": "Kg",
      "status": "Pending",
      "perItemPrice": "SAR 680.00",
      "quantity": "2",
      "totalPrice": "SAR 1360.00",
      "itemImage": "{require('../../assets/images/profileicon/faqIcon')"
    }, {
      "itemId": "2",
      "itemCategory": "Tasty- Pro Fitness Conscious",
      "itemName": "Edible Oil",
      "weight": "2",
      "unit": "Kg",
      "perItemPrice": "SAR 680.00",
      "quantity": "2",
      "status":"Mark as Collected",
      "totalPrice": "SAR 1360.00",
      "itemImage": "{require('../../assets/images/profileicon/faqIcon')"
    }, {
      "itemId": "3",
      "itemCategory": "Tasty- Pro Fitness Conscious",
      "itemName": "Edible Oil",
      "weight": "2",
      "unit": "Kg",
      "status":"Pending",
      "perItemPrice": "SAR 680.00",
      "quantity": "2",
      "totalPrice": "SAR 1360.00",
      "itemImage": "{require('../../assets/images/profileicon/faqIcon')"
    }, {
      "itemId": "4",
      "itemCategory": "Tasty- Pro Fitness Conscious",
      "itemName": "Edible Oil",
      "weight": "2",
      "unit": "Kg",
      "status":"Pending",
      "perItemPrice": "SAR 680.00",
      "quantity": "2",
      "totalPrice": "SAR 1360.00",
      "itemImage": "{require('../../assets/images/profileicon/faqIcon')"
    }],
    "orderSummary": "SAR 2720.00",
    "subTotal": "SAR 160.00",
    "discount": "SAR 20.00",
    "deliveryCharge": "SAR 10.00",
    "totalWithTax": "SAR 172.00",
    "paymentMethod": "Credit Card",
    "note": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
  },{
      "orderId": "ORD123457787",
      "scheduledOn": "12 July 2020, 2pm-5pm",
      "location": "Jedda, AlNameem",
      "status": 0,
      "customerName": "John Doe",
      "deliveryAdress": {
        "address": "Al Bassam",
        "place": "Al Naemia Street",
        "nearBy": "Near Al Naemia Juma Masjid",
        "po": "PO Box: 2211",
        "city": "Al Namem",
        "state": "Jeddah",
        "number": "+966 7489328930"
      },
      "billingAdress": {
        "address": "Al Bassam",
        "place": "Al Naemia Street",
        "nearBy": "Near Al Naemia Juma Masjid",
        "po": "PO Box: 2211",
        "city": "Al Namem",
        "state": "Jeddah",
        "number": "+966 7489328930"
      },
      "orderDetails": [{
        "itemId": "1",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"Pending",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')"
      }, {
        "itemId": "2",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"Mark as Collected",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')"
      }, {
        "itemId": "3",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"Pending",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')"
      }, {
        "itemId": "4",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"Mark as Collected",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')"
      }],
      "orderSummary": "SAR 2720.00",
      "subTotal": "SAR 160.00",
      "discount": "SAR 20.00",
      "deliveryCharge": "SAR 10.00",
      "totalWithTax": "SAR 172.00",
      "paymentMethod": "Credit Card",
      "note": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    }, {
      "orderId": "ORD123457788",
      "scheduledOn": "12 July 2020, 2pm-5pm",
      "location": "Jedda, AlNameem",
      "status": 0,
      "customerName": "John Doe",
      "deliveryAdress": {
        "address": "Al Bassam",
        "place": "Al Naemia Street",
        "nearBy": "Near Al Naemia Juma Masjid",
        "po": "PO Box: 2211",
        "city": "Al Namem",
        "state": "Jeddah",
        "number": "+966 7489328930"
      },
      "billingAdress": {
        "address": "Al Bassam",
        "place": "Al Naemia Street",
        "nearBy": "Near Al Naemia Juma Masjid",
        "po": "PO Box: 2211",
        "city": "Al Namem",
        "state": "Jeddah",
        "number": "+966 7489328930"
      },
      "orderDetails": [{
        "itemId": "1",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"Pending",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')}"
      }, {
        "itemId": "2",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"Mark as Collected",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')}"
      }, {
        "itemId": "3",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"Pending",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')}"
      }, {
        "itemId": "4",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"Mark as Collected",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')}"
      }],
      "orderSummary": "SAR 2720.00",
      "subTotal": "SAR 160.00",
      "discount": "SAR 20.00",
      "deliveryCharge": "SAR 10.00",
      "totalWithTax": "SAR 172.00",
      "paymentMethod": "Credit Card",
      "note": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    }, {
      "orderId": "ORD123457789",
      "scheduledOn": "12 July 2020, 2pm-5pm",
      "location": "Jedda, AlNameem",
      "status": 0,
      "customerName": "John Doe",
      "deliveryAdress": {
        "address": "Al Bassam",
        "place": "Al Naemia Street",
        "nearBy": "Near Al Naemia Juma Masjid",
        "po": "PO Box: 2211",
        "city": "Al Namem",
        "state": "Jeddah",
        "number": "+966 7489328930"
      },
      "billingAdress": {
        "address": "Al Bassam",
        "place": "Al Naemia Street",
        "nearBy": "Near Al Naemia Juma Masjid",
        "po": "PO Box: 2211",
        "city": "Al Namem",
        "state": "Jeddah",
        "number": "+966 7489328930"
      },
      "orderDetails": [{
        "itemId": "1",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"Pending",//edited
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')}"
      }, {
        "itemId": "2",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status": "Mark as Collected",//edited
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')}"
      }, {
        "itemId": "3",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"Pending",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": ""
      }, {
        "itemId": "4",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"In Warehouse",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')}"
      }],
      "orderSummary": "SAR 2720.00",
      "subTotal": "SAR 160.00",
      "discount": "SAR 20.00",
      "deliveryCharge": "SAR 10.00",
      "totalWithTax": "SAR 172.00",
      "paymentMethod": "Credit Card",
      "note": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    }, {
      "orderId": "ORD123457790",
      "scheduledOn": "12 July 2020, 2pm-5pm",
      "location": "Jedda, AlNameem",
      "status": 0,
      "customerName": "John Doe",
      "deliveryAdress": {
        "address": "Al Bassam",
        "place": "Al Naemia Street",
        "nearBy": "Near Al Naemia Juma Masjid",
        "po": "PO Box: 2211",
        "city": "Al Namem",
        "state": "Jeddah",
        "number": "+966 7489328930"
      },
      "billingAdress": {
        "address": "Al Bassam",
        "place": "Al Naemia Street",
        "nearBy": "Near Al Naemia Juma Masjid",
        "po": "PO Box: 2211",
        "city": "Al Namem",
        "state": "Jeddah",
        "number": "+966 7489328930"
      },
      "orderDetails": [{
        "itemId": "1",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"Pending",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')}"
      }, {
        "itemId": "2",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"Mark as Collected",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')}"
      }, {
        "itemId": "3",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"In Warehouse",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')}"
      }, {
        "itemId": "4",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"Mark as Collected",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')}"
      }],
      "orderSummary": "SAR 2720.00",
      "subTotal": "SAR 160.00",
      "discount": "SAR 20.00",
      "deliveryCharge": "SAR 10.00",
      "totalWithTax": "SAR 172.00",
      "paymentMethod": "Credit Card",
      "note": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    }, {
      "orderId": "ORD123457791",
      "scheduledOn": "12 July 2020, 2pm-5pm",
      "location": "Jedda, AlNameem",
      "status": 0,
      "customerName": "John Doe",
      "deliveryAdress": {
        "address": "Al Bassam",
        "place": "Al Naemia Street",
        "nearBy": "Near Al Naemia Juma Masjid",
        "po": "PO Box: 2211",
        "city": "Al Namem",
        "state": "Jeddah",
        "number": "+966 7489328930"
      },
      "billingAdress": {
        "address": "Al Bassam",
        "place": "Al Naemia Street",
        "nearBy": "Near Al Naemia Juma Masjid",
        "po": "PO Box: 2211",
        "city": "Al Namem",
        "state": "Jeddah",
        "number": "+966 7489328930"
      },
      "orderDetails": [{
        "itemId": "1",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"Pending",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')}"
      }, {
        "itemId": "2",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"Mark as Collected",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')}"
      }, {
        "itemId": "3",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"In Warehouse",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')}"
      }, {
        "itemId": "4",
        "itemCategory": "Tasty- Pro Fitness Conscious",
        "itemName": "Edible Oil",
        "weight": "2",
        "unit": "Kg",
        "status":"Mark as Collected",
        "perItemPrice": "SAR 680.00",
        "quantity": "2",
        "totalPrice": "SAR 1360.00",
        "itemImage": "{require('../../assets/images/profileicon/faqIcon')}"
      }],
      "orderSummary": "SAR 2720.00",
      "subTotal": "SAR 160.00",
      "discount": "SAR 20.00",
      "deliveryCharge": "SAR 10.00",
      "totalWithTax": "SAR 172.00",
      "paymentMethod": "Credit Card",
      "note": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
    }];
  return async dispatch => {
    //dispatch(apiServiceActionLoading());
    dispatch(updateReturnedOder(orderArray));
  }
}
