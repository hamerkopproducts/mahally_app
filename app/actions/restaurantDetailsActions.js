import * as ActionTypes from './types';
import apiHelper from "../lib/apiHelper"
import axios from 'react-native-axios';

export const apiServiceActionLoading = () => ({
    type: ActionTypes.REST_DETAILS_LOADING
});

export function GetrestDetailsSuccess(responseData) {
    return {
        type: ActionTypes.REST_DETAILS_SUCCESS,
        responseData: responseData
    };
}
export function GetrestDetailsError(responseData) {
    return {
        type: ActionTypes.REST_DETAILS_ERROR,
        responseData: responseData

    };
}
export function clearRestDetails() {
	return {
		type: ActionTypes.CLEAR_REST_DETAILS,
	};
}
export function restaurantDetails(params, id) {
    return async dispatch => {
        dispatch(apiServiceActionLoading());
        await axios.post(apiHelper.getRestDetailsAPI(id), params)
            .then(response => {
                dispatch(GetrestDetailsSuccess(response.data));
            }).catch(error => {
                dispatch(GetrestDetailsError(error.response.data.error));
              
            });
    };
}


