

export const SUCCESS_SUFFIX = '_SUCCESS';
export const ERROR_SUFFIX = '_ERROR';

export const LOGIN_SERVICE_LOADING = 'login_service_loading'
export const LOGIN_SERVICE_ERROR = 'login_service_error'

export const LOGOUT_SUCCESS = 'logout_success'
export const LANGUAGE_SELECTION_SUCCESS = 'language_selection_success'
export const OTP_SUCCESS = 'otp_success'



//get otp
export const GET_OTP_SUCCESS = 'get_otp_success'
export const GET_OTP_SERVICE_ERROR = 'get_otp_service_error'

//notifications
export const NOTIFICATION_ACTIVE = 'notification_active'
export const NOTIFICATION_COUNT = 'notification_count'

//login
export const LOGIN_SUCCESS = 'login_success'
export const LOGIN_ERROR = 'login_error'
export const LOGIN = 'login'
export const GUEST_LOGIN = 'guest_login'
export const RESET_LOGOUT_DATA = 'reset_logout_data'
//howtouse
export const HOWTOUSE_SUCCESS = 'howtouse_success'
export const HOWTOUSE_ERROR = 'howtouse_error'


//resend otp
export const RESEND_OTP_SUCCESS = 'resend_otp_success'
export const RESEND_OTP_ERROR = 'resend_otp_error'

//offers
export const OFFERS_SERVICE_LOADING = 'offers_service_loading'
export const OFFERS_SERVICE_SUCCESS = 'offers_service_success'
export const OFFERS_SERVICE_ERROR = 'offers_service_error'

//menu details
export const MENU_DETAILS_SERVICE_LOADING = 'menu_details_service_loading'
export const MENU_DETAILS_SUCCESS = 'menu_details_success'
export const MENU_DETAILS_ERROR = 'menu_details_error'
export const CLEAR_MENU_DETAILS = 'clear_menu_details'

//home
export const GEO_LOCATION = 'geo_location';
export const REST_TYPE_LOADING = 'rest_type_loading';
export const REST_TYPE_SUCCESS = 'rest_type_success';
export const REST_TYPE_ERROR = 'rest_type_error';
export const LANGUAGE_SWITCH_SCREEN = 'language_switch_screen';

export const GET_ORDER_LOADING = 'get_order_loading'
export const GET_ORDER_ERROR = 'get_order_error'
export const GET_ORDER_TO_DELIVER_SUCCESS = 'get_order_to_deliver_success'
export const GET_ORDER_TO_COLLECT_SUCCESS = 'get_order_to_collect_success'
export const GET_ORDER_TO_RETURN_SUCCESS = 'get_order_to_return_success'

//order
export const GET_COMPLETED_ORDER_LOADING = 'get_completed_order_loading'
export const GET_COMPLETED_ORDER_ERROR = 'get_completed_order_error'
export const GET_DELIVERED_ORDER_SUCCESS = 'get_delivered_order_success'
export const GET_RETURNED_ORDER_SUCCESS = 'get_returned_order_success'

//restaurant details
export const REST_DETAILS_SUCCESS= 'rest_details_success'
export const REST_DETAILS_ERROR= 'rest_details_error'
export const REST_DETAILS_LOADING= 'rest_details_loading'
export const CLEAR_REST_DETAILS= 'clear_rest_details'

//Loyalty Meals
export const LOYALTY_SERVICE_LOADING= 'loyalty_service_loading'
export const LOYALTY_SERVICE_SUCCESS= 'loyalty_service_success'
export const LOYALTY_SERVICE_ERROR= 'loyalty_service_error'
export const CLEAR_LOYALTY_DETAILS= 'clear_loyalty_details'

//Suuport Chat
export const SUPPORT_CHAT_SERVICE_LOADING = 'support_chat_service_loading'
export const SUPPORT_CHAT_SERVICE_SUCCESS = 'support_chat_service_success'
export const SUPPORT_CHAT_SERVICE_ERROR = 'support_chat_service_error'
export const CLEAR_SUPPORT_CHAT_MSG = 'clear_support_chat_msg'


//Terms

export const TERMS_SUCCESS = 'terms_success'
export const TERMS_ERROR = 'terms_error'
export const TERMS_LOADING = 'terms_loading'

//privacy

export const PRIVACY_SUCCESS = 'privacy_success'
export const PRIVACY_ERROR = 'privacy_error'
export const PRIVACY_LOADING = 'privacy_loading'

//about

export const ABOUT_SUCCESS = 'about_success'
export const ABOUT_ERROR = 'about_error'

//faq

export const FAQS_SUCCESS = 'faqs_success'
export const FAQS_ERROR = 'faqs_error'
export const FAQS_LOADING = 'faqs_loading'
//EditUser
export const EDIT_USER_SUCCESS = 'edit_user_success'
export const EDIT_USER_ERROR = 'edit_user_error'
export const EDIT_USER_LOADING = 'edit_user_loading'

//UpdateUser
export const UPDATE_USER_SUCCESS = 'update_user_success'
export const UPDATE_USER_ERROR = 'update_user_error'
export const UPDATE_USER_LOADING = 'update_user_loading'

// Settings
export const GET_SETTINGS_CONTENT = 'get_settings_content';
export const GET_SETTINGS_CONTENT_SUCCESS =
  GET_SETTINGS_CONTENT + SUCCESS_SUFFIX;
export const GET_SETTINGS_CONTENT_ERROR = GET_SETTINGS_CONTENT + ERROR_SUFFIX;
export const RESET_GET_SETTINGS_CONTENT_ERROR = 'reset_settings_content';

//cart
export const MODIFY_CART_DATA = 'cart_data';
export const SELECT_RESTAURANT = 'select_restaurant';
export const COUPON_CODE = 'coupon_code';

export const POST_SCAN_ADD_ITEM_DATA = 'post_scan_item_data';