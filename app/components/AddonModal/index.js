import React, { useEffect, useState, useRef } from "react";
import { styles } from "./styles";
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  ScrollView,
} from "react-native";

import appTexts from "../../lib/appTexts";
import PropTypes from "prop-types";
import Modal from "react-native-modal";
import CartCard from "../CartCard";
import BannerButton from "../BannerButton";
import _ from "lodash";

const AddonModal = (props) => {
  const {
    isAddonModalVisible,
    openAddonModal,
    addonsDetails,
    addAddon,
    addonsSelected,
    onCheckoutClick,
    addedCartItems,
    onViewClick,
    isCart,
    isProductDetail,
  } = props;

  const [backDropOpacity, setBackDropOpacity] = useState(0.7);
  const [updateDropOpacity, setUpdateDropOpacity] = useState(true);
  const isMounted = useRef(false);

  useEffect(() => {
    isMounted.current = true;
    if (isCart) {
      setTimeout(() => {
        if (isMounted.current) {
          setBackDropOpacity(0.1);
        }
      });
      setTimeout(() => {
        if (isMounted.current) {
          setBackDropOpacity(0.7);
        }
      }, 3000);
    }
    return () => (isMounted.current = false);
  }, [isAddonModalVisible, updateDropOpacity]);

  const changeBackDropOpacity = () => {
    setUpdateDropOpacity(!updateDropOpacity);
  };

  const renderAddonCard = ({ item }) => (
    <CartCard
      item={item}
      addAddon={(count, item) => {
        addAddon(count, item);
        changeBackDropOpacity();
      }}
      addonsSelected={addonsSelected}
    />
  );

  return (
    <Modal
      transparent={true}
      animationIn="slideInUp"
      animationOut="slideOutRight"
      onSwipeComplete={openAddonModal}
      isVisible={isAddonModalVisible}
      style={styles.modalMaincontentLogout}
      backdropOpacity={backDropOpacity}
    >
      <View style={{ width: "100%", maxHeight: 600 }}>
        <View style={styles.screenContainerScrollView}>
          <View style={styles.modalmainviewLogout}>
            <View style={styles.addonList}>
              
              <View style={{ flexDirection: "row", marginTop: 50, marginBottom:20 }}>
                <View style={[styles.headingLine, { width: "80%" }]}>
                  <Text style={styles.head}>{appTexts.ADDON.head}</Text>
                  <Text style={styles.sub}>
                    {
                      Object.keys(_.isEmpty(addonsDetails) ? {} : addonsDetails)
                        .length
                    }{" "}
                    {appTexts.ADDON.sub}
                  </Text>
                </View>

                <View style={{ width: "18%", alignSelf: "flex-end" }}>
                  <TouchableOpacity
                    style={styles.buttohelpnwrapper}
                    onPress={() => {
                      openAddonModal();
                    }}
                  >
                    <Text style={styles.xText}>X</Text>
                  </TouchableOpacity>
                </View>
              </View>

              <FlatList
                data={_.isEmpty(addonsDetails) ? {} : addonsDetails}
                extraData={_.isEmpty(addonsDetails) ? {} : addonsDetails}
                keyExtractor={(item, index) => index.toString()}
                renderItem={renderAddonCard}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{paddingBottom:180}}
              />
            </View>
          </View>
        </View>
      </View>
      {isProductDetail ? (
        <BannerButton
          isAdd={true}
          text1={appTexts.BANNER.count}
          text2={appTexts.BANNER.view}
          text3={appTexts.BANNER.check}
          text4={appTexts.BANNER.SAR}
          onCheckoutClick={onCheckoutClick}
          addedCartItems={addedCartItems}
          openAddonModal={openAddonModal}
          onViewClick={onViewClick}
          isCart={isCart}
        />
      ) : (
        <BannerButton
          text1={appTexts.BANNER.count}
          text2={appTexts.BANNER.view}
          text3={appTexts.BANNER.check}
          text4={appTexts.BANNER.SAR}
          onCheckoutClick={onCheckoutClick}
          addedCartItems={addedCartItems}
          openAddonModal={openAddonModal}
          onViewClick={onViewClick}
          isCart={isCart}
        />
      )}
    </Modal>
  );
};

AddonModal.propTypes = {
  isAddonModalVisible: PropTypes.bool,
  openAddonModal: PropTypes.func,
};

export default AddonModal;
