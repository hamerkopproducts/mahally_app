import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const images = {
  star: require("../../assets/images/modal/Star.png"),
  human:require("../../assets/images/ItemCard/img3.png")
};

const styles = StyleSheet.create({
  modalMaincontentLogout: {
    justifyContent:'flex-end',
    margin: 0,
    alignItems: 'center',

  },
  modalmainviewLogout: {
    backgroundColor: "white",
    paddingLeft: '4%',
    paddingRight: '2%',
    borderTopLeftRadius: 15,
    borderTopRightRadius:15,
    justifyContent: 'center',
    borderColor: "rgba(0, 0, 0, 0.1)",
    width:'100%',
   // maxHeight: '70%'
  },
  screenContainerScrollView:{
    width: globals.SCREEN_SIZE.width,
    // height: globals.SCREEN_SIZE.height,
    // flex:1,
    // marginTop:hp('30%'),
    //maxHeight: hp('70%'),
    borderTopLeftRadius: 15,
    borderTopRightRadius:15,
  },
  lineTop:{
      width:150,
      borderColor:globals.COLOR.greyBackground,
      borderWidth:2,
      borderRadius:5,
      marginLeft:'28%',
      
      marginTop:'3%'
  },
  xText: {
    fontSize: 20,
    color: globals.COLOR.greyText,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },

  
  head:{
    fontSize: 14,
    color: globals.COLOR.Text,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helveticaBold, 
    textAlign:'left',
  },
  sub:{
    fontSize: 12,
    color: globals.COLOR.greyText,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica, 
    textAlign:'left',   
  },
  
  buttohelpnwrapper: {
    flexDirection: "row",
    justifyContent: 'flex-end',
    paddingTop: '5%',
    paddingRight: '5%'
  },
  addonList:{
      marginTop:20,
      marginBottom:10
  },
 



});

export { images, styles };