import PropTypes from "prop-types";
import React, { useState } from "react";
import {
  View,
  Text,
  TouchableOpacity
} from "react-native";
import { styles } from "./styles";
import appTexts from "../../lib/appTexts";

const BannerButton = (props) => {
  const {
    onCheckoutClick,
    isCart,
    isDetail,
    text2,
    text3,
    isHome,
    addedCartItems,
    openAddonModal,
    onViewClick,
    isAdd
  } = props;

  let count_total = 0;
  let total_price = 0;
  try {
    count_total = Object.keys(addedCartItems).length + " " + (Object.keys(addedCartItems).length > 1 ? appTexts.BANNER.Item_s : appTexts.BANNER.Item);
    let addons_total = 0;
    for(let inc=0; inc<Object.keys(addedCartItems).length; inc++) {
        let items = addedCartItems[Object.keys(addedCartItems)[inc]];
        total_price += (typeof items.count == 'undefined' ? 0 : items.count ) * items.discount_price;
        addons_total += Object.keys(items.addonsSelected).length;
        for(let inc2=0; inc2<Object.keys(items.addonsSelected).length; inc2++) {
          const addon_item = items.addonsSelected[ Object.keys(items.addonsSelected)[inc2] ];
          total_price += (typeof addon_item.count == 'undefined' ? 0 : addon_item.count ) * addon_item.price;
        }
    }
    count_total += ` , ${addons_total}` + " " + (addons_total > 1 ? appTexts.BANNER.on_s : appTexts.BANNER.on);
  } catch(err) {
    count_total = 0;
  }

  return (
    <View style={isCart ? styles.bannerCart : styles.bannerStyle}>
      <View style={isHome ? styles.colOne : styles.colOne}>
        <View style={isHome ? styles.three : styles.three}>
          <View style={isHome ? styles.hm2 : styles.hm2}>
            <Text style={styles.count}>{ count_total }</Text>
          </View>
        </View>
        {isHome ? null : (
          <View style={isHome ? null : styles.SAR50}>
            <Text style={styles.buttonText}>{appTexts.STRING.sar} { total_price }</Text>
          </View>
        )}
      </View>
      
        <TouchableOpacity
          onPress={() => {
            onViewClick();
          }}
        >
          <View style={styles.colView}>
          {(typeof onViewClick != 'undefined' && onViewClick != null) &&
            <>
              {!isCart ? null : <Text style={styles.buttonText}>{text2}</Text>}
            </>
          }
          </View>
        </TouchableOpacity>
      
        {(isCart || isDetail || isHome) ? (
          <TouchableOpacity
          onPress={() => {
            onCheckoutClick();
          }}
        >
          <View style={isCart ?isAdd ? styles.newCol : styles.colTwo:styles.colCHECK}>
            <Text style={styles.buttonText}>{text3}</Text>
          </View>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
          onPress={() => {
            openAddonModal();
          }}
        >
          <View style={styles.colTwos}>
            <Text style={styles.buttonText}>{appTexts.BANNER.done}</Text>
          </View>
          </TouchableOpacity>
        )}
      
    </View>
  );
};

BannerButton.propTypes = {
  buttonText: PropTypes.string,
  onViewClick: PropTypes.func,
  onCheckoutClick: PropTypes.func,
};

export default BannerButton;
