import { StyleSheet, I18nManager } from "react-native";
import { widthPercentageToDP } from "react-native-responsive-screen";
import globals from "../../lib/globals"

const styles = StyleSheet.create({
    bannerStyle: {
        backgroundColor: globals.COLOR.purple,
        width: '100%',
        height: 65,
      //  justifyContent: 'center',
      paddingHorizontal:"5%",
        alignItems: 'center',
        flexDirection: 'row',


    },
    bannerCart: {
        backgroundColor: globals.COLOR.purple,
        width: '100%',
        height: 65,
        paddingHorizontal: '5%',
        // justifyContent:'center',
        alignItems: 'center',
        flexDirection: 'row',
    },


    buttonText: {
        color: 'white',
        fontFamily: I18nManager.isRTL ? globals.FONTS. notokufiarabicBold : globals.FONTS.helveticaBold,
        fontSize: 11,
        
    },
    colOne: {
        flexBasis: '37%',
        alignItems: 'flex-start',

    },
    count: {
        color: 'white',
        fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
        fontSize: 12,
},
    colmOne: {
        alignItems: 'center',
        // justifyContent:'space-between',
        flexDirection: 'row',

    },
    homThree: {
        alignItems: 'center',
        // justifyContent:'space-between',
        flexDirection: 'row',
        marginHorizontal: '20%',

    },
    hm: {
        //margin:'35%'

        marginHorizontal: I18nManager.isRTL ? '20%' : '30%',
    },
    hm1: {
        //margin:'35%'
        marginHorizontal: '10%',
    },
    hm2: {
        //margin:'35%'
      //  marginHorizontal: '2%',
    },
    SAR50: {
      //  marginHorizontal: '3%',
        alignItems:"center"
    },
    colTwo: {
      //  flexBasis: '10%',
        // alignItems: 'center',
        // justifyContent: 'center',
        marginLeft: widthPercentageToDP('16%'),


    },
    colView: {
        //  flexBasis: '10%',
          // alignItems: 'center',
          // justifyContent: 'center',
          marginLeft: widthPercentageToDP('6%'),
  
  
      },
    newCol:{
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: '34%',
    },
    colTwos: {
     // flexBasis: '30%',
    //    alignItems: 'flex-end',
    //     justifyContent: 'flex-end',
        marginLeft: widthPercentageToDP('28%'),

        //marginRight:'8%',
       //  marginHorizontal:'0%'

    },
    colCHECK: {
        // flexBasis: '30%',
        //   alignItems: 'flex-end',
        //    justifyContent: 'flex-end',
           marginLeft: I18nManager.isRTL ? widthPercentageToDP('38%'):widthPercentageToDP('30%'),
           //marginRight:'8%',
          //  marginHorizontal:'0%'
   
       },
    colTwosCart: {
        flexBasis: '33%',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        marginRight: '5%',
        marginLeft: '30%'

    },

});

export { styles };