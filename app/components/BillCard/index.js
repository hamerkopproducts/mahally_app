import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View,Text,Image } from 'react-native';
import { images, styles } from "./styles";
import appTexts from "../../lib/appTexts";
import globals from "../../lib/globals";

import DotedDivider from '../DotedDivider';

const BillCard = (props) => {
  const {
    subtotal,
    total_price,
    discount,
    is_amount_promo_type,
    promo_value,
    promo_code,
    checkout,
    vat_percentage
  } = props;

  let grant_total = parseFloat(total_price - discount).toFixed(2);
  if(grant_total < 0) {
    grant_total = 0;
  }

  let total_vat = 0;
  let total_include_vat = parseFloat(grant_total);
  if(typeof vat_percentage != 'undefined' && vat_percentage > 0) {
    total_vat = (total_include_vat/100) * vat_percentage;
    total_include_vat += parseFloat(total_vat);
    total_include_vat = total_include_vat.toFixed(2);
  }

  return (
        <View style={styles.itemCard}>
          <View style={styles.lineOne}>
              <Text style={styles.detailsText}>{appTexts.ORDER.details}</Text>
          </View>

          <View style={styles.lineTwo}>
              <Text style={styles.lineTwoText}>{appTexts.ORDER.subtotal}</Text>
              {checkout && 
                <Text style={styles.amt}>{appTexts.STRING.sar} { (parseFloat(total_price)).toFixed(2) }</Text>
              }
              { ! checkout && 
                <Text style={styles.amt}>{appTexts.STRING.sar} { (parseFloat(subtotal)).toFixed(2) }</Text>
              }
          </View>

          {(typeof promo_code == 'undefined' && discount > 0) &&
            <View style={styles.lineTwo}>
              <Text style={styles.lineTwoText}>{appTexts.ORDER.discount}</Text>
              <Text style={styles.amt}>{appTexts.STRING.sar} { discount.toFixed(2) }</Text>
            </View>
          }

          {(typeof promo_code != 'undefined' && promo_code != '') &&
            <View style={styles.lineTwo}>
              <Text style={styles.lineTwoText}>{appTexts.ORDER.discount}</Text>
              {is_amount_promo_type && <Text style={styles.amt}>{appTexts.STRING.sar} { promo_value.toFixed(2) }</Text> }
              { ! is_amount_promo_type && <Text style={styles.amt}>{ promo_value }%</Text> }
            </View>
          }

          {(typeof vat_percentage != 'undefined' && vat_percentage > 0) &&
            <View style={styles.lineTwo}>
              <Text style={styles.lineTwoText}>{appTexts.ORDER.vat}</Text>
              <Text style={styles.amt}>{ vat_percentage }%</Text>
            </View>
          }

          <View style={styles.line}>
              <DotedDivider/>
          </View>
          <View style={styles.lineTwo}>
              <Text style={styles.lineTwoText}>{appTexts.ORDER.total}</Text>
              {checkout && <Text style={styles.amt}>{appTexts.STRING.sar} { total_include_vat }</Text> }
              { ! checkout && <Text style={styles.gamt}>{appTexts.STRING.sar} { total_price.toFixed(2) }</Text> }
          </View>
     
        </View>
  );
};
BillCard.propTypes = {

};

export default BillCard;