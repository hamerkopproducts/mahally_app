import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals"

const images = {

};

const styles = StyleSheet.create({
  itemCard:{
    width:'92%',
    marginLeft:'4%',
    marginRight:'4%',
    borderWidth:0.5,
    borderRadius:20,
    borderColor:'white',
    backgroundColor:'white',
   
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
    //elevation: 2,
    marginTop:'4%',
    marginBottom:'5%'
  },
  lineOne:{
      marginTop:'5%',
      marginLeft:'5%',
      marginBottom:'2%'
  },
  lineTwo:{
      flexDirection:'row',
      justifyContent:'space-between',
      marginLeft:'5%',
      marginRight:'5%',
      marginVertical:'3%'
  },
  lineTwoText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.greyText,
    fontSize:14,   
  },
  amt:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.Text,
    fontSize:14, 
  },
  gamt:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.helveticaBold,
    color:globals.COLOR.Text,
    fontSize:14, 
  },
  line:{
      width:'90%',
      marginLeft:'5%',
      marginRight:'5%'
  },
  detailsText:{
    textAlign:'left',
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },

 
 
}
);

export { images, styles };
