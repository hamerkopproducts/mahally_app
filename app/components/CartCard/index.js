import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { View, Text, Image, I18nManager, TouchableOpacity } from 'react-native';
import { styles } from "./styles";
import appTexts from "../../lib/appTexts";
import _ from "lodash";

const CartCard = (props) => {
    const {
        item,
        addAddon,
        addonsSelected
    } = props;

    count = 0;
    try {
        count = addonsSelected[item.id.toString()].count;
    } catch(err) {
        count = 0;
    }

    const [isExpanded, setIsExpanded] = useState(count);

    const onOvalClick = () => {
        setIsExpanded(isExpanded + 1);
        addAddon(1, item);
    };
    const onDecrement = () => {
        setIsExpanded(isExpanded - 1);
        addAddon(-1, item);
    };
    const onIncrement = () => {
        setIsExpanded(isExpanded + 1);
        addAddon(1, item);
    };

    return (
        <View style={styles.itemCard}>

            <View style={styles.cardImage}>
                <Image source={{ uri: item.image_path }} style={styles.foodImage}></Image>
                <View style={styles.colText}>
                    <View style={{flex: 2}}>
                        <View style={styles.lebaneseView}>
                            <Text style={styles.lebaneseText}>{
                                I18nManager.isRTL ?
                                    _.get(item, 'lang[1].name', '') :
                                    _.get(item, 'lang[0].name', '')} </Text>
                        </View>
                        <View style={styles.countView}>
                            <Text style={styles.countTxt}>{appTexts.STRING.sar} {item.price}</Text>
                        </View>
                    </View>
                    <View style={{flex: 1, marginRight: 20}}>
                        {isExpanded > 0 ? (
                            <View style={styles.ovalView}>
                                <View style={styles.content}>

                                    <View style={styles.minusView}>
                                        <TouchableOpacity style={{padding: 8}} onPress={() => { onDecrement(); }}>
                                            <Text style={styles.minusText}>-</Text>
                                        </TouchableOpacity>
                                    </View>

                                    <View style={styles.line}></View>
                                    <View style={styles.minusView}>
                                        <Text style={styles.minusText}>{isExpanded}</Text>
                                    </View>
                                    <View style={styles.line}></View>

                                    <View style={styles.minusView}>
                                        <TouchableOpacity style={{padding: 8}} onPress={() => { onIncrement(); }}>
                                            <Text style={styles.minusText}>+</Text>
                                        </TouchableOpacity>
                                    </View>

                                </View>
                            </View>
                        ) :
                            <TouchableOpacity onPress={() => { onOvalClick(); }}>
                                <View style={styles.ovalView}>
                                    <View style={styles.plusView}>
                                        <Text style={styles.plusText}>+</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>}
                    </View>
                </View>
            </View>

        </View>

    );
};
CartCard.propTypes = {
    onOvalClick: PropTypes.func,

};

export default CartCard;