import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals"

const images = {
  food:require('../../assets/images/ItemCard/img.png'),
  addOn:require('../../assets/images/ItemCard/img3.png'),
  pen:require('../../assets/images/ItemCard/edit.png')
};

const styles = StyleSheet.create({
  itemCard:{
     
      width:'98%',
      borderRadius:10,
      borderWidth: 0.5,
      borderColor:globals.COLOR.greyText,
      paddingLeft:'2%',
      paddingRight:'2%',
      marginTop:'2%',
      marginBottom:'2%'
   
  },
  foodImage:{
      width:70,
      height:70,
      borderRadius: 35,
  },
  cardImage:{
      flexDirection:'row',
      alignItems:'center',
      marginTop:'2%',
      marginBottom:'2%',
      
      
  },
  colText:{
      marginLeft:'3%',
      flex: 1,
      flexDirection: 'row',
      alignItems:'center',
      justifyContent:'center'
  },
  lebaneseText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.Text,
    fontSize:I18nManager.isRTL ?13: 14, 
    textAlign:'left',
    lineHeight: Platform.OS === 'ios' ? 25 : null

  },
  countText:{
      fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helveticaBold,
      color:globals.COLOR.greyText,
      fontSize:10,
      textDecorationLine:'line-through',
      lineHeight: Platform.OS === 'ios' ? 25 : null
  },
  countTxt:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helveticaBold,
    color:globals.COLOR.Text,
    fontSize:10,
  },
  price:{
      marginLeft:'5%',
  },
  amount:{
      marginLeft:'18%'
  },
  amt:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.Text,
    fontSize:13,
  },
  addonView:{
    marginLeft:'15%',
    marginTop:'5%',
    
  },
  foodImages:{
    width:35,
    height:35,
    resizeMode:'contain'
},
addonHeading:{
  marginBottom:'2%'
},
addText:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  color:globals.COLOR.greyText,
  fontSize:13,

},
lebaneseView:{
  flexDirection:'row',
     alignItems:'center',
  width:'90%'
},
cardImageAdd:{
  flexDirection:'row',
  alignItems:'center',
  marginTop:'2%',
  marginBottom:'3%'
  
},
divide:{
  marginTop:'2%'
},
amt:{
  paddingTop:'5%',
},
amtText:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  color:globals.COLOR.greyText,
  fontSize:12,
},
oval:{
  width:40,
  height:25,
  backgroundColor:globals.COLOR.purple,
  borderRadius:5,
  alignItems:'center',
  justifyContent:'center'
},
ovalView:{
 //marginLeft:'40%',
  //alignItems:'center',
  marginHorizontal:'18%',
  borderWidth:0.5,
  borderColor:globals.COLOR.purple,
  borderRadius:5,
  width:85,
  height:35,
  alignItems:'center',
  justifyContent:'center'
},
plus:{
  marginBottom:'10%'
},
content:{
    flexDirection:'row',
alignItems:'center',
justifyContent:'center',
},
minusView:{
    flexBasis:'33%',
    alignItems:'center',

},
line: {
    flexDirection: 'column',
    borderLeftWidth: 0.5,
    height: 20,
    borderLeftColor: globals.COLOR.greyText,
  },
one:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  color:'white',
  fontSize:12,
},
plusText:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  color:globals.COLOR.purple,
  fontSize:16,
},
minus:{
  marginTop:'5%'
},
 checkAdd:{
   marginRight:'5%',
   marginTop:'3%'
 },
 addonViewCheckout:{
   backgroundColor:'white',
   width:'100%',
   //marginLeft:'15%'
 },
 rowOne:{
   flexDirection:'row',
   justifyContent:'space-between',
  // marginLeft:'15%',
   marginTop:'5%'

 },
 penView:{
   flexDirection:'row',
   alignItems:'center'
 },
 penImage:{
   height:18,
   width:18,
 },
 customText:{
   marginLeft:'8%'
 },
 countView:{
     flexDirection:'row',
     alignItems:'center',

 },
}
);

export { images, styles };
