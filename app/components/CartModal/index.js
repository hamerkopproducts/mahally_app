import React, { useState } from "react";
import { styles } from "./styles";
import { View, Text, TouchableOpacity, FlatList } from "react-native";
import appTexts from "../../lib/appTexts";
import PropTypes from "prop-types";

import Modal from "react-native-modal";
import ItemCard from "../ItemCard";
import BannerButton from "../BannerButton";
import { ScrollView } from "react-native-gesture-handler";
import functions from "../../lib/functions";

const CartModal = (props) => {
  const {
    isCartModalVisible,
    setIsCartAddon,
    openCartModal,
    onCheckoutClick,
    addedCartItems,
    addToCart,
    openAddonModalCustomize
  } = props;

  const [isFirstAddonVisible, setIsFirstAddonVisible] = useState(false);
  const [backDropOpacity, setBackDropOpacity] = useState(.7);

  const changeBackDropOpacity = () => {
    setBackDropOpacity(.1);
    setTimeout(() => {
      setBackDropOpacity(.7);
    }, 3000);
  }

  const firstItemPress = () => {
    setIsFirstAddonVisible(!isFirstAddonVisible);
  };

  const onCustomiseClick = (item) => {
    openAddonModalCustomize(item);
    openCartModal();
  };

  const all_addedCartItems = Object.assign({}, addedCartItems);
  let all_data = [];
  for(let inc=0; inc<Object.keys(all_addedCartItems).length; inc++) {
    let items = addedCartItems[Object.keys(addedCartItems)[inc]];
    all_data.push( items );
  }

  return (
    <Modal
      transparent={true}
      animationIn="slideInUp"
      animationOut="slideOutRight"
      isVisible={isCartModalVisible}
      style={styles.modalMaincontentLogout}
      backdropOpacity={backDropOpacity}
    >
      <View style={{width: '100%'}}>
        <ScrollView
          style={styles.screenContainerScrollView}
          showsVerticalScrollIndicator={true}
        >
          <View style={styles.modalmainviewLogout}>
            <View style={styles.lineTop}></View>
            <TouchableOpacity
              style={styles.buttohelpnwrapper}
              onPress={() => {
                openCartModal();
              }}
            >
              <Text style={styles.xText}>X</Text>
            </TouchableOpacity>

            <View style={styles.headingLine}>
              <Text style={styles.head}>{appTexts.CART.heading}</Text>
            </View>

            <View style={styles.cardSection}>
                <FlatList
                  data={all_data}
                  extraData={all_data}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={(item) => {
                    return <View>
                      <ItemCard
                        addons={item.item.addonsSelected}
                        item={item}
                        isCart={true}
                        isCheckout={true}
                        onItemCardPress={firstItemPress}
                        isCheckoutAddon={isFirstAddonVisible}
                        onCustomiseClick={() => onCustomiseClick(item.item)}
                        updateCart={(id, count) => {
                          let allItems = Object.assign({}, addedCartItems);
                          let old_count = (!isNaN(parseFloat(allItems[id].count)) && isFinite(allItems[id].count)) ? allItems[id].count : 0;
                          if(old_count == 1 && count == -1) {
                            delete allItems[id];
                            functions.displayToast('error', 'top', appTexts.EDITPROFILE.success, appTexts.ORDER.itemremoved);
                          } else {
                            allItems[id].count = old_count + count;
                            functions.displayToast('success', 'top', appTexts.EDITPROFILE.success, appTexts.ORDER.itemupdated);
                          }
                          changeBackDropOpacity();
                          addToCart(allItems);
                        }}
                      />
                    </View>
                  }}
                />
            </View>
          </View>
        </ScrollView>

        <BannerButton
        isAdd={true}
        isCart={true}
        text1={appTexts.BANNER.count}
        text3={appTexts.BANNER.check}
        text4={appTexts.BANNER.SAR}
        onCheckoutClick={onCheckoutClick}
        addedCartItems={addedCartItems}
        openAddonModal={openCartModal}
      />

      </View>

      
    </Modal>
  );
};

CartModal.propTypes = {
  isCartModalVisible: PropTypes.bool,
  openCartModal: PropTypes.func,
};

export default CartModal;
