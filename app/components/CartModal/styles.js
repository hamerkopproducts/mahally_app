import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const images = {
  star: require("../../assets/images/modal/Star.png"),
  human:require("../../assets/images/ItemCard/img3.png")
};

const styles = StyleSheet.create({
  modalMaincontentLogout: {
    justifyContent:'flex-end',
    margin: 0,
    alignItems: 'center',
   // minHeight:'70%'
      },
      // outerView:{
      //  // height: '40%',
      //   minHeight:'80%'
      // },
  modalmainviewLogout: {
    backgroundColor: "white",
    paddingLeft: '4%',
    paddingRight: '2%',
    justifyContent: 'center',
    width:'100%',
    borderColor: "rgba(0, 0, 0, 0.1)",
   // marginTop:'50%',
    paddingBottom:'5%',
    borderTopLeftRadius: 15,
    borderTopRightRadius:15,
  },
  lineTop:{
      width:150,
      borderColor:globals.COLOR.greyBackground,
      borderWidth:2,
      borderRadius:5,
      marginLeft:'28%',
      marginTop:'3%'
  },
  xText: {
    fontSize: 20,
    color: globals.COLOR.greyText,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },

  
  head:{
    fontSize: 14,
    color: globals.COLOR.Text,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.helveticaBold, 
    textAlign:'left',
  },
  screenContainerScrollView:{
    width: globals.SCREEN_SIZE.width,
    // height: globals.SCREEN_SIZE.height,
    // flex:1,
    // marginTop:hp('30%'),
    maxHeight: hp('70%'),
    borderTopLeftRadius: 15,
    borderTopRightRadius:15,
  },
  rate:{
    fontSize: 12,
    color: globals.COLOR.greyText,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  comment:{
      marginTop:'4%',
  },
  commentStyle:{
    fontSize: 13,
    color: globals.COLOR.Text,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  box:{
      width:'100%',
      height:120,
      borderRadius:5,
      borderWidth:0.5,
      marginTop:'2%',
      marginBottom:'5%',
      //alignItems:'center'
  },
  buttohelpnwrapper: {
    flexDirection: "row",
    justifyContent: 'flex-end',
    paddingTop: '5%',
    paddingRight: '5%'
  },
  rectangle:{
      marginTop:'5%',
      marginLeft:'5%',
      width:80,
      height:50,
      borderRadius:5,
      borderWidth:0.4,
      backgroundColor:globals.COLOR.purple,
      alignItems:'center',
      justifyContent:'center'
  },
  four:{
    fontSize: 13,
    color: 'white',
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  boxLine:{
      flexDirection:'row',
      alignItems:'center',
  },
  colText:{
      marginLeft:'6%',
  },
  stars:{
     // marginLeft:'5%',
      flexDirection:'row',
  },
  good:{
      marginLeft:'5%',
  },
  vgood:{
    fontSize: 10,
    color: globals.COLOR.Text,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    textAlign:'left'
  },
  rating:{
    fontSize: 7,
    color: globals.COLOR.greyText,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,   
    textAlign:'left'
  },
  col1:{
      justifyContent:'flex-start',
      alignItems:'center',
      flexBasis:'32%'
  },
  col2:{
      justifyContent:'center',
      alignItems:'center',
      flexBasis:'33%'
  },
  col3:{
      justifyContent:'flex-end',
      alignItems:'center',
      flexBasis:'32%'
  },
  rowText:{
      flexDirection:'row',
     // marginLeft:'3%',
      marginTop:'4%'
  },
  service:{
    fontSize: 12,
    color: '#2F4F4F',
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,   
  },
  starImage:{
      height:20,
      width:20,
      resizeMode:'contain'
  },
  custText:{
    fontSize: 14,
    textAlign:'left',
    color: globals.COLOR.Text,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica, 
  },
  imageRow:{
      flexDirection:'row',
      alignItems:'center',
      marginTop:'5%'
     // justifyContent:'space-around',
      
  },
  name:{
      marginLeft:'3%',
  },
  nameText:{
    fontSize: 12,
    color: globals.COLOR.Text,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica, 
  },
  strs:{
      flexDirection:'row',
      marginLeft:'5%'
  },
  viewButton:{
 
  alignItems:'center',
  justifyContent:'center'
  //backgroundColor:globals.COLOR.purple,
  //borderRadius:10,
},
buttonText:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  fontSize: 12,
  color:globals.COLOR.purple,
},
textStyle:{
  alignItems:'center',
  justifyContent:'center',
  width:'80%',
  height:60,
  marginBottom:'5%'
  //marginTop:'6%',
},
humanImage:{
    width:30,
    height:30,
    resizeMode:'contain',
    //borderRadius:200
},
textRow:{
    marginTop:'3%',
    marginBottom:'3%',
},
textLines:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 10,  
    lineHeight:14,
    color:globals.COLOR.lightBlueText,

},
bannerCart:{
    margin:0,
    marginTop:'3%',
    paddingLeft:0,
    paddingRight:0,

},
cardSection:{
   // marginBottom:'%'
},
bannerView:{
  position:'absolute',
  bottom:5,
  width:'100%',
  height:200,
},




});

export { images, styles };