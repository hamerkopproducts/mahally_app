import React, { Component, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { View, FlatList, Text, Image, TouchableOpacity } from 'react-native';
import { images, styles } from "./styles";
import appTexts from "../../lib/appTexts";
import { ScrollView } from 'react-native-gesture-handler';




const CheckBox = (props) => {

    const {
        item,
        pushTypesId,
        restTypesFilter
    } = props;

    const [isIconClicked, setIsIconClicked] = useState(restTypesFilter.indexOf(item.id) == -1);

    const checkIconClicked = (id) => {
        pushTypesId(id);
        setIsIconClicked(!isIconClicked)
    };

    return (
    <ScrollView>
        <View style={styles.loCListView}>
            <TouchableOpacity onPress={() => checkIconClicked(item.id)}>
                <View style={styles.iconView}>

                    <Image style={styles.imageCheck}
                        source={isIconClicked === false ? images.radioOn : images.radioOff} />

                </View>
            </TouchableOpacity>
            <View style={styles.listView}>
                <Text style={styles.locationText}>{item.rest_type}</Text>
            </View>
        </View>
        </ScrollView>

    );
};
CheckBox.propTypes = {
    pushTypesId: PropTypes.func,
    pullTypesId: PropTypes.func,
};


export default CheckBox;
