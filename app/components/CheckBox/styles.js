import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const images = {
    radioOff: require("../../assets/images/listCard/checkBlank.png"),
    radioOn: require("../../assets/images/listCard/CheckTick.png")

};

const styles = StyleSheet.create({
    mainScreen: {
        justifyContent: 'flex-start',
        //alignItems:'flex-start',

    },
    listView: {
        marginLeft: '3%',
        //backgroundColor:'green',
        //width:'100%'
        //paddingTop:'10%',
        // padding:10,
        // paddingBottom:'2%'
    },
    locationText: {
        fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
        color: globals.COLOR.Text,
        fontSize: 14,
    },
    loCListView: {
        flexDirection: 'row',
        marginVertical: 15,
        alignItems: "center",
        
        

    },
    iconView: {
        alignSelf: 'center',
    },
    mainView: {
        marginTop: "1%"
    },
    radioImage: {
        width: 25,
        height: 25,
        resizeMode: 'contain',

    },
    imageCheck: {
        width: 25,
        height: 25,
    },
});
export { styles, images };