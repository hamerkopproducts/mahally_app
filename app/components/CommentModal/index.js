import React, { useState } from "react";
import { images, styles } from "./styles";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  I18nManager,
  ScrollView
} from "react-native";

import appTexts from "../../lib/appTexts";
import PropTypes from "prop-types";
import Modal from "react-native-modal";
import Rating from "../../components/Rating/Rating";
import FastImageLoader from '../../components/FastImage/FastImage';
const CommentModal = (props) => {

    const [showAll, setShowAll] = useState(false);

  const {
    isCommentModalVisible,
    restaurantDataDetails,
    openCommentModal,
    all_segments,
    userReviews,
  } = props;

  let all_reviews = [];
  if (userReviews) {
    for (let incr = 0; incr < Object.keys(userReviews).length; incr++) {
      all_reviews.push(userReviews[Object.keys(userReviews)[incr]][0]);
    }
  }

  let displayItems = all_reviews;
  if(showAll == false) {
    
    displayItems = all_reviews.slice(0, 2);
  }

  const showAllItems = () => {
    setShowAll(true);
  }

  return (
    <Modal
      transparent={true}
      animationIn="slideInUp"
      animationOut="slideOutRight"
      isVisible={isCommentModalVisible}
      style={styles.modalMaincontentLogout}
    >
      
      <View style={styles.modalmainviewLogout}>
        <View style={styles.lineTop}></View>
        <TouchableOpacity
          style={styles.buttohelpnwrapper}
          onPress={() => {
            openCommentModal();
          }}
        >
          <Text style={styles.xText}>X</Text>
        </TouchableOpacity>
        <View style={styles.headingLine}>
          <Text style={styles.head}>{appTexts.COMMENT.head}</Text>
        </View>
        <View style={styles.box}>
          <View style={styles.boxLine}>
            <View style={styles.rectangle}>
              <Text style={styles.four}>{restaurantDataDetails.rating}</Text>
            </View>
            <View style={styles.colText}>
              <View style={styles.stars}>
                <Rating
                  is_clickable={true}
                  half_star={false}
                  onPress={(val) => null}
                  rating={restaurantDataDetails.rating}
                />
              </View>
              <View style={styles.good}>
                {/* <Text style={styles.vgood}>{appTexts.COMMENT.good}</Text> */}
              </View>
              <View style={styles.good}>
                <Text style={styles.rating}>
                  {appTexts.COMMENT.rating.replace(
                    "{*}",
                    restaurantDataDetails.rated_count
                  )}
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.rowText}>
            <FlatList
              data={all_segments}
              horizontal
              showsHorizontalScrollIndicator={false}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item }) => {
                const lang = I18nManager.isRTL ? 1 : 0;
                let name = "";
                try {
                  name = item.segment.lang[lang].name;
                } catch (err) {}
                return (
                  <View style={styles.col1}>
                    <View style={styles.line1}>
                      <Text style={styles.service}>{name}</Text>
                    </View>
                    <View style={styles.line2}>
                      <Text style={styles.service}>4/5</Text>
                    </View>
                  </View>
                );
              }}
            />
          </View>
        </View>
        <ScrollView>
        <View style={styles.custView}>
          <Text style={styles.custText}>{appTexts.COMMENT.customer}</Text>
        </View>

        <View style={{   marginHorizontal:'2%' }}>
          <FlatList
            data={displayItems}
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
            // contentContainerStyle={{paddingBottom:'50%'}}
            renderItem={({ item }) => {
              const lang = I18nManager.isRTL ? 1 : 0;
              let name = "";
              let photo= "";
              try {
                name = item.customerdata.name;
                photo = item.customerdata.photos
              } catch (err) {}
              return (
                <View style={{ flex: 1 }}>
                  <View style={styles.imageRow}>
                    <View style={styles.human}>
                    {/* source={images.human} */}
                    {photo ?  
                    <FastImageLoader
                    resizeMode={'cover'}
                    photoURL={photo}
                    style={styles.logo}
                    loaderStyle={{}}
                  />: <Image source={images.person} style={styles.logo} />}
                      {/* <Image  source={{ uri: photo }} style={styles.humanImage} /> */}
                    </View>
                    <View style={styles.name}>
                      <Text style={styles.nameText}>{name}</Text>
                    </View>
                    <View style={styles.strs}>
                      <Rating
                        is_clickable={true}
                        half_star={false}
                        onPress={(val) => null}
                        rating={item.rating}
                      />
                    </View>
                  </View>

                  {item.comment != "" && item.comment != null && (
                    <View style={styles.textRow}>
                      <Text style={styles.textLines}>{item.comment}hkjg</Text>
                    </View>
                  )}
                </View>
              );
            }}
          />
        </View>

        {(showAll == false && all_reviews.length > 2) &&
            <TouchableOpacity style={styles.viewButton} onPress={() => showAllItems() }>
                <View style={styles.textStyle}>
                    <Text style={styles.buttonText}>{appTexts.COMMENT.all}</Text>
                </View>
            </TouchableOpacity>
        }
        </ScrollView>
      </View>
    </Modal>
  );
};

CommentModal.propTypes = {
  isCommentModalVisible: PropTypes.bool,

  openCommentModal: PropTypes.func,
};

export default CommentModal;
