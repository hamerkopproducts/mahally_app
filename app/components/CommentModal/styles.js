import { StyleSheet, I18nManager, Platform } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const images = {
  star: require("../../assets/images/modal/Star.png"),
  starhalf: require("../../assets/images/modal/Star-half.png"),
  human:require("../../assets/images/ItemCard/img3.png"),
  person:require("../../assets/images/profileicon/profile-image.jpg"),
};

const styles = StyleSheet.create({
  modalMaincontentLogout: {
    justifyContent:'flex-end',
    margin: 0,
    alignItems: 'center',
  },
  modalmainviewLogout: {
    backgroundColor: "white",
    paddingLeft: '4%',
    paddingRight: '2%',
    borderTopLeftRadius: 15,
    borderTopRightRadius:15,
    justifyContent: 'center',
    borderColor: "rgba(0, 0, 0, 0.1)",
    width:'100%',
    paddingBottom:'15%',
    marginTop: 200,
  },
  logo: {
    width: 75,
    height: 72,
    resizeMode: 'cover',
    borderRadius: 50,
    alignSelf: 'center',

  },
  lineTop:{
      width:150,
      borderColor:globals.COLOR.greyBackground,
      borderWidth:2,
      borderRadius:5,
      marginLeft:'28%',
      
      marginTop:'3%'
  },
  xText: {
    fontSize: 20,
    color: globals.COLOR.greyText,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },

  
  head:{
    fontSize: 14.5,
    color: globals.COLOR.Text,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helveticaBold, 
    textAlign:'left',
  },
  rate:{
    fontSize: 12,
    color: globals.COLOR.greyText,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  comment:{
      marginTop:'4%',
  },
  commentStyle:{
    fontSize: 13,
    color: globals.COLOR.Text,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  box:{
      width:'94%',
      //height:120,
      borderRadius:5,
      borderWidth:0.5,
      marginTop:'2%',
      paddingBottom:'3%',
      marginBottom:'8%',
      marginHorizontal:'2%',
      
      //alignItems:'center'
  },
  custView:{
    marginHorizontal:'2%',
    marginBottom:"4%"
  },
  headingLine:{
    marginHorizontal:'2%',
    marginBottom:'5%'
  },
  buttohelpnwrapper: {
    marginTop: 20,
    flexDirection: "row",
    justifyContent: 'flex-end',
    paddingTop: '5%',
    paddingRight: '5%'
  },
  rectangle:{
      marginTop:'5%',
      marginLeft:'5%',
      width:80,
      height:50,
      borderRadius:5,
      borderWidth:0.4,
      backgroundColor:globals.COLOR.purple,
      alignItems:'center',
      justifyContent:'center'
  },
  four:{
    fontSize: 13,
    color: 'white',
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  boxLine:{
      flexDirection:'row',
      alignItems:'center',
     
  },
  colText:{
      marginLeft:'6%',
      
  },
  stars:{
     // marginLeft:'5%',
      flexDirection:'row',
  },
  good:{
      marginLeft:'5%',
      
  },
  vgood:{
    fontSize: 10,
    color: globals.COLOR.Text,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helveticaBold,
    textAlign:'left'
  },
  rating:{
    fontSize: 10,
    color: globals.COLOR.greyText,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,   
    textAlign:'left'
  },
  col1:{
      justifyContent:'flex-start',
      alignItems:'center',
      flexBasis:'32%',
      width: 80
  },
  col2:{
      justifyContent:'center',
      alignItems:'center',
      flexBasis:'33%'
  },
  col3:{
      justifyContent:'flex-end',
      alignItems:'center',
      flexBasis:'32%'
  },
  rowText:{
      flexDirection:'row',
     // marginLeft:'3%',
      marginTop:'4%',
    
  },
  service:{
    fontSize: 12,
    color: '#2F4F4F',
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,   
  },
  starImage:{
      height:20,
      width:20,
      resizeMode:'contain'
  },
  custText:{
    fontSize: 14.5,
    textAlign:'left',
    color: globals.COLOR.Text,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helveticaBold, 
  },
  imageRow:{
      flexDirection:'row',
      alignItems:'center',
      marginTop:'5%'
     // justifyContent:'space-around',
      
  },
  name:{
      marginLeft:'3%',
  },
  nameText:{
    fontSize: 16,
    color: '#2F4F4F',
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica, 
  },
  strs:{
      flexDirection:'row',
      marginLeft:'5%'
  },
  viewButton:{
 
  alignItems:'center',
  justifyContent:'center'
  //backgroundColor:globals.COLOR.purple,
  //borderRadius:10,
},
buttonText:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  fontSize: 12,
  color:globals.COLOR.purple,
},
textStyle:{
  alignItems:'center',
  justifyContent:'center',
  width:'80%',
  height:60,
  marginBottom:'5%'
  //marginTop:'6%',
},
humanImage:{
    width:50,
    height:50 ,
    borderRadius: 30,
    resizeMode:'contain',
    //borderRadius:200
},
textRow:{
    marginTop:'3%',
    marginBottom:'3%',
    marginHorizontal:"2%"
},
textLines:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 14,  
    lineHeight:20,
    color:globals.COLOR.lightBlueText,
    textAlign:'left'

},



});
 
export { images, styles };