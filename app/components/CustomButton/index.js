import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { View,Text} from 'react-native';
import { styles } from "./styles";

const CustomButton = (props) => {
	const {
		buttonText,
		buttonstyle,
	} = props;

  return (
	   
	<View style={buttonstyle}>
	<Text style={styles.buttonText}>{buttonText}</Text>
	</View>
  );
};

 CustomButton.propTypes = {
	buttonText: PropTypes.string,
 };

export default CustomButton;
