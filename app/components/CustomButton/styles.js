import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"

const styles = StyleSheet.create({
  // buttonStyle:{
  //   backgroundColor:globals.COLOR.purple,
  //    width:150,
  //    height: 45,
  //    borderRadius:15,
  //    justifyContent:'center',
  //    alignItems:'center',
  //  //  marginLeft:6.5

  //   // backgroundColor:'#cccccc'
  // },
  
  buttonText:{
    color:'white',
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize:globals.FONTSIZE.fontSizeFifteen,
    textAlign:"center",
    justifyContent:'center',
    alignItems:'center',
    },
  
});

export { styles  };