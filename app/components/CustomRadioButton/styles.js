import { StyleSheet,I18nManager } from "react-native";
import globals from '../../lib/globals';
const styles = StyleSheet.create({
  circle: {
    // height: 20,
    // width: 20,
    height :21,
    width: 21,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#ACACAC',
    alignItems: 'center',
    justifyContent: 'center',
  },
  checkedCircle: {
    width: 14,
    height: 14,
    borderRadius: 7,
    backgroundColor: globals.COLOR.purple,
  },

});

export { styles };
