import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { View } from 'react-native';
import { styles } from "./styles";

const DividerLine = (props) => {
	const {
		dividerStyle,
		isCheck,
	} = props;

  return (
	  <View style={[isCheck ? styles.div : styles.dividerStyle],dividerStyle}/>
  );
};

DividerLine.propTypes = {
	//dividerStyle: PropTypes.object
};

export default DividerLine;
