import { StyleSheet } from "react-native";
import globals from "../../lib/globals"
const styles = StyleSheet.create({
  dividerStyle:{
    width:'90%',
    height: 0.5,
   backgroundColor: globals.COLOR.borderColor,
  },
  div:{
    width:'100%',
    height: 0.5,
   backgroundColor: globals.COLOR.borderColor,
  }
});

export { styles  };
