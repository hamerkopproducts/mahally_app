import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { View } from 'react-native';
import { styles } from "./styles";

const DotedLine = (props) => {
	const {
		dividerStyle
	} = props;

  return (
	  <View style={styles.dividerStyle}/>
  );
};

DotedLine.propTypes = {
	dividerStyle: PropTypes.object
};

export default DotedLine;
