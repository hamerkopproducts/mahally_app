import { StyleSheet } from "react-native";
import globals from "../../lib/globals"
const styles = StyleSheet.create({
  dividerStyle:{
    height: 1, 
    // marginTop: 2,
    // marginBottom: 2,
    borderWidth: 0.5,
    borderStyle: 'dashed',
    borderColor: globals.COLOR.borderColor,
    width:'100%',
    borderRadius: 1,
  }
});

export { styles  };
