<Modal isVisible={isModalVisible} style={styles.modalMainContent}>
          <View style={styles.modalmainView}>
            <View style={styles.mainView}>
              <View style={styles.mainViewStyle}/>
            </View>
            <TouchableOpacity
              style={styles.buttonwrapper}
              onPress={() => {
                toggleModal();
              }}
            >
              <Image
                source={images.close}
                style={styles.closeStyle}
              />
            </TouchableOpacity>
            <View style={styles.modaltextWarpper}>
              <Text style={styles.headText}>
                {appTexts.EDITPROFILE.Heading}
              </Text>
            </View>
            <View style={styles.modalimageWarpper}>
              <Image
                source={images.main}
                style={styles.modallogo}
              />
              <Image
                source={images.cam}
                style={styles.camStyle}
              />
            </View>
            <View style={styles.formWrappers}>
              <View style={styles.subjectWrapper}>
                <Text style={styles.subText}>{appTexts.EDITPROFILE.Name}</Text>
                <View style={{ marginTop: -10 }}>
                  <TextInput
                    style={styles.input}
                    underlineColorAndroid="lightgray"
                    autoCapitalize="none"
                  />
                </View>
              </View>
              <View style={styles.messageWrapper}>
                <Text style={styles.subText}>
                  {appTexts.EDITPROFILE.Email}*
                </Text>
                <View style={{ marginTop: -10 }}>
                  <TextInput
                    style={styles.input}
                    underlineColorAndroid="lightgray"
                    //placeholder = "Password"
                    //placeholderTextColor = "#9a73ef"
                    autoCapitalize="none"
                  />
                </View>
              </View>

              <View style={styles.messageWrapper}>
                <Text style={styles.subText}>
                  {appTexts.EDITPROFILE.Phone}*
                </Text>
                <View style={{ marginTop: -2 }}>
                  <View>
                    <View style={{ flexDirection: "row" }}>
                      <View style={styles.newflagWrapper}>
                        <Image
                          source={require("../../assets/images/Icons/flag.png")}
                          style={{ height: 20, width: 20 }}
                        />
                        <TextInput
                          editable={false}
                          style={styles.fixedInput}
                          placeholder={"+966"}
                          placeholderTextColor="#282828"
                        />
                      </View>
                      <View style={styles.newinputWrapper}>
                        <TextInput style={styles.numberInput} />
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </View>
            <View style={styles.buttoninsidewrapper}>
              <TouchableOpacity
                style={styles.buttononview}
                onPress={() => this.props.onPress()}
              >
                <LinearGradient
                  start={{ x: 0, y: 0 }}
                  end={{ x: 1, y: 2.45 }}
                  locations={[0, 3]}
                  colors={["#ff8001", "#fbc203"]}
                  style={styles.yButtonon}
                >
                  <Text style={styles.yellowbuttonText}>
                    {appTexts.EDITPROFILE.Update}
                  </Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>