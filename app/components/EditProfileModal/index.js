import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Image, TextInput, Switch, ScrollView, Keyboard, I18nManager, Platform } from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import { images, styles } from "./styles";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import Modal from 'react-native-modal';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import _ from "lodash"
import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import Loader from "../../components/Loader";
import * as editUserActions from "../../actions/editUserActions";
import functions from "../../lib/functions";
import CustomButton from '../../components/CustomButton';

import { launchImageLibrary } from 'react-native-image-picker';
import { CropView } from 'react-native-image-crop-tools';
import ServiceWrapperAwait from '../../service/ServiceWrapperAwait';
import FastImageLoader from '../../components/FastImage/FastImage';
import apiHelper from '../../lib/apiHelper';
import RNFetchBlob from 'rn-fetch-blob';

let cropViewRef = null;
const EditProfileModal = (props) => {

  const {
    isModalVisible,
    toggleModal,
  } = props;

  const root_url = apiHelper.getBaseUrl();
  const [imageData, setImageData] = useState({ logo: null });
  const [loader, setLoader] = useState(false);
  const [backDropOpacity, setBackDropOpacity] = useState(.7);

  const updateProfile = async (_data) => {
    const sAsyncWrapper = new ServiceWrapperAwait();
    setLoader(true);
    const response = await sAsyncWrapper.post('user-app/profile/update', _data, 'multipart/form-data');
    setLoader(false);
    try {
      const data = new Promise((resolve, reject) => {
        try {
          resolve(response);
        } catch (err) {
          reject(err);
        }
      });
      return data;
    } catch (err) {
    }
  }
  const changeBackDropOpacity = () => {
    setBackDropOpacity(.1);
    setTimeout(() => {
      setBackDropOpacity(.7);
    }, 3000);
  }
  const saveUserData = async (_data) => {
    const data = await updateProfile(_data);
    changeBackDropOpacity();
    if (data.success == true) {
      toggleModal();
      toggleModal();
      props.editUserData(props.token);
      functions.displayToast(
        "success",
        "top",
        appTexts.EDITPROFILE.profilemsg
      );
    } else {
      functions.displayToast('error', 'top', 'Error', data?.error?.msg || '');
    }
  }
  const pickLogo = async () => {
    const options = {
      title: 'Select profile picture',
    };
    try {
      launchImageLibrary(options, (res) => {
        if (res.didCancel) {
        } else if (res.error) {
        } else {
          if (res.fileSize / 1000000 > 10) {
            functions.displayToast('error', 'top', 'Error', 'Image size should not exceed 1MB');
            return false;
          }
          const ext = res.uri.substring(res.uri.lastIndexOf('.'));

          setImageData({
            logo: res,
            logoUrl: null,
            selected: true,
            cropped: false,
            typeImage: res.type,
            nameImage: res.fileName
          });
        }
      });

    } catch (err) {
    }
  }

  const cropLogo = () => {
    const quality = Platform.OS === 'ios' ? .5 : 50;
    cropViewRef.saveImage(true, quality);

    return false;
  }


  const [isEnabled, setIsEnabled] = useState(false);
  const [isEnabled2, setIsEnabled2] = useState(false);

  const [name, setName] = useState(_.get(props, "editUserDetails.data.name", ''));
  const [email, setEmail] = useState(_.get(props, "editUserDetails.data.email", ''));
  const [phone, setPhone] = useState(_.get(props, "editUserDetails.data.phone_number", ''));
  const [photo, setPhoto] = useState(_.get(props, "editUserDetails.data.photo", null));

  const [nameerrorflag, setnameerrorflag] = useState(false);
  const [emailerrorflag, setemailerrorflag] = useState(false);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  };


  const handleComponentUnmount = () => { };

  //updated
  useEffect(() => handleComponentUpdated(), []);

  const handleComponentUpdated = () => {
  };

  const validatename = flag => {
    setnameerrorflag(flag);
  };
  const validateEmail = flag => {
    setemailerrorflag(flag);
  };
  const updateUserDetails = () => {

    if (name === '') { // || !functions.isValidName(name)) {
      validatename(true)
      setLoader(false)
    }
    else if (email === '' || !functions.isValidEmail(email)) {
      validateEmail(true)
      setLoader(false)
    }
    else {
      let _imageData = imageData;
      if(_imageData.logo != null && _imageData.cropped == false) {
        setImageData({
          logo: null,
          cropped: false,
          selected: false,
        });
        _imageData = {logo: null };
      }
      Keyboard.dismiss()
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          if(Platform.OS !== 'ios') {
            let _data = new FormData();
            if (_imageData.logo != null) {
              _data.append('photo', {
                name: _imageData.nameImage,
                type: _imageData.typeImage,
                uri: Platform.OS === 'android' ? _imageData.logo.uri : _imageData.logo.uri.replace("file://", "")
              });
            }
            _data.append('name', name.trim());
            _data.append('email', email.trim());
            _data.append('ph_in_order', isEnabled ? "enable" : "disable");
            _data.append('ph_in_review', isEnabled2 ? "enable" : "disable")
            saveUserData(_data);
          } else {
            iosProfileUpdate(_imageData);
          }
        } else {
          functions.displayToast('error', 'top', "network Error", "No network");
        }
      });
    }
  };

  const iosProfileUpdate = (_imageData) => {
    let _image = {};
    if (_imageData.logo != null) {
      _image = { 
        name : 'photo', 
        filename : _imageData.nameImage, 
        type: _imageData.typeImage, 
        data: RNFetchBlob.wrap( _imageData.logo.uri.replace("file://", "") )
      }
    }
    setLoader(true);
    RNFetchBlob.fetch('POST', root_url + 'user-app/profile/update', {
      Authorization : "Bearer " + props.token,
      otherHeader : "foo",
      'Content-Type' : 'multipart/form-data',
    }, [
      _image,
      { name : 'name', data : name.trim()},
      { name : 'email', data : email.trim() },
      { name : 'ph_in_order', data : isEnabled ? "enable" : "disable" },
      { name : 'ph_in_review', data : isEnabled2 ? "enable" : "disable" },
    ]).then((response) => {
      setLoader(false);
      changeBackDropOpacity();
      let resp_data = response.data;
      if(typeof resp_data == 'string') {
        resp_data = JSON.parse(resp_data);
      }
      if (resp_data.success == true) {
        toggleModal();
        toggleModal();
        props.editUserData(props.token);
        functions.displayToast(
          "success",
          "top",
          appTexts.EDITPROFILE.profilemsg
        );
      } else {
        functions.displayToast('error', 'top', 'Error', resp_data.error?.msg || 'Something went wrong!');
      }
    }).catch((err) => {
      functions.displayToast('error', 'top', 'Error', '');
      setLoader(false);
    })
  }

  return (
    <ScrollView>
      <Modal
        isVisible={isModalVisible}
        animationIn='fadeIn'
        animationOut='fadeOut'
        backdropOpacity={backDropOpacity}
        style={styles.modalMainContent}>
        {(loader) && <Loader />}
        <View style={styles.modalimageWarpper}>
          {/* <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: '25%' }}>
            
          </View> */}
          <TouchableOpacity onPress={() => { pickLogo(); }} >
            {imageData.selected == false && imageData.cropped == true && (
              <>
                {imageData.logo != null && (
                  <>
                    {imageData.selected == false &&
                      imageData.cropped == true && (
                        <View>
                          <Image
                            source={{ uri: imageData.logo.uri }}
                            style={styles.modallogo}
                          />
                          <Image source={images.frame} style={styles.fram} />
                        </View>
                      )}
                  </>
                )}
              </>
            )}

            {/* <Image source={images.frame} style={styles.fram}/>    */}
            {imageData.logo == null && (
              <>
                {photo != null &&
                  <View>
                    <FastImageLoader
                      resizeMode={'cover'}
                      photoURL={photo}
                      style={styles.modallogo}
                      loaderStyle={{}}
                    />
                    <Image source={images.frame} style={styles.fram} />
                  </View>
                }
                {photo == null &&
                  <View>
                    <Image source={images.main} style={styles.modallogo} />
                    <Image source={images.frame} style={styles.fram} />
                  </View>
                }
              </>
            )}
            {/* <Image source={images.main} style={styles.modallogo}/> */}
          </TouchableOpacity>
        </View>
        <View style={styles.modalimageWarpper}>
          {imageData.selected == true && imageData.cropped == false && (
            <CropView
              style={{ width: 200, height: 200, alignItems: 'center', marginHorizontal: '15%' }}
              sourceUrl={imageData.logo.uri}
              setLoader={false}
              ref={(ref) => (cropViewRef = ref)}
              onImageCrop={(res) =>
                setImageData({
                  logo: res,
                  cropped: true,
                  selected: false,
                  typeImage: imageData.typeImage,
                  nameImage: imageData.nameImage
                })
              }
              keepAspectRatio
              aspectRatio={{ width: 1, height: 1 }}
            />
          )}

          {imageData.selected == true && imageData.cropped == false && (
            <TouchableOpacity
              style={{ alignSelf: "center" }}
              onPress={() => cropLogo()}
            >
              <View style={styles.cus}>
                <CustomButton buttonstyle={styles.custombuttonStyle} buttonText={"Crop"} />
              </View>
            </TouchableOpacity>
          )}
        </View>
        <View style={styles.modalmainView}>
          <KeyboardAwareScrollView scrollEnabled={true} contentContainerStyle={{ justifyContent: 'flex-end' }}>
            <TouchableOpacity style={styles.buttonwrapper} onPress={() => { toggleModal(); }}>
              <Text style={styles.closeText}>X</Text>
            </TouchableOpacity>
            <View>
              <View style={styles.subjectWrapper}>
                <Text style={styles.subText}>{appTexts.EDITPROFILE.Name}</Text>
              
              </View>
              <View>
                <TextInput style={nameerrorflag ? styles.redInput : styles.input}
                  //underlineColorAndroid="lightgray"
                  autoCapitalize="none"
                  value={name}
                  onChangeText={(val) => {
                    validatename(false);
                    setName(val);

                  }}

                />

              </View>

              <View style={styles.messageWrapper}>
                <View style={styles.subjectWrapper}>
                  <Text style={styles.subText}>{appTexts.EDITPROFILE.Email}</Text>
                  
                </View>
                <View >
                  <TextInput
                    style={emailerrorflag ? styles.redInput : styles.input}
                    //underlineColorAndroid="lightgray"
                    //placeholder = "Password"
                    placeholderTextColor='grey'
                    autoCapitalize="none"
                    value={email}
                    onChangeText={(val) => {
                      validateEmail(false);
                      setEmail(val);
                    }}
                  />
                </View>
              </View>
              <View style={styles.subjectWrapper}>
                <Text style={styles.subText}>{appTexts.EDITPROFILE.mobile}</Text>
              </View>
              <View style={styles.phoneScetion}>
                <View style={styles.firstSection}>
                  <Image
                    source={require("../../assets/images/chooseLanguage/Arabic.png")}
                    style={[{ height: 20, width: 32, borderRadius: 70},
                      I18nManager.isRTL && {transform: [{scaleX: -1}]}
                    ]}
                  />
                  <View style={{ flexDirection: 'row', marginLeft: 2, marginRight: 5 }}>
                    <TextInput
                      editable={false}
                      style={styles.disableText}
                      placeholder={"+966"}
                      placeholderTextColor={globals.COLOR.lightBlueText}
                      style={[ I18nManager.isRTL && { transform: [{scaleX: -1}] }]}
                    />
                  </View>
                  <View style={styles.line}></View>
                </View>
                <View style={styles.secondSection}>
                  <TextInput
                    style={styles.enableText}
                    keyboardType="number-pad"
                    value={phone}
                    editable={false}
                  />
                </View>
              </View>
              <View style={styles.disText}>
                <Text style={styles.display}>{appTexts.EDITPROFILE.displayOne}</Text>
                <Switch trackColor={{ false: 'grey', true: 'grey' }}
                  thumbColor={isEnabled ? 'purple' : 'white'}

                  ios_backgroundColor="white"
                  onValueChange={(value) => {
                    setIsEnabled(!isEnabled);
                  }
                  }
                  value={isEnabled}

                  style={{ transform: [{ scaleX: 1.3 }, { scaleY: 1.3 }] }}
                />
              </View>
              <View style={styles.disText}>
                <Text style={styles.display}>{appTexts.EDITPROFILE.displayTwo}</Text>
                <Switch trackColor={{ false: 'grey', true: 'grey' }}
                  thumbColor={isEnabled2 ? 'purple' : 'white'}
                  ios_backgroundColor="white"
                  onValueChange={(value) => {
                    setIsEnabled2(!isEnabled2);
                  }
                  }
                  value={isEnabled2}
                  style={{ transform: [{ scaleX: 1.3 }, { scaleY: 1.3 }] }}
                />
              </View>

            </View>
            <TouchableOpacity
              style={styles.helpbuttononview}
              onPress={() => updateUserDetails()}>
              <View style={styles.helpbuttoninsidewrapper}>

                <Text style={styles.helpyellowbuttonText}>
                  {appTexts.EDITPROFILE.save}
                </Text>

              </View>
            </TouchableOpacity>
          </KeyboardAwareScrollView>

        </View>

      </Modal>
    </ScrollView>
  );
};

EditProfileModal.propTypes = {
  isModalVisible: PropTypes.bool,
  toggleModal: PropTypes.func,
  setName: PropTypes.func,
  setEmail: PropTypes.func,
  setPhone: PropTypes.func,

  validatename: PropTypes.func,
  validateEmail: PropTypes.func,
};
const mapStateToProps = (state, props) => {
  return {
    //userDetailsUpdated: _.get(state, 'editUserDetailsReducer.updateduserdetails', ''),
    token: _.get(state, "loginReducer.userData.data.access_token", ''),
    editUserDetails: _.get(state, "editUserReducer.editUserData", ''),
    isloading: _.get(state, "editUserReducer.isLoading", ''),
    updateUserData: _.get(state, "editUserReducer.updateUserData", ''),
  };
};
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      editUserData: editUserActions.doUserDetailsEdit,
      doUpdateUserDetails: editUserActions.doUpdateUserDetails,
    },
    dispatch
  );
};

const EditProfileModalWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(EditProfileModal);

EditProfileModalWithRedux.navigationOptions = ({ navigation }) => ({
  header: null,
});
export default EditProfileModalWithRedux;

