import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const images = {
  close: require("../../assets/images/profileicon/cloo.png"),
  main:require("../../assets/images/profileicon/profile-image.jpg"),
  frame: require('../../assets/images/profileicon/frame.png')
};
const styles = StyleSheet.create({

  modalMainContent: {
    justifyContent: 'flex-end',
    margin: 0,
   
  },
  custombuttonStyle:{
    backgroundColor:globals.COLOR.purple,
     width:150,
     height: 45,
     borderRadius:15,
     justifyContent:'center',
     alignItems:'center',
   //  marginLeft:6.5

    // backgroundColor:'#cccccc'
  },
  modalmainView: {
    backgroundColor: 'white',
    padding: "5%",
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
    borderColor: "rgba(0, 0, 0, 0.1)",

  },
  fram: {
    resizeMode: 'cover',
    width: 105,
    height: 105,
    position: 'absolute',
    zIndex: 2,
    top: -10,
    justifyContent: "center",
    alignSelf: 'center',

  },
  mainView: { flexDirection: "row", justifyContent: "center" },
  mainViewStyle: {
    width: "25%",
    height: 3,
    borderRadius: 5,
    marginTop: hp("0.5%"),
    backgroundColor: "#FFF0F5",
  },
  buttonwrapper: {
    flexDirection: "row",
    justifyContent: 'flex-end'
    

  },
  closeText: {
    fontSize: 22,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color: globals.COLOR.greyText,
  },

  modallogo: {
    width: 85,
    height: 85,
    resizeMode: 'cover',
    borderRadius: 50,
    alignSelf: 'center',


  },
  modalimageWarpper: {
    alignSelf: 'center',
    position: 'relative',
    top:25,
    zIndex: 2

  },
  subjectWrapper: {

    flexDirection: 'row',
    paddingBottom: hp('1%'),

  },
  cus:{
    marginTop:'4%',
  },
  star: {
    color: 'red',
  },
  subjectWrappers: {
    paddingBottom: '4%'
  },
  line: {
    flexDirection: 'column',
    borderLeftWidth: 0.5,
    height: 37,
    borderLeftColor: globals.COLOR.greyText,
  },
  disText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: '3%',
    marginBottom: '3%',
    paddingRight:'2%'
  },
  
  input: {
    height: 50,
    borderColor: globals.COLOR.greyText,
    borderWidth: 0.5,
    borderRadius: 10,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 15,
    color: globals.COLOR.lightBlueText,
    textAlign: I18nManager.isRTL ? "right" : "left",
    marginBottom:'8%',
    paddingLeft:'5%'
    
  },
  redInput:{
    height: 50,
    borderColor: 'red',
    borderWidth: 0.5,
    borderRadius: 10,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: hp('2.3%'),
    color: globals.COLOR.lightBlueText,
    textAlign: I18nManager.isRTL ? "right" : "left",
    marginBottom:'8%',
    paddingLeft:'5%'
  },
  subText: {
    fontSize: 15,
    color: globals.COLOR.Text,
    paddingLeft: '1%',
    paddingRight: '2%',
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  phoneScetion: {
    flexDirection: 'row',
    paddingLeft: '2%',
    paddingRight: '3%',
    marginBottom: '5%',
    borderWidth: 0.5,
    borderColor: globals.COLOR.greyText,
    borderRadius: 10,
    overflow: 'hidden',
    transform: [{scaleX: I18nManager.isRTL ? -1 : 1}], // arabic normal
    backgroundColor: '#dfdfdf'
  },
  firstSection: {
    // flexBasis: '22%',
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  disableText: {
    lineHeight: 20,
    fontSize: 15,
    color: globals.COLOR.blackTextColor,
    width: "80%",
    flexDirection: 'row',
    marginLeft: 5,
    // textAlign: I18nManager.isRTL ? "right" : "left",
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  secondSection: {

    marginLeft: '7%',
    flexBasis: '78%',
    flexDirection: 'row'
  },
  enableText: {
    width: "100%",
    fontSize: 15,
    alignItems: "center",
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color: globals.COLOR.lightBlueText,
    transform: [{scaleX: I18nManager.isRTL ? -1 : 1}], // arabic normal
  },
  display: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 14,
  },
  helpbuttoninsidewrapper: {
    flexDirection: "row",
    justifyContent: "center",
    width: 280,
    height: 50,
    borderRadius: 15,
    alignItems: "center",
    alignSelf: 'center',
    marginTop: hp('2.5%'),
    backgroundColor: globals.COLOR.purple,
  },
  helpbuttononview: {
    alignItems: "center",
    justifyContent: "center",
  },
  hylpyButtonon: {
    width: 140,
    height: 50,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center",

  },
  helpyellowbuttonText: {
    fontSize: 14,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color: 'white',
    textAlign: "center",
    alignSelf: 'center'
  },



});


export { styles, images };