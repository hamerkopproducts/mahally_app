import React, { useState } from 'react';
import { styles } from "./styles";
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
import appTexts from "../../lib/appTexts";
import PropTypes from 'prop-types';
import Modal from 'react-native-modal';

import CheckBox from '../../components/CheckBox';

const FilterModal = (props) => {

  const {
    isFilterModalVisible,
    toggleFilterModal,
    applyFilter,
    restTypes,
    restTypesFilter
  } = props;

  let typestemp= [];

  const pushTypes = (id) => { 
      typestemp.push(id)
      pushTypesId(id)
  };

  const [filteredItems, setFilteredItems] = useState( restTypesFilter );

  const pushTypesId = (id) => {
    let items = filteredItems;
    if(filteredItems.indexOf(id) == -1) {
      items.push(id);
    } else {
      items.splice(filteredItems.indexOf(id), 1);
    }
    setFilteredItems( items );
  };

  const resertFilter = () => {
    setFilteredItems( [] );
    applyFilter( [] );
    toggleFilterModal();
  };

  return (
    <Modal
      isVisible={isFilterModalVisible}
      style={styles.modalMaincontentHelp}
    >
      <View style={styles.modalmainviewHelp}>
        <View style={styles.border}></View>
        <TouchableOpacity onPress={() => { toggleFilterModal(); }}>
          <View style={styles.clo}>
            <Text style={styles.x}>X</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.headingLine}>
          <View style={styles.helptextWarpper}>
            <Text style={styles.helpheadText}>{appTexts.STATUS.headingFilter}</Text>
          </View>
          <View style={styles.applytext}>
            <TouchableOpacity onPress={() => { applyFilter(filteredItems) }}>
              <Text style={styles.applyTexts}>{appTexts.STATUS.apply}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.resettext}>
            <TouchableOpacity onPress={() => { resertFilter() }}>
              <Text style={styles.resetTexts}>{appTexts.STATUS.reset}</Text>
            </TouchableOpacity>
          </View>
        </View>
        
        <View style={styles.listV}>
          
            <FlatList
            showsVerticalScrollIndicator={true}
            contentContainerStyle={{}}
            style={{height:'100%'}}
              data={restTypes}
              renderItem={({item}) => {
                return (
                  <CheckBox
                    pushTypesId={pushTypes}
                    restTypesFilter={restTypesFilter}
                    typestemp={typestemp}
                    item={item} 
                  />
                )
              }}
              keyExtractor={(item, index) => index.toString()}
            />

        </View>
        

      </View>
    </Modal>
  );
};

FilterModal.propTypes = {
  toggleFilterModal: PropTypes.func,
  applyFilter: PropTypes.func,
  resertFilter: PropTypes.func,
  isFilterModalVisible: PropTypes.bool
};

export default FilterModal;