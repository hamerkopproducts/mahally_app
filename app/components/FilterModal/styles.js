import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const images = {
 
};

const styles = StyleSheet.create({
modalMaincontentHelp: {
    //justifyContent: "center",
    justifyContent:'flex-end',
    margin: 0,
  },
  
  modalmainviewHelp: {
    backgroundColor: "white",
    //width: wp("90%"),
    padding: "4%",
    //borderRadius: 10,
    borderTopRightRadius:15,
    borderTopLeftRadius:15,
    borderColor: "rgba(0, 0, 0, 0.1)",
    maxHeight: 400,
   // flex:1,
  },
  border:{
      borderWidth:2,
      borderColor:'lightgrey',
      width:'50%',
      alignItems:'center',
      justifyContent:'center',
      marginLeft:'25%',
      marginRight:'20%',
      marginBottom:'3%',

  },
  clo:{
      justifyContent:'flex-end',
      alignItems:'flex-end',
      paddingRight:'2%'
  },

  headingLine:{
      flexDirection:'row',
  },
  x:{
      color:globals.COLOR.greyText,
      fontSize:18,
  },
  helptextWarpper:{
    flexDirection:'row',
    //justifyContent:'center',
    paddingTop:hp('2%'),
    paddingBottom:hp('1.5%'),
  width:'50%'
    },
    applytext:{
      marginLeft: 30,
        paddingTop:hp('2%'),
    paddingBottom:hp('1.5%'),
    //paddingLeft:'10%',
    width:'20%'
    
    },
    resettext:{
        paddingTop:hp('2%'),
    paddingBottom:hp('1.5%'),
    //paddingLeft:'5%',
    width:'25%'
    
    },
    helpheadText:{
      fontSize: 16,
      fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
      textAlign:'center',
      color:globals.COLOR.Text,
    },
    applyTexts:{
      fontSize: 15,
      fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
      textAlign:'center',
      color:globals.COLOR.purple,
    },
    resetTexts:{
      fontSize: 15,
      fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
      textAlign:'center',
      color:globals.COLOR.greyText,
    },

    // boxVies:{
    //     height:50,
    //     width:'95%',
    //     borderWidth:0.5,
    //     borderColor:'#707070',
    //     flexDirection:'row',
    //     justifyContent:'space-between',
    //           marginTop:'8%',
    //     borderRadius:5,
        
    //   marginLeft:'3%'
    //      },
    //      hinput: {
    //         //marginTop:-4
    //         // margin: 15,
    //         // height: 40,
    //         // borderColor: '#7a42f4',
    //         // borderWidth: 1
    //         fontFamily: globals.FONTS.openSansLight,
    //         fontSize: hp('2%'),
    //         textAlign: I18nManager.isRTL ? "right" : "left",
    //         color:"#707070",
    //         marginLeft:'2%'
            
    //       },
    //       arrowv:{
    //         marginRight:'4%',
    //         alignItems:'center',
    //         justifyContent:'center'
    //           },
    //           search:{
    //             width:20,
    //             height:20,
    //             alignSelf:'center'
    //           },
              listV:{
               // marginTop:"5%",
                paddingBottom:30,
                
                //backgroundColor:'red'
                //flex:1,
                },
                flatStyle:{
                  
                  //flex:1,
                  //backgroundColor:'purple'
                },
            });

            export { images, styles };