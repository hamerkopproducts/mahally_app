import { StyleSheet,I18nManager, Platform } from "react-native";
import globals from "../../lib/globals"

const images = {
  homeIconSelected : require("../../assets/images/footerTabItem/Homefill.png"),
  homeIconUnSelected : require("../../assets/images/footerTabItem/home.png"),
  offerIconSelected:require("../../assets/images/footerTabItem/Offer-a.png"),
  offerIconUnSelected:require("../../assets/images/footerTabItem/Offer.png"),
  orderIconSelected : require("../../assets/images/footerTabItem/Order-a.png"),
  orderIconUnSelected : require("../../assets/images/footerTabItem/order.png"),
  scanIconSelected : require("../../assets/images/footerTabItem/scan.png"),
  scanIconUnSelected : require("../../assets/images/footerTabItem/scan.png"),
  profileIconSelected : require("../../assets/images/footerTabItem/Profile-a.png"),
  profileIconUnSelected : require("../../assets/images/footerTabItem/Profile.png")
};

const styles = StyleSheet.create({
  tabBarItem: {
    width: (globals.SCREEN_SIZE.width / 4),
    height: globals.INTEGER.footerTabBarHeight,
    justifyContent: 'center',
    alignItems: 'center'
  },
  tabBarIconContainer:{
    width: (globals.SCREEN_SIZE.width / 4),
    height: globals.INTEGER.footerTabBarHeight-50,
    justifyContent: 'center',
    alignItems: 'center',
    
  },
  tabBarIconContainerScan:{
    marginBottom: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    width: 62,
    height: 62,
    alignItems: 'center',
    justifyContent: 'center',
   
    shadowOpacity: 0.1,
    shadowOffset: { x: 2, y: 0 },
    shadowRadius: 2,
    borderRadius: 30,
    position: 'absolute',
    bottom: 20,
    right: 0,
    top: 5,
    left: 5,
    shadowOpacity: Platform.OS == 'ios' ? 0 :  5.0,

},
scanBtn: {
  position: 'absolute',
  alignSelf: 'center',
  backgroundColor: '#F0F1F0',
  width: 72,
  height: 72,
  borderRadius: 35,
  bottom: 20,
  zIndex: 10
},
actionBtn: {
    textShadowOffset: { width: 20, height: 5 },
},
  badgeCountContainer:{
    position: 'absolute',
    top: (globals.INTEGER.footerTabBarHeight/2)-15,
    left: ((globals.SCREEN_SIZE.width / 4)/2),
    width: 18,
    height: 18,
    backgroundColor: 'red',
    borderRadius: 9,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 2
  },
  bottomimage:{
    height:50,
    width:50
  },
  bottomimagescan:{
    height:70,
    width:70
  },
  badgeCount:{
    color:globals.COLOR.white,
    fontFamily: globals.FONTS.helveticaNeueLight,
    textAlign: 'center',
    fontSize: globals.SCREEN_SIZE.width * 0.030
  },
  tabLabel:{
    top:0,
    color:'black',
    fontSize:10
  },
  tabBarItem: {
    width: (globals.SCREEN_SIZE.width / 4),
    height: globals.INTEGER.footerTabBarHeight,
    justifyContent: 'center',
    alignItems: 'center'
  },
  tabBarIconContainer:{
    width: (globals.SCREEN_SIZE.width / 4),
    height: globals.INTEGER.footerTabBarHeight-50,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop:'5%',
    marginBottom:'5%'
  },
  badgeCountContainer:{
    position: 'absolute',
    top: (globals.INTEGER.footerTabBarHeight/2)-15,
    left: ((globals.SCREEN_SIZE.width / 4)/2),
    width: 18,
    height: 18,
    backgroundColor: 'red',
    borderRadius: 9,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 2
  },
  badgeCount:{
    color:globals.COLOR.white,
    fontFamily: globals.FONTS.helveticaNeueLight,
    textAlign: 'center',
    fontSize: globals.SCREEN_SIZE.width * 0.030
  },
  tabLabel:{
    top:0,
    color:'black',
    fontSize:10,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  tabIconStyle:{
    width:19,
    height:19,
  },
  barText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.purple,
    fontSize:I18nManager.isRTL ? 8 : 10,
    
  },
  tabTextView:{
    top:20,
  },
});

export { images, styles  };
