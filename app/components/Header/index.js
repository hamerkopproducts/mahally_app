import PropTypes from "prop-types";
import React, { useState } from "react";
import {
  StatusBar,
  View,
  Text,
  Image,
  TouchableOpacity,
  I18nManager,
  Switch,
} from "react-native";

import RNRestart from "react-native-restart";
import globals from "../../lib/globals";
import { images, styles } from "./styles";
import appTexts from "../../lib/appTexts";
import DividerLine from "../DividerLine";

const Header = (props) => {
  const {
    isBackButtonRequired,
    isRightButtonRequired,
    isNotification,
    isLanguageButtonRequired,

    headerTitle,
    onRightButtonPress,
    customHeaderStyle,
    onBackButtonClick,
    iscenterLogoRequired,
    isleftlogoRequired,
    heading,
is_ready,
is_kitchen,
    isOrderDetailPage,
    orderId,
    created_at,
    restaurantData,
    isDineIn,
    tableId,
    cooking_time,
delivery_status,
    logo,
    screen,
    languageSwitchScreen,
    notification_badge,
    istoggle,
    changeNotificationSettings,
  } = props;

  let name = "";
  let r_data = restaurantData;
  if (typeof r_data == "undefined") {
    r_data = {};
  }
  try {
    for (inc = 0; inc < r_data.lang.length; inc++) {
      name = r_data.lang[inc].name;
    }
  } catch (err) {}

  let headerStyle = styles.headerContainer;
  if (customHeaderStyle) {
    headerStyle = customHeaderStyle;
  }

  const switchlang = async () => {
    if (I18nManager.isRTL) {
      I18nManager.forceRTL(false);
    } else {
      I18nManager.forceRTL(true);
    }
    try {
      // home orders offer profile scan
      languageSwitchScreen(screen);
    } catch (err) {
      
    }
    setTimeout(() => {
      RNRestart.Restart();
    }, 500);
  };

  return (
    <View style={[headerStyle, { flexDirection: "row" }]}>
      <StatusBar
        backgroundColor={globals.COLOR.headerColor}
        barStyle="dark-content"
      />
      <View style={{ flexDirection: "row" }}>
        {isBackButtonRequired && (
          <TouchableOpacity
            style={styles.leftIconContainer}
            onPress={() => {
              onBackButtonClick();
            }}
          >
            <Image
              resizeMode="contain"
              source={images.backImage}
              style={{
                alignItems: "center",
                justifyContent: "center",
                width: 20,
                height: 20,
                transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }],
              }}
            />
            {isleftlogoRequired && (
              <View style={styles.headTitle}>
                <Text style={styles.verify}>{heading}</Text>
              </View>
            )}
            {isOrderDetailPage && (
              <View style={styles.leftIconContainer3}>
                <View style={styles.orderNoContainer}>
                  <Text style={styles.noText}>{orderId}</Text>
                </View>
                <View style={styles.dateLine}>
                  <Text style={styles.dateText}>{created_at}</Text>
                </View>
                <View style={styles.div}>
                  <DividerLine />
                </View>
              </View>
            )}
          </TouchableOpacity>
        )}
      </View>
      {isOrderDetailPage ? (
        <View style={styles.orderContainer}>
          <View style={styles.imageLine}>
            <View style={styles.imageView}>
              <Image
                resizeMode={"cover"}
                source={{ uri: logo }}
                style={styles.king}
              />
            </View>
            <View style={styles.colText}>
              <View style={styles.khalied}>
                <Text style={styles.rest}>{name}</Text>
              </View>
              <View style={styles.locView}>
                <View style={styles.locImage}>
                  <Image source={images.gps} style={styles.loc} />
                </View>
                <View style={styles.riyadh}>
                  <Text numberOfLines={1} style={styles.riyadhText}>{r_data.location} </Text>
                </View>
              </View>
              {isDineIn == "dine" && (
                <>
                <View style={styles.tabView}>
                  <View style={styles.ovalView}>
                    <Text style={styles.ovalText}>{appTexts.ORDER.dine}</Text>
                  </View>
                  {tableId != 0 && (
                    <View style={styles.ovalViews}>
                      <Text style={styles.ovalText}>
                        {"TAB-"}
                        {tableId}
                      </Text>
                    </View> 
                  )}
                </View>
                {cooking_time != null ? (
                  <View style={[styles.tabView, {marginTop: 2} ]}>
                    <View style={[styles.ovalView ]}>
                      <Text style={styles.ovalText}>{cooking_time}</Text>
                    </View>
                  </View>
                ):(
                  <View style={is_kitchen ? null :styles.tabView}>
                  <View style={is_kitchen ? null :styles.ovalView }>
                <Text style={styles.ovalText}>{
                  I18nManager.isRTL ?
                    delivery_status?.ar :
                   delivery_status?.en
                }</Text>
                </View>
                </View>)}
                </>
              )}
              {isDineIn != "dine" && (
                <View style={styles.tabView}>
                  <View style={styles.ovalView}>
                    <Text style={styles.ovalText}>{appTexts.ORDER.takeaway}</Text>
                  </View>

                  {cooking_time != null ? (
                <View style={styles.tabView}>
                  <View style={[styles.ovalView ]}>
                    <Text style={styles.ovalText}>{cooking_time}</Text>
                  </View>
                </View>
              ):
              (
                <View style={is_kitchen ? null :styles.tabView}>
                  <View style={is_kitchen ? null :styles.ovalView }>
                <Text style={styles.ovalText}>{
                  I18nManager.isRTL ?
                  delivery_status?.ar :
                  delivery_status?.en
                }</Text>

               </View>
                </View>
              )}

                </View>
              )}
            </View>
          </View>
        </View>
      ) : null}
      {isLanguageButtonRequired ? (
        <TouchableOpacity
          style={styles.leftIconContainer}
          onPress={() => {
            switchlang();
          }}
        >
          <View style={styles.langContainer}>
            <Image
              resizeMode="contain"
              source={
                I18nManager.isRTL
                  ? images.languageFlagImageEN
                  : images.languageFlagImageAR
              }
              style={styles.langIcon}
            />
            <View style={styles.langTextContainer}>
              <Image
                style={styles.langImage}
                source={I18nManager.isRTL ? images.eng : images.Arbic}
              ></Image>
            </View>
          </View>
        </TouchableOpacity>
      ) : null}
      <View style={styles.headerTitleContainer}>
        {iscenterLogoRequired ? (
          <Image
            source={images.logo}
            resizeMode="contain"
            style={styles.headerLogo}
          />
        ) : (
          <Text style={styles.headerTitleText}>{headerTitle}</Text>
        )}
      </View>
      <View style={styles.rightIconContainer}>
        {isRightButtonRequired ? (
          <TouchableOpacity onPress={onRightButtonPress}>
            {notification_badge === "false" && (
              <Image source={images.bellIcon} style={styles.rightIcon} />
            )}
            {notification_badge === "true" && (
              <Image
                source={images.bellIconActive}
                style={styles.rightIconActive}
              />
            )}
          </TouchableOpacity>
        ) : null}
        {isNotification ? (
          <View style={styles.notice}>
            <Switch
              trackColor={{ false: "lightgrey", true: "lightgrey" }}
              thumbColor={istoggle ? "purple" : "white"}
              ios_backgroundColor="white"
              onValueChange={changeNotificationSettings}
              value={istoggle}
              style={{ transform: [{ scaleX: 1.1 }, { scaleY: 1 }] }}
            />
          </View>
        ) : null}
      </View>
    </View>
  );
};

Header.propTypes = {
  isBackButtonRequired: PropTypes.bool,
  isLanguageButtonRequired: PropTypes.bool,
  isRightButtonRequired: PropTypes.bool,
  isNotification: PropTypes.bool,
  isProfileButtonRequired: PropTypes.bool,
  isLogoRequired: PropTypes.bool,
  headerTitle: PropTypes.string,
  isFilterRequired: PropTypes.bool,
  filterTitle: PropTypes.string,
  onRightButtonPress: PropTypes.func,
  onFilterPress: PropTypes.func,
  customHeaderStyle: PropTypes.object,
  onBackButtonClick: PropTypes.func,
  onProfileButtonPress: PropTypes.func,
  iscenterLogoRequired: PropTypes.bool,
  isleftlogoRequired: PropTypes.bool,
  isOrderDetailPage: PropTypes.bool,
  heading: PropTypes.string,
};

export default Header;
