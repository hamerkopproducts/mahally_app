import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals"
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const images = {
  backImage: require("../../assets/images/chooseLanguage/backarow.png"),
  logo: require('../../assets/images/chooseLanguage/Arabic.png'),
  bellIcon: require('../../assets/images/header/bells.png'),
  bellIconActive: require('../../assets/images/bell/Bell.png'),
  profileIcon: require('../../assets/images/header/profile3x.png'),
  eng: require('../../assets/images/header/eng.png'),
  Arbic: require('../../assets/images/header/Arbic.png'),
  king:  require('../../assets/images/HomeCard/Img1.png'),
  gps:require('../../assets/images/HomeCard/location.png'),
};

const styles = StyleSheet.create({
  headerContainer: {
    flex: 1,
    height: globals.INTEGER.headerHeight,
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: globals.COLOR.headerColor,
    marginTop: hp("2%"),
  },
  leftIconContainer: {
    marginTop: hp("2%"),
    marginLeft: 25,
    marginRight: 25,
    justifyContent: "center",
    alignItems: 'center',
    height: "100%",
    flexDirection: 'row'
  },
  leftIconContainer2: {
    marginTop: hp("3%"),
    paddingRight: globals.SCREEN_SIZE.width <= 320 ? globals.MARGIN.marginEight : globals.MARGIN.marginFifteen,
    justifyContent: "center",
    alignItems: 'center',
    alignSelf: 'center',
    width: 200
  },
  leftIconContainer3: {
  //marginTop: 10,
   // marginLeft: 15,
    paddingRight: globals.SCREEN_SIZE.width <= 320 ? globals.MARGIN.marginEight : globals.MARGIN.marginFifteen,
    justifyContent: "center",
    alignItems: 'center',
    alignSelf: "center",
    width: 180
  },
  headTitle: {
    marginHorizontal: 15
  },
  verify: {
    fontSize: 16,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  headerTitleContainer: {
    justifyContent: "center",
    alignItems: "center",
    height: "100%",
    alignSelf: 'center',
    marginTop: hp("2%"),
    // width:'40%',
    
     marginLeft: '20%'

  },
  notice:{
    marginRight:I18nManager.isRTL ? '16%':'25%',
  },
  headerTitleText: {

    textAlign: "center",
    color: globals.COLOR.purple,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    //fontSize: globals.SCREEN_SIZE.width * 0.04,
    fontSize: 20
  },
  headerLogo: {
    height: 20,
    width: 120,
    alignSelf: "flex-start"
  },
  langImage: {
    height: 22,
    width: 25,
    borderRadius: 3
  },
  rightIconContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: "flex-end",
    alignItems: "center",
    
    // position: "absolute",
    height: "100%",
   // right: 0,
    //marginRight: '2%',
    marginTop: hp("2%"),
    //backgroundColor:'green'
  },
  progileIconContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: "center",
    marginTop: hp("2%"),
  },
  rightIcon: {
    marginRight: globals.MARGIN.marginFifteen,
    width: 40,
    height: 40,
    resizeMode: 'cover',
  },
  rightIconActive: {
    marginRight: globals.MARGIN.marginFifteen,
    width: 25,
    height: 25,
    resizeMode: 'cover',
  },
  profileIcon: {
    width: 50,
    height: 50,
    resizeMode: 'cover',

  },
  filterContainer: {
    justifyContent: "center",
    alignItems: "center",
    marginRight: globals.MARGIN.marginTen,
    height: "100%"
  },
  caseFilterDropDownTextStyle: {
    textAlign: "center",
    color: globals.COLOR.white,
    fontFamily: globals.FONTS.avenirLight,
    fontSize: globals.SCREEN_SIZE.width * 0.037
  },
  king:{
    height:85,
    width:85,
  },
  imageView: {
    marginLeft: 25
  },
  imageLine:{
    flexDirection:'row',
  
  },
  orderNoContainer:{
  //  marginLeft:'14%'
    
  },
  div:{
    marginVertical:'2%',
    width:350,
    

  },
  orderContainer:{
    width:350,
    position:'absolute',
    top:100,
  
  },
  noText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize:16,
    color:globals.COLOR.Text,
    textAlign:'left'
  },
  dateLine:{
    //marginBottom:'2%',
    // marginLeft:'14%',

  },
  loc:{
    width:25,
    height:25,
    resizeMode:'contain',
    alignSelf:'center'
  },
  locView:{
    flexDirection:'row',
    marginVertical:'2%',
    alignItems:'center'
  },
  colText:{
    
marginLeft:'2%',

  },
  riyadh:{
    flexDirection:'row',
    alignItems:'center',
    width:240
  },
  tabView:{
    flexDirection:'row',
    marginLeft:'3.2%'
  },
  ovalView:{
    minWidth:'38%',
    height:30,
    borderRadius:5,
    backgroundColor:globals.COLOR.peach,
    alignItems:'center',
    justifyContent:'center',
    paddingHorizontal:"5%"
  },
  ovalViews:{
    width:'38%',
    height:30,
    borderRadius:5,
    backgroundColor:globals.COLOR.purple,
    marginLeft:'4%',
    alignItems:'center',
    justifyContent:'center'
  },
  locImage:{
    alignSelf:'center'
  },
  khalied:{
    marginLeft:'2.5%',
    flexDirection:'row',
    alignItems:'center',
    width:220
  },
  ovalText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize:12,
    color:'white'
  },
  riyadhText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize:12,
    color:globals.COLOR.greyText,
    flexWrap:'wrap',
    flexShrink:1
  },
  dateText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize:12,
    color:globals.COLOR.greyText,
    textAlign:'left'
  },
  rest:{
    textAlign:'left',
    flexWrap:'wrap'
  },
});

export { images, styles };
