import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
  Platform,
} from "react-native";

import { styles } from "./styles";
import NetInfo from "@react-native-community/netinfo";
import { connect } from "react-redux";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import Modal from "react-native-modal";

import * as supportActions from "../../actions/supportActions";
import _ from "lodash";
import { bindActionCreators } from "redux";
import functions from "../../lib/functions";
import Loader from "../../components/Loader";

const HelpModal = (props) => {

  const { isHelpModalVisible, toggleHelpModal } = props;
  const [subject, setSubject] = useState("");
  const [message, setMessage] = useState("");
  const [subjecterrorflag, setsubjecterrorflag] = useState(false);
  const [messageerrorflag, setmessageerrorflag] = useState(false);

  const validatesubject = (flag) => {
    setsubjecterrorflag(flag);
  };

  const validatemessage = (flag) => {
    setmessageerrorflag(flag);
  };

  useEffect(() => handleComponentMounted(), []);
  const handleComponentMounted = () => {
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        props.resetSuccessMessage();
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  };
  
  useEffect(() => handleComponentUpdated());
  const handleComponentUpdated = () => {
    if (props.sendRequestSuccess.success) {
      functions.displayToast(
        "success",
        "top",
        appTexts.ALERT_MESSAGES.success,
        _.get(props, "sendRequestSuccess.msg", "")
      );
      props.resetSuccessMessage();
      toggleHelpModal();
    }
  };

  const onChatPress = () => {
    if (subject.trim() === "") {
      validatesubject(true);
    } else if (message.trim() == "") {
      validatemessage(true);
    } else {
      Keyboard.dismiss();
      NetInfo.fetch().then((state) => {
        if (state.isConnected) {
          let apiParam = {
            subject: subject.trim(),
            message: message.trim(),
          };
          props.supportRequest(apiParam, props.token);
        } else {
          functions.displayToast(
            "error",
            "top",
            appTexts.VALIDATION_MESSAGE.Type,
            appTexts.VALIDATION_MESSAGE.Mobile_error_msg
          );
        }
      });
    }
  };

  return (
    <View style={styles.popUp}>
      <Modal
        isVisible={isHelpModalVisible} 
        style={styles.modalMaincontentHelp} 
        onBackdropPress={() => toggleHelpModal() }
        >
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
          <KeyboardAvoidingView behavior={ Platform.OS == 'ios' ? "padding" : null}>
            <View style={styles.modalmainviewHelp}>
              {props.isloading && <Loader />}
              <View style={styles.helpModalView}>
                <View style={styles.helptextWarpper}>
                  <Text style={styles.helpheadText}>
                    {appTexts.HELPPOPUP.Heading}
                  </Text>
                </View>
                <TouchableOpacity
                  style={styles.buttohelpnwrapper}
                  onPress={() => {
                    toggleHelpModal();
                  }}
                >
                  <Text style={styles.xText}>X</Text>
                </TouchableOpacity>
              </View>

              <View style={styles.formWrappers}>
                <View style={styles.helpsubjectWrapper}>
                  <Text style={styles.helpsubText}>
                    {appTexts.HELPPOPUP.Subject}
                  </Text>
                  <View style={subjecterrorflag ? styles.boxerror : styles.box}>
                    <TextInput
                      style={[styles.hinput]}
                      value={subject}
                      autoCapitalize="none"
                      placeholderTextColor="grey"
                      onChangeText={(val) => {
                        setSubject(val);
                        validatesubject(false);
                      }}
                    />
                  </View>
                </View>

                <View style={styles.helpmessageWrapper}>
                  <Text style={styles.helpsubText}>
                    {appTexts.HELPPOPUP.Message}
                  </Text>
                  <View
                    style={messageerrorflag ? styles.boxeserror : styles.boxes}
                  >
                    <TextInput
                      style={[styles.hinput, { height: 80 }]}
                      autoCapitalize="none"
                      textAlignVertical={"top"}
                      multiline={true}
                      value={message}
                      onChangeText={(val) => {
                        validatemessage(false);
                        setMessage(val);
                      }}
                    ></TextInput>
                  </View>
                </View>
              </View>

              <TouchableOpacity
                style={styles.helpbuttononview}
                onPress={() => {
                  onChatPress();
                }}
              >
                <View style={styles.helpbuttoninsidewrapper}>
                  <Text style={styles.helpyellowbuttonText}>
                    {appTexts.HELPPOPUP.Send}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </KeyboardAvoidingView>
        </TouchableWithoutFeedback>
      </Modal>
    </View>
  );
};

const mapStateToProps = (state, props) => {
  return {
    token: _.get(state, "loginReducer.userData.data.access_token", ""),
    isloading: _.get(state, "supportChatReducer.isLoading", ""),
    sendRequestSuccess: _.get(
      state,
      "supportChatReducer.supportchatsendsuccessmsg",
      ""
    ),
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      resetSuccessMessage: supportActions.resetSuccess,
      supportRequest: supportActions.supportRequestSend,
    },
    dispatch
  );
};

const HelpModalWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(HelpModal);

HelpModalWithRedux.navigationOptions = ({ navigation }) => ({
  header: null,
});

export default HelpModalWithRedux;
