import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const images = {

  close: require("../../assets/images/profileicon/Close.png"),

};
const styles = StyleSheet.create({
  helpModalView: {
    flexDirection: "row",
    //justifyContent: "center",
    justifyContent: "space-around",
    alignItems: "center",
    paddingBottom: "3%",
    paddingLeft: "20%",
  },
  popUp: {
    flex: 1,
  },
  modalMaincontentHelp: {
    justifyContent: 'flex-end',
    margin: 0,
  },

  modalmainviewHelp: {
    backgroundColor: "white",
    margin: 0,
    bottom: 0,
    padding: "4%",
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  xText: {
    fontSize: 16,
    color: globals.COLOR.greyText,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },

  buttohelpnwrapper: {
    flexDirection: "row",
    paddingLeft: '12%'
  },
  helptextWarpper: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: hp('2%'),
    paddingBottom: hp('1.5%'),
  },
  helpheadText: {
    fontSize: hp('2.6%'),
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color: globals.COLOR.Text,
  },
  buttohelpnwrapper: {
    flexDirection: "row",
    paddingLeft: '12%'
  },
  formWrappers: {
    backgroundColor: 'white',
  },
  helpsubjectWrapper: {
    paddingTop: hp('2%'),
    // paddingBottom:'5%'
  },
  helpsubText: {
    fontSize: hp('2.1%'),
    color: globals.COLOR.Text,
    paddingLeft: '1%',
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    textAlign: 'left'
  },
  box: {
    borderWidth: 0.5,
    borderColor: globals.COLOR.greyText,
    height: 50,
    width: '95%',
    borderRadius: 10,
    marginTop: 10,
    marginBottom: 10,
  },
  boxerror: {
    borderWidth: 1.5,
    borderColor: 'red',
    height: 50,
    width: '95%',
    borderRadius: 10,
    marginTop: '2%',
    marginBottom:'3%',
  },
  boxes: {
    marginTop: 20,
    borderWidth: 0.5,
    borderColor: globals.COLOR.greyText,
    height: 80,
    width: '95%',
    borderRadius: 10,
    marginTop: 10
  },
  boxeserror: {
    marginTop: 20,
    borderWidth: 0.5,
    borderColor: 'red',
    borderWidth: 1.5,
    height: 80,
    width: '95%',
    borderRadius: 10,
    marginTop: 10
  },
  hinput: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 15,
    textAlign: I18nManager.isRTL ? "right" : "left",
    color: globals.COLOR.greyText,
    paddingLeft:'3%',
    width:'100%',
    height: 50,
  },
  helpmessageWrapper: {
    marginTop: 10,
  },
  helpbuttoninsidewrapper: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop:'5%',
    
    width: 240,
    height: 50,
    borderRadius: 15,
    alignItems: "center",
    
  
    backgroundColor: globals.COLOR.purple,
  },
  helpbuttononview: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: hp('2.5%'),
  },
  hylpyButtonon: {
    width: 140,
    height: 50,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center",

  },
  helpyellowbuttonText: {
    fontSize: hp('2.1%'),
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color: 'white',
    textAlign: "center"
  },
});

export { styles, images };
