import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, TouchableOpacity,FlatList, Image, Text, I18nManager } from "react-native";
import { images, styles } from "./styles";
import appTexts from "../../lib/appTexts";
import DividerLine from "../DividerLine";
import Rating from "../../components/Rating/Rating";
import _ from 'lodash'
const HomeCard = (props) => {

  const { homeCardButtonPress, data, mapFlag } = props;

  const photo = data.cover_photo;
  const distance = data.distance;

  const preorder= data.pre_order;

  let name = data.name;
  const lang = I18nManager.isRTL ? 'ar' : 'en';
  try {
    name = data.lang[lang].name;
  } catch(err) {}
  
  const id = data.restuarants_id;
  let rating_count = 0;
  let rating_value = 0;
  try {
    rating_count = data.rating.count;
    rating_value = data.rating.value;
  } catch(err) {}
  const _location = data.location;
  const renderRestType = ({ item, index }) => (
		<View>
      <Text style={styles.countryText}>
        { I18nManager.isRTL ?
            _.get(item, ['lang', 1 , 'name'], ''):
            _.get(item, ['lang', 0 , 'name'], '')
            } {' '}
      </Text>
    </View>
	);

  return (
    <TouchableOpacity
      onPress={() => {
        homeCardButtonPress(data.id, data.latitude, data.longitude, data.pre_order);
      }}
    >
      
      <View style={ mapFlag ? styles.mapContainer : styles.container}>
        <View style={styles.cardSection}>
          <View style={styles.burgerView}>
            {photo === null && <Image style={styles.burger} source={{}}></Image> }
            {photo !== null && <Image key={'pic' + data?.id} style={styles.burger} source={{uri: photo}}></Image>}
          </View>

          <View styles={styles.khalied}>
            <View styles={styles.khaliedview}>
              <Text style={styles.khaliedText}>{name}</Text>
            </View>
            <View >
            <FlatList style={styles.flatListStyle}
										data={data.type}
										keyExtractor={(item, index) => index.toString()}
										renderItem={renderRestType}
                     contentContainerStyle={{flexDirection: 'row'}}
                  
									/>
              
            </View>
            <View style={styles.starRating}>
              <View style={styles.starRow}>

                <Rating rating={rating_value}/>
              </View>
              <View style={styles.rating}>
                <Text style={styles.ratingText}>{rating_count}</Text>
              </View>
            </View>
            <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between', minWidth: 180}}>
            <View>
              {data.time_status === 'CLOSED' && <Image style={styles.openImage} source={images.close} />}
              {data.time_status === 'OPEN' && <Image style={styles.openImage} source={images.open} />}
              {(data.time_status !== 'CLOSED' && data.time_status !== 'OPEN') && <Text style={styles.label3}>{ data.time_status } </Text> }
            </View>
            { preorder == 'enable' && (
            <View style={styles.ratingpre}>
              <Text style={styles.ratingTextpre}>{I18nManager.isRTL ? 'الطلب المسبق متاح' : 'Pre-order available'}</Text>
              </View>
            )}
            </View>
          </View>
        </View>
        <View style={styles.div}>
          <DividerLine dividerStyle={[styles.divStyle]} />
        </View>
        <View style={styles.bottom}>
          <View style={styles.location}>
            <Image style={styles.locaImage} source={images.location}></Image>
            <Text numberOfLines={2} style={styles.locaText}>{_location}</Text>
          </View>

          <View style={mapFlag ? styles.knVie: styles.distance}>
            
            <Image style={styles.locaImage} source={images.distance}></Image>
                     
            <Text style={mapFlag ? styles.dsText : styles.disText}>{distance} KM</Text>
        </View>
      
        </View>
      </View>
    </TouchableOpacity>
  );
};

HomeCard.propTypes = {
  homeCardButtonPress: PropTypes.func,
};

export default HomeCard;
