import { StyleSheet,I18nManager, Platform } from "react-native";
import globals from "../../lib/globals";
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
  } from "react-native-responsive-screen";

const images = {
    
    burger:require("../../assets/images/HomeCard/Img1.png"),
    open:require("../../assets/images/HomeCard/open.png"),
    close:require("../../assets/images/HomeCard/Closed.png"),
    star:require("../../assets/images/HomeCard/Star1.png"),
    halfstar:require("../../assets/images/HomeCard/star3.png"),
    distance:require("../../assets/images/HomeCard/Distance.png"),
    location:require("../../assets/images/HomeCard/location.png"),
    
   
};
const styles = StyleSheet.create({
    container:{
       width: Platform.OS === 'ios' ? '97%' : '97%',
        backgroundColor:'#ffffff',
        borderRadius:20,
        flexDirection:'column',
        alignSelf:'center',
       // bottom:hp('20%'),
        marginBottom:'5%',
        marginHorizontal:wp('1%'),  
    },
    mapContainer:{
        width: Platform.OS === 'ios' ? '93%' : '93%',
        backgroundColor:'#ffffff',
        borderRadius:20,
        flexDirection:'column',
        alignSelf:'center',
        // bottom:hp('30%'), // Commented to make card on map clickable.
        //marginBottom:'5%',
       // marginHorizontal:wp('1%'),  
        marginLeft:20,
        marginRight:20,
        //position:'absolute'
    },

    burger :{
        height:81,
        width:136,
        resizeMode:'contain',
    },
    cardSection:{
        marginVertical:hp('0.8%'),
        flexDirection:'row',
        //justifyContent:"space-between",
        alignItems:"center",
       // top: 5,
        // padding:8,
      //  width:wp("58%"),
             
    },
    burgerView:{
        marginTop:hp('0.2%')
    //    right:10,
         //marginHorizontal:wp('2%'),
       // width:"75%",
           },
    khalied:{
       // flexDirection: 'row',
       // marginHorizontal:wp('2%')
    },
    khaliedview:{
       // width:wp('40%'),
      
        },
 
    starRating:{
        flexDirection:'row',
        marginTop: 5
       // justifyContent:'space-between',
      //  width:'80%'
    },
    starImage:{
        height:20,
        width:20,
        resizeMode:'contain',
        // tintColor:'#FFD700'
    },
    rating:{
       // alignSelf:'center',
        backgroundColor:globals.COLOR.purple,
        width:25,
        height:15,
        borderRadius:20,
        alignSelf:"center",
        justifyContent:'center',
        marginLeft:hp("2%")
        // padding:6,
    },
    ratingpre:{
        backgroundColor:globals.COLOR.purple,
        width:100,
        height:16,
        borderRadius:20,
        marginLeft:hp("3.5%"),
        marginTop:hp('1%'),
        alignSelf:"center",
        justifyContent:'center',
    },
    khaliedText:{
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
        textAlign:"left",
        width:"90%",
        marginTop:hp('1%')
    },
    starRow:{
        flexDirection:'row',
    },
    flatListStyle: {
        width: '100%',
        //backgroundColor:'green',
      ///  flex:1,
        flexDirection:'row',
        marginTop: Platform.OS === 'ios' ? 5 : 0

      },
    border:{
        borderWidth:1,
        width:"90%",
      //  marginLeft:18,
      //  marginVertical:hp('0.8%'),
        borderColor:globals.COLOR.borderColor
    },
    bottom:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginBottom:hp('1%'),
       // flexWrap:'wrap',
        minWidth:'90%'
      //  paddingBottom:10,
        //paddingRight:10
    },
    location:{
        flexDirection:'row',
        //justifyContent:'space-between',
       minWidth:'50%',
        alignItems:'center',
    
         marginLeft:'4%',
         //marginRight:'4%'
    },
    locaImage:{
        resizeMode:'contain',
        height:30,
        width:30,
        alignSelf:'center'
    },
    locaText:{
        textAlign:'left',
        //marginTop:3,
        fontFamily:globals.FONTS.helvetica,
        fontSize:12,
        flex: 1,
        //lineHeight:17,
        flexWrap:'wrap',
        
    },
    disText:{
        textAlign:'right',
        flexWrap:'wrap',
        overflow:"hidden",
       fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
        fontSize:I18nManager.isRTL ? 10 : 12,
    },
    dsText:{
        textAlign:'right',
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
         fontSize:I18nManager.isRTL ?10: 12,
        
    },
    distance:{
        flexDirection:'row',
        justifyContent:'flex-end',
        marginRight:24,
        flexWrap:'wrap',
        alignItems:'center',
       flex:1,
       
    },
    knVie:{
       flex:1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent:'flex-end',
    flexWrap:'wrap',
    marginRight:22
    },
    kView:{
        //width:'90%',
        flexWrap:'wrap',
        
alignSelf:'center',
justifyContent:'flex-end',
flexDirection:'row',

    },
    openImage:{
        resizeMode:'contain',
        height:30,
        width:30
    },
    countryText:{
        flexDirection:'row',
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
        fontSize:9,
        textAlign:'left',
    },
    ratingText:{
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
        fontSize:8,
        textAlign:'center',
        color:globals.COLOR.white,
    },
    ratingTextpre:{
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helveticaBold,
        fontSize:9,
        textAlign:'center',
        color:globals.COLOR.white,
    },
    restTpe:{
        flexDirection: 'row',
    },
    div:{
    marginBottom:'2%',
     alignItems:'center',
     justifyContent:'center',
     //width:'98%'   
    },
    divStyle:{
      //  marginLeft:hp('2%'),
      top: 5,
        width:'87%',
        height: 0.5,
       backgroundColor: globals.COLOR.borderColor,
       //marginLeft:'6%',  
      },

      label3: {

        textAlign: 'left',
        fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
        fontSize: 11,
        color: 'orange',
       // flexShrink:1

    },
    });

    export { styles,images  };