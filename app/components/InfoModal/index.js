import LinearGradient from "react-native-linear-gradient";
import React from 'react';
import { images, styles } from "./styles";
import { View, Text, TouchableOpacity, I18nManager, Image, TextInput, FlatList } from 'react-native';
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import PropTypes from 'prop-types';
import RoundButton from "../RoundButton";
import RestaurantList from '../../components/RestaurantList';
import Modal from 'react-native-modal';
import _ from "lodash"
import FastImageLoader from '../../components/FastImage/FastImage';

const InfoModal = (props) => {

    const {
        isInfoModalVisible,
        toggleInfoModal,
        restTermsDetails,
        restFacilitiesDetails,
        restDescription
    } = props;

    const renderTerms = ({ item, index }) =>
        <View style={styles.noSmo}>
             {item.item_image ? <FastImageLoader
                    resizeMode={'cover'}
                    photoURL={item.item_image}
                    style={styles.cigerImage}
                    loaderStyle={{}}
                  />: null}
            {/* <Image style={styles.cigerImage} source={{ uri: item.item_image }} ></Image> */}
            <View style={styles.contentW}>
                <Text style={styles.desText}>{item.item_name}</Text>
            </View>
        </View>;
    const renderFacilities = ({ item, index }) =>
        <View style={styles.noSmo}>
             {item.cover_photo ? <FastImageLoader
                    resizeMode={'cover'}
                    photoURL={item.cover_photo}
                    style={styles.cigerImage}
                    loaderStyle={{}}
                  />: null}
            {/* <Image style={styles.cigerImage} source={{ uri: item.cover_photo }} ></Image> */}
            <View style={styles.contentW}>
                <Text style={styles.desText}>{item.item_name}</Text>
            </View>
        </View>;
    return (
        <Modal
            isVisible={isInfoModalVisible}
            style={styles.modalMaincontentHelp}
            animationIn="slideInUp"
            animationOut="slideOutRight"
            onSwipeComplete={toggleInfoModal}
            swipeDirection={["left", "right", "up", "down"]}
        >
            <View style={styles.modalmainviewHelp}>
                <View style={styles.border}></View>
                <TouchableOpacity onPress={() => { toggleInfoModal(); }}>
                    <View style={styles.clo}>
                        <Text style={styles.x}>X</Text>
                    </View>
                </TouchableOpacity>
                <View style={{marginHorizontal:'3%'}}>
                <View style={styles.headingLine}>
                    <View style={styles.helptextWarpper}>
                        <Text style={styles.helpheadText}>{appTexts.STATUS.description}</Text>
                    </View>
                </View>
                <View style={styles.listV}>
                    <Text style={styles.desText1}>{I18nManager.isRTL ?
                        _.get(restDescription, 'ar.description', '') :
                        _.get(restDescription, 'en.description', '')} </Text>
                </View>
                <View>
                    <View style={styles.helptextWarpper}>
                        <Text style={styles.helpheadText}>{appTexts.STATUS.terms}</Text>

                    </View>
                    <FlatList style={styles.flatListStyle}
                        contentContainerStyle={{ paddingBottom: "5%" }}
                        data={_.isEmpty(restTermsDetails) ? [] : restTermsDetails}
                        extraData={_.isEmpty(restTermsDetails) ? [] : restTermsDetails}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={renderTerms}>

                    </FlatList>
                </View>
                <View>
                    <View style={styles.helptextWarpper}>
                        <Text style={styles.helpheadText}>{appTexts.STATUS.facilities}</Text>
                    </View>
                    <FlatList style={styles.flatListStyle}
                        contentContainerStyle={{ paddingBottom: "5%" }}
                        data={_.isEmpty(restFacilitiesDetails) ? [] : restFacilitiesDetails}
                        extraData={_.isEmpty(restFacilitiesDetails) ? [] : restFacilitiesDetails}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={renderFacilities}>

                    </FlatList>
                </View>
</View>

            </View>
        </Modal>
    );
};

InfoModal.propTypes = {
    toggleInfoModal: PropTypes.func,
    isInfoModalVisible: PropTypes.bool
};

export default InfoModal;