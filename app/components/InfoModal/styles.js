import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const images = {
    
    ciger:require("../../assets/images/Icons/Terms-1.png"),
    panda:require("../../assets/images/Icons/terms.png"),
    
    
    
   
};

const styles = StyleSheet.create({
modalMaincontentHelp: {
    //justifyContent: "center",
    justifyContent:'flex-end',
    margin: 0,
  },
  cigerImage:{
      width:40,
      height:40,
      resizeMode:"contain"
  },
  modalmainviewHelp: {
    backgroundColor: "white",
    //width: wp("90%"),
    padding: "4%",
    //borderRadius: 10,
    borderTopRightRadius:15,
    borderTopLeftRadius:15,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  noSmo:{
    flexDirection:'row',
    alignItems:"center",
    justifyContent:'space-between',
    width:"100%",
    marginVertical:"2%"
  },
  contentW:{
width:"85%"
  },
  border:{
      borderWidth:2,
      borderColor:'lightgrey',
      width:'50%',
      alignItems:'center',
      justifyContent:'center',
      marginLeft:'25%',
      marginRight:'20%',
      marginBottom:'3%',

  },
  clo:{
      justifyContent:'flex-end',
      alignItems:'flex-end',
      paddingRight:'2%'
  },
  desText:{
    fontSize: 13,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    textAlign:'left',
    color:globals.COLOR.Text,
  },
  desText1:{
    fontSize: 13,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    textAlign:'left',
    color:globals.COLOR.greyText,
    lineHeight:22
  },
  headingLine:{
      flexDirection:'row',
      marginBottom:'5%'
  },
  x:{
      color:globals.COLOR.greyText,
      fontSize:18,
  },
  helptextWarpper:{
    flexDirection:'row',
    //justifyContent:'center',
    paddingTop:hp('1%'),
    // paddingBottom:hp('1.5%'),
  width:'50%'
    },
    applytext:{
        paddingTop:hp('2%'),
    paddingBottom:hp('1.5%'),
    //paddingLeft:'10%',
    width:'25%'
    
    },
    resettext:{
        paddingTop:hp('2%'),
    paddingBottom:hp('1.5%'),
    //paddingLeft:'5%',
    width:'25%'
    
    },
    helpheadText:{
      fontSize: 15,
      fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.helveticaBold,
      textAlign:'left',
      color:globals.COLOR.Text,
    },
    applyTexts:{
      fontSize: 15,
      fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
      textAlign:'center',
      color:globals.COLOR.purple,
    },
    resetTexts:{
      fontSize: 15,
      fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
      textAlign:'center',
      color:globals.COLOR.greyText,
    },

    // boxVies:{
    //     height:50,
    //     width:'95%',
    //     borderWidth:0.5,
    //     borderColor:'#707070',
    //     flexDirection:'row',
    //     justifyContent:'space-between',
    //           marginTop:'8%',
    //     borderRadius:5,
        
    //   marginLeft:'3%'
    //      },
    //      hinput: {
    //         //marginTop:-4
    //         // margin: 15,
    //         // height: 40,
    //         // borderColor: '#7a42f4',
    //         // borderWidth: 1
    //         fontFamily: globals.FONTS.openSansLight,
    //         fontSize: hp('2%'),
    //         textAlign: I18nManager.isRTL ? "right" : "left",
    //         color:"#707070",
    //         marginLeft:'2%'
            
    //       },
    //       arrowv:{
    //         marginRight:'4%',
    //         alignItems:'center',
    //         justifyContent:'center'
    //           },
    //           search:{
    //             width:20,
    //             height:20,
    //             alignSelf:'center'
    //           },
              listV:{
                marginTop:"1%",
                marginBottom:'5%'
                },
            });

            export { images, styles };