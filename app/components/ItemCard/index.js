import React, { useState } from "react";
import PropTypes from "prop-types";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  I18nManager,
  FlatList,
} from "react-native";
import { images, styles } from "./styles";
import appTexts from "../../lib/appTexts";
import DividerLine from "../DividerLine";

const ItemCard = (props) => {
  const {
    isCheckout,
    isCheckoutAddon,
    onItemCardPress,
    isAddonListVisible,
    isCart,
    onCustomiseClick,
    item,
    addons,
    updateCart,
    isCheck,
    isOrderDetails
  } = props;

  const item_count = typeof item.item.item_count != 'undefined' ? item.item.item_count : item.item.count;
  const [visibleAddon, setVisibleAddon] = useState(false);

  let all_addons = Object.assign({}, addons);
  let all_addons_data = [];
  if(typeof item.item.addon != 'undefined') {
    all_addons_data = item.item.addon;
  } else {
    for (let inc = 0; inc < Object.keys(all_addons).length; inc++) {
      let _items = all_addons[Object.keys(all_addons)[inc]];
      all_addons_data.push(_items);
    }
  }

  let has_addons = false;
  try {
    has_addons = item.item.item_addons.length > 0;
  } catch(err) {
    try {
      has_addons = item.item.addon.length > 0;
    } catch(err) {
      has_addons = false;
    }
  }

  let cover_photo = "";
  try {
    if(typeof item.item.menu_items != 'undefined') {
      cover_photo = item.item.menu_items.cover_photo;
    } else {
      cover_photo = item.item.cover_photo;
    }
  } catch (err) {
    cover_photo = "";
  }

  const lang = I18nManager.isRTL ? "ar" : "en";

  let name = "";
  let proname = "";
  
  try {
    const lang_name = typeof item.item.menu_items != 'undefined' ? item.item.menu_items : item.item;
    for (var inc = 0; inc < lang_name.lang.length; inc++) {
      if (lang_name.lang[inc].language == lang) {
        name = lang_name.lang[inc].name;
      }
    }
  } catch (err) {
    name = "";
  }

  if((typeof item.item.is_offer != 'undefined' && item.item.is_offer == true) || 
  (typeof item.item.is_loyalty != 'undefined' && item.item.is_loyalty == true)
  ) {
    name = item.item.item_name;
  }

  if((typeof item.item.is_offer != 'undefined' && item.item.is_offer == true) || 
  (typeof item.item.is_loyalty != 'undefined' && item.item.is_loyalty == true)
  ) {
    proname = item.item.pro_name;
  }


  let actual_price = 0;
  try {
    actual_price = typeof item.item.menu_items != 'undefined' ? item.item.menu_items.price : item.item.discount_price;
  } catch (err) {
    actual_price = 0;
  }

  return (
    <View style={{flex:1}} >
      <View style={ isCart ? styles.itemCart : styles.itemCard }>
      
        <View style={styles.cardImage}>
         
          <View style={{ width: '20%' }}>
            <Image
              source={{
                uri: cover_photo,
              }}
              style={styles.foodImage}
            />
          </View>

          <View style={[styles.colText ]}>
            <View style={styles.lebaneseView}>
              <Text style={styles.lebaneseText}>{name}</Text>
              {has_addons?(
                <View style={styles.plusad}>
                  <Text style={styles.addonesText}>{"+"}</Text>
                  <Text style={styles.addonesText}>{appTexts.STRING.adones}</Text>
                </View>
              ):
              (
                <View style={styles.amt}>
                <Text style={styles.amtTextab}>{proname}</Text>
              </View>
              )
              }
            </View>
            {isCheckout ? (
              <View style={styles.amt}>
                <Text style={styles.amtText}>{appTexts.STRING.sar} {actual_price}</Text>
              </View>
            ) : (
                <View style={styles.countView}>
                  <Text style={styles.countText}> x {item_count}</Text>
                </View>
              )}
          </View>

          <View style={isOrderDetails ? styles.style:null}>
            {isCheckout ? (
              <View style={isCheck ? styles.ovalCheck : styles.ovalView}>
                <TouchableOpacity style={{ width: 50, alignItems: 'center'}} onPress={() => {
                  updateCart(item.item.id, 1)
                }}>
                  <View style={styles.plus}>
                    <Text style={styles.plusText}>+</Text>
                  </View>
                </TouchableOpacity>
                <View style={styles.oval}>
                  <Text style={styles.one}>{ item_count }</Text>
                </View>
                <TouchableOpacity style={{ width: 50, alignItems: 'center'}} onPress={() => {
                  updateCart(item.item.id, -1)
                }}>
                <View style={styles.minus}>
                  <Text style={styles.plusText}>-</Text>
                </View>
                </TouchableOpacity>
              </View>
            ) : (
              <View style={isOrderDetails ? styles.amtsar : styles.amount}>
                <Text  style={styles.amt}>{appTexts.STRING.sar} {parseFloat(item.item.item_price).toFixed(2)}</Text>
              </View>
            )}
          </View>
        </View>
        {has_addons &&
        <View style={styles.addonViewCheckout}>
          <View style={styles.divide}>
            <DividerLine />
          </View>
          <View style={styles.rowOne}>
            <View style={styles.addonHeading}>
              <Text style={styles.addText}>{appTexts.ORDER.addon}</Text>
            </View>
            {isCheckout ? (
            <TouchableOpacity
              onPress={() => {
                onCustomiseClick();
              }}
            >
              <View style={styles.rightView}>
                <View style={styles.penView}>
                  <Image source={images.pen} style={styles.penImage} />
                  <View style={styles.customText}>
                    <Text style={styles.customise}>
                      {appTexts.CHECKOUT.custom}
                    </Text>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
            ):null}
          </View>

          <FlatList
            data={all_addons_data}
            extraData={all_addons_data}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => {
              let addon_name = "";
              try {
                const resturant_addons = typeof item.resturant_addons != 'undefined' ? item.resturant_addons : item;
                for (var inc = 0; inc < resturant_addons.lang.length; inc++) {
                  if (resturant_addons.lang[inc].language == lang) {
                    addon_name = resturant_addons.lang[inc].name;
                  }
                }
              } catch (err) {
                addon_name = "";
              }
              let _count = 0;
              if(typeof item.item_count != 'undefined') {
                _count = item.item_count;
              } else {
                _count = item.count;
              }
              let _price = 0;
              if(typeof item.item_price != 'undefined') {
                _price = item.item_price;
              } else {
                _price = item.price;
              }
              let image_path = item.image_path;
              if(typeof item.resturant_addons != 'undefined') {
                image_path = item.resturant_addons.image_path;
              }
              return (
                <View style={styles.cardImageAdd}>
                  <Image
                    source={{
                      uri: image_path,
                    }}
                    style={styles.foodImages}
                  />
                  <View style={isOrderDetails ? styles.colTexts : styles.colText}>
                    <View style={styles.lebaneseView}>
                      <Text style={styles.lebaneseText}>{addon_name}</Text>
                    </View>
                    {isOrderDetails ? <View style={styles.countView}>
                      <Text style={styles.countText}>X { _count }</Text>
                       <Text style={styles.Text}>{appTexts.STRING.sar} { _price }</Text> 
                    </View> :
                    isCart ? <View style={styles.countView}>
                    <Text style={styles.countText}>{appTexts.STRING.sar}  { _price }</Text></View> :
                    <View style={styles.countView}>
                      <Text style={styles.countText}>{appTexts.STRING.sar}  { _price } x { _count }</Text>
                    </View>}
                  </View>
                </View>
              );
            }}
          />
        </View>
        }
      </View>
    </View>
  );
};

ItemCard.propTypes = {
  isCheckout: PropTypes.bool,
  isCheckoutAddon: PropTypes.bool,
  isAddonListVisible: PropTypes.bool,
  isCart: PropTypes.bool,
};

export default ItemCard;
