import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals"

const images = {
  food:require('../../assets/images/ItemCard/img.png'),
  addOn:require('../../assets/images/ItemCard/img3.png'),
  pen:require('../../assets/images/ItemCard/edit.png')
};

const styles = StyleSheet.create({
  itemCard:{
      marginHorizontal:'3%',
     // marginRight:'8%',
     marginTop:'3%',
      width:'100%',
      //padding:'5%',
    // paddingRight: '25%',
  },
  itemCart:{
    //padding:'5%',
    width:'100%',
    borderWidth:0.2,
    borderRadius:5,
    //marginTop:'5%',
    marginVertical:'3%',
    paddingLeft:'4%',
    paddingTop:'3%'
   // paddingLeft:'5%',
    // paddingRight:'23%',
  },
  foodImage:{
      width:70,
      height:70,
      resizeMode:'contain',
      borderRadius:10,
      marginRight: 20
  },
  cardImage:{
      flexDirection:'row',
      alignItems:'center',
      width: '100%',
      //paddingLeft:'5%'
      //marginTop:'2%'
      
  },
  colText:{
     // marginLeft:'3%',
      // marginHorizontal:'8%',
      width: '50%',
      marginLeft: 16
  },
  colTexts:{
    // marginLeft:'3%',
     marginHorizontal:'10%',
     width: '100%'
 },
  lebaneseText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.Text,
    fontSize:I18nManager.isRTL ?14:14,  
    textAlign:'left', 
    lineHeight: Platform.OS === 'ios' ? 20 : null

  },
  countText:{
      fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.helveticaBold,
      color:globals.COLOR.greyText,
      fontSize:12,
      textAlign:'left',
   //   flexDirection:I18nManager.isRTL ? "row":"row-reverse",
     // marginLeft:'15%',
  },
  amount:{
     marginTop:"5%",
    
  },
  style:{
    flex:1,
    right:50,
  },
  amtsar:{
   flex:1,
    flexDirection: 'row',
    bottom:10,
    //top:10,
    justifyContent:'flex-end',
    alignItems:'flex-end',
   // backgroundColor:'green',

   // width:200,
   width:I18nManager.isRTL ?200:null
    
  },
  Text:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.Text,
    fontSize:13,
    //textAlign:'right'
   // textAlign: I18nManager.isRTL ? "right" : "left",
  },
  amt:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.Text,
    fontSize:10,
    flexWrap:'wrap',
    //textAlign:'left'
   // textAlign: I18nManager.isRTL ? "right" : "left",
  },
  addonView:{
    marginLeft:'15%',
    marginTop:'5%',
    
  },
  countView:{
  flexDirection: "row",
    alignItems:"center",
    marginRight:'30%',
    justifyContent:'space-between',
    flex:1
  },
  foodImages:{
     width:35,
     height:35,
   // resizeMode:'contain',
    borderRadius:20,
},
addonHeading:{
  marginBottom:'2%'
},
addText:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  color:globals.COLOR.greyText,
  fontSize:13,
  textAlign:'left'

},
lebaneseView:{
  width:'100%'
},
cardImageAdd:{
  flexDirection:'row',
  alignItems:'center',
  marginTop:'2%',
  marginBottom:'3%'
  
},
divide:{
  marginVertical:'2%',
  width:'170%',
  
  
},
amt:{
//  paddingTop:'5%',
//top:20,
  fontSize:I18nManager.isRTL ? 10:12,
  flex:1,
textAlign:I18nManager.isRTL ? "left": "right",
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,

},
amtText:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  color:globals.COLOR.greyText,
  fontSize:12,
 textAlign:'left',
  //textAlign: I18nManager.isRTL ? "right" : "left",
},
amtTextab:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  color:globals.COLOR.Text,
  fontSize:13,
 textAlign:'left',
  //textAlign: I18nManager.isRTL ? "right" : "left",
},
amtNew:{
marginLeft:10
},
oval:{
  width:40,
  height:25,
  backgroundColor:globals.COLOR.purple,
  borderRadius:5,
  alignItems:'center',
  justifyContent:'center'
},
ovalView:{
  marginHorizontal:'4%',
  alignItems:'center'
},
ovalCheck:{
  // marginLeft:'18%',
  // marginRight:'2%',
  alignItems:'center',
},
plus:{
  marginBottom:'10%'
},
one:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  color:'white',
  fontSize:12,
},
plusText:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  color:globals.COLOR.purple,
  fontSize:16,
},
minus:{
  marginTop:'5%'
},
 checkAdd:{
   marginRight:'5%',
   marginTop:'3%'
 },
 addonViewCheckout:{
   backgroundColor:'white',
   width:'90%',
   marginLeft:'5%',
   marginRight:'5%'
 },
 rowOne:{
   flexDirection:'row',
   justifyContent:'space-between',
  // marginLeft:'15%',
   marginTop:'5%',
   //marginRight:'10%'
   //marginLeft:20

 },
 penView:{
   flexDirection:'row',
   alignItems:'center',
  
 },
 penImage:{
   height:18,
   width:18,
 },
 customText:{
   marginLeft:'3%',
   fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,

 },
 customise:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  fontSize:12,
 },
 rightView:{
   //marginLeft:'2%',
 },
 plusad:{
  flexDirection:"row",
  alignItems:"center",
},
addonesText:{
    color:globals.COLOR.purple,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize:11,
    margin:1,
    lineHeight: Platform.OS === 'ios' ? 20 : null
},
}
);

export { images, styles };
