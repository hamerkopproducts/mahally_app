import React, { Component, useState } from "react";
import PropTypes from "prop-types";
import { View, FlatList, I18nManager, TouchableOpacity, Image, Text, StatusBar } from "react-native";
import { images, styles } from "./styles";
import appTexts from "../../lib/appTexts";
import globals from "../../lib/globals";
import LinkButton from "../LinkButton";
import DividerLine from "../DividerLine";
import InfoModal from '../../components/InfoModal';
import DotedLine from '../../components/DotedLine';
import { showLocation } from 'react-native-map-link';
import _ from "lodash"
const KhaliedCard = (props) => {

 
  const { onReviewClick, restaurantDataDetails, restTypesDetails,restDescription, restTermsDetails,restFacilitiesDetails } = props;
  const [isInfoModalVisible, setIsInfoModalVisible] = useState(false);

  const openInfoModal = () => {
    setIsInfoModalVisible(!isInfoModalVisible)
  };

  const renderRestTypes = ({ item, index }) => 
  <Text style={styles.countryText}>{item.rest_type_name} </Text>
  ;

  const t_status = restaurantDataDetails.time_status;
  const opn_close_status = restaurantDataDetails.opn_close_status;
  
  return (

    <View style={styles.container}>
      <StatusBar
        backgroundColor='transparent'
        barStyle="dark-content"
        translucent={true}
      />
      <View style={styles.cardSection}>
        <View style={styles.imagemainview}>
          <View style= {{ padding: 5}}>
            <Image style={styles.burger} source={{ uri: restaurantDataDetails.logo }}></Image>
          </View>

          <View styles={styles.toTal}>
            <View style={styles.khalied}>
              <Text style={styles.khaliedText}>
                {
                  I18nManager.isRTL ?
                    _.get(restaurantDataDetails, 'lang.ar.name', '') :
                    _.get(restaurantDataDetails, 'lang.en.name', '')
                }
              </Text>
            </View>
            <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
            <View style={{alignItems: 'flex-start',marginLeft:'1%'}}>
              <FlatList
                horizontal
                data={restTypesDetails}
                extraData={restTypesDetails}
                keyExtractor={(item, index) => index.toString()}
                renderItem={renderRestTypes}
              />

            </View>
            
            { restaurantDataDetails.pre_order == 'enable' && (
            <View style={styles.ratingpre}>
              <Text style={styles.ratingTextpre}>{appTexts.STRING.preorder}</Text>
              </View>
            )}
            
                </View>
            <View style={styles.newRow}>
              <View style={styles.starRating}>
                <View style={styles.starRow}>
                  <Image style={styles.starImage} source={images.star}></Image>
                  <Text style={styles.ratingText}>
                    {restaurantDataDetails.rating}
                  </Text>

                </View>
              
                <TouchableOpacity onPress={() => { onReviewClick(); }}>
                  <View style={styles.reviewView}>
                  <Text style={styles.simple}>(</Text>
                    <Text style={styles.reviewText}>
                      
                      {restaurantDataDetails.rated_count}{appTexts.STRING.review}
                    </Text>
                    <Text style={styles.simple}>)</Text>
                  </View>
                </TouchableOpacity>
              </View>

              <View style={styles.closeView}>
                <View style={[
                  styles.dot,
                  
                   t_status === 'OPEN' && { backgroundColor: '#03A70D'},
                  ( 
                   t_status != 'OPEN' && 
                  t_status != 'CLOSED') && { color: 'orange'}
                ]}></View>
                <Text style={[
                    styles.locaText1 ,
                    
                   t_status === 'OPEN' && { color: 'green'},
                    (
                     t_status != 'OPEN' && 
                    t_status != 'CLOSED') && { color: 'orange'}
                  ]}>{ I18nManager.isRTL ?
                    opn_close_status?.ar :
                   opn_close_status?.en}</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
      <View style={styles.line}>
      <DotedLine />
      </View>
      <View style={styles.bottom}>
        <View style={styles.countryKm}>
          {restaurantDataDetails.longitude &&
          <TouchableOpacity onPress={() => {
            showLocation({
              latitude: restaurantDataDetails.latitude,
              longitude: restaurantDataDetails.longitude,
              googleForceLatLon: false,
              alwaysIncludeGoogle: true,
              cancelText: 'Cancel',
              title: restaurantDataDetails.location,
              appsWhiteList: ['google-maps']
            })
          }}>
          <View style={styles.location}>
            <Image style={styles.locaImage} source={images.location} />
            <View style={styles.cntryText}>
              <Text numberOfLines={2} style={styles.countryText1}>{restaurantDataDetails.location}</Text>
            </View>
          </View>
          </TouchableOpacity>}
          <View style={styles.border1}></View>
          <View style={styles.distance1}>
            <Text style={styles.kmText}>{restaurantDataDetails.distance}{appTexts.STRING.km1}</Text>
          </View>
        </View>


        <View style={styles.distance}>
          <TouchableOpacity style={styles.middleTabView} onPress={() => { openInfoModal(); }}>
            <Text style={styles.locaText}>{appTexts.STRING.more}</Text>
            <Image style={styles.moreImage} source={images.more}></Image>
          </TouchableOpacity>
        </View>
        <InfoModal 
        restTermsDetails={restTermsDetails} 
        restFacilitiesDetails={restFacilitiesDetails} 
        isInfoModalVisible={isInfoModalVisible}
        restDescription={restDescription}
         toggleInfoModal={openInfoModal} />

      </View>
    </View>
  );
};

KhaliedCard.propTypes = {
  onReviewClick: PropTypes.func,
};

export default KhaliedCard;
