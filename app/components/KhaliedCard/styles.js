import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import DotedDivider from "../DotedDivider";

const images = {

    burger: require("../../assets/images/HomeCard/img4.png"),
    open: require("../../assets/images/HomeCard/open.png"),
    star: require("../../assets/images/HomeCard/StarPurple.png"),
    halfstar: require("../../assets/images/HomeCard/star3.png"),
    distance: require("../../assets/images/HomeCard/Distance.png"),
    location: require("../../assets/images/HomeCard/location.png"),
    more: require("../../assets/images/Icons/viewmore.png"),


};
const styles = StyleSheet.create({
    container: {
        // width: "100%",
        // height: globals.SCREEN_SIZE.height <= 600 ? hp("25%") : (globals.SCREEN_SIZE.height <= 700 ? hp("22%") : hp("20%")),
        backgroundColor: '#ffffff',
        flexDirection: 'column',


    },
    burger: {
        height: 70,
        width: 70,
        borderRadius: 35,
        resizeMode: 'contain',

    },
    cardSection: {

                flexDirection: 'row',
                marginTop:'2%',
                

      
    },
    toTal: {
        flexDirection: "column",
        justifyContent:'center'
        
    },
    imagemainview: {
        flexDirection: "row",
        width: "100%",
        alignItems:"center",
        marginHorizontal:'2%',
        paddingLeft:'2%'
    },
    khalied: {
        marginLeft:'1%',
        alignItems: 'flex-start', 
        marginTop:"1%"
       // mflex:1

    },
    newRow: {
        flexDirection: "row",
        marginLeft:'0.5%'
        
    },
    middleTabView: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: "90%"
    },
    khaliedview: {


    },
    locaText: {
        fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
        fontSize: 12,
        color: globals.COLOR.purple
    },
    border1: {
        borderRightWidth: 1,
        height: 10,
        width: "10%",
        marginRight:10,
       // marginHorizontal: "10%",
        borderColor: globals.COLOR.greyText,
    },
    reviewView: {
     
        // width:70,
        marginLeft: 5,
        //justifyContent:'space-between',
flexDirection:'row',
        //backgroundColor:'red'
    },
    starRating: {
        flexDirection: 'row',
        justifyContent:'flex-start',
        //width:"50%",
        alignItems: "center",
        //marginHorizontal: "0%"
        //backgroundColor:'red'
        width: "35%"
    },
    line:{
        marginRight:'5%',
        marginLeft:'5%',
        //marginTop:'2%'
    },
    starImage: {
        height: 20,
        width: 20,
        resizeMode: 'contain',
        // tintColor:'#FFD700'
    },
    simple:{
        transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
    },
    rating: {

           },
    khaliedText: {
        textAlign: 'left',
        fontSize: 14,
        width: "90%",

        fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.helveticaBold,
    },
    starRow: {
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"center",
       
           },
    reviewText: {
        fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
        fontSize: 12,
        textDecorationLine: "underline",
        

        
     
    },
    circle: {
        width: 5,
        height: 5,
        borderRadius: 10,
        backgroundColor: 'orange',
        marginHorizontal: 5

    },
    locT: {
        width: "500%"
    },
    border: {
        borderWidth: 0.2,
        width: "93%",
        marginHorizontal:"5%",
        borderColor: globals.COLOR.borderColor,
        borderStyle: 'dotted',
        borderRadius: 10
    },
    bottom: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: "center",
        marginLeft: 10,
        marginTop: 20,
        // marginVertical: "2%",
        width: "95%",
        bottom:18
        
        
    },
    location: {
        flexDirection: 'row',
        // justifyContent:'space-between',
       // width: hp("15%"),
        alignItems: "center",
       minWidth:'5%'
    },
    kmText: {
        fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
        fontSize: 11,
        color: globals.COLOR.greyText,
        textAlign:"left"
    },
    cntryText: {
        // width: "100%",

    },
    distance1:
    {
        width:hp( "40%"),
     // paddingHorizontal0:
    },
    moreImage: {


        height: 15,
        width: 15,
        resizeMode: 'contain',
        transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
    },
    locaImage: {


        height: 28,
        width: 28,
        resizeMode: 'contain',

    },
    countryText1: {
        textAlign: 'left',

        fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
        fontSize: 11,
        color: globals.COLOR.greyText,
        textDecorationLine: 'underline'
    },
    locaText1: {

        textAlign: 'left',
        fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
        fontSize: 11,
        color: 'orange',
       // flexShrink:1

    },
    ratingpre:{
        backgroundColor:globals.COLOR.purple,
        width:100,
        height:16,
        borderRadius:20,
        marginRight:hp("6%"),
        
        alignSelf:"center",
        justifyContent:'center',
     
    },
    ratingTextpre:{
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helveticaBold,
        fontSize:9,
        textAlign:'center',
        color:globals.COLOR.white,
    },
    rateCloseView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: "96%",
        alignItems: "center",
        right: 3

    },
    distance: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: "16%",

    },
    closeView: {
        flexDirection: 'row',
         width:"49%",
       // flex:1,
        alignItems: 'center',
        justifyContent:'flex-end',
        marginHorizontal:0,
        flexWrap:'wrap',
        //marginRight:2,


    },
    dot:{
        width:8,
        height:8,
        borderRadius:5,
        backgroundColor:'orange',
        marginRight:6,
    },
    countryKm: {
        flexDirection: 'row',
        justifyContent: 'space-between',
       width: "52%",
        alignItems: "center",
        //marginBottom: 10
    },
    minsView: {
        width: "100%",
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    openImage: {
        resizeMode: 'contain',
        height: 25,
        width: 25
    },
    countryText: {
        fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
        fontSize: 11,
        textAlign: 'left',
        lineHeight: 25

    },
    ratingText: {

        fontFamily:globals.FONTS.helvetica,
        fontSize: 12,
        textAlign: 'left',

        // bottom:2,
        color: globals.COLOR.purple,


    }
});

export { styles, images };