import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity,View, Text } from 'react-native';
import { styles } from "./styles";


const LinkButton = (props) => {
  const {
    isUnderLine,
    linkColor,
    buttonText,
    buttonClick
  } = props;

  
  return (
    <TouchableOpacity style={styles.buttonContainer} onPress={() => { buttonClick()}}>
      <Text style={[styles.buttonTextStyle, { color: linkColor }, isUnderLine && { textDecorationLine: 'underline',fontSize:12 } ]}>{buttonText}</Text>
    </TouchableOpacity>
  );
};
LinkButton.propTypes = {
  isUnderLine: PropTypes.bool,
  linkColor: PropTypes.string,
  buttonText: PropTypes.string,
  buttonClick: PropTypes.func,
};

export default LinkButton;