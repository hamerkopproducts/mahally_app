import { StyleSheet } from "react-native";
import globals from "../../lib/globals"
const styles = StyleSheet.create({
  buttonContainer: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonTextStyle:{
    color: globals.COLOR.textColorGreen,
    fontFamily: globals.FONTS.avenirMedium,
    fontSize: 12,
    textAlign: 'center'
  }
});

export { styles  };
