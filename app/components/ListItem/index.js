import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image, Text } from 'react-native';
import { images, styles } from "./styles";
import appTexts from "../../lib/appTexts";
import globals from "../../lib/globals";
import LinkButton from "../LinkButton";
import DividerLine from '../DividerLine';
const ListItem = (props) => {
  const {
    itemData,
    tabIndex,
    itemOnPress,
    listButtonClick,
    checkBoxClick,
    isPendingStatusRequired,
    isBottomButtonRequired
  } = props;
  return (
    <TouchableOpacity style={styles.rowContainer} onPress={() => { itemOnPress(itemData)}}>
      <View style={styles.orderIdContainer}>
        <View style={styles.orderIdLabelContainer}>
          <Text style={styles.boldLabel}>{itemData.orderId}</Text>
        </View>
        {isPendingStatusRequired &&
        <View style={styles.pend}> 
          <Image source={images.call}style={styles.tick}/>
          <Text style={styles.collection}>{appTexts.STRING.Collection}</Text>
        </View>}
      </View>
      <View style={styles.borderLine}></View>
<View style={styles.bottomView}>
  <Image source={images.cal}style={styles.timeI}/>
  <Text style={styles.oct}>24 Oct 2020</Text>
  <Image source={images.time} style={styles.timeI}></Image>
  <Text style={styles.time}>01:00PM -02.00PM </Text>
</View>
    </TouchableOpacity> 
  );
};
ListItem.propTypes = {
  itemData: PropTypes.object,
  itemOnPress: PropTypes.func,
  listButtonClick: PropTypes.func,
  checkBoxClick: PropTypes.func,
  tabIndex: PropTypes.number,
  isBottomButtonRequired:PropTypes.bool,
  isPendingStatusRequired:PropTypes.bool
};

export default ListItem;