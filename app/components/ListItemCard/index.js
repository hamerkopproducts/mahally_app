import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image, Text } from 'react-native';
import { images, styles } from "./styles";
import appTexts from "../../lib/appTexts";
import globals from "../../lib/globals";
import LinkButton from "../LinkButton";
import DividerLine from '../../components/DividerLine';
const ListItemCard = (props) => {
  const {
    itemData,
    tabIndex,
    itemOnPress,
    listButtonClick,
    checkBoxClick,
    isBottomButtonRequired
  } = props;
  return (
    <TouchableOpacity style={styles.rowContainer} onPress={() => { itemOnPress(itemData)}}>
      <View style={styles.orderIdContainer}>
          <Text style={styles.boldLabel}>{itemData.orderId}</Text>
        {tabIndex !== 0 && isBottomButtonRequired && <TouchableOpacity style={styles.checkBoxContainer} onPress={() => { checkBoxClick(itemData) }}>
          {/* <Image style={styles.checkBox} source={itemData.isSelected ? images.checkBoxSelected:images.checkBox} /> */}
        </TouchableOpacity>}
      </View>
      <View style={styles.lakeContainer}>
        <View style={styles.lakeView}>
          <View>
          <Image source={images.Delivery}style={styles.lakeImage}/>
          </View>
          <View>
          <Text style={styles.lakeText}>{appTexts.STRING.lake}</Text>
          </View>
        </View>
        <View style={styles.locationView}>
          <Text style={styles.viewStyle}>{appTexts.STRING.View}</Text>
          <View style={{paddingLeft:'4%'}}>
          <Image source={images.view} style={styles.viewArrow}/>
          </View>
        </View>
      </View>
      <View style={styles.borderLine}></View>
      
      <View style={styles.custText}>
        <Text style={styles.custStyle}>Customer</Text>
      </View>
      <View style={styles.custTexts}>
        <View style={{width:'70%'}}>
        <Text style={styles.muhd}>Muhammad Basheer</Text>
        </View>
        <View style={styles.oval}>
          <View style={styles.callIcon}>
            <Image source={images.hand}style={styles.callI}/>
            <Text style={styles.now}>Call now</Text>
          </View>
        </View>
      </View>
      <View style={styles.borderLine}></View>

<View style={styles.bottomView}>
  <Image source={images.cal}style={styles.timeI}/>
  <Text style={styles.oct}>24 Oct 2020</Text>
  <Image source={images.time} style={styles.timeI}></Image>
  <Text style={styles.time}>01:00PM -02.00PM </Text>
</View>





      {/* <View style={styles.addressContainer}>
        <View style={styles.scheduleContainer}>
          <Text style={styles.lightLabel}>{appTexts.STRING.deliverySchedule}</Text>
          <Text style={styles.mediumLabel}>{itemData.scheduledOn}</Text>
        </View>
        <View style={styles.locationContainer}>
          {isBottomButtonRequired && <View style={styles.locationIconContainer}>
            <Image source={images.locationIcon} style={styles.locationIcon} />
          </View>}
          <View style={styles.locationLabelContainer}>
            <Text style={styles.lightLabel}>{appTexts.STRING.deliveryLocation}</Text>
            <Text style={styles.mediumLabel}>{itemData.location}</Text>
          </View>
        </View>
      </View>
      {isBottomButtonRequired &&
      <View style={styles.buttonContainer}>
        <View style={styles.buttonView}>
          <LinkButton buttonText={tabIndex === 0 ? appTexts.STRING.markAsDelivered : tabIndex === 1 ? appTexts.STRING.markAsCollected : appTexts.STRING.markAsReturned} linkColor={tabIndex === 0 ? globals.COLOR.textColorGreen : tabIndex === 1 ? globals.COLOR.tabUnderLineColor : globals.COLOR.redTextColor} buttonClick={listButtonClick}/>
        </View>
      </View>}*/}

    </TouchableOpacity> 
  );
};
ListItemCard.propTypes = {
  itemData: PropTypes.object,
  itemOnPress: PropTypes.func,
  listButtonClick: PropTypes.func,
  checkBoxClick: PropTypes.func,
  tabIndex: PropTypes.number,
  isBottomButtonRequired:PropTypes.bool
};

export default ListItemCard;