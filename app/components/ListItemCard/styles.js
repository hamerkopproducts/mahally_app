import { StyleSheet } from "react-native";
import globals from "../../lib/globals"

const images = {
  locationIcon: require("../../assets/images/listCard/locationIcon.png"),
  checkBox: require("../../assets/images/listCard/check-box.png"),
  checkBoxSelected: require("../../assets/images/listCard/check-box-active.png"),
  Delivery:require('../../assets/images/listCard/Delivery.png'),
  view:require('../../assets/images/listCard/view.png'),
  call:require('../../assets/images/listCard/phone.png'),
  hand:require('../../assets/images/login/call.png'),
  cal:require('../../assets/images/listCard/date.png'),
  time:require('../../assets/images/listCard/Time.png'),
};

const styles = StyleSheet.create({
  rowContainer: {
    marginTop:20,
    width: '99%',
    marginLeft:'.5%',
    backgroundColor: globals.COLOR.headerColor,
    shadowColor: globals.COLOR.borderColor,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.5,
    shadowRadius: 0.5,
    elevation: 1,
    borderRadius:10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  orderIdContainer:{
    marginTop:15,
    width:'100%',
    height:30,
    justifyContent:'flex-start',
    alignItems: 'center',
    paddingLeft: 15,
    paddingRight: 15,
    flexDirection: 'row'
  },
  lakeContainer:{
    flexDirection:'row',
    marginTop:8,
    width:'100%',
    //height:30,
    //justifyContent:'flex-start',
    alignItems: 'center',
    //paddingLeft: 15,
    //paddingRight: 15,
    //flexDirection: 'row'
  },
  lakeView:{
    //alignSelf:'center',
    flexDirection:'row',
    alignItems:'center',
    marginHorizontal:"1%",
    width:'65%'
  },
  locationView:{
    flexDirection:'row',
    justifyContent:'flex-end',
    alignItems:'center',
    //alignSelf:'flex-end'
   // paddingLeft:'14%',
    
  },
  viewArrow:{
    width:10,
    height:10,
    paddingLeft:'2%',
  },
  boldLabel:{
    fontFamily:globals.FONTS.openSansSemiBold,
    color:globals.COLOR.textColor,

  },
  viewStyle:{
    color:'#008dea',
    fontFamily:globals.FONTS.openSansLight,
  },
  lakeText:{
    textAlign:'left',
    fontFamily:globals.FONTS.openSansLight,
    color:globals.COLOR.textColor,

  },
  lakeImage:{
    //alignSelf:'center'
    width:30,
    height:30,
  },
  borderLine:{
    borderWidth:0.5,
    borderColor:globals.COLOR.borderColor,
    width:'94%',
    marginLeft:"1%",
    // paddingLeft:'5%',
    // paddingRight:'5%',
    marginTop:'3%',
  },
  custText:{
    marginTop:'2%',
    justifyContent:'flex-start',
    alignSelf:'flex-start',
    marginHorizontal:"5%",
  },
  custTexts:{
    marginTop:'2%',
    justifyContent:'flex-start',
    alignSelf:'flex-start',
    marginHorizontal:"5%",
    flexDirection:'row',
  },
  custStyle:{
    fontFamily:globals.FONTS.openSansSemiBold,
    color:globals.COLOR.lightTextColor,
  },
  muhd:{
    fontFamily:globals.FONTS.openSansBold,
    color:globals.COLOR.textColor,
    fontSize:14,
  },
  oval:{
    width:100,
    height:35,
    borderRadius:15,
    borderColor:'#00836f',
    borderWidth:0.5,
    //marginLeft:'28%',
  },
  callIcon:{
    flexDirection:'row',
    paddingTop:'6%',
    paddingLeft:'5%',
  },
  callI:{
    width:20,
    height:20,
    //alignItems:'center',
  },
  now:{
    textAlign:'center',
    paddingLeft:'6%',
    fontFamily:globals.FONTS.openSansSemiBold,
    color:'#00836f',
    fontSize:14,
  },
  bottomView:{
    marginTop:'4%',
    flexDirection:'row',
    //marginTop:'2%',
    justifyContent:'flex-start',
    alignSelf:'flex-start',
    marginHorizontal:"5%",
    paddingBottom:'5%'
    
  },
  oct:{
    paddingLeft:'4%',paddingRight:'10%',
    fontFamily:globals.FONTS.openSansSemiBold,
    color:globals.FONTS.textColorBlack,
    fontSize:14,
  },
  time:{
    paddingLeft:'4%',
    fontFamily:globals.FONTS.openSansSemiBold,
    color:globals.FONTS.textColorBlack,
    fontSize:14,
  },
  timeI:{
    height:20,
    width:20,
  },
  // addressContainer: {
  //   width: '100%',
  //   paddingLeft: 15,
  //   paddingRight: 15,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   flexDirection:'row'
  // },
  // buttonContainer: {
  //   width: '100%',
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   paddingLeft: 15,
  //   paddingRight: 15,
  //   borderBottomLeftRadius:20,
  //   borderBottomRightRadius:20
  // },
  // buttonView:{
  //   width: '100%',
  //   borderColor: globals.COLOR.borderColor,
  //   borderTopWidth: 0.5,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  // },
  // checkBoxContainer:{
  //   width:'15%',
  //   height:'100%',
  //   alignItems:'flex-end',
  //   justifyContent:'center'
  // },
  // scheduleContainer: {
  //   width: '50%',
  //   justifyContent: 'center',
  //   alignItems: 'center'
  // },
  // locationContainer: {
  //   width: '50%',
  //   paddingBottom:15,
  //   paddingTop:15,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   flexDirection: 'row'
  // },
  // locationIconContainer: {
  //   width: '20%',
  //   justifyContent: 'center',
  //   alignItems: 'center'
  // },
  // locationLabelContainer:{
  //   marginLeft:15,
  //   width: '70%',
  //   justifyContent: 'center',
  //   alignItems: 'center'
  // },
  // orderIdLabelContainer:{
  //   width: '85%',
  //   justifyContent: 'center',
  //   alignItems: 'flex-start'
  // },
  // boldLabel:{
  //   textAlign:'left',
  //   color: globals.COLOR.textColor,
  //   fontFamily: globals.FONTS.avenirHeavy,
  //   fontSize:14
  // },
  // mediumLabel: {
  //   width: '100%',
  //   textAlign: 'left',
  //   color: globals.COLOR.textColor,
  //   fontFamily: globals.FONTS.avenirMedium,
  //   fontSize: 12
  // },
  // lightLabel: {
  //   width: '100%',
  //   textAlign: 'left',
  //   paddingBottom: 5,
  //   color: globals.COLOR.lightTextColor,
  //   fontFamily: globals.FONTS.avenirLight,
  //   fontSize: 10
  // },
  // buttonLabel: {
  //   width: '100%',
  //   textAlign: 'center',
  //   color: globals.COLOR.lightTextColor,
  //   fontFamily: globals.FONTS.avenirLight,
  //   fontSize: 16
  // },
  // locationIcon:{
  //   width:30,
  //   height:30
  // },
  // labelColorOrange:{
  //   color: globals.COLOR.tabUnderLineColor
  // },
  // labelColorGreen: {
  //   color:globals.COLOR.textColorGreen
  // }
});

export { images, styles };
