import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, I18nManager, Image, Text } from 'react-native';
import { images, styles } from "./styles";
import appTexts from "../../lib/appTexts";
import DividerLine from "../DividerLine";

const ListItemComp = (props) => {
  const {
    itemData,
  } = props;
  
  return (
    <View style={styles.rowContainer}>
      <View style={styles.rowItemContainer}>
      <View style={styles.orderImageContainer}>
          <Image source={images.itemImage } style={styles.itemImage}/>
      </View>
      <View style={styles.orderDataContainer}>
        <View style={styles.labelContainer}>
            <Text style={styles.mediumLabel}>{itemData.itemCategory}</Text>
        </View>
        <View style={styles.middleLabelContainer}>
            <Text style={styles.mediumLabel}>{itemData.itemName}</Text>
            <Text style={styles.lightLabel}>{itemData.weight }</Text>
            <Text style={styles.lightLabel}>{itemData.unit}</Text>
        </View>
        <View style={styles.labelContainer}>
            <Text style={styles.leftLabel}>{itemData.perItemPrice}</Text>
            {/* <Text style={styles.middleLabel}>{"X " + itemData.product_count}</Text> */}
            <Text style={styles.middleLabel}>{I18nManager.isRTL ? itemData.quantity+  ' X' : 'X ' + itemData.quantity }</Text>
            <Text style={styles.rightLabel}>{itemData.totalPrice}</Text>
        </View>
      </View>
      </View>
      <DividerLine/>
    </View>
  );
};
ListItemComp.propTypes = {
  itemData: PropTypes.object
};

export default ListItemComp;