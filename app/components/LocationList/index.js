import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, FlatList, Text,Image } from 'react-native';
import { images, styles } from "./styles";
import appTexts from "../../lib/appTexts";
import { TouchableOpacity } from 'react-native-gesture-handler';


const locationArray = [
    {id: '1', value: 'Al Jahrs'},
    {id: '2', value: 'Frawaniya'},
    {id: '3', value: 'Kuwait'},
    {id: '4', value: 'Mubarak al Kabeer'},
    {id: '5', value: 'Al Jahrs'},
    {id: '6', value: 'Kuwait'},
    {id: '7', value: 'Mubarak al Kabeer'},
    
  ];
const ItemView =({item}) =>{
    return(
        
        <View style={styles.loCListView}>
            
        <View style={styles.iconView}>
            <Image style={styles.radioImage} source={images.radioOff}></Image>
            </View>
        <View style={styles.listView}>
            <Text style={styles.locationText}>{item.value}</Text>
        </View>
        </View>
        
    );
};
  const LocationList = () =>{
      return(
          <View style={styles.mainScreen}>
            <FlatList
                data={locationArray}
                renderItem=
                   
                    {ItemView}
                    
                keyExtractor={(item, index) => index.toString()}
        />
          </View>
      );
  };
      LocationList.propTypes = {

      };
      
      
      export default LocationList;
  