import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const images = {
    radioOff: require("../../assets/images/listCard/radioOff.png"),
    
  };
  
const styles= StyleSheet.create({
mainScreen:{
    justifyContent:'flex-start',
    //alignItems:'flex-start',
    
},
listView:{
    marginLeft:'3%',
    //paddingTop:'10%',
    // padding:10,
    // paddingBottom:'2%'
   },
locationText:{
    fontFamily:globals.FONTS.openSansLight,
    color:'#000000',
    textAlign:"center"
},
loCListView:{
    flexDirection:'row',
   alignItems:"center",
   marginVertical:'4%',
  
},
iconView:{
    
    alignItems:"center"
},
radioImage:{
    width:25,
    height:25,
    resizeMode:'contain',
    
},
});
export  {styles,images};