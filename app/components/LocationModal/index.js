import PropTypes from 'prop-types';
import React from 'react';
import { View, Text, TouchableOpacity,Image,TextInput } from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import { images, styles } from "./styles";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import LocationList from '../LocationList';
import Modal from 'react-native-modal';

import RoundButton from "../RoundButton";
const LocationModal = (props) => {

  const {
        isLocationModalVisible,
        toggleLocationModal,
      } = props;

  
  return (
    <Modal
    animationIn="slideInUp" 
    animationOut="slideOutRight" 
    onSwipeComplete={toggleLocationModal}
    swipeDirection={["left", "right", "up", "down"]}
    isVisible={isLocationModalVisible}
    style={styles.modalMaincontentHelp}
  >
    <View style={styles.modalmainviewHelp}>
       <View style={styles.helptextWarpper}>
          <Text style={styles.helpheadText}>{appTexts.STATUS.headingLocation}</Text>
        </View>
        <View style={styles.listV}>
        <LocationList />
        </View>
    </View>
  </Modal> 
  );
};

LocationModal.propTypes = {
toggleLocationModal:PropTypes.func,
isLocationModalVisible:PropTypes.bool
};

export default LocationModal;