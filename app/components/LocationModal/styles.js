import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const images = {
  
};

const styles = StyleSheet.create({
    listV:{
        marginTop:"5%"
        },
        modalMaincontentHelp: {
            //justifyContent: "center",
            justifyContent:'flex-end',
            margin: 0,
          },
          modalmainviewHelp: {
            backgroundColor: "white",
            //width: wp("90%"),
            padding: "4%",
            //borderRadius: 10,
            borderTopRightRadius:15,
            borderTopLeftRadius:15,
            borderColor: "rgba(0, 0, 0, 0.1)",
          },
          helptextWarpper:{
            flexDirection:'row',
            justifyContent:'center',
            paddingTop:hp('2%'),
            paddingBottom:hp('1.5%'),
            paddingLeft:'10%',
            paddingRight:'10%'
            //paddingLeft:'19%'
            },
            helpheadText:{
              fontSize: hp('2.6%'),
              fontFamily: globals.FONTS.avenirHeavy,
              textAlign:'center'
            },
});

export { images, styles };