import React from "react";
import { styles } from "./styles";
import { View, Text, Image, TouchableOpacity, I18nManager } from "react-native";
import appTexts from "../../lib/appTexts";
import Modal from "react-native-modal";

const LoginPromptModal = (props) => {
  const { isLoginPromptModalVisible, closeModal, gotoLogin } = props;

  return (
    <Modal
      transparent={true}
      animationIn="slideInUp"
      animationOut="slideOutRight"
      swipeDirection={[]}
      isVisible={isLoginPromptModalVisible}
      style={styles.modalMaincontentLogout}
    >
      <View style={styles.modalmainviewLogout}>
        <TouchableOpacity
          style={styles.buttohelpnwrapper}
          onPress={() => {
            closeModal(false);
          }}
        >
          <Text style={styles.xText}>X</Text>
        </TouchableOpacity>

        <View style={styles.logoutImageView}>
          <View style={styles.profileContainer}>
            <View style={styles.roundImage}>
              <Image
                source={require("../../assets/images/profileicon/profile-image.jpg")}
                style={styles.logo}
              />
            </View>
            <View style={styles.infoContainer}>
              <TouchableOpacity
                onPress={() => {
                  closeModal();
                  gotoLogin();
                }}
                style={styles.butt}
              >
                <View style={styles.joinoval}>
                  <Text style={styles.join}>{appTexts.PROFILE.join}</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.logoutTextView}>
          <Text style={styles.logoutTextStyle}>
            { I18nManager.isRTL ? "الرجاء تسجيل الدخول لعرض طلبك" : "Please login to view your orders"}
          </Text>
        </View>
        <View style={styles.logButtons}></View>
      </View>
    </Modal>
  );
};

export default LoginPromptModal;
