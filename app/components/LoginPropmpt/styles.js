import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const images = {
  logoutImage: require("../../assets/images/listCard/Logout.png"),
};

const styles = StyleSheet.create({
  modalMaincontentLogout: {
    justifyContent: "flex-end",
    // width: '100%'
  },
  modalmainviewLogout: {
    backgroundColor: "white",
    borderRadius: 15,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  xText: {
    fontSize: 20,
    color: globals.COLOR.greyText,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },

  buttohelpnwrapper: {
    flexDirection: "row",
    justifyContent: 'flex-end',
    paddingTop: '5%',
    paddingRight: '5%'
  },
  logoutImageView: {
    alignItems: 'center',
    justifyContent: 'center',
    // marginTop: '5%'
  },
  logoutImage: {
    width: 80,
    height: 80,
  },
  logoutTextView: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '5%',
    marginBottom: '5%',
    width: '100%'
  },
  logoutTextStyle: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color: globals.COLOR.Text,
    fontSize: 16,
  },
  logoutTetStyle: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color: globals.COLOR.blackTextColor,
    fontSize: 18,
  },
  logButtons: {

    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: '5%'
  },
  clickBut: {
    width: 250,
    height: 50,
    backgroundColor: globals.COLOR.purple,
    borderRadius: 100,
    borderWidth: 0.2,
    alignSelf: 'center',
  },
  clickText: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color: 'white',
    alignSelf: 'center', marginTop: '5%'
  },
  buttonStyle: {
    width: 200,
    height: 50,
    borderRadius: 20,
    alignSelf: 'center'
  },
  buttonText: {
    color: globals.COLOR.purple,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    alignSelf: 'center', marginTop: '5%'
  },

  buttonOne: {
    marginBottom: '4%',
  },
  buttonTwo: {
    marginBottom: '4%'
  },
  logo: {
    width: 75,
    height: 72,
    resizeMode: 'cover',
    borderRadius: 50,
    alignSelf: 'center',

  },
  joinoval: {
    borderWidth: 0.5,
    borderColor: globals.COLOR.Text,
    width: 170,
    borderRadius: 20,
    alignItems:'center',
    justifyContent:'center',
    padding: 5
  },
  roundImage: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  butt: {
    height: 30,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },


});

export { images, styles };