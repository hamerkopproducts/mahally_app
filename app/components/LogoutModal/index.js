import LinearGradient from "react-native-linear-gradient";
import React, { useState } from 'react';
import { images, styles } from "./styles";
import { View, Text, TouchableOpacity, Image, TouchableHighlight } from 'react-native';
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import PropTypes from 'prop-types';
import RoundButton from "../RoundButton";
import Modal from 'react-native-modal';
import CustomButton from '../CustomButton';

const LogoutModal = (props) => {

  const {
    isLogoutModalVisible,
    logoutButtonPress,
    toggleLogoutModal,
  } = props;

  const [butClick, setButClick] = useState(false);
  const [upButClick, setUpButClick] = useState(true);

  const cancelClick = () => {
    setButClick(!butClick);
    setUpButClick(false);
    toggleLogoutModal();
  };

  const logoutClick = () => {
    logoutButtonPress();
    
    setUpButClick(!upButClick);
    setButClick(false);
  };


  return (
    <Modal
      transparent={true}
      animationIn="slideInUp"
      animationOut="slideOutRight"
      onSwipeComplete={logoutButtonPress}
      swipeDirection={["left", "right", "up", "down"]}
      isVisible={isLogoutModalVisible}
      style={styles.modalMaincontentLogout}
    >
      <View style={styles.modalmainviewLogout}>
        <TouchableOpacity style={styles.buttohelpnwrapper}
          onPress={() => { toggleLogoutModal(); }}  >
          <Text style={styles.xText}>X</Text>
        </TouchableOpacity>

        <View style={styles.logoutImageView}>
          <Image source={images.logoutImage} style={styles.logoutImage} />
        </View>
        <View style={styles.logoutTextView}>
          <Text style={styles.logoutTextStyle}>{appTexts.PROFILE.logout}</Text>
        </View>
        <View style={styles.logButtons}>
          <TouchableOpacity style={styles.logButtons} onPress={() => { cancelClick(); }}>
            {butClick ?
              <View style={styles.clickBut}>
                <Text style={styles.clickText}>{appTexts.STRING.cancel}</Text>
              </View> : <View style={styles.buttonStyle}>
                <Text style={styles.buttonText}>{appTexts.STRING.cancel}</Text>
              </View>}
          </TouchableOpacity >
          <TouchableOpacity style={styles.logButtons} onPress={() => { logoutClick(); }}>
            {upButClick ? <View style={styles.clickBut}>
              <Text style={styles.clickText}>{appTexts.PROFILELISTING.Logout}</Text>
            </View> :
              <View style={styles.buttonStyle}>
                <Text style={styles.buttonText}>{appTexts.PROFILELISTING.Logout}</Text>
              </View>}
          </TouchableOpacity>


        </View>

      </View>
    </Modal>
  );
};

LogoutModal.propTypes = {

  isLogoutModalVisible: PropTypes.bool,
  logoutButtonPress: PropTypes.func,
  toggleLogoutModal: PropTypes.func,
};

export default LogoutModal;