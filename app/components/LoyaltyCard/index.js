import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { View, TouchableOpacity, Image, Text, I18nManager } from "react-native";
import { images, styles } from "./styles";
import appTexts from "../../lib/appTexts";
import DotedDivider from "../DotedDivider";
import moment from "moment";

const LoyaltyCard = (props) => {
  const {
    item,
    isOffer,
    onMenuCardPress,
    restLocation,
    restaurantName,
    addloyalty,
    addedCartItems,
    postScanItemToAddData,
    postScanItemToAdd,
    from,
  } = props;

  let addedLoyalty = {};
  try {
    addedLoyalty = addedCartItems["loyalty_" + item.item_id.toString()];
  } catch (err) {
    addedLoyalty = {};
  }
  let addedItemCount = 0;
  if (typeof addedLoyalty != "undefined") {
    addedItemCount = addedLoyalty.count;
  }

  const addToloyalty = (item, freemeals) => {
    addloyalty(item, freemeals);
  };

  const orderCount =
    typeof item.order_count == "undefined" ||
    item.order_count == "" ||
    item.order_count == null
      ? 0
      : item.order_count;
  const remaing_count =
    typeof item.remaing_count == "undefined" ||
    item.remaing_count == "" ||
    item.remaing_count == null
      ? 0
      : item.remaing_count;
  const orderCountRounded = orderCount % remaing_count;
  let availableFreeMeals = item.no_offer_available;

  let remainingcanAdd = availableFreeMeals - addedItemCount;
  let msg = 'You can add upto ${num} item' +
    (remainingcanAdd > 1 ? "s" : "");
  if(I18nManager.isRTL) {
    msg = 'يمكنك إضافة ما يصل إلى ${num} عنصرًا';
  }
  msg = msg.replace('${num}', remainingcanAdd);

  useEffect(() => {
    if (
      postScanItemToAddData &&
      postScanItemToAddData.item_id == item.item_id &&
      postScanItemToAddData.type == "loyality"
    ) {
      addToloyalty(item, availableFreeMeals);
      postScanItemToAdd({});
    }
  }, []);

  let _name = item.rest_name;
  let _loc = item.location;
  if(isOffer) {
    _name = restaurantName;
    _loc = restLocation;
  }

  const available_time = item?.available_time || [];
  let isMenuOpen = false;
  if(available_time) {
    available_time.map((itm, index) => {
      const format = 'hh:mm A';
      const time = moment();
      const beforeTime = moment(itm.time.start_time, format);
      const afterTime = moment(itm.time.end_time == '00:00' ? '23:59' : itm.time.end_time, format);

      if (time.isBetween(beforeTime, afterTime)) {
        isMenuOpen = true;
      }
    });
  }

  return (
    <View style={[styles.loyaltyCard, ! isMenuOpen && {opacity: .5} ]}>
      <TouchableOpacity
        onPress={() => {
          onMenuCardPress(item.item_id, item, "loyality");
        }}
        disabled={ ! isMenuOpen}
      >
        <View style={styles.foodCard}>
          <Image source={{ uri: item.cover_photo }} style={styles.foodImage} resizeMode={'cover'} />
        </View>
      </TouchableOpacity>

      <View style={isOffer ? styles.offertextCard : styles.textCard}>
        <View style={styles.rateOval}>
          <View style={styles.ovalText}>
            <View style={styles.burger}>
              <Image source={images.burger} style={styles.bur} />
            </View>
            <View style={styles.no}>
              <Text style={styles.bug}>
                {orderCountRounded}/{remaing_count}
              </Text>
            </View>
          </View>
        </View>

        <View style={styles.lineOne}>
          <Text style={styles.headLine}>{item.item_name}</Text>
        </View>
        <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
        <View style={styles.lineOneab}>
        <Text style={styles.proname}>{item.pro_name}</Text>
        </View>
        
        { item.pre_order == 'enable' && (
            <View style={styles.ratingpre}>
              <Text style={styles.ratingTextpre}>{appTexts.STRING.preorder}</Text>
              </View>
            )}
           
        </View>
        <View style={styles.lineTwo}>
          <View style={styles.kalied}>
            <Text style={styles.khalid}>{_name}</Text>
          </View>
          <View style={styles.loc}>
            <View style={styles.locIcon}>
              <Image source={images.location} style={styles.location} />
            </View>
            <View style={styles.locText}>
              <Text style={styles.khalid}>{_loc}</Text>
            </View>
          </View>
        </View>
        
        <View style={styles.divide}>
          <DotedDivider />
        </View>
        
        <View style={styles.offerLine}>
            <View style={{ flex: 2, justifyContent: "space-between" }}>
              <View>
                <Text style={styles.line3}>{item.remaining_message}</Text>
              </View>
              {item.remaining_days === 0 ? (
                <View>
                  {remainingcanAdd > 0 && (
                    <Text style={styles.mealsAvailable}>{msg}</Text>
                  )}
                  <Text style={styles.line4}>
                    {appTexts.LOYALTY.endsToday}
                  </Text>
                </View>
              ) : (
                <View>
                  {remainingcanAdd > 0 && (
                    <Text style={styles.mealsAvailable}>{msg}</Text>
                  )}
                  <Text style={isOffer ? styles.offerDay : styles.line4}>
                    {appTexts.LOYALTY.day.replace('{{}}', item.remaining_days)}
                  </Text>
                </View>
              )}
            </View>
            <View style={{ justifyContent: "space-between" }}>
              <TouchableOpacity
                onPress={() => {
                  addToloyalty(item, availableFreeMeals);
                }}
                disabled={ ! isMenuOpen}
              >
                <View style={styles.oval}>
                  <Text style={styles.plus}>+</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
          
      </View>
    </View>
  );
};

LoyaltyCard.propTypes = {
  isOffer: PropTypes.bool,
};

export default LoyaltyCard;
