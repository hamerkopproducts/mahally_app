import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals"
import { widthPercentageToDP } from "react-native-responsive-screen";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const images = {
  location: require('../../assets/images/listCard/location.png'),
  burger: require('../../assets/images/listCard/freemeal.png'),
};

const styles = StyleSheet.create({
  container: {
    width: '90%',
    borderWidth: 0.5,
    borderRadius: 10,
    backgroundColor: 'white',
    marginLeft: '5%',
    marginRight: '5%',
    borderColor: globals.COLOR.greyText,
    marginTop: '2%'
  },
  ratingpre:{
    backgroundColor:globals.COLOR.purple,
    width:100,
    height:16,
    borderRadius:20,
    marginRight:hp("1.5%"),
   
    alignSelf:"center",
    justifyContent:'center',
 
},
  ratingTextpre:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helveticaBold,
    fontSize:9,
    textAlign:'center',
    color:globals.COLOR.white,
},
  middleTabView: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  card: {
    borderRadius: 20,
    width: '90%',
    borderWidth: 0.5,
    borderColor: globals.COLOR.greyText,
  
  },
  loyaltyCard: {
    flex:1,
    width: widthPercentageToDP('90%'),
    borderRadius: 10,
    backgroundColor: 'white',
    marginLeft: widthPercentageToDP('5%'),
    marginRight: widthPercentageToDP('5%'),
    borderBottomColor: 'white',
    justifyContent: 'space-between',
    marginBottom: 20
  },
  foodCard: {
    width: '100%',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    backgroundColor: 'white'
  },
  foodImage: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    width: widthPercentageToDP('90%'),
    height: widthPercentageToDP('45%'),
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
  },
  ovala: {
    borderWidth: 0.5,
    borderColor: globals.COLOR.borderColor,
    height: 32,
    borderRadius: 8,
    top:5,
    alignItems:'center',
    justifyContent:'center',
    width:80,
    
    
     },
     touch:{
       width:80,
       height:32,
     },
     middleTabView: {

      justifyContent: 'center',
      alignItems: 'center'
    },
  plusa: {
    fontSize: 20,
    color: globals.COLOR.purple,
  },
  textCard: {
    borderWidth: 0.5,
    borderRadius: 10,
    backgroundColor: 'white',
    borderColor: globals.COLOR.greyText,
    width: '100%',
    borderColor: '#ddd',
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    // elevation: 3,
  },
  offertextCard: {
    borderWidth: 0.5,
    borderRadius: 10,
    backgroundColor: 'white',
    borderColor: globals.COLOR.greyText,
    bottom:10,
    width: '100%',
    borderColor: '#ddd',
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  lineOne: {
    marginTop: '3%',
    marginLeft: '3%',

  },
  lineOneab: {
    marginTop: '1%',
    marginLeft: '3%',

  },
  proname:{
   
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 14,
    color: globals.COLOR.Text,
    
    textAlign: 'left'
  
},
  headLine: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.helveticaBold,
    fontSize: 12,
    color: globals.COLOR.Text,
    textAlign: 'left',
  },
  lineTwo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: '3%',
    marginRight: '3%',
    alignItems: 'center'
  },
  khalid: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 12,
    color: globals.COLOR.greyText,
    
  },
  mealsAvailable: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 12,
    color: globals.COLOR.peach,
    marginTop: 3,
    textAlign: 'left'
  },
  loc: {
    flex:0.5,
    flexDirection: 'row',
    alignItems: 'center',
   justifyContent:"flex-end"
  },
  locText:{
   
    
  },
  location: {
    height: 30,
    width: 30,
  },
  separator: {
    marginTop: '2%',
    marginBottom: '25'
  },
 divide:{
   width:'96%',
   marginLeft:'2%',
   marginRight:'2%'
 },
  lineThree: {
    marginLeft: '3%',
    marginRight: '3%',
    flexDirection: 'row',
    width:'60%',
    //flexWrap:'wrap',
    justifyContent: 'space-between',
  
  },
  offerLine:{
    flex: 1,
    // zIndex:999,
    marginLeft: '3%',
    marginRight: '3%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
  },
  line3: {
    textAlign: 'left',
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 10,
    lineHeight: 14,
    color: globals.COLOR.Text,
  },
  line4: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 10,
    color: '#F5A389',
    textAlign: 'left'
  },
  offerDay:{
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 10,
    color: globals.COLOR.purple,
    textAlign: 'left'
  },
  oval: {
    borderWidth: 0.5,
    borderColor: globals.COLOR.greyText,
    height: 35,
    width: 80,
    borderRadius: 10,
    //paddingLeft:'10%',
    alignItems: 'center',
    // marginRight:'10%',
    top: '2%',
    //right: 20,
    justifyContent: 'center'
  },
  ovalTouch:{
    height: 34,
    width: '25%',
   // backgroundColor:'red',
    zIndex: 9999
  },
  
  ovalOffer:{
    
    
    borderWidth: 0.5,
    borderColor: globals.COLOR.greyText,
    borderRadius: 10,
    //paddingLeft:'10%',
    alignItems: 'center',
    // marginRight:'10%',
    top: '2%',
    //right: 20,
    justifyContent: 'center'
  },
  plu: {
    // alignSelf: 'center',

  },
  plus: {
    fontSize: 20,
    color: globals.COLOR.purple,
  },
  rateOval: {
    width: '25%',
    height: 42,
    backgroundColor: '#F5A389',
    alignItems: 'center',
    position: 'absolute',
    borderRadius: 10,
    top:-20,
    zIndex: 2,
    right: 10,
  },
  rateOvala: {
    width: '25%',
    height: 42,
    backgroundColor: '#F5A389',
    alignItems: 'center',
    position: 'absolute',
    borderRadius: 10,
    bottom:130,
    zIndex: 2,
    right: 10,
  },
  ovalText: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  bur: {
    height: 30,
    width: 30,
    resizeMode: 'contain'
  },
  burger: {
    marginRight: 7,
    marginLeft: 7,
    position: 'absolute',
    zIndex: 2,
    top: 8,
    right: 10,
  },
  no: {
    alignSelf: 'center',
    top: 10,
    right: -18,
  },
  bug: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 16,
    color: 'white'
  },
  line1:{
   // marginBottom:'4%',
  },
}
);

export { images, styles };
