import React, { useEffect, useRef, useState } from "react";
import PropTypes from "prop-types";
import { 
  View, TouchableOpacity, 
  Image, Text, I18nManager,
  FlatList
} from "react-native";

import { images, styles } from "./styles";
import appTexts from "../../lib/appTexts";
import _ from "lodash";
import functions from "../../lib/functions";
import moment from "moment";

const MenuOfferCard = (props) => {
  const {
    items,
    itemData,
    isAddonsAvailable,
    onMenuCardPress,
    isDetail,
    addedCartItems,
    addToCart,
    onCheckoutClick,
    selectedRestaurant,
    restaurant_id,
    redirectToScan,
    postScanItemToAdd,
    from,
    postScanItemToAddData,
    isAddonModalVisible,
    setIsAddonModalVisible,
    time_status
  } = props;

  let hasAddons = false;
  const scrollRef = useRef(); 
  const [xposition, setXPositin] = useState(0); 

  if(isDetail) {
    hasAddons = false;
  } else {
    hasAddons = itemData.item_addons.length > 0;
  }

  let addonsSelected = {};
  try{
    addonsSelected = addedCartItems[itemData.id].addonsSelected;
  } catch(err) {
    
  }

  const addItem = async() => {

    if(time_status === 'CLOSED') {
      functions.displayToast('error', 'top', appTexts.REST.Heading_closed , appTexts.REST.closed);
      return false;
    }

    console.debug( selectedRestaurant, '==>>>')

    if(typeof selectedRestaurant.restaurant_id != 'undefined' && selectedRestaurant.restaurant_id != restaurant_id) {
      await functions.displayAlertWithCallBack(
        appTexts.REST.already_in_cart, 
        appTexts.REST.cart_has_items, 
        (data) => {
          if(data) {
            addToCart({}, {});
            redirectToScan(false, 'menu', itemData.id);
            addToCartItem(true);
            
          }
        }
      );
    } else if(typeof selectedRestaurant.restaurant_id == 'undefined') {
      redirectToScan(false, 'menu', itemData.id);
      addToCartItem(true);
    } else {
      addToCartItem(false);
    }
  }

  const addToCartItem = (clear_cart) => {
    let addedItems = (typeof addedCartItems == 'undefined' || clear_cart == true) ? {} : Object.assign({}, addedCartItems);
      if(Object.keys(addedItems).indexOf(itemData.id.toString()) == -1) {
        let itemToAdd = Object.assign({}, itemData);
        itemToAdd.discount_price = itemToAdd.discount_price != null ? itemToAdd.discount_price : itemToAdd.price;
        itemToAdd.addonsSelected = {};
        itemToAdd.count = 1;
        addedItems[itemData.id] = itemToAdd;
        setTimeout(() => {
          functions.displayToast('success', 'top', appTexts.EDITPROFILE.success, appTexts.ORDER.itemadded);
        }, 400);
      } else {
        let all_items = Object.assign({}, addedItems);
        all_items[itemData.id].count += 1;
        addedItems = all_items;
        setTimeout(() => {
          functions.displayToast('success', 'top', appTexts.EDITPROFILE.success, appTexts.EDITPROFILE.itemupdated);
        }, 400);
      }
      addToCart(addedItems, itemData);
      if(hasAddons) {
        setIsAddonModalVisible( true );
      }
  }

  useEffect(() => {
    if(from == 'ScanView') {
      if(postScanItemToAddData.item_id == itemData.id && postScanItemToAddData.type == 'menu') {
        addItem();
        postScanItemToAdd({});
      }
    }
  }, []);

  useEffect(() => {
    const _intrvl = setInterval(() => {
      if(scrollRef) {
        try {
          if(itemData && itemData.available_time) {
            const nxtSlide = itemData.available_time.length-1 <= xposition ? 0 : (xposition+1);
            setXPositin(nxtSlide);
            scrollRef.current.scrollToIndex({animated: true, index: nxtSlide});
          }
        } catch(err) {
          
        }
      }
    }, 1500);

    return () => { clearInterval(_intrvl);}
  }, [xposition]);

  let isMenuOpen = false;
  if(itemData && itemData.available_time) {
    itemData.available_time.map((itm,index) => {
      const format = 'HH:mm';
      const time = moment();
      const beforeTime = moment(itm.time.start_time, format);
      const afterTime = moment(itm.time.end_time == '00:00' ? '23:59' : itm.time.end_time, format);
      if (time.isBetween(beforeTime, afterTime)) {
        isMenuOpen = true;
      }
    });
  }

  return (
    <View style={isDetail ? styles.detail : styles.container}>
      <View style={styles.imageCardView}>
        {isDetail ? (
          <View style={styles.imageView}>
            <Image
              source={{ uri: _.get(items, "cover_photo", "") }}
              style={styles.foodImage}
            />
          </View>
        ) : (
          <TouchableOpacity
            onPress={() => {
              onMenuCardPress(itemData.id, itemData, 'menu');
            }}
            disabled={ ! isMenuOpen}
          >
            <View style={styles.imageView}>
              <Image
                source={{ uri: _.get(itemData, "cover_photo", "") }}
                style={[styles.foodImage, ! isMenuOpen && {opacity: .5} ]}
              />
            </View>
          </TouchableOpacity>
        )}

        <View style={styles.ResName}>
          <View style={[styles.lebView, ! isMenuOpen && {opacity: .5} ]}>
            {isDetail ? (
              <Text style={styles.extraText}>
                {_.get(items, "item_name", "")}
              </Text>
            ) : (
              <Text style={styles.lebText}>
                {I18nManager.isRTL
                  ? _.get(itemData, "lang[1].name", "")
                  : _.get(itemData, "lang[0].name", "")}
              </Text>
            )}
          </View>
          {isDetail ?  (<View style={styles.sar1}>
              <Text style={styles.surText}>{appTexts.STRING.sar}</Text>
            
                
                <Text style={styles.sur1Text}>{_.get(items, "price", "")}</Text>
                </View>):null
                
              }
          <TouchableOpacity
            onPress={() => {
              onMenuCardPress(itemData.id, itemData, 'menu');
            }}
            disabled={ ! isMenuOpen}
          >
            { ! isDetail && itemData && itemData.available_time &&
                <View style={{ width: 100 }}>
                <FlatList
                  ref={scrollRef}
                  showsHorizontalScrollIndicator={false}
                  horizontal={true}
                  data={itemData.available_time}
                  pagingEnabled={true}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={({item}) => (
                    <View style={[styles.timeView, {width: 100} ]}>
                      <Image source={images.time} style={styles.timeImage} />
                      <Text style={styles.timeText}>
                        {item.time.start_time} - {item.time.end_time}{' '}
                      </Text>
                    </View>
                  )}
                />
                </View>
            }

            {hasAddons ?
              <View style={styles.plusad}>
                <Text style={styles.addonesText}>{"+"}</Text>
                <Text style={styles.addonesText}>{appTexts.STRING.adones}</Text>
              </View> :
              <View style={styles.plusadempt}>
              <Text style={styles.addonesText}></Text>
              <Text style={styles.addonesText}></Text>
            </View>
            }
          </TouchableOpacity>

          <View style={styles.sar}>
            {isDetail ? null : (
              <>
              {_.get(itemData, "discount_price", null) != null &&
              <View style={styles.sar1}>
                <Text style={styles.sartext}>{appTexts.STRING.sar}</Text>
                <View style={styles.sarvText}>
                  <Text style={styles.sarvalue}>
                    {_.get(itemData, "price", "")}
                  </Text>
                </View>
              </View>
              }
              </>
            )}
            
           {isDetail ? null : <View style={styles.sar1}>
              <Text style={styles.surText}>{appTexts.STRING.sar}</Text>
              
                <Text style={styles.sur1Text}>
                  {_.get(itemData, "discount_price", null) != null ? _.get(itemData, "discount_price", "") : _.get(itemData, "price", null)}
                </Text>
              
            </View>
          }
          </View>
        </View>
      </View>

      {isDetail ? null : (
        <TouchableOpacity
          onPress={async() => {
            addItem();
          }}
          disabled={ ! isMenuOpen}
        >
          <View style={styles.oval}>
            <Text style={[
              styles.plus,
              ! isMenuOpen && {color: 'gray'}
              ]}>+</Text>
          </View>
          </TouchableOpacity>
      )}

    </View>
  );
};

MenuOfferCard.propTypes = {
  onPlusPress: PropTypes.func,
};

export default MenuOfferCard;
