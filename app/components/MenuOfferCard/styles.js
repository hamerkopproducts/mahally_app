import { StyleSheet, I18nManager, Platform } from "react-native";
import globals from "../../lib/globals"

const images = {
  location: require('../../assets/images/listCard/location.png'),
  burger: require('../../assets/images/listCard/freemeal.png'),
  round:require('../../assets/images/ItemCard/img3.png'),
  time:require('../../assets/images/ItemCard/Timing.png'),
};

const styles = StyleSheet.create({
 container:{
        flex:1,
        flexDirection: 'row',

         // flex:1,
    borderWidth: 0.5,
    borderRadius: 10,
    backgroundColor: 'white',
    // alignItems:"flex-start",
    justifyContent: 'space-between',
    // borderColor: globals.COLOR.greyText,
    
    marginBottom:"1%",
    marginTop:"1%",
    // top: 80,
    width: '100%',
    // height: '100%',
    borderColor: globals.COLOR.borderColor,
    
 },
 detail:{
  flex:1,
  // flex:1,
borderWidth: 0.5,
borderRadius: 30,
backgroundColor: 'white',

// alignItems"flex-start",
// justifyContent:"flex-start",
// borderColor: globals.COLOR.greyText,

marginBottom:"1%",
marginTop:"1%",
// top: 80,
width: '100%',
// height: '100%',
borderColor: globals.COLOR.borderColor,
 },
  card: {
    borderRadius: 20,
    width: '90%',
    borderWidth: 0.5,
    borderColor: globals.COLOR.greyText,
    
  },
plusad:{
  flexDirection:"row",
  alignItems:"center",
},
plusadempt:{
  flexDirection:"row",
  alignItems:"center",
  marginTop: 7
},
addonesText:{
    color:globals.COLOR.purple,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize:11,
    margin:1,
    lineHeight: Platform.OS === 'ios' ? 25 : null
},
lebText:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.helveticaBold,
  fontSize:I18nManager.isRTL ?12:13,
  textAlign:'left',
  lineHeight: Platform.OS === 'ios' ? 25 : null
},
extraText:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helveticaBold,
  fontSize:13,
  textAlign:'left',
},
timeText:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  fontSize:10,
  color:globals.COLOR.greyText,
  lineHeight: Platform.OS === 'ios' ? 25 : null
},
surText:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  fontSize:11
},
sur1Text:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  fontSize:14,
  left:3,

},
timeView:{
 flexDirection:'row',
  alignItems:'center',
},
timeImage:{
  marginTop: I18nManager.isRTL && Platform.OS === 'ios'  ?  3 : 0,
  marginRight: 3,
  //marginTop: 6,
width:13,
height:13,

},
  foodCard: {
    //height:'10%',
    width: '100%',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    
  },
  foodImage: {
    borderRadius: 10,
    width:90,
    height:90,
    right: 10,
    marginLeft: 10,
    marginTop: 7,
    marginBottom: 7
   
  },
  foodImageSqure: {
    borderRadius:10,
    width:70,
    height:70,
   
  },
  imageCardView:{
    // flex: 1,
    flexDirection:"row",
    alignItems:"center",
    width: '65%',
    marginLeft:'2%',
    
  },
ResName:{
    flexDirection:'column',
},
lebView:{
width:"100%"
},
sartext:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
fontSize:I18nManager.isRTL ?9:10,
textDecorationLine:'line-through',
color:globals.COLOR.greyText
},
sarvalue:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
fontSize:I18nManager.isRTL ?12:13,
lineHeight: Platform.OS === 'ios' ? 25 : null,

textDecorationLine:'line-through',
color:globals.COLOR.greyText
},
  imageView:{
     padding:7,
    alignSelf:"center",
    justifyContent:"center",
  },
  roundedView:{
     backgroundColor:'red',
     borderRadius: 50
  },
  sarvText:{
    marginHorizontal:'5%',
    bottom:1
  },
  sar:{
    flexDirection:'row',
    
    // width:"30%",
    alignItems:"center"
  },
  sar1:{
    flexDirection:'row',
    
   
    alignItems:"center"
    
  },
 
//   textCard: {
//     // flex:1,
//     borderWidth: 0.5,
//     borderRadius: 10,
//     backgroundColor: 'white',
//     alignItems:"flex-start",
//     justifyContent:"flex-start",
//     // borderColor: globals.COLOR.greyText,
    
//     position: 'relative',
//     // top: 80,
//     width: '100%',
//     // height: '100%',
//     borderColor: '#ddd',
//     shadowColor: "#000",
//     shadowOffset: { width: 0, height: 3 },
//     shadowOpacity: 0.22,
//     shadowRadius: 2.22,
//     elevation: 3,
//   },
  lineOne: {
    marginTop: '3%',
    marginLeft: '3%',

  },
  headLine: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 14,
    color: globals.COLOR.Text,
    textAlign: 'left',
  },
  lineTwo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: '3%',
    marginRight: '3%',
    alignItems: 'center'
  },
  khalid: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 14,
    color: globals.COLOR.greyText,
  },
  loc: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  location: {
    height: 30,
    width: 30,
  },
  separator: {
    marginTop: '2%',
    marginBottom: '25'
  },
  line: {
    borderWidth: 1,
    borderColor: globals.COLOR.greyBackground,
    borderStyle: 'dashed',
    marginTop: '1%',
    marginBottom: '1%',
    marginLeft: '3%',
    marginRight: '3%',
    borderRadius: 10,
  },
  lineThree: {
    marginLeft: '3%',
    marginRight: '3%',
    flexDirection: 'row',
    //width:'60%',
    //flexWrap:'wrap',
    justifyContent: 'space-between',
  },
  line3: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 12,
    color: globals.COLOR.Text,
  },
  line4: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 10,
    color: '#F5A389',
    top: 12,
    textAlign: 'left'
  },
  oval: {
    borderWidth: 0.5,
    borderColor: globals.COLOR.borderColor,
    height: 36,
    borderRadius: 8,
    // top: 15,
    
    alignItems:'center',
     justifyContent:'center',
    width:80,
    margin:"15%",
    top:15
     },
     touch:{
       width:80,
       height:32,
     },
  
  plus: {
    fontSize: 20,
    color: globals.COLOR.purple,
  },
  rateOval: {
    width: '25%',
    height: 42,
    backgroundColor: '#F5A389',
    alignItems: 'center',
    position: 'absolute',
    borderRadius: 10,
    top: -15,
    zIndex: 2,
    right: 10,
  },
  ovalText: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  bur: {
    height: 30,
    width: 30,
    resizeMode: 'contain'
  },
  burger: {
    position: 'absolute',
    zIndex: 2,
    top: 8,
    right: 10,
  },
  no: {
    alignSelf: 'center',
    top: 10,
    right: -18,
  },
  bug: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 16,
    color: 'white'
  },
}
);

export { images, styles };
