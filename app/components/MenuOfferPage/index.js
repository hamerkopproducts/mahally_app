import React from "react";
import {
  View,
  Text,
  I18nManager,
  FlatList,
} from "react-native";
import { styles } from "./styles";
import MenuOfferCard from "../../components/MenuOfferCard";
import _ from "lodash";

const MenuOfferPage = (props) => {
  const { 
    item, 
    onMenuCardPress, 
    addedCartItems, 
    addToCart, 
    selectedRestaurant, 
    onCheckoutClick, 
    restaurant_id, 
    redirectToScan,
    postScanItemToAddData, 
    from, 
    postScanItemToAdd,
    isAddonModalVisible,
    setIsAddonModalVisible,
    time_status
  } = props;

  const renderItemMenuCard = ({ item, index }) => (
    <MenuOfferCard
      itemData={item}
      onMenuCardPress={onMenuCardPress}
      isAddonsAvailable={true}
      addedCartItems={addedCartItems}
      addToCart={addToCart}
      selectedRestaurant={selectedRestaurant}
      onCheckoutClick={onCheckoutClick}
      restaurant_id={restaurant_id}
      redirectToScan={redirectToScan}
      postScanItemToAddData={postScanItemToAddData}
      from={from}
      postScanItemToAdd={postScanItemToAdd}
      isAddonModalVisible={isAddonModalVisible}
      setIsAddonModalVisible={setIsAddonModalVisible}
      time_status={time_status}
    />
  );

  return (
    <View>
      <View style={styles.container}>
        <Text style={styles.leftTopic}>
          {I18nManager.isRTL
            ? item[0].menu_category.lang[1].name
            : item[0].menu_category.lang[0].name}
        </Text>
      </View>
      <View>
        <FlatList
          style={styles.flatListStyle}
          contentContainerStyle={{ paddingBottom: "5%" }}
          data={_.isEmpty(item) ? [] : item}
          extraData={item}
          keyExtractor={(item, index) => index.toString()}
          renderItem={renderItemMenuCard}
        />
      </View>
    </View>
  );
};

export default MenuOfferPage;
