import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const images = {
  calendarIcon: require("../../assets/images/temp/calendern.png"),
  arrowIcon: require("../../assets/images/temp/calenderarrow.png"),
  filterIcon: require("../../assets/images/temp/filterNoBadge.png"),
  SortIcon: require("../../assets/images/temp/Sort.png")
};

const styles = StyleSheet.create({
    flatListStyle: {
        width: '90%',
        
        alignSelf:'center',
      },
  container: {marginLeft: 10,marginTop: 10, marginBottom: 10},
  boldTopic:{
    height:"2.5%",
    marginVertical:"5%",
    marginHorizontal:18
  },
  leftTopic:{
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.helveticaBold,
    fontSize:14,
    textAlign:'left',  
    marginLeft: 10
  },
});

export { images, styles };
