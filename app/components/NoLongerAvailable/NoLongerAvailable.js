import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Image, I18nManager } from 'react-native';
import Modal from 'react-native-modal';
import RoundButton from "../../components/RoundButton";
import StyleSheetFactory from './styles';
import appTexts from '../../lib/appTexts'

class NoLongerAvailable extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isModalVisible: true,
    };
  }

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  render() {
    const styles = StyleSheetFactory.getSheet(I18nManager.isRTL);
    const {body, visible, hide, redirect} = this.props;
    let btntext = 'Done';

    return (
      <View>
        <Modal isVisible={visible} style={styles.modalMainContent}>
          <View style={styles.modalmainView}>

            <View style={styles.logo}>
            </View>

            <View style={styles.body}>
              <Text style={[styles.bodytext, { paddingTop: 5 }]}>{appTexts.ALERT_MESSAGES.error}</Text>
              <Text style={[styles.contenttext, { paddingTop: 8 }]}>{body}</Text>
            </View>
            <View style={{ paddingBottom: '8%' }}>
            <RoundButton buttonPosition={'center'} buttonText={btntext} buttonClick={() => {
                redirect();
               }} />
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

export default NoLongerAvailable;
