import React, { useEffect, useState } from "react";
import {
  View,
  TouchableOpacity,
  Image,
  Text,
  I18nManager,
} from "react-native";
import { styles } from "./styles";
import appTexts from "../../lib/appTexts";
import moment from "moment";

const OfferScreenCard = (props) => {
  const {
    item,
    onMenuCardPress,
    openScanModal,
    postScanItemToAddData,
    postScanItemToAdd,
    from
  } = props;

  const [hideAllLinesHeading, setHideAllLinesHeading] = useState(true);
  const {discount_type, discount_value, price, discount_price, available_time} = item;

  const getPrice = () => {
    if(discount_price == null) {
      return price;
    }
    return discount_price;
  }

  const getDiscount = () => {
    let discount = 0;
    if(discount_type == 'Percentage') {
      discount = (getPrice() / 100) * discount_value;
    } else {
      discount = discount_value;
    }
    discount = getPrice() - discount;

    return discount.toFixed(2);
  }

  const displayTtileoferPrice = getDiscount() + ' ' + (I18nManager.isRTL ? 'ر.س ' : appTexts.STRING.sar);

  useEffect(() => {
    if(postScanItemToAddData && postScanItemToAddData.item_id == item.item_id && postScanItemToAddData.type == 'offer') {
      openScanModal(item);
      postScanItemToAdd({});
    }
  }, []);

  let isMenuOpen = false;
  if(available_time) {
    available_time.map((itm, index) => {
      const format = 'hh:mm A';
      const time = moment();
      const beforeTime = moment(itm.time.start_time, format);
      const afterTime = moment(itm.time.end_time == '00:00' ? '23:59' : itm.time.end_time, format);

      if (time.isBetween(beforeTime, afterTime)) {
        isMenuOpen = true;
      }
    });
  }

  return (
    <View style={[styles.loyaltyCard, !isMenuOpen && {opacity: .5} ]}>
      <TouchableOpacity
        style={styles.middleTabView}
        onPress={() => {
          onMenuCardPress(item.item_id, item, 'offer');
        }}
        disabled={ ! isMenuOpen}
      >
        <View style={styles.foodCard}>
          <Image source={{ uri: item.cover_photo }} resizeMode={'cover'} style={styles.foodImage} />
        </View>
      </TouchableOpacity>

      <View style={[styles.textCard]}>
        <View style={styles.lineThree}>
          <View style={styles.lineOval}>
            <View style={styles.discount}>
              <View style={[styles.line, {maxWidth: '65%'} ]}>
                <TouchableOpacity onPress={() => setHideAllLinesHeading( ! hideAllLinesHeading ) }>
                  <Text numberOfLines={hideAllLinesHeading ? 2 : 10} style={styles.line5}>{item.item_name}</Text>
                </TouchableOpacity>
              </View>
              <Text style={styles.line3}>
                {displayTtileoferPrice}
              </Text>
            </View>
            <TouchableOpacity
              onPress={() => {
                openScanModal(item);
              }}
              disabled={ ! isMenuOpen}
              style={{width: '25%'}}
            >
              <View style={styles.ovala}>
                <Text style={styles.plusa}>+</Text>
              </View>
            </TouchableOpacity>
          </View>
          <Text style={styles.proname}>{item.pro_name}</Text>
          <View style={{flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
            <View>
            <Text style={styles.line4}>
              {appTexts.LOYALTY.day.replace('{{}}', item.remaining_days > 0 ? item.remaining_days : 1)}
            </Text>
            </View>
            { item.pre_order == 'enable' && (
            <View style={styles.ratingpre}>
              <Text style={styles.ratingTextpre}>{appTexts.STRING.preorder}</Text>
              </View>
            )}
          </View>
        </View>
      </View>
    </View>
  );
};

OfferScreenCard.propTypes = {};

export default OfferScreenCard;
