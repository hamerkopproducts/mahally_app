import { StyleSheet, I18nManager } from "react-native";
import { widthPercentageToDP } from "react-native-responsive-screen";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const images = {
  location: require('../../assets/images/listCard/location.png'),
  burger: require('../../assets/images/listCard/freemeal.png'),
};

const styles = StyleSheet.create({
  container: {
    width: '90%',
    height: '30%',
    borderWidth: 0.5,
    borderRadius: 10,
    backgroundColor: 'white',
    marginLeft: '5%',
    marginRight: '5%',
    borderColor: globals.COLOR.greyText,
    marginTop: '2%'
  },
  ovala: {
    borderWidth: 0.5,
    borderColor: globals.COLOR.borderColor,
    height: 35,
    borderRadius: 8,
    top:5,
    alignItems:'center',
    justifyContent:'center',
    width:80,
     },
     touch:{
       width:80,
       height:32,
     },
     middleTabView: {

      justifyContent: 'center',
      alignItems: 'center'
    },
  plusa: {
    fontSize: 20,
    color: globals.COLOR.purple,
  },
  lineOval:{
    flexDirection:'row',
    justifyContent:"space-between",
    width:"100%"
  },
  card: {
    borderRadius: 20,
    width: '90%',
    borderWidth: 0.5,
    borderColor: globals.COLOR.greyText,
    backgroundColor: 'red',
  },
   ratingTextpre:{
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helveticaBold,
        fontSize:9,
        textAlign:'center',
        color:globals.COLOR.white,
    },
  loyaltyCard: {
    flex:1,
    width: widthPercentageToDP('90%'),
    borderRadius: 10,
    backgroundColor: 'white',
    marginLeft: widthPercentageToDP('5%'),
    marginRight: widthPercentageToDP('5%'),
    borderBottomColor: 'white',
    marginBottom: 22,
    justifyContent: 'space-between',
  },
  ratingpre:{
    backgroundColor:globals.COLOR.purple,
    width:100,
    height:16,
    borderRadius:20,
    marginLeft:hp("3.5%"),
    left:4,
    alignSelf:"center",
    justifyContent:'center',
 
},
  foodCard: {
    width: '100%',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    // height: 180
  },
  foodImage: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    width: widthPercentageToDP('90%'),
    height: widthPercentageToDP('45%'),
  },
  textCard: {
    paddingBottom:"5%",
    borderWidth: 0.5,
    borderRadius: 10,
    backgroundColor: 'white',
    borderColor: globals.COLOR.greyText,
    width: '100%',
    borderColor: '#ddd',
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    // elevation: 3,
  },
  discount:{
    width:"75%",
    flexDirection:'row',
    marginTop: 6
  },
  lineOne: {
    marginTop: '3%',
    marginLeft: '3%',
  },
  line: {
    marginHorizontal: '2%',
  },
  headLine: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 14,
    color: globals.COLOR.Text,
    textAlign: 'left',
  },
  lineTwo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: '3%',
    marginRight: '3%',
    alignItems: 'center'
  },
  khalid: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 14,
    color: globals.COLOR.greyText,
  },
  loc: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  location: {
    height: 30,
    width: 30,
  },
  separator: {
    marginTop: '2%',
    marginBottom: '25'
  },
  line: {
    borderWidth: 1,
    borderColor: globals.COLOR.greyBackground,
    borderStyle: 'dashed',
    // marginTop: '1%',
    // marginBottom: '1%',
    // marginLeft: '3%',
    marginRight: '3%',
    borderRadius: 10,
  },
  lineThree: {
    marginLeft: '3%',
    marginRight: '3%',
    flexDirection: 'column',
    // width:'90%',
    // height:'50%',
    //flexWrap:'wrap',
    top:5,
    marginLeft:15,
    justifyContent: 'space-between',
  },
  line3: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabicBold : globals.FONTS.helveticaBold,
    fontSize: 14,
    color: globals.COLOR.Text,
    top: I18nManager.isRTL ? 5 : 0,
  },

  line5: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 13,
    color: globals.COLOR.Text,
    textAlign: 'left',
    textAlignVertical: 'top',
    // height: 40,
    // backgroundColor: 'red'
  },
  line4: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 10,
    color: globals.COLOR.purple,
    
    textAlign: 'left'
  },
  proname:{
   
      fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
      fontSize: 14,
      color: globals.COLOR.Text,
      
      textAlign: 'left'
    
  },
  oval: {
    borderWidth: 0.5,
    borderColor: globals.COLOR.greyText,
    height: 35,
    width: '30%',
    borderRadius: 10,
    //paddingLeft:'10%',
    alignItems: 'center',
    // marginRight:'10%',
    top: '7%',
    right: 38,
    justifyContent: 'center'
  },
  plu: {
    alignSelf: 'center',

  },
  plus: {
    fontSize: 20,
    color: globals.COLOR.purple,
  },
  rateOval: {
    width: '25%',
    height: 42,
    backgroundColor: '#F5A389',
    alignItems: 'center',
    position: 'absolute',
    borderRadius: 10,
    top: -15,
    zIndex: 2,
    right: 10,
  },
  ovalText: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  bur: {
    height: 30,
    width: 30,
    resizeMode: 'contain'
  },
  burger: {
    position: 'absolute',
    zIndex: 2,
    top: 8,
    right: 10,
  },
  no: {
    alignSelf: 'center',
    top: 10,
    right: -18,
  },
  bug: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 16,
    color: 'white'
  },
}
);

export { images, styles };
