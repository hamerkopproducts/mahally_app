import React from "react";
import PropTypes from "prop-types";
import {
  View,
  TouchableOpacity,
  Image,
  Text,
} from "react-native";
import { images, styles } from "./styles";
import appTexts from "../../lib/appTexts";
import DividerLine from "../../components/DividerLine";

const OrderCard = (props) => {
  const { isPastOrders, onOrderCardClick, item } = props;

  let name = '';
  let location = '';
  let order_id = '';
  let order_date = '';
  try {
    name = item.restaurant.lang[0].name;
  } catch(err) {
    name = '';
  }
  try {
    location = item.restaurant.location;
  } catch(err) {
    location = '';
  }
  try {
    order_id = item.order_id;
    order_date = item.order_date;
  } catch(err) {
    order_id = '';
    order_date = '';
  }

  return (
    <TouchableOpacity
      onPress={() => {
        onOrderCardClick(item.id);
      }}
    >
      <View style={styles.OrderCard}>
        <View style={styles.lineOne}>
          <View style={styles.orderNo}>
            <Text style={styles.orderStyle}>{ order_id }</Text>
          </View>
          <View style={styles.dateView}>
            <Text style={styles.dateText}>{ order_date }</Text>
          </View>
        </View>
        <View style={styles.rowTwo}>
          <View style={styles.bugImage}>
            <Image source={{uri: item.restaurant.logos }} style={styles.bug}></Image>
          </View>
          <View style={styles.wrap}>
            <View style={styles.restLine}>
              <Text style={styles.restText}>{name}</Text>
            </View>
            <View style={styles.locStyle}>
              <View style={styles.locIcon}>
                <Image source={images.gps} style={styles.gps} />
              </View>
              <View style={styles.riyadhView}>
                {isPastOrders ? (
                  <Text numberOfLines={1} style={styles.riyadhText}>{location}</Text>
                ) : (
                  <Text numberOfLines={1} style={styles.riyadhTexts}>
                    { location }
                  </Text>
                )}
              </View>
              {isPastOrders ? null : <View style={styles.border}></View>}

              {isPastOrders ? null : (
                <View style={styles.km}>
                  <Text style={styles.kmText}>{appTexts.ORDER.km}</Text>
                </View>
              )}
            </View>
          </View>
        </View>
        <View style={styles.div}>
          <DividerLine dividerStyle={styles.divider} />
        </View>
        {isPastOrders ? (
          <View style={styles.orderView}>
            <Text style={styles.orderText}>{appTexts.ORDER.view}</Text>
            <Image source={images.tag} style={styles.tag} />
          </View>
        ) : (
          <View style={styles.oderViews}>
            {item.order_status_id !=1 &&
              <View style={styles.acceptImage}>
                <View>
                  <Image source={images.accept} style={styles.accep}></Image>
                </View>
                <View style={styles.acceptTextView}>
                  <Text style={styles.acceptText}> {appTexts.ORDER.accept}</Text>
                </View>
              </View>
            }
            {item.order_status_id ==1 &&
              <View style={styles.acceptImage}>
                <View style={{marginRight:6}}>
                  <Image source={images.pend}style={styles.acce}/>
                  </View>
                <View style={styles.acceptTextView}>
                  <Text style={styles.acceptText}>{ item.order_status }</Text>
                </View>
              </View>
            }

            <View style={styles.viewView}>
              <Text style={styles.orderText}>{appTexts.ORDER.view}</Text>
              <Image source={images.tag} style={styles.tag} />
            </View>
          </View>
        )}
      </View>
    </TouchableOpacity>
  );
};
OrderCard.propTypes = {
  isPastOrders: PropTypes.bool,
  orderId: PropTypes.string,
};

export default OrderCard;
