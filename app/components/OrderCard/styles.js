import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals"
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
  } from "react-native-responsive-screen";
const images = {
  location: require('../../assets/images/listCard/location.png'),
  burger: require('../../assets/images/HomeCard/Img1.png'),
  gps:require('../../assets/images/HomeCard/location.png'),
  accept:require('../../assets/images/OrderCard/accept.png'),
  tag:require('../../assets/images/OrderCard/viewmore.png'),
  pend:require('../../assets/images/OrderCard/Pending.png'),
};

const styles = StyleSheet.create({
  
  OrderCard: {
    width:'90%',
    backgroundColor:'white',
    borderWidth:0.5,
    marginTop:'3%',
    marginLeft:'5%',
    marginRight:'5%',
    borderColor: '#ddd',
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
    borderRadius:10,
    paddingLeft:'5%',
    paddingRight:'5%',
    //paddingTop:'3%'
      },
    lineOne:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginTop:'2%'
    },
    orderStyle:{
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiarabicBold  : globals.FONTS.helveticaBold,
        
    },
    dateText:{
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
        color:globals.COLOR.greyText,
        fontSize:12,
    },
    bug:{
        width:55,
        height:55,
        resizeMode:'contain'
    },
    rowTwo:{
        flexDirection:'row',
        marginVertical:'3%',
    },
    restLine:{
      marginLeft:8,
         width:hp('22%'),
        //marginTop:'2%',
        //justifyContent:"center",
       // alignItems:'center'
    },
    pIcon:{
        width:20,
        height:20,
        resizeMode:'contain',
    },
    gps:{
        width:30,
        height:30,
    },
    wrap:{
        lineHeight: Platform.OS === 'ios' ? 20 : null,
        marginLeft:'4%',
        //width:hp('0%'),
        alignItems:'flex-start',
        justifyContent:'flex-start'
    },
    locStyle:{
        flexDirection:'row',
        alignItems:'center',
        
    },
    restText:{
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiarabicBold  : globals.FONTS.helveticaBold,
        fontSize:14,
        color:globals.COLOR.Text,  
        textAlign:'left',
        lineHeight: Platform.OS === 'ios' ? 20 : null
    },
    riyadhView:{
        justifyContent:"flex-start",
     // minWidth:50,
      alignItems:'flex-start',
     // flexShrink:1,
      flexDirection:'row',
     // flex:1.5
    },
    riyadhText:{
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
        fontSize:12,
        color:globals.COLOR.greyText,
        textAlign:'left',
        maxWidth:150,
        flexWrap:'wrap',
        alignItems:'flex-start',
        lineHeight: Platform.OS === 'ios' ? 20 : null
    
    },
    divider:{
        width:'100%',
        height: 0.5,
       backgroundColor: globals.COLOR.borderColor,
      },
    riyadhTexts:{
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
        fontSize:12,
        color:globals.COLOR.greyText,
        textDecorationLine:'underline',
        textAlign:'left',
        flexWrap:'wrap',
        alignItems:'flex-start',
        maxWidth:130,
        //minWidth:1,
        lineHeight: Platform.OS === 'ios' ? 20 : null
        
    
},
km:{
    //marginRight:30
},
border:{
    flexDirection: 'column',
    borderLeftWidth: 0.5,
    height: 10,
    borderLeftColor: globals.COLOR.greyText,
    marginHorizontal:'3%',
    //marginRight:'14%'
},
kmText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.greyText,
    fontSize:12,
},
div:{
    marginVertical:'3%',
    
},
orderView:{
    justifyContent:'center',
    alignItems:'center',
    marginBottom:'5%',
    flexDirection:'row',
},
orderText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.purple,
    fontSize:14, 
},
oderViews:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between',
    marginBottom:'3%',
    
},
leftItems:{
    flexDirection:'row',
    alignItems:'center'
},
acceptImage:{
    marginLeft:0,
    flexDirection:'row',
    alignItems:'center'
},
accep:{
    height:25,
    width:25,
},
acce:{
    height:21,
    width:21,
},
acceptText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.lightBlueText,
    fontSize:14, 
},
viewView:{
   // marginHorizontal:'20%',
    flexDirection:'row',
    alignItems:'center'
},
tag:{
    height:16,
    width:16,
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }] 
},
}
);

export { images, styles };
