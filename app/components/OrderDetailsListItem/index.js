import React, { Component,useState } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image, Text,TextInput } from 'react-native';
import { images, styles } from "./styles";
import appTexts from "../../lib/appTexts";
import DividerLine from "../DividerLine";
import Modal from "react-native-modal";
import LocationList from '../LocationList';
import ShelfList from '../ShelfList';
import WarehouseModal from '../WarehouseModal';
import LocationModal from '../LocationModal';
import ShelfModal from '../ShelfModal';


const OrderDetailsListItem = (props) => {
  const {
    itemData,
    itemOnClick
  } = props;

  const [isWarehouseModalVisible,setIsWarehouseModalVisible] = useState(false);
  const [isLocationModalVisible,setIsLocationModalVisible] = useState(false);
  const [isShelfModalVisible,setIsShelfModalVisible] = useState(false);

  const toggleWarehouseModal = () => {
    setIsWarehouseModalVisible(false)
  };
  const openWarehouseModal = () => {
    setIsWarehouseModalVisible(true)
  };
  const openLocationModal = () => {
    setIsLocationModalVisible(true)
  };

  const toggleLocationModal = () => {
    setIsLocationModalVisible(false)
  };
  const openShelfModal = () => {
    setIsShelfModalVisible(true)
  };

  const toggleShelfModal = () => {
    setIsShelfModalVisible(false)
  };

  let statusTextColor =
    itemData.status === "Pending" 
      ? "#fa7c00"
      : itemData.status === "Mark as Collected"
      ? "#2db881"
      : itemData.status === "In Warehouse"
      ? "#2db881"
      : "none";

  return (
    <TouchableOpacity onPress={() => { itemOnClick(itemData)}}>
    <View style={styles.rowContainer}>
      <View style={styles.rowItemContainer}>
      <View style={styles.orderImageContainer}>
        <Image source={images.itemIcon} style={styles.itemImage}/>
      </View>
      <View style={styles.orderDataContainer}>
        <View style={styles.labelContainer}>
            <Text style={styles.mediumLabel}>{itemData.itemCategory}</Text>
        </View>
        <View style={styles.middleLabelContainer}>
            <Text style={styles.mediumLabel}>{itemData.itemName}</Text>
            <Text style={styles.lightLabel}>{itemData.weight + " " + itemData.unit}</Text>
        </View>
        </View>
    </View>
      <View style={styles.borderLine}></View>
       {itemData.status === "Pending" && (  
      <View style={styles.lastLine}>
        <View style={styles.leftView}>
        
        <Image source={images.pending}style={styles.pendingStyle}/>
        <View style={{paddingLeft:'2%'}}>
        <Text style={[styles.pendingText, { color: statusTextColor }]}>{appTexts.STRING.Pending}</Text>
        </View>
        </View>
        <View style={styles.ovalView}>
          <Text style={styles.mark}>{appTexts.STRING.markCollected}</Text>
        </View>
        
      </View>
        )} 
      {itemData.status === "Mark as Collected" && (
        <View style={styles.lastLine}>
        <View style={styles.leftView}>
        
        <Image source={images.pending}style={styles.pendingStyle}/>
        <View style={{paddingLeft:'2%'}}>
        <Text style={[styles.pendingText, { color: statusTextColor }]}>{appTexts.STRING.markCollected}</Text>
        </View>
        </View>
        <TouchableOpacity onPress={() => {openWarehouseModal();}}>
        <View style={styles.ovalView}>
          <Text style={styles.mark}>{appTexts.STRING.warehouse}</Text>
        </View>
        </TouchableOpacity>
        
        {isWarehouseModalVisible && 
          <WarehouseModal
            isWarehouseModalVisible={isWarehouseModalVisible} 
            toggleWarehouseModal={toggleWarehouseModal} 
            setIsLocationWarehouse={() => setIsLocationModalVisible(true)}
            setIsShelfWarehouse={() => setIsShelfModalVisible(true)}
          />
        }
        {isLocationModalVisible && 
        <LocationModal toggleLocationModal={toggleLocationModal} isLocationModalVisible={isLocationModalVisible}/>}
        {isShelfModalVisible && 
        <ShelfModal isShelfModalVisible={isShelfModalVisible} toggleShelfModal={toggleShelfModal}/>}
            
      </View>
      
      )}
      {itemData.status === "In Warehouse"  && (
        <View style={styles.lastLine}>
          <TouchableOpacity  >
        <View style={styles.leftView}>
        
        <Image source={images.pending}style={styles.pendingStyle}/>
        <View style={{paddingLeft:'2%'}}>
        <Text style={[styles.pendingText, { color: statusTextColor,fontSize:12 }]}>{appTexts.STRING.warehouse}</Text>
        </View>
        </View>
        </TouchableOpacity>

        <TouchableOpacity onPress = {() => {openLocationModal();}}>
        <View style={styles.leftView}>
        
        <Image source={images.pending}style={styles.pendingStyle}/>
        <View style={{paddingLeft:'2%'}}>
        <Text style={[styles.pendingText, { color: '#40475a',fontSize:12 }]}>{'Al Jahrs'}</Text>
        </View>
        </View>
        </TouchableOpacity>

        <TouchableOpacity onPress = {() => {openShelfModal();}}>
        <View style={styles.leftView}>
        <Image source={images.pending}style={styles.pendingStyle}/>
        <View style={{paddingLeft:'2%'}}>
        <Text style={[styles.pendingText, { color: '#40475a' ,fontSize:12}]}>{'Shelf ID 243'}</Text>
        </View>
        </View>
        </TouchableOpacity>
        
        
               
        </View>
      )}
    </View>
    </TouchableOpacity>
  );
};
OrderDetailsListItem.propTypes = {
  itemData: PropTypes.object,
  itemOnClick:PropTypes.func,
};

export default OrderDetailsListItem;