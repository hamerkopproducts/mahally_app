import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const images = {
  itemIcon: require("../../assets/images/temp/main.png"),
  pending: require('../../assets/images/listCard/pending.png'),
  varrow:require('../../assets/images/profileicon/arrow.png'),
  search:require('../../assets/images/listCard/Search.png'),
};

const styles = StyleSheet.create({
  rowContainer: {
    width: '100%',
    backgroundColor: globals.COLOR.headerColor,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft:'1%',
    marginRight:'1%',
    marginTop:'5%',
     borderWidth:0.5,
   borderColor:'grey',
   alignSelf:'center'
   
  },
  rowItemContainer: {
    width: '100%',
    backgroundColor: globals.COLOR.headerColor,
    justifyContent: 'center',
    alignItems: 'flex-start',
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom:10,
    paddingLeft:'3%',
    paddingRight:'3%',
  },
  orderImageContainer: {
    paddingTop: 10,
    width: '25%',
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  itemImage:{
    marginLeft:0,
    width:70,
    height: 70,
    borderRadius:10,
  },
  orderDataContainer: {
    paddingTop:10,
    width: '75%',
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  labelContainer: {
    paddingBottom:5,
    width:"100%",
   flexDirection:'row'
  },
  middleLabelContainer:{
    paddingBottom: 15,
    width: "100%",
    flexDirection: 'row'
  },
  mediumLabel: {
    textAlign: 'left',
    color: globals.COLOR.textColor,
    fontFamily: globals.FONTS.openSansSemiBold,
    fontSize: 14
  },
  lightLabel: {
    marginLeft: 30,
    //textAlign: 'left',
    color: globals.COLOR.textColor,
    fontFamily: globals.FONTS.avenirLight,
    fontSize: 14,
    //paddingTop:'1%'
  },
  leftLabel: {
    textAlign: 'left',
    color: globals.COLOR.textColor,
    fontFamily: globals.FONTS.avenirHeavy,
    fontSize: 12
  },
  middleLabel: {
    marginLeft:20,
    textAlign: 'left',
    color: globals.COLOR.textColor,
    fontFamily: globals.FONTS.avenirHeavy,
    fontSize: 12
  },
  rightLabel:{
    position:'absolute',
    right:0,
    textAlign: 'left',
    color: globals.COLOR.textColor,
    fontFamily: globals.FONTS.avenirHeavy,
    fontSize: 12
  },
  borderLine:{
    borderWidth:0.5,
    borderColor:globals.COLOR.borderColor,
    width:'100%',
   // paddingLeft:'20%',
    //paddingRight:'5%',
    marginTop:'2%',
    borderStyle:'dashed',
    borderRadius:1,
    justifyContent:'center',
    alignItems:'center',
    alignSelf:'center',
    //marginLeft:20,
    //paddingBottom:'5%'
    
  },
  lastLine:{
    flexDirection:'row',
    justifyContent:'space-between',
    //alignItems:'sp',
    paddingBottom:'4%',
    paddingTop:'3%',
    width:'100%',
    paddingRight:'2%',
    paddingLeft:'2%'
  },
  pendingStyle:{
    alignSelf:'center',
    width:20,
    height:20,
    //paddingRight:'3%'
  },
  pendingText:{
    fontFamily: globals.FONTS.openSansSemiBold,
    fontSize: 14,
    color:'#fa7c00'

    
  },
  leftView:{
    alignSelf:'flex-start',
    flexDirection:'row',
    paddingTop:'2%'
  },
  ovalView:{
    width:150,
    height:35,
    borderRadius:20,
    borderColor:'#00836f',
    borderWidth:0.5,
    //marginLeft:'15%',
  },
  mark:{
  paddingTop:'4%',
  paddingLeft:'12%',
  fontFamily: globals.FONTS.openSans,
    fontSize: 14,
    color:'#2db881'

  },
  modalMaincontentHelp: {
    //justifyContent: "center",
    justifyContent:'flex-end',
    margin: 0,
  },
  
  modalmainviewHelp: {
    backgroundColor: "white",
    //width: wp("90%"),
    padding: "4%",
    //borderRadius: 10,
    borderTopRightRadius:15,
    borderTopLeftRadius:15,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  helpModalView:{
    
    flexDirection: "row",
    //justifyContent: "center",
    justifyContent: "space-around",
    alignItems: "center",
    paddingBottom: "3%",
    paddingLeft: "10%",
    paddingRight:'10%'

},
helptextWarpper:{
  flexDirection:'row',
  justifyContent:'center',
  paddingTop:hp('2%'),
  paddingBottom:hp('1.5%'),
  paddingLeft:'10%',
  paddingRight:'10%'
  //paddingLeft:'19%'
  },
  helpheadText:{
    fontSize: hp('2.6%'),
    fontFamily: globals.FONTS.avenirHeavy,
    textAlign:'center'
  },
  helpsubjectWrapper:{
    //paddingTop:hp('2%'),
    backgroundColor:'pink'
        },
    helpsubText:{
      fontSize: hp('2.1%'),
      color:"#6f6f6f",
      paddingLeft:'1%',
      fontFamily: globals.FONTS.openSansSemiBold,
    },
    hinput: {
      //marginTop:-4
      // margin: 15,
      // height: 40,
      // borderColor: '#7a42f4',
      // borderWidth: 1
      fontFamily: globals.FONTS.openSansLight,
      fontSize: hp('2%'),
      textAlign: I18nManager.isRTL ? "right" : "left",
      color:"#707070",
      marginLeft:'2%'
      
    },
    helpmessageWrapper:{
      paddingTop:hp('2%'),
    },
    helpbuttoninsidewrapper: {
      flexDirection:  "row",
      justifyContent: "center",
      paddingTop: "3%",
      paddingBottom: "7%",
      width: '85%',
      height: 50,
      //borderRadius: 15,
      alignItems: "center",
      alignSelf:'center',
      marginTop: '10%',
      backgroundColor:'#2eb781'
    },
    helpbuttoninside: {
      flexDirection:  "row",
      justifyContent: "center",
      paddingTop: "3%",
      paddingBottom: "7%",
      width: '85%',
      height: 50,
      //borderRadius: 15,
      alignItems: "center",
      alignSelf:'center',
      //marginTop: '10%',
      backgroundColor:'#2eb781'
    },
    helpbuttononview: {
      alignItems: "center",
      justifyContent: "center",
      marginTop: hp('2.5%'),
      // width: 140,
      // height: 50,
      // borderRadius: 15,
      //color:'#2eb781'
  
  },
  hylpyButtonon:{
    width: 140,
      height: 50,
      borderRadius: 15,
      alignItems: "center",
      justifyContent: "center",
      // shadowOpacity: 0.58,
      // shadowRadius: 16.00,
      // elevation:5,
      // borderColor:'#d0d0d0',
      // borderWidth:1
  },
  helpyellowbuttonText:{
    fontSize: hp('2.1%'),
    fontFamily: globals.FONTS.avenirMedium,
    color:'white'
  },
  buttohelpnwrapper: {
    flexDirection: "row",
    paddingLeft:'12%'
    //paddingRight:'4%'
    //paddingLeft:'30%'
    //backgroundColor:'red',
    //alignContent:'flex-end'
    //alignItems: 'center'
    //alignSelf:"flex-end",
    //paddingRight:'10%'
     //justifyContent:'flex-end'
  
  },
  boxView:{
    height:60,
    width:'95%',
    borderWidth:0.5,
    borderColor:'#707070',
    flexDirection:'row',
    justifyContent:'space-between',
    marginTop:'5%',
    //marginLeft:'5%',
    marginRight:'5%'
     },
  boxViews:{
    height:60,
    width:'95%',
    borderWidth:0.5,
    borderColor:'#707070',
    flexDirection:'row',
    justifyContent:'space-between',
    //backgroundColor:'pink',
    marginTop:'8%',
    
     },
     boxVies:{
      height:50,
      width:'95%',
      borderWidth:0.5,
      borderColor:'#707070',
      flexDirection:'row',
      justifyContent:'space-between',
            marginTop:'8%',
      borderRadius:5,
      
    marginLeft:'3%'
       },
  varrow:{
    width:15,
    height:15,
    alignSelf:'center'
  },
  search:{
    width:20,
    height:20,
    alignSelf:'center'
  },
  arrowv:{
marginRight:'4%',
alignItems:'center',
justifyContent:'center'
  },
  listV:{
  marginTop:"5%"
  },
});

export { images, styles };
