import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image, Text, FlatList,TextInput} from 'react-native';
import { images, styles } from "./styles";
import appTexts from "../../lib/appTexts";
import globals from "../../lib/globals";
import {useDispatch, useSelector} from 'react-redux';

const PayCard = (props) => {
    const dispatch = useDispatch();
    const {settingsData, settingsError, isSettingsLoading} = useSelector(
      state => state.settingsReducer,
    );
  const {
onCounterpayPress,

  } = props;
  const payOptions = settingsData?.data?.payment_options;
  let payOptionPardsed = {};
  try {
    payOptionPardsed = JSON.parse(payOptions);
  } catch (err) {
    payOptionPardsed = {};
  }
 
  const counter = payOptionPardsed?.counter;
  const online = payOptionPardsed?.online;
  
  return (
    <View style={styles.cardView}>
        <View style={styles.select}>

          <Text style={styles.selectText}>{appTexts.PAY.select}</Text>
          
        </View>
        {online == 'Y' && (<View style={styles.boxView}>
           <View style={styles.img1}>
               <Image source={images.img1} style={styles.payImg}/>
           </View>
           <View style={styles.payText}>
               <Text style={styles.textPay}>{appTexts.PAY.online}</Text>
           </View>
           <View style={styles.arrow}>
               <Image source={images.arrow}style={styles.arrowImg}/>
           </View>
        </View>
         )}
       
       {counter == 'Y' && (
        <TouchableOpacity onPress={()=>{onCounterpayPress();}}>
        <View style={styles.boxView}>
           <View style={styles.img1}>
               <Image source={images.img2} style={styles.payImg}/>
           </View>
           <View style={styles.payText}>
               <Text style={styles.textPay}>{appTexts.PAY.counter}</Text>
           </View>
           <View style={styles.arrow}>
               <Image source={images.arrow}style={styles.arrowImg}/>
           </View>
        </View>
        </TouchableOpacity>
          )}
    </View>
  );
};
PayCard.propTypes = {
isPastOrders:PropTypes.bool,
orderId:PropTypes.string,
openThankyouModal:PropTypes.func,
};

export default PayCard;