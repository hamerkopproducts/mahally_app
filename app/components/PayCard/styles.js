import { StyleSheet, I18nManager, Platform } from "react-native";
import globals from "../../lib/globals"

const images = {
  img1:require('../../assets/images/Pay/online.png'),
  arrow:require('../../assets/images/Pay/arrow.png'),
  img2:require('../../assets/images/Pay/counter.png'),
};

const styles = StyleSheet.create({
 boxView:{
     borderWidth: Platform.OS == 'ios' ? .25 : 0.2,
     borderColor: 'lightgray',
     width:'100%',
    alignItems:'center',
     flexDirection:'row',
     height:50,
     marginTop:'5%'
 } ,
 payImg:{
     width:30,
     height:30,
     resizeMode:'contain'
 },
 img1:{
     marginLeft:'5%'
 },
 payText:{
     marginLeft: 10,
     width:'40%'
 },
 arrowImg:{
     width:16,
     height:16,
     resizeMode:'contain',
     transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
 },
 arrow:{
     marginLeft:'34%'
    
 },
 cardView:{
     marginBottom:'10%'
 },
 selectText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.Text,
    fontSize:14,
    textAlign:'left'
 },
 textPay:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.Text,
    fontSize:12,
    textAlign:'left'   
 }
  

}
);

export { images, styles };
