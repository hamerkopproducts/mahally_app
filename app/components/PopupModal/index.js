import PropTypes from 'prop-types';
import React from 'react';
import { Modal, View, Text, TouchableOpacity,Image } from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import { images, styles } from "./styles";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";

import RoundButton from "../RoundButton";
const PopupModal = (props) => {

  const {
    popupIndex,
      isPopupVisible,
      popupNoClick,
      popupYesClick
      } = props;

  
  return (
    <Modal
            animationType="fade"
            transparent={true}
            onRequestClose={() => null}
            visible={isPopupVisible}>
            <View style={styles.popupContainer}>
              <View style={styles.popupBackground} />
        <View style={styles.popupContentContainer}>
          <View style={styles.popupDialogContainer}>
            <View style={styles.popupContent}>
              <View style={styles.popupIconContainer}>
                <Image source={popupIndex === 0 ? images.modal1Icon : images.modal2Icon} style={styles.modalIcon}/>
              </View>
              <View style={styles.popupTextContainer}>
                <Text style={styles.popupDescription}>{popupIndex === 0 ? appTexts.COMMON_POPUP.modalText1 : popupIndex === 1 ? appTexts.COMMON_POPUP.modalText2 : appTexts.COMMON_POPUP.modalText3}</Text>
              </View>
            <View style={styles.popupButtonContainer}> 
                <RoundButton buttonPosition={'left'} buttonText={appTexts.COMMON_POPUP.No} buttonClick={popupNoClick}/>
                <RoundButton buttonPosition={'right'} buttonText={appTexts.COMMON_POPUP.Yes} buttonClick={popupYesClick}/>
            </View>
             </View>
          </View>
              </View>
            </View>
  </Modal>
  );
};

PopupModal.propTypes = {
  popupIndex: PropTypes.number,
  isPopupVisible: PropTypes.bool,
  popupNoClick: PropTypes.func,
  popupYesClick: PropTypes.func
};

export default PopupModal;
