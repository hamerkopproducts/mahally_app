import { StyleSheet } from "react-native";
import globals from "../../lib/globals"

const images = {
  modal1Icon: require("../../assets/images/modal/modal1.png"),
  modal2Icon: require("../../assets/images/modal/modal2.png"),
};

const styles = StyleSheet.create({
	popupContainer:{
      flex:1,
      flexDirection: 'column',
      backgroundColor: globals.COLOR.transparent
    },
    popupBackground:{
    	flex: 1,
    	flexDirection: 'column',
    	backgroundColor: globals.COLOR.black,
    	opacity:0.7,
    	zIndex: 1
    },
    popupContentContainer:{
    	top:0,
    	position:'absolute',
    	flex: 1,
    	width: '100%',
    	height: '100%',
    	flexDirection: 'column',
    	alignItems: 'center',
    	justifyContent: "center",
    	zIndex: 3
    },
    popupDialogContainer:{
        zIndex:5,
        width:"88%",
        borderRadius:10,
      backgroundColor: globals.COLOR.white,
      alignItems: 'center',
      justifyContent: "center",
       
    },
  popupContent:{
    marginBottom:40,
    marginTop:40,
    width:"80%",
    alignItems: 'center',
    justifyContent: "center",
  },
  popupIconContainer: {
    alignItems: 'center',
    justifyContent: "center",
  },
  popupTextContainer: {
    alignItems: 'center',
    justifyContent: "center",
  },
  modalIcon:{
    width: 180,
    height:180
  },
    popupDescription:{
        marginTop: 14,
        textAlign: 'center',
        fontFamily: globals.FONTS.avenirMedium,
        fontSize:16
       
    },
    popupButtonContainer:{
        marginTop: 24,
        marginBottom:10,
        width:'100%',
        height: 42,
        flexDirection: 'row'
    },
    popupButton:{
        flex: 1,
        alignItems: 'center',
        justifyContent: "center"
    },
});

export { images, styles  };
