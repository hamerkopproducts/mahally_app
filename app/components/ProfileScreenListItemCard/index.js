import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image, Text } from 'react-native';
import { images, styles } from "./styles";
import appTexts from "../../lib/appTexts";

const ProfileScreenListItemCard = (props) => {
  const {
    itemData,
    tabIndex,
    itemOnPress,
    cardLeftButtonClick,
    cardRightButtonClick,
    checkBoxClick,
  } = props;

  return (
    <TouchableOpacity style={styles.rowContainer} onPress={() => { itemOnPress(itemData)}}>
      <View style={{ flexDirection: "row" }}>
        <View style={styles.imagemainWrapper}>
          <Image
            source={require("../../assets/images/profileicon/faq.png")}
            style={styles.imageMain}
          />
        </View>
        <View style={styles.contentsWrapper}>
          <Text style={styles.contentHeading}>
            {appTexts.PROFILELISTING.Faq}
          </Text>
        </View>
      </View>
      <View style={styles.arrowWrapper}>
        <Image
          source={require("../../assets/images/profileicon/forward_pink.png")}
          style={styles.arrowImage}
        />
      </View>
    </TouchableOpacity>
  );
};
ProfileScreenListItemCard.propTypes = {
  itemData: PropTypes.object,
  itemOnPress: PropTypes.func,
  cardLeftButtonClick: PropTypes.func,
  cardRightButtonClick: PropTypes.func,
  checkBoxClick: PropTypes.func,
  tabIndex: PropTypes.number
};

export default ProfileScreenListItemCard;