import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity
} from "react-native";

import { styles } from "./styles";
import appTexts from "../../lib/appTexts";

const PromoCode = (props) => {

  const { coupon_code, validateCouponCode, code, setCode } = props;

  useEffect(() => {
    if(code != coupon_code) {
      setCode(coupon_code);
    }
  }, [coupon_code])

  return (
    <View style={styles.OrderCard}>
      <View style={styles.lineOne}>
        <Text style={styles.apply}>{appTexts.PROMO.apply}</Text>
      </View>
      <View style={styles.textBox}>
        <TextInput
          style={styles.input}
          placeholder={appTexts.PROMO.place}
          value={code}
          onChangeText={(txt) => setCode(txt) }
        />
        
        <TouchableOpacity style={styles.applyBox} onPress={() => validateCouponCode(code) }>
          <Text style={styles.applyText}>{appTexts.PROMO.app}</Text>
        </TouchableOpacity>

      </View>
    </View>
  );
};

PromoCode.propTypes = {
  isPastOrders: PropTypes.bool,
  orderId: PropTypes.string,
};

export default PromoCode;
