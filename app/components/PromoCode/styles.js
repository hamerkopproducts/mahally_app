import { StyleSheet, I18nManager, Platform } from "react-native";
import globals from "../../lib/globals"

const images = {
  
};

const styles = StyleSheet.create({
  
  OrderCard: {
    width:'90%',
    backgroundColor:'white',
    borderWidth:0.5,
    //marginTop:'3%',
    marginLeft:'5%',
    marginRight:'5%',
    borderColor: '#ddd',
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
    borderRadius:10,
    paddingLeft:'5%',
    paddingRight:'5%',
    //paddingTop:'3%'
      },
      lineOne:{
          marginTop:'5%'
      },
      textBox:{
          width:'100%',
          height:50,
          borderRadius: 10,
          borderWidth: Platform.OS == 'ios' ? 0.6 : 0.2,
          marginTop:'5%',
          marginBottom:'5%',
          paddingLeft:'3%',
          paddingRight:'3%',
          borderColor: globals.COLOR.purple,
          justifyContent: 'center',
          flexDirection: 'row'
      },
      applyBox:{
          width: '30%',
          height:40,
          borderWidth:0.5,
          borderRadius:10,
          backgroundColor: globals.COLOR.purple,
          // position:'absolute',
          top:5,
          right:4,
          alignItems:'center',
          justifyContent:'center'
      },
      applyText:{
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
        color:'white',
        fontSize:12,
        textAlign:'left',
      },
      apply:{
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
        color:globals.COLOR.Text,
        fontSize:14,
        textAlign:'left'
      },
      input:{
        fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
        color:globals.COLOR.Text,
        fontSize:14,
        textAlign: I18nManager.isRTL ? "right" : "left",
        width: '70%'
      }

}
);

export { images, styles };
