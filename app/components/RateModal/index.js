import React from "react";
import { images, styles } from "./styles";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  TextInput,
  I18nManager
} from "react-native";

import appTexts from "../../lib/appTexts";
import PropTypes from "prop-types";
import Modal from "react-native-modal";
import Rating from "../../components/Rating/Rating";

const RateModal = (props) => {
  const { isRateModalVisible, segments, openRateModal, onPress, backDropOpacity } = props;

  return (
    <Modal
      transparent={true}
      animationIn="slideInUp"
      animationOut="slideOutRight"
      isVisible={isRateModalVisible}
      style={styles.modalMaincontentLogout}
      backdropOpacity={backDropOpacity}
    >
      <View style={styles.modalmainviewLogout}>
        <TouchableOpacity
          style={styles.buttohelpnwrapper}
          onPress={() => {
            openRateModal();
          }}
        >
          <Text style={styles.xText}>X</Text>
        </TouchableOpacity>
        <View style={styles.headingLine}>
          <Text style={styles.head}>{appTexts.RATE.head}</Text>
        </View>
        <View style={styles.line}></View>

        <FlatList
          data={ segments }
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item}) => {
            const lang = I18nManager.isRTL ? 'ar' : 'en';
            let name = '';
            const id = item.id;
            try {
              name = item.lang[lang].name;
            } catch(err) {}
            return (
            <View style={styles.lineOne}>
              <View style={styles.leftContainer}>
                <Text style={styles.rate}>{ name }</Text>
              </View>
              <View style={styles.rateContainer}>
                <Rating
                  is_clickable={true} 
                  half_star={false} 
                  onPress={(val) => onPress(id, val, 'rate')}
                />
              </View>
            </View>
          )
          }}
        />
        
        <View style={styles.comment}>
          <Text style={styles.commentStyle}>{appTexts.RATE.line4}</Text>
        </View>
        <View style={styles.box}>
          <TextInput 
          style={styles.input} 
          onChangeText={(txt) => onPress(0, txt, 'message') }
          />
        </View>
        <View style={styles.buttonView}>
          <TouchableOpacity style={styles.viewButton} onPress={() => onSubmitRating()}>
            <View style={styles.textStyle}>
              <Text style={styles.buttonText}>{appTexts.RATE.send}</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

RateModal.propTypes = {
  isRateModalVisible: PropTypes.bool,

  openRateModal: PropTypes.func,
};

export default RateModal;
