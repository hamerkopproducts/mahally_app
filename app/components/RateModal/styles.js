import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const images = {
  star: require("../../assets/images/modal/str.png"),
};

const styles = StyleSheet.create({
  modalMaincontentLogout: {
    justifyContent: "center",
    alignItems: 'center',

  },
  modalmainviewLogout: {
    backgroundColor: "white",
    //width: wp("90%"),
    //padding: "4%",
    //flex:1,
    paddingLeft: '6%',
    paddingRight: '5%',
    // flex:0.5,
    width: 330,
    borderRadius: 15,
    justifyContent: 'center',
    //alignItems:'center',
    //borderRadius: 10,
    // borderTopRightRadius:15,
    // borderTopLeftRadius:15,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  xText: {
    fontSize: 20,
    color: globals.COLOR.greyText,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },

  buttohelpnwrapper: {
    flexDirection: "row",
    justifyContent: 'flex-end',
    paddingTop: '5%',
  },
  line:{
      width:20,
      borderWidth:1.4,
      borderRadius:5,
      borderColor:globals.COLOR.purple,
  },
  lineOne:{
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center',
      marginTop:'4%',
  },
  rateContainer:{
      flexDirection:'row',
  },
  starStyle:{
      height:25,
      width:25,
      resizeMode:'contain'
  },
  head:{
    fontSize: 15,
    color: globals.COLOR.Text,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica, 
    textAlign:'left'
  },
  rate:{
    fontSize: 13,
    color: globals.COLOR.greyText,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    textAlign:'left'
  },
  comment:{
      marginTop:'4%',
      marginBottom: 5
  },
  commentStyle:{
    fontSize: 14,
    color: globals.COLOR.Text,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    textAlign:'left'
  },
  box:{
      width:'100%',
      height:120,
      borderRadius:5,
      borderWidth:0.5,
      marginTop:'2%',
      marginBottom:'5%',
      paddingLeft:'3%',
paddingRight:'3%',
  },
  buttonView:{
    marginTop:'6%',
    marginBottom:'6%',
    alignItems:'center'
   
    
},
viewButton:{
  width:'80%',
  height:50,
  backgroundColor:globals.COLOR.purple,
  borderRadius:10,
},
buttonText:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  fontSize: 14,
  color:'white',
},
textStyle:{
  alignItems:'center',
  justifyContent:'center',
  marginTop:'6%',
},
input:{
  textAlign: I18nManager.isRTL ? "right" : "left",
  width:'100%',
  height:70,
},



});

export { images, styles };