import React from 'react';
import { StyleSheet, View, I18nManager } from 'react-native';
import Stars from 'react-native-stars';
import Icon from 'react-native-vector-icons/MaterialIcons';
import globals from "../../lib/globals";

const Rating = ({ style, rating, is_clickable, half_star, onPress }) => {

  const touchable = is_clickable ? false : true;
  const can_half_star = half_star ? true : false;
  try {
    if (typeof rating != 'undefined') {
      rating -= rating % .5 > 0 ? ( (rating % .5)) : 0;
      rating = parseFloat(rating);
    }
  } catch (error) {
    
  }

  return (
    <View style={[
      style && style,
      { transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }] }
    ]}>
      <Stars
        spacing={3}
        disabled={touchable}
        default={rating}
        count={5}
        half={can_half_star}
        fullStar={
          <Icon name={'star'} solid={true} style={[styles.myStarStyle]} />
        }
        update={(val) => { onPress(val) }}
        emptyStar={
          <Icon
            name={'star-border'}
            style={[styles.myStarStyle, styles.myEmptyStarStyle]}
          />
        }
        halfStar={<Icon name={'star-half'} style={[styles.myStarStyle, { transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }] }]} />}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  myStarStyle: {
    color: '#FFD700',
    backgroundColor: 'transparent',
    fontSize: 20,
    marginRight: 0,
  },
  myEmptyStarStyle: {
    color: '#FFD700'
  },
});

export default Rating;
