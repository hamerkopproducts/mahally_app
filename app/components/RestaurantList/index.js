import React, { Component, useState } from 'react';
import PropTypes from 'prop-types';
import { View, FlatList, Text, Image, TouchableOpacity } from 'react-native';
import { images, styles } from "./styles";
import CheckBox from '../../components/CheckBox';
import appTexts from "../../lib/appTexts";




const RestaurantList = (props) => {
      const {
        restTypes,
        pushTypesId,
        restTypesFilter
    } = props;
   let typestemp= [];

   const pushTypes = (id) => { 
        typestemp.push(id)
        pushTypesId(id)
    };

    const ItemList = ({ item }) => {

        return (
            <CheckBox
                pushTypesId={pushTypes}
                restTypesFilter={restTypesFilter}
                typestemp={typestemp}
                item={item} 
            />
        );
    }; 

   
    return (
        <View style={styles.mainScreen}>
            <FlatList
                data={restTypes}
                renderItem={ItemList}
                keyExtractor={(item, index) => index.toString()}
            />
        </View>
    );
};
RestaurantList.propTypes = {
    pushTypesId: PropTypes.func,
    pullTypesId: PropTypes.func,
};


export default RestaurantList;
