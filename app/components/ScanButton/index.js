import PropTypes from "prop-types";
import React, { useState } from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import { styles, images } from "./styles";
import appTexts from "../../lib/appTexts";
import CartModal from "../CartModal";

const ScanButton = (props) => {
  const { redirectToScan } = props;

  return (
    <TouchableOpacity
    onPress={() => {
      redirectToScan(true);
    }}
  >
    <View style={styles.buttonStyle}>
        
      <View style={styles.scanView}>
     
          <Image source={images.scan} style={styles.scanImage}></Image>
      
      </View>

      <View style={styles.txtLow}>
        <Text style={styles.buttonText}>{appTexts.SCAN.text}</Text>
      </View>
    </View>
    </TouchableOpacity>
  );
};

ScanButton.propTypes = {};

export default ScanButton;
