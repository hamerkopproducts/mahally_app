import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"

const images = {
   scan:require("../../assets/images/footerTabItem/scan.png"),
    
  };
const styles = StyleSheet.create(
    
    
 {

  buttonStyle:{
    backgroundColor : Platform.OS === 'ios' ? 'white' : "#0000" ,
     width:globals.SCREEN_SIZE.width,
     height: 90,
     borderColor:'lightgrey',
     borderBottomColor:'white',
     borderBottomWidth:0.2,
     borderTopLeftRadius:25,
     borderTopRightRadius:25,
     justifyContent:'center',
     alignItems:'center',
     shadowOffset: { width: 1, height: 1 },
    // shadowColor: 'black',
     shadowOpacity: 2,
    //  elevation: 3,
    // shadowColor: "black",
    // shadowRadius: 5.26,
    // shadowOffset: {
    //   width: 1,
    //   height: 1,
    // },
    // shadowOpacity: 8.2,
    elevation: Platform.OS === 'ios' ? 5 : 3,
  },

  scanImage:{
      resizeMode:'cover',
      height:71,
      width:71,
    
       
  },
  txtLow:{
    paddingTop:'3%',
  },
   scanView:{
    position: 'absolute',
    alignSelf: 'center',
    backgroundColor: '#F0F1F0',
    width: 72,
    height: 72,
    borderRadius: 38,
    bottom: 55,
    zIndex: 10
   },
  buttonText:{
    fontSize: 13,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    textAlign:'center',
    color:globals.COLOR.lightBlueText,
  }
 
  
});

export { styles,images  };