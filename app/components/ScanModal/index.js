import React from "react";
import { images, styles } from "./styles";
import { View, Text, TouchableOpacity, Image } from "react-native";

import appTexts from "../../lib/appTexts";
import PropTypes from "prop-types";
import Modal from "react-native-modal";

const ScanModal = (props) => {
  const { isScanModalVisible, toggleScanModal, onMenuCardPress, showPre } = props;

  return (
    <Modal
      isVisible={isScanModalVisible}
      style={styles.modalMaincontentHelp}
      animationIn="slideInUp"
      animationOut="slideOutRight"
      onSwipeComplete={toggleScanModal}
      swipeDirection={["left", "right", "up", "down"]}
    >
      <View style={styles.modalmainviewHelp}>
        <TouchableOpacity
          onPress={() => {
            toggleScanModal();
          }}
        >
          <View style={styles.clo}>
            <Text style={styles.x}>X</Text>
          </View>
        </TouchableOpacity>

        <View style={styles.headingLine}>
          <View style={styles.helptextWarpper}>
            <Text style={styles.helpheadaText}>{appTexts.SCANA.prefer}</Text>
          </View>
        </View>
        
        <View style={styles.formWrapper}>
          <TouchableOpacity
            onPress={() => {
              onMenuCardPress('dine');
            }}
          >
            <View style={styles.wholeView}>
              <View style={styles.Outerview}>
                <Image style={styles.DineImage} source={images.dine} />
                <View style={styles.doubText}>
                  <Text style={styles.helpheadText}>{appTexts.SCANA.dine}</Text>
                </View>
              </View>
              <View style={styles.imgstyle}>
                <Image source={images.more} style={styles.arrowImage} />
              </View>
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.formWrapper}>
          <TouchableOpacity
            onPress={() => {
              onMenuCardPress('take');
            }}
          >
            <View style={styles.wholeView}>
              <View style={styles.Outerview}>
                <Image style={styles.DineImage} source={images.take}></Image>
                <View style={styles.doubText}>
                  <Text style={styles.helpheadText}>{appTexts.SCANA.take}</Text>
                </View>
              </View>
              <View style={styles.imgstyle}>
                <Image source={images.more} style={styles.arrowImage}></Image>
              </View>
            </View>
          </TouchableOpacity>
        </View>

        {showPre &&
          <View style={styles.formWrapper}>
            <TouchableOpacity
              onPress={() => {
                onMenuCardPress('pre');
              }}
            >
              <View style={styles.wholeView}>
                <View style={styles.Outerview}>
                  <Image style={styles.DineImage} source={images.pre}></Image>
                  <View style={styles.doubText}>
                    <Text style={styles.helpheadText}>{appTexts.SCANA.pre}</Text>
                  </View>
                </View>
                <View style={styles.imgstyle}>
                  <Image source={images.more} style={styles.arrowImage}></Image>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        }

      </View>
    </Modal>
  );
};

ScanModal.propTypes = {
  toggleScanModal: PropTypes.func,
  isScanModalVisible: PropTypes.bool,
};

export default ScanModal;
