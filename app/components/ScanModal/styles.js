import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const images = {
  dine:require("../../assets/images/Icons/DineIn.png"),
  take:require("../../assets/images/Icons/Takeaway.png"),
  more:require("../../assets/images/Icons/viewmore.png"),
  pre:require("../../assets/images/Icons/pre.png"),
};

const styles = StyleSheet.create({
modalMaincontentHelp: {
    //justifyContent: "center",
    justifyContent:'flex-end',
    margin: 0,
  },
  DineImage:{
    height:40,
    width:40,
    resizeMode:"contain",
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }],
  },
  arrowImage:{
    height:20,
    width: 20,
    tintColor:globals.COLOR.greyText,
    resizeMode:"contain",
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }],
  },
  imgstyle:{
    width:'25%',
    alignItems: 'flex-end'
  },
  Outerview:{
      flexDirection:'row',
      width:"70%",
      alignItems:'center',
  },
  modalmainviewHelp: {
    backgroundColor: "white",
    width: wp("90%"),
    height: wp("90%"),
    padding: "3%",
    marginBottom:"60%",
    alignSelf:'center',
   // paddingBottom:'20%',
    //borderRadius: 10,
    borderRadius:10,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  border:{
      borderWidth:2,
      borderColor:'lightgrey',
      width:'50%',
      alignItems:'center',
      justifyContent:'center',
      marginLeft:'25%',
      marginRight:'20%',
      marginBottom:'3%',

  },
  clo:{
      justifyContent:'flex-end',
      alignItems:'flex-end',
      paddingRight:'2%'
  },

  headingLine:{
      flexDirection:'column',
      justifyContent:"center",
      alignItems:"center",
      marginVertical:"2%"
  },
  x:{
      color:globals.COLOR.greyText,
      fontSize:18,
  },
  helptextWarpper:{
    flexDirection:'row',
    //justifyContent:'center',
    paddingTop:hp('1%'),
    
  // width:'50%'
    },
    formWrapper:{
      marginHorizontal:"3%",
      borderWidth: 1,
      borderRadius: 5,
      height:"25%",
      borderColor:globals.COLOR.borderColor,
      marginVertical:"2%",
      padding:10,
    },
    applytext:{
        paddingTop:hp('2%'),
    paddingBottom:hp('1.5%'),
    //paddingLeft:'10%',
    width:'25%'
    
    },
    textinp:{
      color:globals.COLOR.Text,
      textAlign:"center",
      marginLeft:"20%"
    },
    resettext:{
        paddingTop:hp('2%'),
    paddingBottom:hp('1.5%'),
    //paddingLeft:'5%',
    width:'25%'
    
    },
    wholeView:{
        flexDirection:'row',
        alignItems:'center',
        marginHorizontal:"3%",
        height: '100%',
        width: '100%',
    },
    helpheadaText:{
      fontSize: 17,
      fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
      textAlign:'left',
      color:globals.COLOR.Text,
    },
    helpheadText:{
      fontSize: 15,
      fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
      textAlign:'left',
      color:globals.COLOR.Text,
    },
    applyTexts:{
      fontSize: 13,
      fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
      textAlign:'center',
      color:globals.COLOR.greyText,
    },
    doubText:{
        marginHorizontal:"15%",
        
        
    },
   
    resetTexts:{
      fontSize: 15,
      fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
      textAlign:'center',
      color:globals.COLOR.greyText,
    },

    // boxVies:{
    //     height:50,
    //     width:'95%',
    //     borderWidth:0.5,
    //     borderColor:'#707070',
    //     flexDirection:'row',
    //     justifyContent:'space-between',
    //           marginTop:'8%',
    //     borderRadius:5,
        
    //   marginLeft:'3%'
    //      },
    //      hinput: {
    //         //marginTop:-4
    //         // margin: 15,
    //         // height: 40,
    //         // borderColor: '#7a42f4',
    //         // borderWidth: 1
    //         fontFamily: globals.FONTS.openSansLight,
    //         fontSize: hp('2%'),
    //         textAlign: I18nManager.isRTL ? "right" : "left",
    //         color:"#707070",
    //         marginLeft:'2%'
            
    //       },
    //       arrowv:{
    //         marginRight:'4%',
    //         alignItems:'center',
    //         justifyContent:'center'
    //           },
    //           search:{
    //             width:20,
    //             height:20,
    //             alignSelf:'center'
    //           },
              listV:{
                marginTop:"5%"
                },
            });

            export { images, styles };