import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, FlatList, Text,Image } from 'react-native';
import { images, styles } from "./styles";
import appTexts from "../../lib/appTexts";
import { TouchableOpacity } from 'react-native-gesture-handler';


const ShelfArray = [
    {id: '1', value: 'Arabian'},
    {id: '2', value: 'Chinese'},
    {id: '3', value: 'Indian'},
    {id: '4', value: 'Continental'},
    {id: '5', value: 'European'},
    
    
  ];
const ItemView =({item}) =>{
    return(

        <View style={styles.loCListView}> 
        <View style={styles.iconView}>
            <Image style={styles.radioImage} source={images.radioOff}></Image>
            </View>
        <View style={styles.listView}>
            <Text style={styles.locationText}>{item.value}</Text>
        </View>
        </View>
        
        
    );
};
  const ShelfList = () =>{
      return(
          <View style={styles.mainScreen}>
            <FlatList
                data={ShelfArray}
                renderItem=
                   
                    {ItemView}
                    
                keyExtractor={(item, index) => index.toString()}
        />
          </View>
      );
  };
      ShelfList.propTypes = {

      };
      
      
      export default ShelfList;
  