import LinearGradient from "react-native-linear-gradient";
import React from 'react';
import { images, styles } from "./styles";
import {  View, Text, TouchableOpacity,Image,TextInput } from 'react-native';
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import PropTypes from 'prop-types';
import RoundButton from "../RoundButton";
import ShelfList from '../ShelfList';
import Modal from 'react-native-modal';

const ShelfModal = (props) => {

  const {
        isShelfModalVisible,
        toggleShelfModal,
      } = props;

  
  return (
    <Modal
    isVisible={isShelfModalVisible}
    style={styles.modalMaincontentHelp}
    animationIn="slideInUp" 
    animationOut="slideOutRight" 
    onSwipeComplete={toggleShelfModal}
    swipeDirection={["left", "right", "up", "down"]}
  >
    <View style={styles.modalmainviewHelp}>
       <View style={styles.helptextWarpper}>
          <Text style={styles.helpheadText}>{appTexts.STATUS.headingShelf}</Text>
        </View>
        <View style={styles.boxVies}>
            <TextInput
              style={styles.hinput}
              //underlineColorAndroid="lightgray"
              autoCapitalize="none"
              placeholder="Search Shelf Location"
              placeholderTextColor="#707070"
            />
            <View style={styles.arrowv}>
            <Image source={images.search} style={styles.search}></Image>
            </View>
          </View>
          <View style={styles.listV}>
                      <ShelfList />
          </View>
         
    </View>
  </Modal>
  );
};

ShelfModal.propTypes = {
toggleShelfModal:PropTypes.func,
isShelfModalVisible:PropTypes.bool
};

export default ShelfModal;