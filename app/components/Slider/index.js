import React from 'react'
import {
    Text,View,Image, ScrollView, Dimensions, StyleSheet
} from 'react-native'
import { images } from '../Header/styles'
import { Item } from 'native-base'
import { styles } from '../../containers/RestaurantDetailScreen/styles';

const {width} = Dimensions.get("window");
const height = width * 0.6
export default class Slider extends React.Component{
    state = {
        active: 0
    }
    change = ({nativeEvent}) => {
        const slide = Math.ceil(nativeEvent.contentOffset.x / nativeEvent.layoutMesurement.width);
        if(slide !== this.state.active)
        {
            this.setState({active: slide})
        }

    }
    render(){
        return(
            <View style={styles.container}>
                <ScrollView 
                pagingEnabled 
                horizontal 
                onScroll={this.change}
                style={styles.container}>
                {
                    this.props.images.map((image,index) => (

                        <Image
                        key={index}
                        source={{uri: image.cover_photo}}
                        style={styles.imagess} />
        
                    ))
                }
               </ScrollView>
               <View style={styles.pagination}>
                   {
                       this.props.images.map((i,k) => (
                        <Text key={k} style={this.state.active ? styles.pagingActiveText : styles.pagingText}>•</Text>
                       ))
                   }
                   
               </View>
            </View>
        )
    }
}
const style = StyleSheet.create({
    container: { width, height},
    scroll: {width,height},
    imagess: {width, height, resizeMode: 'cover'},
    pagination: {flexDirection: 'row', position: 'absolute', bottom: 0, alignSelf: 'center'},
    pagingText: {color: '#888', margin: 3},
    pagingActiveText: {color: '#fff', margin: 3}

})