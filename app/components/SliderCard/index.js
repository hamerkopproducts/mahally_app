import React from "react";
import PropTypes from "prop-types";
import {
  View,
  Image,
  Text,
  FlatList,
} from "react-native";
import { styles } from "./styles";
import appTexts from "../../lib/appTexts";
import { TouchableOpacity } from "react-native-gesture-handler";
import moment from "moment";

const SliderCard = (props) => {

  const { item, onRealteditemPress } = props;

  let isMenuOpen = false;
  if(item.timing) {
    item.timing.map((itm,index) => {
      const format = 'hh:mm A';
      const time = moment();
      const beforeTime = moment(itm.start_time, format);
      const afterTime = moment(itm.end_time == '00:00' ? '23:59' : itm.end_time, format);

      if (time.isBetween(beforeTime, afterTime)) {
        isMenuOpen = true;
      }
    });
  }

  const renderRelatedItemsTime = ({ item, index }) => (
    <View style={styles.row}>
      <View style={styles.oval}>
        <View style={styles.tet}>
          <Text style={styles.ovalText}>
            {item.start_time}-{item.end_time}
          </Text>
        </View>
      </View>
    </View>
  );

  return (
    <View style={[styles.container,  ! isMenuOpen && {opacity: .5} ]}>
      <TouchableOpacity disabled={ ! isMenuOpen } onPress={() => onRealteditemPress(item.item_id) }>
      <View style={styles.foodSection}>
        <Image source={{ uri: item.cover_photo }} style={styles.foodImage} />
      </View>
      <FlatList
        style={styles.flatlist}
        showsHorizontalScrollIndicator={false}
        data={item.timing}
        numColumns={3}
        extraData={item.timing}
        keyExtractor={(item, index) => index.toString()}
        renderItem={renderRelatedItemsTime}
      />

      <View style={styles.textSection}>
        <View style={styles.lineOnea}>
          <Text style={styles.leb}>{item.item_name}</Text>
        </View>
        {item.hasAddOns === true ? (
          <View style={styles.lineOne}>
            <Text style={styles.add}>{appTexts.SLIDER.add}</Text>
          </View>
        ) : (
          <View style={styles.emptyAddons}>
            <Text style={styles.add}></Text>
          </View>
        )}
        <View style={styles.price}>
          {item.discount_price != null &&
            <View style={styles.txt1}>
              <View>
                <Text style={styles.txt1Style}>SAR</Text>
              </View>
              <View style={styles.fifty}>
                <Text style={styles.no}>{item.price}</Text>
              </View>
            </View>
          }
          <View style={styles.txt1}>
            <View>
              <Text style={styles.txt2Style}>SAR</Text>
            </View>
            <View style={styles.forty}>
              <Text style={styles.no4}>{item.discount_price != null ? item.discount_price : item.price}</Text>
            </View>
          </View>
        </View>
      </View>
      </TouchableOpacity>
    </View>
  );
};
SliderCard.propTypes = {
  onPlusPress: PropTypes.func,
};

export default SliderCard;
