import { StyleSheet, I18nManager, Platform } from "react-native";
import globals from "../../lib/globals"

const images = {
  location: require('../../assets/images/listCard/location.png'),
  burger: require('../../assets/images/listCard/freemeal.png'),
  food:require("../../assets/images/MenuOfferCard/egg.png"),
  time:require('../../assets/images/attributes/time.png'),
};

const styles = StyleSheet.create({
 container:{
   //justifyContent: 'space-between',
    borderWidth: 0.5,
    borderRadius: 10,
    backgroundColor: 'white',
    borderColor: globals.COLOR.borderColor,
    marginRight: 10,
    overflow: 'hidden',
   
 },
 lineOnea:{
   // top:10
 },
 foodSection:{
   alignItems: "flex-start",
     borderRadius:10,
     overflow: 'hidden',
     position:'relative'
 },
 foodImage:{
    width: 242,
    height: 194,
resizeMode:'cover',
 },
 textSection:{
paddingTop: 10,
   //  bottom:27,
    
   //  right:7,
   marginLeft: 10, 
   marginBottom:10,
   // paddingLeft:10,
   borderTopWidth:0,
   borderColor:globals.COLOR.greyText,
    
 },
 leb:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.Text,
    fontSize:16,  
    textAlign:'left',  
 },
 emptyAddons:{
    marginTop: Platform.OS === 'ios' ? 15 : 15 
 },
 add:{
    lineHeight: Platform.OS === 'ios' ? 30 : 30,
    marginTop: 5,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.purple,
    fontSize:12,  
    textAlign:'left', 
 },
 price:{
     flexDirection:'row',
     alignItems:'center',
   //   marginVertical: 10
 },
 txt1Style:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.greyText,
    fontSize:12,  
    textAlign:'left',  
    textDecorationLine:'line-through'
 },
 txt1:{
     flexDirection:'row',
     alignItems:'center',
     marginRight: 10
     
 },
 fifty:{
     marginLeft: 10,
     
 },
 txt2Style:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.Text,
    fontSize:16,  
    textAlign:'left', 
 },
 forty:{
    marginLeft: 10,
 },
 no4:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.Text,
    fontSize:16,  
    textAlign:'left', 
 },
 no:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.greyText,
    fontSize:16,  
    textAlign:'left',
    textDecorationLine:'line-through'
 },
 oval:{
   //  width:'75%',
    height:20,
    flexDirection:'row',
   //  paddingLeft:'5%',
    //paddingRight:'2%',
    borderRadius:60,
    alignItems:'center',
    justifyContent:'center',
   //position:'absolute',
   //  zIndex:20,
    borderWidth:0.5,
   // top:140,
   // right:5,
    backgroundColor:'rgba(52,52,52,0.8)'
  },
 ovalemp:{
   //  minWidth:'50%',
    height:30,
    paddingLeft:'2%',
    paddingRight:'2%',
    borderRadius:60,
    alignItems:'center',
    justifyContent:'center',
    position:'absolute',
    top:10,
    borderWidth:0.5,
    
    right:5,
    backgroundColor:'rgba(52,52,52,0.8)'
  },
  row:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
  // width:'60%',
  marginLeft: 8,
  marginRight: 8,

  minWidth: 50,
   //  zIndex:80,
   // bottom:2,
   

  },
  im:{
  },
  flatlist:{
     flex:1,
     position:'absolute',
     top:160,
     alignSelf: 'center',
  },
  time:{
      width:10,
      height:10,
  },
  tet:{
      flexDirection:'row',
  },
  ovalText:{
     marginLeft: 3,
     marginRight: 3,
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:'white',
    fontSize:6,  
  }
}
);

export { images, styles };
