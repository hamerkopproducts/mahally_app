import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Image, FlatList } from 'react-native';
import { images, styles } from "./styles";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import Modal from "react-native-modal";

const SortModal = (props) => {

  const {
    isSortModalVisible,
    closeModal,
    nearadioClicking,
    popradioClicking,
    isNearbySelected,
    isPopularitySelected
  } = props;

  let listData =
    [{ id: 1, name: appTexts.SORT.nearby, value: 'nearby' },
    { id: 2, name:  appTexts.SORT.popularity + ' (' + appTexts.SORT.ratingHigh + ')', value: 'popularity' },
    ]
  return (
    <Modal       
      onRequestClose={() => null}
      style={styles.modalStyle}
      isVisible={isSortModalVisible}>
      <View style={styles.popupContainer}>
        {/* <View style={styles.lineTop}></View> */}
        <View style={styles.closeContainer}>
          <TouchableOpacity style={styles.popupClose} onPress={() => { closeModal() }}>
            <Text style={styles.popupCloseText}>{'X'}</Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.sortByText}>{appTexts.SORT.Heading}</Text>
        <View style={styles.flatListStyle}>
          <View style={styles.labelView}>

            <View style={styles.discountTextContainer}>
              <Text style={styles.discountText}>{listData[0].name}</Text>
            </View>
            <TouchableOpacity style={styles.favIconContainer} onPress={() => {
              nearadioClicking(listData[0].value)
              closeModal()
            }
            }>

              <Image source={isNearbySelected ? require('../../assets/images/modal/Radio-checked.png')
                : require('../../assets/images/modal/Radio-unchecked.png')} style={styles.radio} />
            </TouchableOpacity>
          </View>

          <View style={styles.labelView}>

            <View style={styles.discountTextContainer}>
              <Text style={styles.discountText}>{listData[1].name}</Text>
            </View>
            <TouchableOpacity style={styles.favIconContainer} onPress={() => {
              popradioClicking(listData[1].value)
              closeModal()
            }
            }>

              <Image source={isPopularitySelected ? require('../../assets/images/modal/Radio-checked.png')
                : require('../../assets/images/modal/Radio-unchecked.png')} style={styles.radio} />
            </TouchableOpacity>
          </View>
        </View>
      </View>

    </Modal>
  );
};

SortModal.propTypes = {
  closeModal: PropTypes.func,
  nearadioClicking: PropTypes.func,
  popradioClicking: PropTypes.func,
};

export default SortModal;
