import PropTypes from 'prop-types';
import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Image, FlatList,ImageBackground } from 'react-native';
import { images, styles } from "./styles";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import Swiper from 'react-native-swiper';


const SwiperCard = (props) => {

  const {
   
  } = props;

  
  return (
   <View>
       <Swiper showsButtons={true} 
                horizontal={true} 
                activeDot={<View style={styles.active}/>}
                dot={<View style={styles.dot}/>}
                style={styles.wrapper}
                buttonWrapperStyle={styles.btnWrapper}
                prevButton={<View style={styles.sq}>
                 <TouchableOpacity onPress={() => { onBackButtonPress(); }} >
                     <Image style={styles.bacImage} source={images.backImage} />
                     </TouchableOpacity>
                 </View>}>
            <View style={styles.slide1}>
                <Image source={images.hotel} style={styles.hotel}></Image>
            </View>
            <View style={styles.slide2}>
                <Image source={images.hotel} style={styles.hotel}></Image>
            </View>
            <View style={styles.slide3}>
                <Image source={images.hotel} style={styles.hotel}></Image>
            </View>

        </Swiper>
   </View>
   
  );
};

SwiperCard.propTypes = {
  closeModal: PropTypes.func,
};

export default SwiperCard;
