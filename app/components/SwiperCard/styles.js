import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals"

const images = {
  hotel:require("../../assets/images/HomeCard/hotel-img.png"),
  backImage:require('../../assets/images/attributes/back.png'),
};

const styles = StyleSheet.create({
 hotel:{
  width:'100%',
  resizeMode:'cover'
 },
 dot:{
   width:30,
   backgroundColor:'white'
 },
 active:{
   width:150,
   backgroundColor:'white'
 },
 wrapper:{
   height:'40%'
 },
 
  btnWrapper: {
    flexDirection: 'row', 
    position: 'absolute', 
    justifyContent: 'space-between', 
    alignItems: 'center'
  },
  sq:{
    width:40,
    height:40,
    borderRadius:4,
    backgroundColor:'rgba(52,52,52,0.8)',
    position:'absolute',
    top:40,
    left:16,
    alignItems:'center',
    justifyContent:'center'
    },
 
});

export { images, styles };
