import LinearGradient from "react-native-linear-gradient";
import React from 'react';
import { images, styles } from "./styles";
import {  View, Text, TouchableOpacity,Image,TextInput } from 'react-native';
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import PropTypes from 'prop-types';
import RoundButton from "../RoundButton";
import Modal from 'react-native-modal';

const ThanksModal = (props) => {

  const {
      isContactModalVisible,  
      toggleContactModal,
      } = props;

  
  return (
    <Modal
        transparent={true}
        animationIn="slideInUp" 
          animationOut="slideOutRight" 
          onSwipeComplete={toggleContactModal}
          swipeDirection={["left", "right", "up", "down"]}
          isVisible={isContactModalVisible}
          style={styles.modalMaincontentLogout}
        >
          <View style={styles.modalmainviewLogout}>
            
              <View style={styles.logoutImageView}>
                <Image source={images.contactImage} style={styles.logoutImage}/>
              </View>
              <View style={styles.logoutTextView}>
                <Text style={styles.logoutTextStyle}>{appTexts.PROFILE.thanks}</Text>
              </View>
              <View style ={styles.thanksLine}>
                <Text style={styles.thanksText}>{appTexts.PROFILE.line}</Text>
              </View>
                     
              </View>
        </Modal>
  );
};

ThanksModal.propTypes = {

isContactModalVisible:PropTypes.bool,
toggleContactModal:PropTypes.func,
};

export default ThanksModal;