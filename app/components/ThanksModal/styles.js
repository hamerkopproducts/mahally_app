import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const images = {
    contactImage:require('../../assets/images/profileicon/contact.png'),
};

const styles = StyleSheet.create({
modalMaincontentLogout: {
    justifyContent: "center",
    alignItems:'center',
  
  },
  modalmainviewLogout: {
    backgroundColor: "white",
    //width: wp("90%"),
    //padding: "4%",
    //flex:1,
    paddingLeft:'3%',
    paddingRight:'3%',
   // flex:0.5,
    width:310,
    borderRadius:15,
    justifyContent:'center',
    //alignItems:'center',
    //borderRadius: 10,
    // borderTopRightRadius:15,
    // borderTopLeftRadius:15,
    borderColor: "rgba(0, 0, 0, 0.1)",
  
},
logoutImageView:{
  alignItems:'center',
  justifyContent:'center',
  marginTop:'10%'
},
logoutImage:{
  width:120,
  height:120,
},
logoutTextView:{
  alignItems:'center',
  justifyContent:'center',
  marginTop:'5%',
  marginBottom:'10%'


},
logoutTextStyle:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.openSansSemiBold,
  color:globals.COLOR.blackTextColor,
  fontSize:16,
},
logoutTetStyle:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.openSansSemiBold,
  color:globals.COLOR.blackTextColor,
  fontSize:18,
},
thanksLine:{
    marginTop:'2%',
    justifyContent:'center',
    alignItems:'center',
    marginBottom:'10%',
    marginRight:'3%',
    marginLeft:'3%'
  },
  thanksText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.openSansSemiBold,
    fontSize: 14,
    justifyContent:'center',
    alignItems:'center',
    color:'#707070',
    textAlign:'center'
  },
});

export { images, styles };