import React from "react";
import { images, styles } from "./styles";
import { View, Text, TouchableOpacity, Image, I18nManager } from "react-native";

import appTexts from "../../lib/appTexts";
import PropTypes from "prop-types";
import Modal from "react-native-modal";

const ThankyouModal = (props) => {

  const { isThankyouModalVisible, redirectToDetails, orderId, amount, orderRawId, loyaltyOffer } = props;

  return (
    <Modal
      isVisible={isThankyouModalVisible}
      style={styles.modalMaincontentLogout}
    >
      <View style={styles.modalmainviewLogout}>
        <View style={styles.logoutImageView}>
          <Image source={images.thankyouImage} style={styles.logoutImage} resizeMode={'contain'}/>
        </View>
        <View style={styles.logoutTextView}>
          <Text style={styles.logoutTextStyle}>{appTexts.CHECKOUT.thanks}</Text>
          <Text style={styles.confirmText}>{appTexts.CHECKOUT.confirm}</Text>
        </View>

        {loyaltyOffer &&
          <View style={[styles.logoutTextView, {marginTop: 1} ]}>
            <Text style={[styles.logoutTextStyle, {color: 'orange'} ]}>{I18nManager.isRTL ? "تهانينا" : 'Congratulations'}</Text>
            { ! I18nManager.isRTL && <Text style={styles.confirmText}>{'You are eligible for a free loyalty meal for your last order.'}</Text>}
            {I18nManager.isRTL && <Text style={styles.confirmText}>{'أنت مؤهل للحصول على وجبة ولاء مجانية لطلبك الأخير.'}</Text>}
          </View>
        }

        <View style={styles.lineOneView}>
          <Text style={styles.lineOne}>{appTexts.CHECKOUT.ID}</Text>
          <Text style={styles.lineTwo}>{orderId}</Text>
        </View>
        <View style={styles.lineOneView}>
          <Text style={styles.lineOne}>{appTexts.CHECKOUT.status}</Text>
          <Text style={styles.lineTwo}>{appTexts.CHECKOUT.confirmed}</Text>
        </View>
        <View style={styles.lineOneView}>
          <Text style={styles.lineOne}>{appTexts.CHECKOUT.paid}</Text>
          <Text style={styles.lineTwo}>{appTexts.STRING.sar} {amount}</Text>
        </View>

        <View style={styles.buttonView}>
          <TouchableOpacity
            onPress={() => redirectToDetails(orderRawId)}
            style={styles.viewButton}
          >
            <View style={styles.textStyle}>
              <Text style={styles.buttonText}>{appTexts.CHECKOUT.view}</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

ThankyouModal.propTypes = {
  isThankyouModalVisible: PropTypes.bool,
};

export default ThankyouModal;
