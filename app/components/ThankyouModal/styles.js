import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const images = {
    thankyouImage:require('../../assets/images/modal/Success.png'),
};

const styles = StyleSheet.create({
modalMaincontentLogout: {
    justifyContent: "center",
    alignItems:'center',
  
  },
  modalmainviewLogout: {
    backgroundColor: "white",
    //width: wp("90%"),
    //padding: "4%",
    //flex:1,
    paddingLeft:'3%',
    paddingRight:'3%',
   // flex:0.5,
    width:310,
    borderRadius:15,
    justifyContent:'center',
    //alignItems:'center',
    //borderRadius: 10,
    // borderTopRightRadius:15,
    // borderTopLeftRadius:15,
    borderColor: "rgba(0, 0, 0, 0.1)",
  
},
logoutImageView:{
  alignItems:'center',
  justifyContent:'center',
  marginTop:'10%'
},
logoutImage:{
  width:150,
  height:170,
},
logoutTextView:{
  alignItems:'center',
  justifyContent:'center',
  marginTop:'5%',
  marginBottom:'10%',
 
  
},
logoutTextStyle:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  color:globals.COLOR.Text,
  fontSize:16,
  lineHeight: Platform.OS === 'ios' ? 20 : 20
},
confirmText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.greyText,
    fontSize:10,
    marginTop: 8
},

lineOneView:{
    marginTop:'2%',
    justifyContent:'center',
    alignItems:'center',
    marginRight:'5%',
    marginLeft:'5%',
    flexDirection:'row',
    justifyContent:'space-between',
  },
  thanksText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.openSansSemiBold,
    fontSize: 14,
    justifyContent:'center',
    alignItems:'center',
    color:'#707070',
    textAlign:'center',
  },
  lineOne:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 12,
    color:globals.COLOR.greyText,
    lineHeight: Platform.OS === 'ios' ? 24 : 24
  },
  lineTwo:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 12,
    color:globals.COLOR.Text,
    lineHeight: Platform.OS === 'ios' ? 24 : 24
  },
  buttonView:{
      marginTop:'14%',
      marginBottom:'12%',
      alignItems:'center',
      justifyContent:'center'
     
      
  },
  viewButton:{
    width:'80%',
    height:50,
    backgroundColor:globals.COLOR.purple,
    borderRadius:10,
    alignItems:'center',
      justifyContent:'center'
  },
  buttonText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 12,
    color:'white',
  },
  textStyle:{
    alignItems:'center',
    justifyContent:'center',
    //marginTop:'6%',
  },

  
});

export { images, styles };