import React, { Component, useState } from "react";
import PropTypes from "prop-types";
import { View, TouchableOpacity, Image, Text } from "react-native";

import { images, styles } from "./styles";

const TopTab = (props) => {
  const {
    tabIndex,
    firstTabText,
    secondTabText,
    thirdTabText,
    onTabChange,
    isProductDetail,
    isFilterModalVisible,
    setIsFilterModalVisible,
    isSortModalVisible,
		setIsSortModalVisibile,
    hasFilterApplied
  } = props;

  const openSortModal = () => {
    setIsSortModalVisibile(!isSortModalVisible);
  };

  const openFilterModal = () => {
    setIsFilterModalVisible(!isFilterModalVisible);
  };

  return (
    <View style={tabIndex == 0 ? styles.container : styles.noFloat}>
      <View style={isProductDetail ? styles.detailMapList : styles.mapListView}>
        <View style={isProductDetail ? styles.listProduct : styles.listMap}>
          <View style={tabIndex === 0 ? styles.toptabselected : styles.toptabs}>
            <TouchableOpacity
              style={[styles.middleTabView]}
              onPress={() => {
                onTabChange(0);
              }}
            >
              <View
                style={
                  isProductDetail
                    ? styles.leftHeading
                    : styles.leftHeadingLabelView
                }
              >
                <Text
                  style={
                    tabIndex === 0 ? styles.nameLabelSelected : styles.nameLabel
                  }
                >
                  {firstTabText}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={tabIndex === 1 ? styles.toptabselected : styles.toptabs}>
            <TouchableOpacity
              style={styles.middleTabView}
              onPress={() => {
                onTabChange(1);
              }}
            >
              <View style={styles.leftHeadingLabelView}>
                <Text
                  numberOfLines={1}
                  style={
                    tabIndex === 1 ? styles.nameLabelSelected : styles.nameLabel
                  }
                >
                  {secondTabText}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          {isProductDetail ? (
            <View
              style={tabIndex === 2 ? styles.toptabselected : styles.toptabs}
            >
              <TouchableOpacity
                style={styles.middleTabView}
                onPress={() => {
                  onTabChange(2);
                }}
              >
                <View
                  style={
                    isProductDetail
                      ? styles.leftHeading
                      : styles.leftHeadingLabelView
                  }
                >
                  <Text
                    numberOfLines={1}
                    style={
                      tabIndex === 2
                        ? styles.nameLabelSelected
                        : styles.nameLabel
                    }
                  >
                    {thirdTabText}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          ) : null}
        </View>
      </View>
      {isProductDetail ? null : (
        <View style={styles.sortFilt}>
          <TouchableOpacity
            style={[styles.middleTabView]}
            onPress={() => {
              openSortModal();
            }}
          >
            <View style={styles.toptabs}>
              <Image style={styles.sortFilterIcon} source={images.SortIcon} />
            </View>
          </TouchableOpacity>
          <View style={styles.border}></View>
          <TouchableOpacity
            style={styles.middleTabView}
            onPress={() => {
              openFilterModal();
            }}
          >
            
            <View style={styles.toptabs}>
            {hasFilterApplied ? 
              <Image style={styles.sortFilterIcon} source={images.filterBadgeIcon} /> :
              <Image style={styles.sortFilterIcon} source={images.filterIcon} />}
            </View>
          </TouchableOpacity>
        </View>
      )}
      
    </View>
  );
};
TopTab.propTypes = {
  tabIndex: PropTypes.number,
  onemoretab: PropTypes.number,
  firstTabText: PropTypes.string,
  secondTabText: PropTypes.string,
  thirdTabText: PropTypes.string,
  firstTabCount: PropTypes.number,
  secondTabCount: PropTypes.number,
  thirdTabCount: PropTypes.number,
  onTabChange: PropTypes.func,
  isCalenderRequired: PropTypes.bool,
  isProductDetail: PropTypes.bool,
  onCalendarFilterPress: PropTypes.func,
  nearadioClicking: PropTypes.func,
  applyFilter: PropTypes.func,
  popradioClicking: PropTypes.func,
  setIsFilterModalVisible: PropTypes.func,
  refreshPage: PropTypes.func,
  sortValue: PropTypes.string,
};

export default TopTab;
