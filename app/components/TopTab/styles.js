import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const images = {
  calendarIcon: require("../../assets/images/temp/calendern.png"),
  arrowIcon: require("../../assets/images/temp/calenderarrow.png"),
  filterIcon: require("../../assets/images/temp/filterNoBadge.png"),
  filterBadgeIcon: require("../../assets/images/temp/filterBadge.png"),
  SortIcon: require("../../assets/images/temp/Sort.png"),
};

const styles = StyleSheet.create({
  container: {
    marginLeft: 10,
    marginRight: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    overflow: "hidden",
    marginTop: 10,
    
  },
  noFloat: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
    overflow: "hidden",
  },
  leftTabView: {
    justifyContent: "center",
    alignItems: "center",
  },
  toptabs: {
    // flex: 1,
    // height:40,
    justifyContent: "center",
    // padding:10,
    alignSelf: "center",
    alignItems: "center",
    marginLeft: 13,
    marginRight: 13,
  },
  toptabselected: {
    // flex: 1,
    height: 39,
    justifyContent: "center",
    alignItems: "center",
    borderBottomWidth: 2,
    borderRadius: 1,
    borderBottomColor: globals.COLOR.purple,
    marginLeft: 12,
    marginRight: 12,
    //paddingHorizontal:15,
    // padding:10,
  },
  middleTabView: {
    justifyContent: "center",
    alignItems: "center",
  },
  rightTabView: {
    justifyContent: "center",
    alignItems: "flex-end",
  },
  leftHeadingLabelView: {
    //flexDirection: 'row',
    justifyContent: "center",
    alignItems: "center",
    marginLeft:  I18nManager.isRTL ?8:5,
    marginRight:I18nManager.isRTL ?8:5,
    //backgroundColor:globals.COLOR.themeGreen,
  },
  leftHeading: {
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: I18nManager.isRTL ? "9%" : "8%",
  },
  nameLabel: {
    alignContent: "center",
    justifyContent: "center",
    // height: 20,
    color: "grey",
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.helvetica,
    fontSize: 13,
  },
  nameLabelSelected: {
    alignContent: "center",
    justifyContent: "center",
    color: "#0c1d1d",
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.helveticaBold,
    fontSize: 14,
  },
  linkText: {
    top: 0,
    color: globals.COLOR.linkTextColor,
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.helvetica,
    fontSize: 12,
  },
  leftIcon: {
    width: 20,
    height: 20,
  },
  underLineStyle: {
    backgroundColor: globals.COLOR.themeGreen,
  },
  transparentUnderLineStyle: {
    marginTop: 5,
    width: "75%",
    height: 4,
    borderRadius: 2,
    backgroundColor: globals.COLOR.transparent,
  },
  underLineWidth: {
    width: "75%",
  },
  calenderFilterContainer: {
    marginLeft: "4%",
    width: "36%",
  },
  calenderFilterView: {
    width: "100%",
    height: 30,
    backgroundColor: globals.COLOR.headerColor,
    shadowColor: globals.COLOR.borderColor,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.5,
    shadowRadius: 0.5,
    elevation: 1,
    borderRadius: 4,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    padding: 5,
  },
  sortFilterIcon: {
    position: "absolute",
    justifyContent: "center",
    alignSelf: "center",
    width: 40,
    height: 40,
    transform: [{scaleX: I18nManager.isRTL ? -1 : 1}],
  },
  dateText: {
    textAlign: "left",
    color: globals.COLOR.textColor,
    fontFamily: I18nManager.isRTL
      ? globals.FONTS.notokufiArabic
      : globals.FONTS.helvetica,
    fontSize: 10,
  },
  arrowIcon: {
    position: "absolute",
    right: 5,
    width: 10,
    height: 10,
  },
  mapListView: {
    borderWidth: 1,
    height: 42,
    // width:'96%',
    borderRadius: 20,
    borderColor: "#fff",
    backgroundColor: "#ffffff",
    flexDirection: "row",
    justifyContent: "space-between",
    // paddingRight:30,
    //paddingHorizontal:5,
    marginLeft: globals.SCREEN_SIZE.height <= 600 ? 0 : 5,
  },
  detailMapList: {
    borderWidth: 1,
    height: 50,
    // width:'96%',
    borderRadius: 20,
    borderColor: "#fff",
    backgroundColor: "#ffffff",
    flexDirection: "row",
    justifyContent: "space-between",
    // paddingRight:30,
    //paddingHorizontal:5,
    marginLeft: globals.SCREEN_SIZE.height <= 600 ? 0 : 5,
  },
  listMap: {
    flexDirection: "row",
    height: 37,
    width: "70%",

    // paddingRight:'2%',
  },
  listProduct: {
    flexDirection: "row",
    height: 40,
    width: "90%",
    marginLeft: "5%",
    justifyContent: "center",
    alignItems: "center",
  },
  sortFilt: {
    borderWidth: 1,
    height: 42,
    width: "25%",
    borderRadius: 20,
    backgroundColor: "#ffffff",
    borderColor: "#fff",
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 6,
    //  marginRight:"10%",
    marginHorizontal: globals.SCREEN_SIZE.height <= 600 ? 0 : 2,
  },
  border: {
    borderRightWidth: 1,
    height: 18,
    marginTop: 5,
    borderColor: globals.COLOR.borderColor,
  },
});

export { images, styles };
