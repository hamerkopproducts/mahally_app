import PropTypes from 'prop-types';
import React from 'react';
import { View, Text, TouchableOpacity,Image,TextInput } from 'react-native';
import LinearGradient from "react-native-linear-gradient";
import { images, styles } from "./styles";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";

import Modal from 'react-native-modal';

import RoundButton from "../RoundButton";
const WarehouseModal = (props) => {

  const {
        isWarehouseModalVisible,
        toggleWarehouseModal,
        setIsLocationWarehouse,
        setIsShelfWarehouse,
      } = props;

const openLocationModal = () => {
  toggleWarehouseModal();
  setTimeout(() => {
    setIsLocationWarehouse();
  },400);
};

const openShelfModal = () => {
  toggleWarehouseModal();
  setTimeout(() => {
    setIsShelfWarehouse();
  },400);
};
  
  return (
  <Modal
    //backdropColor='transparent'
    animationIn="slideInUp" 
    animationOut="slideOutRight" 
    onSwipeComplete={toggleWarehouseModal}
    swipeDirection={["left", "right", "up", "down"]}
    isVisible={isWarehouseModalVisible}
    style={styles.modalMaincontentHelp}
  >
    <View style={styles.modalmainviewHelp}>
       <View style={styles.helptextWarpper}>
          <Text style={styles.helpheadText}>{appTexts.STATUS.heading}</Text>
        </View>
        <View >
          <TouchableOpacity onPress={() => {openLocationModal()}}style={styles.box} >
            <View style={styles.boxView}>
            <TextInput
              style={styles.hinput}
              autoCapitalize="none"
              editable={false}
              placeholder="Warehouse Location"
              placeholderTextColor="#707070"
            />
          <View style={styles.arrowv}>
            <Image source={images.varrow} style={styles.varrow}></Image>
            </View>
          </View>
          </TouchableOpacity>
         </View>
        <View >
         <TouchableOpacity onPress={()=> {openShelfModal()}}>
          <View style={styles.boxView}>
            <TextInput
              style={styles.hinput}
              //underlineColorAndroid="lightgray"
              autoCapitalize="none"
              editable={false}
              placeholder="Shelf Location"
              placeholderTextColor="#707070"
            />
            <View style={styles.arrowv}>
            <Image source={images.varrow} style={styles.varrow}></Image>
            </View>
          </View>
          </TouchableOpacity>
                  </View>
              <View style={styles.helpbuttoninsidewrapper}>
        <TouchableOpacity
          style={styles.helpbuttononview}
          onPress={() => this.props.onPress()}>
          <Text style={styles.helpyellowbuttonText}>
              {appTexts.STATUS.submit}
            </Text>
         </TouchableOpacity>
      </View>
    </View>
  </Modal>
   );
};

WarehouseModal.propTypes = {
toggleWarehouseModal:PropTypes.func,
isWarehouseModalVisible:PropTypes.bool
};

export default WarehouseModal;