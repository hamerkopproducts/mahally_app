import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const images = {
  calendarIcon: require("../../assets/images/temp/calendern.png"),
  arrowIcon: require("../../assets/images/temp/calenderarrow.png"),
  varrow:require('../../assets/images/profileicon/arrow.png'),
  search:require('../../assets/images/listCard/Search.png'),
};

const styles = StyleSheet.create({
modalMaincontentHelp: {
    //justifyContent: "center",
    justifyContent:'flex-end',
    margin: 0,
  },
  
  modalmainviewHelp: {
    backgroundColor: "white",
    //width: wp("90%"),
    padding: "4%",
    //borderRadius: 10,
    borderTopRightRadius:15,
    borderTopLeftRadius:15,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  helpModalView:{
    
    flexDirection: "row",
    //justifyContent: "center",
    justifyContent: "space-around",
    alignItems: "center",
    paddingBottom: "3%",
    paddingLeft: "10%",
    paddingRight:'10%'

},
helptextWarpper:{
    flexDirection:'row',
    justifyContent:'center',
    paddingTop:hp('2%'),
    paddingBottom:hp('1.5%'),
    paddingLeft:'10%',
    paddingRight:'10%'
    //paddingLeft:'19%'
    },
    helpheadText:{
      fontSize: hp('2.6%'),
      fontFamily: globals.FONTS.avenirHeavy,
      textAlign:'center'
    },
    helpsubjectWrapper:{
      //paddingTop:hp('2%'),
      backgroundColor:'pink'
          },
      helpsubText:{
        fontSize: hp('2.1%'),
        color:"#6f6f6f",
        paddingLeft:'1%',
        fontFamily: globals.FONTS.openSansSemiBold,
      },
      hinput: {
        //marginTop:-4
        // margin: 15,
        // height: 40,
        // borderColor: '#7a42f4',
        // borderWidth: 1
        fontFamily: globals.FONTS.openSansLight,
        fontSize: hp('2%'),
        textAlign: I18nManager.isRTL ? "right" : "left",
        color:"#707070",
        marginLeft:'2%'
        
      },
      helpmessageWrapper:{
        paddingTop:hp('2%'),
      },
      helpbuttoninsidewrapper: {
        flexDirection:  "row",
        justifyContent: "center",
        paddingTop: "3%",
        paddingBottom: "7%",
        width: '85%',
        height: 50,
        //borderRadius: 15,
        alignItems: "center",
        alignSelf:'center',
        marginTop: '10%',
        backgroundColor:'#2eb781'
      },
      helpbuttoninside: {
        flexDirection:  "row",
        justifyContent: "center",
        paddingTop: "3%",
        paddingBottom: "7%",
        width: '85%',
        height: 50,
        //borderRadius: 15,
        alignItems: "center",
        alignSelf:'center',
        //marginTop: '10%',
        backgroundColor:'#2eb781'
      },
      helpbuttononview: {
        alignItems: "center",
        justifyContent: "center",
        marginTop: hp('2.5%'),
           
    },
    boxView:{
      height:60,
      width:'95%',
      borderWidth:0.5,
      borderColor:'#707070',
      flexDirection:'row',
      justifyContent:'space-between',
      marginTop:'5%',
      //marginLeft:'5%',
      marginRight:'5%'
    },
    varrow:{
        width:15,
        height:15,
        alignSelf:'center'
      },
      search:{
        width:20,
        height:20,
        alignSelf:'center'
      },
    arrowv:{
    marginRight:'4%',
    alignItems:'center',
    justifyContent:'center'
      },
      listV:{
      marginTop:"5%"
      },
      boxViews:{
        flexDirection:'row',
        justifyContent:'space-between'
      },
      helpyellowbuttonText:{
        fontFamily:globals.FONTS.openSansSemiBold,
        color:'white',
      },
});

export { images, styles };