import React, {useEffect} from "react";
import {
  View,
  Text,
  TextInput,
  StatusBar,
  SafeAreaView,
  Image,
  TouchableOpacity,
  I18nManager
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import AppIntroSlider from "react-native-app-intro-slider";
import NetInfo from "@react-native-community/netinfo";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import { styles } from "./styles";
import { connect } from 'react-redux'
import { bindActionCreators } from "redux";
import * as loginActions from "../../actions/loginActions";
import CustomButton from '../../components/CustomButton';
import * as TermsActions from '../../actions/TermsPrivacyFaqActions'
import _ from "lodash"
import Loader from "../../components/Loader";
const data = [
  {
    title:appTexts.APP_SLIDER.Title,
    text:
    appTexts.APP_SLIDER.Content,
    image: require("../../assets/images/chooseLanguage/boylogo.png"),
  },
  {
    title:appTexts.APP_SLIDER.Title,
    text:
    appTexts.APP_SLIDER.Content,
    image: require("../../assets/images/chooseLanguage/boylogo.png"),
  },
  {
    title:appTexts.APP_SLIDER.Title,
    text:
    appTexts.APP_SLIDER.Content,
    image: require("../../assets/images/chooseLanguage/boylogo.png"),
  },
];
  const AppIntrosliderloginScreen = (props) =>{

     //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        props.howToUse()
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  }
  //update
  useEffect(() => handleComponentUpdated(), []);

  const handleComponentUpdated = () => {
  }

  _renderItem = ({ item }) => {
    return (
      <View
        style={[
          styles.slide,
          {
            backgroundColor: item.bg,
          },
        ]}
      >
        <SafeAreaView>
          <View style={{marginVertical:globals.SCREEN_SIZE.height <=600? hp('2%'):hp('5%'),}}>
          <Image source={{ uri: item.image }} style={styles.image} />
          </View>
          <View  style={styles.Titleview}>
          <Text style={styles.title}>{item.title}</Text>
          </View>
          <View  style={styles.Textwidth}>
          <Text style={styles.text}>{item.text}</Text>
          </View>
        </SafeAreaView>
      </View>
    );
  };

  _keyExtractor = (item, index) => index.toString();

  submitMobileNumber = () => {
    props.navigation.navigate('LoginScreen')
  }

  onBackButtonpress = () => {
    props.navigation.goBack();
  }
   
    return (
      <View style={{ flex: 1,backgroundColor:'white' }}>
        <StatusBar translucent backgroundColor="transparent" />
        <View style={{ width: "100%", flex: 1 }}>
        {props.isLoading && <Loader />}
          <AppIntroSlider
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
            showDoneButton={false}
            showNextButton={false}
            dotStyle={{ marginTop:25,marginVertical:globals.SCREEN_SIZE.height <=600? hp('2%'):hp('10%'),backgroundColor: globals.COLOR.purple,height:10 }}
            activeDotStyle={{ backgroundColor: globals.COLOR.purple, marginTop:25,marginVertical:globals.SCREEN_SIZE.height <=600? hp('2%'):hp('10%'),width:35,height:10 }}
            data={props.howToUseData}
          />
        </View>
        <View style={styles.loginButton}>
          <TouchableOpacity onPress={() => {this.submitMobileNumber() }}>
            <CustomButton  buttonstyle={[styles.custombuttonStyle,styles.loginTouch]} buttonText={appTexts.APP_SLIDER.get}/>
          </TouchableOpacity>
        </View>
      </View>
    );
}
const mapStateToProps = (state, props) => {
  return {
    howToUseData: _.get(state, 'termsPrivacyFaqReducer.howToUseDataReponse', ""),
    isLoading: _.get(state, 'termsPrivacyFaqReducer.isLoading', ""),
    
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    howToUse: TermsActions.howToUseApp
  }, dispatch)
};

const AppIntrosliderloginScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(AppIntrosliderloginScreen);

AppIntrosliderloginScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});
export default AppIntrosliderloginScreenWithRedux