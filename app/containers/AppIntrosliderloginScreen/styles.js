import { StyleSheet,I18nManager } from "react-native";
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
  } from "react-native-responsive-screen";
import globals from "../../lib/globals";
let headerButtonContainerWidth =
  globals.SCREEN_SIZE.width - globals.MARGIN.marginTen * 2;
let headerButtonWidth =
  (headerButtonContainerWidth - globals.MARGIN.marginTen * 2) / 3;
  const images = {
    backIcon: require("../../assets/images/chooseLanguage/backarow.png"),
    //logoImage: require("../../assets/images/chooseLanguage/logo-top.png"),
    // facebookIcon: require("../../assets/images/signup/facebook.png"),
    // googleIcon: require("../../assets/images/signup/google.png"),
  };
const styles = StyleSheet.create({
  screenMain: {
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    flexDirection: "column",
  },
  slide: {
    flex: 1,
    alignItems: "center",
  },
  imageContainer: {
    flex: 0.2,
    //backgroundColor:'red',
    // width: globals.SCREEN_SIZE.width,
    // height: globals.SCREEN_SIZE.height,
    alignItems: 'center',
   // paddingTop: hp('3%')
    //paddingTop:'10%'

  },
  image: {
    width:220,
    height:220,
    alignSelf: "center",
    resizeMode:'contain',
    marginVertical:hp('4%')
  },
  logo:{
    width:120,
    height:76,
    resizeMode:'contain'
  },
  arrowImage:{
    width:45,
    height:45,
    resizeMode:'contain',
    marginLeft:'5%',
    //marginTop:'10%',
    marginTop:hp('6%'),
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
    
  },
  text: {
    color: "#707070",
    textAlign: "center",
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize:15,
    lineHeight: 24,
    marginTop:globals.SCREEN_SIZE.height <=600? hp('1%'):hp('3%'),

  },
  title:{
    color: "#233249",
    //backgroundColor: "transparent",
    textAlign: "center",
    //marginTop:50,
    //fontSize:13,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 20,
  },
  Textwidth:{
width:"90%",
marginLeft: "4%",
marginRight: "4%",
// marginVertical:hp('1%'),
  },
 Titleview:{
  //marginTop:hp('4%'),
  marginLeft: "4%",
  marginRight: "4%",
  marginVertical:globals.SCREEN_SIZE.height <=600? hp('2%'):hp('3%'),
      },
  loginButton:{
    justifyContent:'center',
    alignItems:'center',
    //marginTop:hp('2%'),
    marginBottom:hp('4%'),
  },
  custombuttonStyle:{
    backgroundColor:globals.COLOR.purple,
     width:150,
     height: 45,
     borderRadius:15,
     justifyContent:'center',
     alignItems:'center',
   //  marginLeft:6.5

    // backgroundColor:'#cccccc'
  },
  loginTouch:{
    width:300,
    height:50,
    borderRadius:10,
    backgroundColor:'#612467',
    justifyContent:'center',
    alignItems:'center',
  },
  getStart:{
    color:'white',
    textAlign:'center',
    justifyContent:'center',
    alignSelf:'center',
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  arrowWrapper: {
    paddingLeft: "5%",
    marginTop:hp("7%"),
    flexDirection:'row'
  },
  arrowicon: {
    width: 20,
    height: 20,
  
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
  },
  
});

export { images, styles };
