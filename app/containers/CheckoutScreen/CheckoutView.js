import React, { useState } from "react";
import {
  View,
  FlatList,
  Text,
  ScrollView,
} from "react-native";
import globals from "../../lib/globals";
import { styles } from "./styles";

import Header from "../../components/Header";
import appTexts from "../../lib/appTexts";
import ItemCard from "../../components/ItemCard";
import DividerLine from "../../components/DividerLine";
import PromoCode from "../../components/PromoCode";

import BillCard from "../../components/BillCard";
import PayCard from "../../components/PayCard";
import ThankyouModal from "../../components/ThankyouModal";
import AddonModal from "../../components/AddonModal";
import _ from "lodash";
import functions from "../../lib/functions";
import Loader from "../../components/Loader";
import NoLongerAvailable from "../../components/NoLongerAvailable/NoLongerAvailable";

const CheckoutView = (props) => {
  const {
    firstItemPress,
    onBackButtonClick,
    isFirstAddonVisible,
    checkoutOnCounter,
    isThankyouModalVisible,
    onCustomiseClick,
    isAddonModalVisible,
    addedCartItems,
    addToCart,
    redirectToDetails,
    orderId,
    amount,
    orderRawId,
    coupon_code,
    validateCouponCode,
    code,
    setCode,
    selectedRestaurant,
    loyaltyOffer,
    noLongerModalVisible,
    setNoLongerModalVisible,
    toHome,
    bodyText,
    settingsData
  } = props;

  const [item, setItem] = useState({});
  const [addonsItemSelected, setAddonsSelected] = useState({});

  const all_addedCartItems = Object.assign({}, addedCartItems);
  let all_data = [];
  let total_price = 0;
  let addons_total = 0;
  let count_total = Object.keys(all_addedCartItems).length + " " + 
                    ( Object.keys(all_addedCartItems).length > 1 ? appTexts.BANNER.Item_s : appTexts.BANNER.Item);
  for(let inc=0; inc<Object.keys(all_addedCartItems).length; inc++) {
    let items = all_addedCartItems[Object.keys(all_addedCartItems)[inc]];
    total_price += (typeof items.count == 'undefined' ? 0 : items.count ) * items.discount_price;
    all_data.push( items );
    addons_total += Object.keys(items.addonsSelected).length;
    for(let inc2=0; inc2<Object.keys(items.addonsSelected).length; inc2++) {
      const addon_item = items.addonsSelected[ Object.keys(items.addonsSelected)[inc2] ];
      total_price += (typeof addon_item.count == 'undefined' ? 0 : addon_item.count ) * addon_item.price;
    }
  }
  count_total += ` , ${addons_total}` + " " +( addons_total > 1 ? appTexts.BANNER.on_s : appTexts.BANNER.on);

  let promoCode = '';
  let discount_price = 0;
  try {
    promoCode = coupon_code.promocode;
    if(promoCode) {
      discount_price = coupon_code.type == 'Amount' ? parseFloat(coupon_code.amount) : 
        parseFloat((total_price/100)*coupon_code.amount);
    }
  } catch(err) {
    promoCode = '';
    discount_price = 0;
  }

  const vat_percentage = selectedRestaurant.vat;

  const payOptions = settingsData?.data?.payment_options;
  let payOptionPardsed = {};
  try {
    payOptionPardsed = JSON.parse(payOptions);
  } catch (err) {
    payOptionPardsed = {};
  }
 
  const counter = payOptionPardsed?.counter;
  const online = payOptionPardsed?.online;
  

  return (
    <View style={styles.screenMain}>

      {props.isLoading && <Loader/>}

      <Header
        navigation={props.navigation}
        isleftlogoRequired={true}
        isBackButtonRequired={true}
        onBackButtonClick={onBackButtonClick}
        heading={appTexts.CHECKOUT.title}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          backgroundColor: globals.COLOR.headerColor,
        }}
      />
      <ScrollView>
        <View style={styles.screenMains}>
          <View style={styles.itemView}>
            <Text style={styles.detail}>{appTexts.ORDER.item}</Text>
            <Text style={[styles.details, {marginTop: 5} ]}>{ count_total }</Text>
          </View>

            <FlatList
                data={all_data}
                extraData={all_data}
                keyExtractor={(item, index) => index.toString()}
                renderItem={(item) => {
                    return  <View style={styles.itemCard}>
                                <ItemCard
                                    addons={item.item.addonsSelected}
                                    item={item}
                                    isCheckout={true}
                                    isCheck={true}
                                    onItemCardPress={firstItemPress}
                                    isCheckoutAddon={isFirstAddonVisible}
                                    onCustomiseClick={() => {
                                      setAddonsSelected(item.item.addonsSelected);
                                      setItem(item.item);
                                      onCustomiseClick();
                                    }}
                                    addToCart={addToCart}
                                    updateCart={(id, count) => {
                                      let allItems = Object.assign({}, all_addedCartItems);
                                      let old_count = (!isNaN(parseFloat(allItems[id].count)) && isFinite(allItems[id].count)) ? allItems[id].count : 0;
                                      
                                      if(allItems[id].is_loyalty == true && allItems[id].no_offer_available < allItems[id].count + count) {
                                        functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, appTexts.STRING.addedmaximumoffer);
                                        return false;
                                      }
                                      if(old_count == 1 && count == -1) {
                                        delete allItems[id];
                                        functions.displayToast('error', 'top', appTexts.EDITPROFILE.success, appTexts.ORDER.itemremoved);
                                      } else {
                                        allItems[id].count = old_count + count;
                                        functions.displayToast('success', 'top', appTexts.EDITPROFILE.success, appTexts.ORDER.itemupdated);
                                      }
                                      addToCart(allItems);
                                    }}
                                />
                            </View>
                }}
            />

          <View style={styles.line}>
            <DividerLine isCheck={true}/>
          </View>
          <View style={styles.promoBox}>
            <PromoCode 
              coupon_code={promoCode} 
              validateCouponCode={validateCouponCode} 
              code={code}
              setCode={setCode}
            />
          </View>
          <View style={styles.billView}>
            <BillCard 
              total_price={total_price} 
              discount={discount_price} 
              checkout={true}
              vat_percentage={vat_percentage}
            />
          </View>
        </View>

        {Object.keys(all_addedCartItems).length > 0 &&
          <View style={styles.pay}>
          
            <PayCard onCounterpayPress={checkoutOnCounter} />
           
          </View>
        }
        <ThankyouModal
          isThankyouModalVisible={isThankyouModalVisible}
          redirectToDetails={redirectToDetails}
          orderId={orderId}
          amount={amount}
          orderRawId={orderRawId}
          loyaltyOffer={loyaltyOffer}
        />
        
        <AddonModal
          // onCheckoutClick={(onCheckoutClick)}
          addAddon={(count, addon_item) => {
            let addonItems = Object.assign({}, addedCartItems);
            if(Object.keys(addonItems[item.id].addonsSelected).indexOf(addon_item.id.toString()) == -1) {
              addon_item.count = 1;
              addonItems[item.id].addonsSelected[addon_item.id] = addon_item;
              functions.displayToast('success', 'top', appTexts.ALERT_MESSAGES.success, appTexts.LOGIN_POP.addonadd);
            } else {
              addonItems[item.id].addonsSelected[addon_item.id].count += count;
              if(addonItems[item.id].addonsSelected[addon_item.id].count == 0) {
                delete addonItems[item.id].addonsSelected[addon_item.id];
                functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.success, appTexts.LOGIN_POP.addonremove);
              } else {
                functions.displayToast('success', 'top', appTexts.ALERT_MESSAGES.success, appTexts.LOGIN_POP.addonupdate);
              }
            }
            addToCart(addonItems);
          }}
          addonsDetails={_.get(item, "item_addons", {})}
          isAddonModalVisible={isAddonModalVisible}
          openAddonModal={() => onCustomiseClick({})}
          addonsSelected={addonsItemSelected}
          addedCartItems={addedCartItems}
          // onViewClick={onViewClick}
        />

        <NoLongerAvailable
          visible={noLongerModalVisible}
          hide={() => setNoLongerModalVisible(false)}
          redirect={toHome}
          body={bodyText}
        />
        
      </ScrollView>
    </View>
  );
};

CheckoutView.propTypes = {};

export default CheckoutView;
