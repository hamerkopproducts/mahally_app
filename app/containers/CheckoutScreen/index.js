import React, { useEffect, useState } from "react";
import NetInfo from "@react-native-community/netinfo";
import { connect } from "react-redux";
import CheckoutView from "./CheckoutView";
import { bindActionCreators } from "redux";

import appTexts from "../../lib/appTexts";
import functions from "../../lib/functions";
import * as cartActions from "../../actions/cartActions";
import {useDispatch, useSelector} from 'react-redux';
import {
    /** Settings */
  getSettingsContent,
  resetSettingsError, 
} from '../../actions';
import ServiceWrapperAwait from "../../service/ServiceWrapperAwait";
import _ from "lodash";
import ScanModal from "../../components/ScanModal";

const CheckoutScreen = (props) => {
  //Initialising states
  const [loader, setLoader] = useState(false);
  const [isFirstAddonVisible, setIsFirstAddonVisible] = useState(false);
  const [isSecondAddonVisible, setIsSecondAddonVisible] = useState(false);
  const [isThirdAddonVisible, setIsThirdAddonVisible] = useState(false);
  const [isThankyouModalVisible, setIsThankyouModalVisible] = useState(false);
  const [isAddonModalVisible, setIsAddonModalVisible] = useState(false);

  const [orderId, setOrderId] = useState("");
  const [amount, setAmount] = useState(0);
  const [orderRawId, setOrderRawId] = useState("");
  const [code, setCode] = useState("");
  const [loyaltyOffer, setLoyaltyOffer] = useState(false);
  const [noLongerModalVisible, setNoLongerModalVisible] = useState(false);
  const [noLongerAvailableText, setNoLongerAvailableText] = useState("");
  const dispatch = useDispatch();
  const {settingsData, settingsError, isSettingsLoading} = useSelector(
    state => state.settingsReducer,
  );
  const [isDineInModalVisible, setIsDineInModalVisible] = useState(false);

  //will focus
  useEffect(() => {
    return props.navigation.addListener("focus", () => {
      NetInfo.fetch().then((state) => {
        if (state.isConnected) {
          const is_from_scan = props?.route?.params?.from == 'scan';
          if(is_from_scan) {
            props.navigation.setParams({from: null});
            checkoutOnCounter();
          }
        } else {
          functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

   //mounted
   useEffect(() => handleComponentMounted(), []);

   const handleComponentMounted = () => {
    dispatch(getSettingsContent());
   };

  //unmount
  useEffect(() => {
    return () => {
      handleComponentUnmount();
    };
  }, []);

  const handleComponentUnmount = () => {};

  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {};

  const onBackButtonClick = () => {
    props.navigation.goBack();
  };
  const firstItemPress = () => {
    setIsFirstAddonVisible(!isFirstAddonVisible);
  };
  const secondItemPress = () => {
    setIsSecondAddonVisible(!isSecondAddonVisible);
  };
  const thirdItemPress = () => {
    setIsThirdAddonVisible(!isThirdAddonVisible);
  };

  const openThankyouModal = () => {
    setIsThankyouModalVisible(!isThankyouModalVisible);
  };
  const onCustomiseClick = () => {
    setIsAddonModalVisible(!isAddonModalVisible);
  };

   /** Settings error */
   if (settingsError && _.get(settingsError, 'data.success') === false) {
    functions.displayToast(
      'error',
      'top',
      appTexts.ALERT_MESSAGES.error,
      _.get(settingsError, 'data.error.msg'),
    );
    dispatch(resetSettingsError());
  }

  const onModalOptionSelected = (option) => {
    let _scannedData = props.selectedRestaurant;
    _scannedData.dine_in = option;
    props.selectRestaurant(_scannedData);
    setIsDineInModalVisible(false);
    if(_scannedData.dine_in == 'dine' && ! _scannedData.table_id) {
      props.navigation.navigate("Scan", { screen: "ScanScreen", params:{ from: '_checkout'} });
    } else if(_scannedData.dine_in == 'take' && _scannedData.has_preOrder != true) {
      props.navigation.navigate("Scan", { screen: "ScanScreen", params:{ from: '_checkout'} });
    } else {
      _scannedData.scanned = true;
      props.selectRestaurant(_scannedData);
      checkoutOnCounter();
    }
  }

  const checkoutOnCounter = async () => {

    if(props.selectedRestaurant.scanned !== true) {
      setIsDineInModalVisible(true);
      return false;
    }

    if (Object.keys(props.userData).length == 0) {
      await functions.displayAlertWithCallBack(
        appTexts.LOGIN_POP.loginrequire,
        appTexts.LOGIN_POP.pleaselogin,
        (data) => {
          if (data) {
            props.navigation.navigate("LoginStackNavigator", {
              screen: "LoginScreen",
            });
          }
        },
        appTexts.LOGIN_POP.ok,
        appTexts.LOGIN_POP.cancel,
      );

      return true;
    }

    const all_addedCartItems = Object.assign({}, props.addedCartItems);

    const _type = props.selectedRestaurant.dine_in;
    let cartData = {
      customer_id: props.userData.data.user.id,
      restaurant_id: props.selectedRestaurant.restaurant_id,
      order_type: _type == 'dine' ? "dine" : (_type == 'pre' ? 'preorder' : "takeaway"),
      table_id: props.selectedRestaurant.table_id,
      promo_code: _.get(props, "coupon_code.promocode", ""),
      item_count: Object.keys(all_addedCartItems).length,
      payment_type: "counter",
      order_items: [],
    };
    let allItems = [];
    for (let inc = 0; inc < Object.keys(all_addedCartItems).length; inc++) {
      let items = all_addedCartItems[Object.keys(all_addedCartItems)[inc]];
      let each_item = {
        item_id:
          items.id.toString().indexOf("_") !== -1
            ? items.id.toString().split("_")[1]
            : items.id,
        item_count: items.count,
        item_type: items.is_offer
          ? "offer"
          : items.is_loyalty
          ? "loyality"
          : "normal",
      };
      if (items.is_offer) {
        each_item["offer_id"] = items.offer_id;
      }
      if (items.is_loyalty) {
        each_item["offer_id"] = items.loyality_id;
      }
      if (Object.keys(items.addonsSelected).length == 0) {
        each_item["addon"] = null;
      } else {
        let _addons = [];
        for (
          let inc2 = 0;
          inc2 < Object.keys(items.addonsSelected).length;
          inc2++
        ) {
          _addons.push({
            addon_id:
              items.addonsSelected[Object.keys(items.addonsSelected)[inc2]].id,
            item_count:
              items.addonsSelected[Object.keys(items.addonsSelected)[inc2]]
                .count,
          });
        }
        each_item["addon"] = _addons;
      }
      allItems.push(each_item);
    }
    cartData.order_items = allItems;

    const data = await checkout(cartData);
    if (data.success == true) {
      setLoyaltyOffer(data?.data?.loyalty_notification);
      setOrderId(data.data.order_id);
      setAmount(data.data.amount);
      setOrderRawId(data.data.id);
      props.addToCart({});
      props.applyCouponCode({});
      setCode("");
      openThankyouModal();
      props.selectRestaurant({});
    } else {
      if (
        data.is_not_available == true ||
        data.msg == "Items no longer available"
      ) {
        setNoLongerModalVisible(true);
        setNoLongerAvailableText(data.msg);
      } else {
        functions.displayToast(
          "error",
          "top",
          "Error",
          data.error?.msg || "Something went wrong!"
        );
      }
    }
  };

  const redirectToDetails = (id) => {
    props.navigation.navigate("OrderDetail", { id: id });
    setIsThankyouModalVisible(false);
  };

  const checkout = async (_data) => {
    const sAsyncWrapper = new ServiceWrapperAwait();
    setLoader(true);
    const response = await sAsyncWrapper.post("user-app/checkout", _data);
    try {
      const data = new Promise((resolve, reject) => {
        setLoader(false);
        try {
          resolve(response);
        } catch (err) {
          reject(err);
        }
      });

      return data;
    } catch (err) {
      setLoader(false);
    }
  };

  const validateCouponCode = async (code) => {
    if (!code || code == "") {
      functions.displayToast(
        "error",
        "top",
        appTexts.ALERT_MESSAGES.error,
       appTexts.LOGIN_POP.validpromo
      );
      return true;
    }

    const all_addedCartItems = Object.assign({}, props.addedCartItems);
    let has_other_products = false;
    for (let inc = 0; inc < Object.keys(all_addedCartItems).length; inc++) {
      let items = all_addedCartItems[Object.keys(all_addedCartItems)[inc]];
      if (typeof items.is_loyalty == "undefined" || items.is_loyalty == false) {
        has_other_products = true;
      }
    }
    if (has_other_products == false) {
      functions.displayToast(
        "error",
        "top",
        appTexts.ALERT_MESSAGES.error,
        appTexts.LOGIN_POP.youcannot
      );
      return true;
    }

    try {
      if (code == props.coupon_code.promocode) {
        functions.displayToast(
          "success",
          "top",
          appTexts.ALERT_MESSAGES.success,
          appTexts.LOGIN_POP.coupon
        );
        return true;
      }
    } catch (err) {
      
    }

    const sAsyncWrapper = new ServiceWrapperAwait();
    setLoader(true);
    const _data = {
      code: code,
      cust_id: _.get(props, "userID", ""),
    };
    const response = await sAsyncWrapper.post("user-app/promocode", _data);
    setLoader(false);
    if (response.success == true) {
      props.applyCouponCode(response.data);
      functions.displayToast(
        "success",
        "top",
        appTexts.ALERT_MESSAGES.success,
        response.msg
      );
    } else {
      setCode("");
      props.applyCouponCode({});
      functions.displayToast(
        "error",
        "top",
        appTexts.ALERT_MESSAGES.error,
        response.error.msg
      );
    }
  };

  const toHome = () => {
    setNoLongerModalVisible(false);
    props.addToCart({});
    props.applyCouponCode({});
    setCode("");
    props.selectRestaurant({});
    props.navigation.navigate("HomeScreen");
  };

  return (
    <>
      <CheckoutView
        onBackButtonClick={onBackButtonClick}
        firstItemPress={firstItemPress}
        isFirstAddonVisible={isFirstAddonVisible}
        secondItemPress={secondItemPress}
        isSecondAddonVisible={isSecondAddonVisible}
        thirdItemPress={thirdItemPress}
        isThirdAddonVisible={isThirdAddonVisible}
        checkoutOnCounter={checkoutOnCounter}
        isThankyouModalVisible={isThankyouModalVisible}
        onCustomiseClick={onCustomiseClick}
        isAddonModalVisible={isAddonModalVisible}
        addedCartItems={props.addedCartItems}
        addToCart={props.addToCart}
        isLoading={loader || isSettingsLoading}
        redirectToDetails={redirectToDetails}
        orderId={orderId}
        amount={amount}
        orderRawId={orderRawId}
        coupon_code={props.coupon_code}
        validateCouponCode={validateCouponCode}
        code={code}
        setCode={setCode}
        selectedRestaurant={props.selectedRestaurant}
        loyaltyOffer={loyaltyOffer}
        noLongerModalVisible={noLongerModalVisible}
        setNoLongerModalVisible={setNoLongerModalVisible}
        toHome={toHome}
        bodyText={noLongerAvailableText}
        settingsData={settingsData}
      />
      {isDineInModalVisible && (
        <ScanModal
          onMenuCardPress={onModalOptionSelected}
          isScanModalVisible={isDineInModalVisible}
          toggleScanModal={() => setIsDineInModalVisible(!isDineInModalVisible)}
          showPre={false}
        />
      )}
    </>
  );
};

const mapStateToProps = (state, props) => {
  return {
    addedCartItems: state.cartReducer.cartAddedData,
    userData: state.loginReducer.userData,
    selectedRestaurant: state.cartReducer.selectedRestaurant,
    coupon_code: state.cartReducer.coupon_code,
    userID: _.get(state, "loginReducer.userData.data.user.id", null),
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      addToCart: cartActions.cartUpdate,
      applyCouponCode: cartActions.applyCouponCode,
      selectRestaurant: cartActions.selectRestaurant,
    },
    dispatch
  );
};

const checkoutScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(CheckoutScreen);

checkoutScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null,
});

export default checkoutScreenWithRedux;
