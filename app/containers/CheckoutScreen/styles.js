import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"

let headerButtonContainerWidth = globals.SCREEN_SIZE.width - (globals.MARGIN.marginTen*2);
let headerButtonWidth = (headerButtonContainerWidth-(globals.MARGIN.marginTen*2)) / 3;

const images = {
  
};

const styles = StyleSheet.create({
  screenMain: {
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    flexDirection: 'column',
    backgroundColor:'white',
    flex:1,
  },
  screenDesignContainer: {
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
  },
  screenContainerWithMargin: {
    width: globals.INTEGER.screenWidthWithMargin,
    height: globals.INTEGER.screenContentHeight,//EDITED TO REDUCE FLATLIST HEIGHT
    marginLeft: globals.INTEGER.leftRightMargin,
    paddingBottom:20
  },
  screenMains: {
    backgroundColor:globals.COLOR.greyBackground,
    borderRadius:30,
    flex:1,
     },
     upView:{
        backgroundColor:globals.COLOR.greyBackground,
        borderRadius:30,
        flex:1,
     },
     orderCardView:{
       marginTop:'5%',
     },
     itemView:{
         marginTop: 15,
         marginLeft:'4%'
     },
     details:{
        fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
        color:globals.COLOR.greyText,
        textAlign:'left',
     },
     detail:{
        fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
        color:globals.COLOR.Text,
        textAlign:'left',
     },
     itemCard:{
         marginLeft:'4%',
         marginRight:'4%',
         backgroundColor:'white',
        //height:100,
        justifyContent:'center',
        borderRadius:15,
        marginTop:'5%'
     },
     line:{
         marginVertical:'2%',
     },
     pay:{
         flex:1,
         marginLeft:'4%',
         marginRight:'4%',
         marginTop:'5%'
     },
 
});

export { images, styles };
