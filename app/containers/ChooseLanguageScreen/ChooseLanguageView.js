import React from "react";
import PropTypes from "prop-types";
import {
  View,
  StatusBar,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  Platform,
} from "react-native";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import { images, styles } from "./styles";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
const ChooseLanguageView = (props) => {
  const { selectLanguage } = props;
  return (
    <View style={styles.screenMain}>
      <View style={styles.imageContainer}>
          <Image source={images.teaImage}  style={styles.logo} />
        </View>
      {/* <StatusBar
        barStyle="dark-content"
        backgroundColor={globals.COLOR.screenBackground}
      /> */}
      {/* <ImageBackground style={{ flex: 1 }} source={images.blurImage}
        blurRadius={ Platform.OS == 'ios' ? 10 : 3 }  
      > */}
        

        <View style={styles.languageContainer}>
          <View style={{ alignItems: "center", }}>
            <View style={{flexDirection:'row',marginVertical:hp('4%')}}>
              <Text style={styles.chooseText}>
                {appTexts.SELECT_LANGUAGE.Choose}
              </Text>
              <Text style={styles.chooseTexts}>
                {appTexts.SELECT_LANGUAGE.Language}
              </Text>
            </View>
            {/* <View style={styles.borderView} /> */}
          </View>
          <TouchableOpacity onPress={() => {selectLanguage("EN") }}>
          <View style={styles.boxView}>
                  <View style={styles.flagLine}>
                    <View style={styles.flagIon}>
                      <Image style={styles.flagView} source={images.redFlag}></Image>
                    </View>
                    <View style={styles.engText}>
                      <Text style={styles.engTexts}>English</Text>
                    </View>
                  </View>
                  <View style={styles.arrowv}>
                  <Image source={images.varrow} style={styles.varrow}></Image>
                  </View>
                </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => {selectLanguage("AR")}}> 
                <View style={styles.boxView}>
                  <View style={styles.flagLine}>
                    <View style={styles.flagIon}>
                      <Image style={styles.flagView} source={images.sFlag}></Image>
                    </View>
                    <View style={styles.engText}>
                      <Text style={styles.engTexts}>عربى</Text>
                    </View>
                  </View>
                  <View style={styles.arrowv}>
                  <Image source={images.varrow} style={styles.varrow}></Image>
                  </View>
                </View>
                </TouchableOpacity>
                <View style={styles.design}>
                  <Image source={images.foodImage} style={styles.greenDesign}>
                  </Image>
                </View>

          
        </View>
      {/* </ImageBackground> */}
    </View>
  );
};

ChooseLanguageView.propTypes = {
  selectLanguage: PropTypes.func,
};

export default ChooseLanguageView;
