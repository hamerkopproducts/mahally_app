import React, { useState, useEffect } from 'react';
import { BackHandler, I18nManager } from 'react-native';
import { connect } from 'react-redux';
import ChooseLanguageView from './ChooseLanguageView';
import { bindActionCreators } from "redux";
import RNRestart from 'react-native-restart';
import * as loginActions from "../../actions/loginActions";

const ChooseLanguageScreen = (props) => {
  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    BackHandler.addEventListener('hardwareBackPress', backPressed = () => {
      BackHandler.exitApp();
      return true;
    });
  };

  //updated
  useEffect(() => handleComponentUpdated());
  const handleComponentUpdated = () => {
  };

  const selectLanguage = (selectedLanguage) => {
      
      if (selectedLanguage === 'EN') {
        I18nManager.forceRTL(false);
        setTimeout(() => {
          RNRestart.Restart();
        }, 500);
      } else if (selectedLanguage === 'AR'){
        I18nManager.forceRTL(true);
        setTimeout(() => {
          RNRestart.Restart();
          props.saveSelectedLanguage(selectedLanguage);
        }, 500);
      }
    props.saveSelectedLanguage(selectedLanguage);
  };
  return (
    <ChooseLanguageView selectLanguage={selectLanguage}/>
  );

};


const mapStateToProps = (state, props) => {
  return {
  
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    saveSelectedLanguage: loginActions.saveSelectedLanguage
  }, dispatch)
};

const chooseLanguageWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(ChooseLanguageScreen);

chooseLanguageWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default chooseLanguageWithRedux;
