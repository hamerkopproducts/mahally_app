import { StyleSheet,I18nManager } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import globals from "../../lib/globals";
let headerButtonContainerWidth =
  globals.SCREEN_SIZE.width - globals.MARGIN.marginTen * 2;
let headerButtonWidth =
  (headerButtonContainerWidth - globals.MARGIN.marginTen * 2) / 3;
const images = {
  teaImage: require("../../assets/images/chooseLanguage/tea.png"),
 
  varrow:require('../../assets/images/profileicon/arrow.png'),
  redFlag:require('../../assets/images/chooseLanguage/eng.png'),
  sFlag:require('../../assets/images/chooseLanguage/Arabic.png'),
  foodImage:require('../../assets/images/chooseLanguage/Bottom.png')
};

const styles = StyleSheet.create({
  screenMain:{
      flex:1,
      backgroundColor: globals.COLOR.screenBackground
  },
  screenMainContainer:{
    position: 'absolute',
    top: 0,
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    alignItems: 'center',
    justifyContent: 'center'
  },
  bgImage: {
    position: 'absolute',
    top: 0,
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height
  },
  imageContainer: {
  flex: 0.4,
  position:"relative",
  alignSelf:"flex-start"
  //  marginTop:hp('1%'),
  
  },
  languageContainer: {
    flex: 1,
  },
  screenContainer: {
    flex: 1,
    backgroundColor: globals.COLOR.transparent,
    marginBottom: globals.INTEGER.screenBottom
  },
  logo: {
    // flex:1,
    // alignSelf:"flex-start",
    width: 280,
    height: 200,
    resizeMode: 'cover'
  },
  chooseText: {
    // color:'black',
     //fontSize:16
    fontSize: hp('2.8%'),
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color: '#1d2226',
  },
  chooseTexts: {
    // color:'black',
    // fontSize:16
    fontSize: hp('2.8%'),
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color: "#1d2226",
    paddingLeft:'2%',
  },
  borderView: {
    width: "16%",
    height: 2,
    //borderRadius: 5,
    marginTop: 10,
    marginBottom: 5,
    backgroundColor: "#ff8001",
    alignItems: 'center', justifyContent: 'center'
  },
  buttonWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    //paddingTop: '10%',
    paddingTop: hp('4%'),
    paddingLeft: '5%',
    paddingRight: '5%'
  },
  engButton: {
    width: 142,
    height: 50,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: 'white',
    borderColor: globals.COLOR.themeGreen,
    borderWidth: 1,
    marginRight:10
  },
  arButton: {
    marginLeft: 10,
    width: 142,
    height: 50,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: globals.COLOR.themeGreen,

  },
  buttononview: {
    alignItems: "center",
    justifyContent: "center",
    //marginTop: hp('2.5%'),

  },
  engText: {
    color: globals.COLOR.themeGreen,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: hp('2.2%'),
  },
  arText: {
    color: 'white',
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: hp('2.2%'),
  },
 blurImage:{
     width:'100%',
      height:150,
      
     //resizeMode:'contain',
     alignSelf:'center'
    //paddingLeft:'5%',transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
    
  },
  boxView:{
    height:60,
    width:'90%',
    borderWidth:0.5,
    borderColor:'#707070',
    flexDirection:'row',
    justifyContent:'space-between',
    marginTop:'2%',
    marginLeft:'5%',
    marginRight:'5%',
    borderRadius:10,
    marginBottom:'2%',
     },
     flagLine:{
       flexDirection:'row',
       justifyContent:'center',
       alignItems:'center',
      marginHorizontal:'5%'
     },
     flagIon:{
       justifyContent:'center',
       alignItems:'center'
     },
     engText:{
       paddingLeft:'12%',
     },
     engTexts:{
       fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
      fontSize:17,
     },
     flagView:{
      height: 22,
      width: 33,
      borderRadius: 10 

     },
     hinput: {
      fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
      fontSize: hp('2%'),
      textAlign: I18nManager.isRTL ? "right" : "left",
      color:"#707070",
      marginLeft:'2%'
      
    },
    varrow:{
      width:20,
      height:20,
      alignSelf:'center'
    },
    arrowv:{
      marginRight:'4%',
      alignItems:'center',
      justifyContent:'center'
        },
        design:{
          bottom:0,
          left:0,
          margin:0,
          position:"absolute",
          marginHorizontal:wp('20%'),
        //  Top:20
        },
        greenDesign:{
          marginTop:hp('1%'),
           width:250,
           height:200,
          resizeMode:'contain'
        },
});

export { images, styles };
