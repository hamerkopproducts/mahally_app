import React, { Component } from "react";
import {
  Button,
  Text,
  View,
  TouchableOpacity,
  Image,
  StatusBar,
  TextInput,
} from "react-native";
import Modal from "react-native-modal";
import { styles } from "./styles";
import LinearGradient from "react-native-linear-gradient";
import globals from "../../lib/globals";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import appTexts from "../../lib/appTexts";
export default class Editprofile extends Component {
  state = {
    isModalVisible: false,
  };

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };
  render() {
    return (
      <View style={styles.screenMain}>
        <View style={styles.headerWrapper}></View>
        <View style={{ height: hp("1%"), backgroundColor: "#E8E8E8" }}></View>
        <View style={styles.formWrapper}>
          <View style={styles.contentWrapper}>
            <View style={styles.profileWrapper}>
              <View style={styles.editSection}>
                <View style={styles.imageSection}>
                  <Image
                    source={require("../../assets/images/temp/main.png")}
                    style={styles.logo}
                  />
                </View>
                <View style={styles.detailsWrapper}>
                  <Text style={styles.Name}>John Mathew</Text>
                  <Text style={styles.Phone}>+966 544537382</Text>
                  <Text style={styles.Email}>johnsnow123@gmail.com</Text>
                </View>
              </View>
              <View style={styles.editButton}>
                <TouchableOpacity onPress={this.toggleModal}>
                  <Text style={styles.editName}>  {appTexts.PROFILELISTING.Edit}</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{ justifyContent: "center", flexDirection: "row" }}>
              <View
                style={{
                  width: "95%",
                  height: 1,
                  borderRadius: 1,
                  marginTop: hp("0.5%"),
                  //marginBottom: hp('1%'),
                  backgroundColor: "lightgray",
                  borderStyle: "dashed",
                }}
              />
            </View>
            <View style={styles.mainList}>
              <View style={styles.listWrapper}>
                <View style={{ flexDirection: "row" }}>
                  <View style={styles.imagemainWrapper}>
                    <Image
                      source={require("../../assets/images/profileicon/faq.png")}
                      style={styles.imageMain}
                    />
                  </View>
                  <View style={styles.contentsWrapper}>
                    <Text style={styles.contentHeading}>
                      {appTexts.PROFILELISTING.Faq}
                    </Text>
                  </View>
                </View>
                <View style={styles.arrowWrapper}>
                  <Image
                    source={require("../../assets/images/profileicon/arrow.png")}
                    style={styles.arrowImage}
                  />
                </View>
              </View>
              <View style={styles.listWrapper}>
                <View style={{ flexDirection: "row" }}>
                  <View style={styles.imagemainWrapper}>
                    <Image
                      source={require("../../assets/images/profileicon/PrivacyPolicy.png")}
                      style={styles.imageMain}
                    />
                  </View>
                  <View style={styles.contentsWrapper}>
                    <Text style={styles.contentHeading}>
                      {appTexts.PROFILELISTING.Privacy}
                    </Text>
                  </View>
                </View>
                <View style={styles.arrowWrapper}>
                  <Image
                    source={require("../../assets/images/profileicon/arrow.png")}
                    style={styles.arrowImage}
                  />
                </View>
              </View>
              <View style={styles.listWrapper}>
                <View style={{ flexDirection: "row" }}>
                  <View style={styles.imagemainWrapper}>
                    <Image
                      source={require("../../assets/images/profileicon/terms.png")}
                      style={styles.imageMain}
                    />
                  </View>
                  <View style={styles.contentsWrapper}>
                    <Text style={styles.contentHeading}>
                      {appTexts.PROFILELISTING.Terms}
                    </Text>
                  </View>
                </View>
                <View style={styles.arrowWrapper}>
                  <Image
                    source={require("../../assets/images/profileicon/arrow.png")}
                    style={styles.arrowImage}
                  />
                </View>
              </View>
              <View style={styles.listWrapper}>
                <View style={{ flexDirection: "row" }}>
                  <View style={styles.imagemainWrapper}>
                    <Image
                      source={require("../../assets/images/profileicon/support.png")}
                      style={styles.imageMain}
                    />
                  </View>
                  <View style={styles.contentsWrapper}>
                    <Text style={styles.contentHeading}>
                      {appTexts.PROFILELISTING.Support}
                    </Text>
                  </View>
                </View>
                <View style={styles.arrowWrapper}>
                  <Image
                    source={require("../../assets/images/profileicon/arrow.png")}
                    style={styles.arrowImage}
                  />
                </View>
              </View>
              <View style={styles.listWrapper}>
                <View style={{ flexDirection: "row" }}>
                  <View style={styles.imagemainWrapper}>
                    <Image
                      source={require("../../assets/images/profileicon/logout.png")}
                      style={styles.imageMain}
                    />
                  </View>
                  <View style={styles.contentsWrapper}>
                    <Text style={styles.contentHeading}>
                      {appTexts.PROFILELISTING.Logout}
                    </Text>
                  </View>
                </View>
                <View style={styles.arrowWrapper}>
                  <Image
                    source={require("../../assets/images/profileicon/arrow.png")}
                    style={styles.arrowImage}
                  />
                </View>
              </View>
            </View>

          </View>
        </View>

        <Modal
          isVisible={this.state.isModalVisible}
          style={styles.modalMainContent}
        >
          <View style={styles.modalmainView}>
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
              <View
                style={{
                  width: "25%",
                  height: 3,
                  borderRadius: 5,
                  marginTop: hp('0.5%'),
                  //marginBottom: hp('1%'),
                  backgroundColor: 'pink',
                }}
              />
            </View>
            <View style={styles.buttonwrapper}>
              <Image
                source={require("../../assets/images/temp/close.png")}
                style={{ resizeMode: "contain", width: 30, height: 30 }}
              />
            </View>
            <View style={styles.modaltextWarpper}>
              <Text style={styles.headText}>{appTexts.EDITPROFILE.Heading}</Text>
            </View>
            <View style={styles.modalimageWarpper}>
              <Image
                source={require("../../assets/images/temp/main.png")}
                style={styles.modallogo}
              />

            </View>

            <View style={styles.formWrappers}>
              <View style={styles.subjectWrapper}>
                <Text style={styles.subText}>{appTexts.EDITPROFILE.Name}</Text>
                <View style={{ marginTop: -14 }}>
                  <TextInput style={styles.input}
                    underlineColorAndroid="lightgray"
                    //placeholder = "Password"
                    //placeholderTextColor = "#9a73ef"
                    autoCapitalize="none"
                  />
                </View>
              </View>
              <View style={styles.messageWrapper}>
                <Text style={styles.subText}>{appTexts.EDITPROFILE.Email}*</Text>
                <View style={{ marginTop: -14 }}>
                  <TextInput style={styles.input}
                    underlineColorAndroid="lightgray"
                    //placeholder = "Password"
                    //placeholderTextColor = "#9a73ef"
                    autoCapitalize="none"
                  />
                </View>
              </View>

              <View style={styles.messageWrapper}>
                <Text style={styles.subText}>{appTexts.EDITPROFILE.Phone}*</Text>
                <View style={{ marginTop: -2 }}>

                  <View style={styles.mobilnumberWrapper}>
                    <View style={styles.flagWarpper}>
                      {/* <Image
                  source={require("./images/flag.png")}
                  style={{ height: 20, width: 20, marginLeft: 8 }}
                /> */}
                      <TextInput
                        editable={false}
                        underlineColorAndroid="lightgray"
                        placeholder={"+966"}
                        placeholderTextColor="#282828"
                      /></View>
                    <View style={styles.phonenumberWrapper}>
                      <TextInput
                        underlineColorAndroid="lightgray"
                        //placeholder = "Password"
                        //placeholderTextColor = "#9a73ef"
                        autoCapitalize="none"
                      />
                    </View>
                  </View>
                </View>
              </View>
            </View>

            <View style={styles.buttoninsidewrapper}>

              <TouchableOpacity
                style={styles.buttononview}
                onPress={() => this.props.onPress()}
              >
                <LinearGradient
                  start={{ x: 0, y: 0 }}
                  end={{ x: 1, y: 2.45 }}
                  locations={[0, 3]}
                  colors={["#ff8001", "#fbc203"]}
                  style={styles.yButtonon}
                >
                  <Text
                    style={styles.yellowbuttonText
                    }
                  >
                    {appTexts.EDITPROFILE.Update}
                  </Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>


          </View>
        </Modal>


      </View>
    );
  }
}
