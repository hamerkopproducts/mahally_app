import { StyleSheet } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import globals from "../../lib/globals";
let headerButtonContainerWidth =
  globals.SCREEN_SIZE.width - globals.MARGIN.marginTen * 2;
let headerButtonWidth =
  (headerButtonContainerWidth - globals.MARGIN.marginTen * 2) / 3;
const images = {};
const styles = StyleSheet.create({
  screenMain: {
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    flexDirection: "column",
  },
  imageContainer: {
    flex: 0.4,
    //backgroundColor:'red',
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    alignItems: 'center',
    paddingTop: '10%'

  },
  languageContainer: {
    flex: 1,
    //backgroundColor:'green',
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    //alignItems: 'center',
    paddingTop: '25%'

  },
  screenContainer: {
    flex: 1,
    backgroundColor: globals.COLOR.transparent,
    marginBottom: globals.INTEGER.screenBottom
  },
  headerWrapper: {
    flex: .1,
    //backgroundColor:'pink',
    flexDirection: 'row',
    alignItems: 'center'

  },
  formWrapper: {
    flex: 1,
    //backgroundColor:'red',
    //alignItems:'center'
  },
  sendPhone: {
    flexDirection: 'row',
    paddingTop: '7%'
  },
  otpWrapper: {
    flexDirection: "row",
    justifyContent: "space-around",
    width: "80%",
    paddingTop: 5,
  }, engButton: {
    width: 142,
    height: 50,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: '#ff8001',

  },
  borderView: {
    width: "12%",
    height: 2,
    //borderRadius: 5,
    marginTop: 10,
    marginBottom: 5,
    backgroundColor: "#ff8001",
    alignItems: 'center', justifyContent: 'center'
  },
  textInput: {
    paddingLeft: 0,
    height: 40,
    fontSize: 13,
    width: "15%",
    borderColor: "gray",
    borderBottomWidth: 1.5,
    textAlign: "center",
  },
  verifyText: {
    fontSize: hp('2.4%'),
    color: '#232020',
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  verifyWrapper: {
    paddingLeft: '12%'

  },
  editText: {
    color: '#ff8001',
    paddingLeft: '6%',
    fontSize: 16
  },
  imageWrapper: {
    paddingLeft: '5%'

  },
  arrowImage: {
    width: 45,
    height: 45,
    resizeMode: 'contain',
    paddingLeft: '5%'

  },
  digitCode: {
    fontSize: 17
  },
  phoneNumber: {
    fontSize: 15
  },
  enterDigit: {
    paddingTop: '25%'
  },
  reseondCode: {
    paddingTop: '9%'
  },
  buttonWrapper: {
    paddingTop: '20%'
  },
  submitText: {
    color: 'white'
  },
  contentWrapper: {

    paddingTop: hp('2%'),
    //borderBottomWidth:1,
    //borderBottomColor:'red',
    //paddingLeft:4
  },
  headingText: {
    fontSize: hp('2.1%'),
    color: '#232020',
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  }, descriptionText: {
    paddingTop: hp('1.5%'),
    fontSize: hp('1.8%'),
    color: '#232020',
    lineHeight: 22,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  profileWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: '1%',
    paddingRight: '1%',


    //alignItems:'center'

  },
  editSection: {
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  imageSection: {
    paddingLeft: '3%',
    //borderRadius: 40,
  },
  editButton: {
    paddingRight: '2%',
    paddingTop: hp('1.5%'),
    
  },
  detailsWrapper: {
    flexDirection: 'column', flexWrap: 'wrap', justifyContent: 'center', paddingLeft: '5%'
  },
  logo: {
    width: 85,
    height: 85,
    resizeMode: 'contain',
    borderRadius: 55
  },
  modallogo: {
    width: 100,
    height: 100,
    resizeMode: 'contain',
    borderRadius: 55,
    alignSelf: 'center'
  },
  Name: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helveticaBold,
    fontSize: hp('1.8%'),
  },
  Email: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: hp('1.6%'),
    color: '#707070'
  },

  Phone: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: hp('1.6%'),
    color: '#707070'
  },
  editName: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: hp('1.7%'),
    color: '#ff8001'
  },
  listWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    //paddingTop:hp('3%'),
    paddingBottom: hp('5%'),
    paddingLeft: '3%'
  },
  mainList: {
    paddingTop: hp('4%')
  },
  imageMain: {
    resizeMode: "contain", width: 30, height: 30,
  },
  contentHeading: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helveticaBold,
    fontSize: hp('2%'),
    paddingLeft: '4%'
    //color:'#707070'
  },
  contentsWrapper: {
    //paddingLeft:'8%'
  },
  imagemainWrapper: {
    //paddingLeft:'5%'
  },
  arrowWrapper: {
    flexDirection: 'row',
    paddingRight: '3%'
  },
  arrowImage: {
    resizeMode: "contain", width: 15,
    height: 15
  },


  modalMainContent: {
    //justifyContent: "center",
    justifyContent: 'flex-end',
    margin: 0
  },
  modalmainView: {
    backgroundColor: "white",
    //width: wp("90%"),
    padding: "4%",
    //borderRadius: 10,
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  buttonwrapper: {
    flexDirection: "row",
    justifyContent: 'flex-end'

  },
  textWarpper: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: hp('1%'),
    paddingBottom: hp('1.5%')
  }, 
  modaltextWarpper: {
    flexDirection: 'row',
    justifyContent: 'center',
    //paddingTop:hp('1%'),
    paddingBottom: hp('1.5%')
  },
  headText: {
    fontSize: hp('2.5%'),
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helveticaBold,
  },
  whitebuttonText: {
    fontSize: hp('2.1%'),
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color: '#FF8001'
  },
  yellowbuttonText: {
    fontSize: hp('2.1%'),
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color: 'white'
  },
  subText: {
    fontSize: hp('2.1%'),
    color: "#6f6f6f",
    paddingLeft: '1%',
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  }, subjectWrapper: {
    paddingTop: hp('2%'),
    //marginTop:-3

  }, messageWrapper: {
    paddingTop: hp('2%'),
  },
  input: {
    //marginTop:-4
    // margin: 15,
    // height: 40,
    // borderColor: '#7a42f4',
    // borderWidth: 1
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: hp('2.2%'),
  },
  mobilnumberWrapper: {
    flexDirection: 'row'
  },
  flagWarpper: {
    flexBasis: '20%',
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",

  }, phonenumberWrapper: {
    flexBasis: '80%'
  },
  buttoninsidewrapper: {
    flexDirection: "row",
    justifyContent: "space-around",
    paddingTop: "3%",
    paddingBottom: "7%",
  }, yButtonon: {
    width: 140,
    height: 50,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center",
    shadowOpacity: 0.58,
    shadowRadius: 16.00,
    elevation: 5,
    // borderColor:'#d0d0d0',
    // borderWidth:1
  },
  buttononview: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: hp('2.5%'),


  },

});

export { images, styles };
