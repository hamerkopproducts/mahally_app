import React from "react";
import PropTypes from "prop-types";
import { View, Text, TouchableOpacity, Image } from "react-native";
import { styles } from "./styles";
import appTexts from "../../lib/appTexts";

const ProfileView = (props) => {
  const { onBackButtonPress } = props;

  return (
    <View style={styles.headerWrapper}>
      <TouchableOpacity
        style={styles.imageWrapper}
        onPress={() => {
          onBackButtonPress();
        }}
      >
        <Image
          source={require("../../assets/images/chooseLanguage/backarow.png")}
          style={styles.arrowImage}
        />
      </TouchableOpacity>
      <View style={styles.verifyWrapper}>
        <Text style={styles.verifyText}> {appTexts.PROFILELISTING.Faq}</Text>
      </View>
    </View>
  );
};

ProfileView.propTypes = {
  logoutButtonPress: PropTypes.func,
};

export default ProfileView;
