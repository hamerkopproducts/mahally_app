import React from 'react';
import { View, Text } from 'react-native';
import globals from "../../lib/globals";
import { styles } from "./styles";

import Header from "../../components/Header";

const FavouritesView = (props) => {
	const {
		
	} = props;

	return (
		
				<View style={styles.screenMain}>
			<Header
				navigation={props.navigation}
				isLogoRequired={true}
				customHeaderStyle={{
					height: globals.INTEGER.headerHeight,
					alignItems: "center",
					backgroundColor: globals.COLOR.headerColor
				}}
			/>
					<View style={styles.screenContainer}>
						<Text>FavouritesView</Text>
					</View>
				</View>
			

	);
};

FavouritesView.propTypes = {
	
};

export default FavouritesView;
