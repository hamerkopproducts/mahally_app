import React, { useEffect, useState } from 'react';

import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import FavouritesView from './FavouritesView';
import { bindActionCreators } from "redux";

import globals from "../../lib/globals";
import functions from "../../lib/functions"


const FavouritesScreen = (props) => {

  

  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
  
        } else {
          functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
      
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });
  }

  //unmount
  useEffect(() => { return () => { handleComponentUnmount(); } }, []);

  const handleComponentUnmount = () => {
   
  };
 
  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
  
  };
  
  return (
    <FavouritesView/>
  );

};


const mapStateToProps = (state, props) => {
  return {
    
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
   
  }, dispatch)
};

const favouriteScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(FavouritesScreen);

favouriteScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default favouriteScreenWithRedux;
