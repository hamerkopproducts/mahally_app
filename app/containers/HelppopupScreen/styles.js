import { StyleSheet ,I18nManager} from "react-native";
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
  } from "react-native-responsive-screen";
import globals from "../../lib/globals";
let headerButtonContainerWidth =
  globals.SCREEN_SIZE.width - globals.MARGIN.marginTen * 2;
let headerButtonWidth =
  (headerButtonContainerWidth - globals.MARGIN.marginTen * 2) / 3;
const images = {};
const styles = StyleSheet.create({
  screenMain: {
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    flexDirection: "column",
  },
  thirdwrapper: {
    flex: 1,
    justifyContent: "flex-start",
    paddingTop: "50%",
  },
  Signupwrapper: {
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
    paddingTop: "2%",
  },

  modalMaincontentHelp: {
    //justifyContent: "center",
    justifyContent:'flex-end',
    margin: 0
  },
  modalmainviewHelp: {
    backgroundColor: "white",
    //width: wp("90%"),
    padding: "4%",
    //borderRadius: 10,
    borderTopRightRadius:15,
    borderTopLeftRadius:15,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  
  buttohelpnwrapper: {
    flexDirection: "row",
     justifyContent:'flex-end'

  },
  helpbuttoninsidewrapper: {
    flexDirection:  "row",
    justifyContent: "space-around",
    paddingTop: "3%",
    paddingBottom: "7%",
  },
  whiteButtonon: {
    width: 130,
    height: 42,
    borderRadius: 22,
    alignItems: "center",
    justifyContent: "center",
    borderColor:'#d0d0d0',
    borderWidth:1
    
},
hylpyButtonon:{
  width: 140,
    height: 50,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center",
    shadowOpacity: 0.58,
    shadowRadius: 16.00,
    elevation:5,
    // borderColor:'#d0d0d0',
    // borderWidth:1
},
helpbuttononview: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: hp('2.5%'),
    

},
helptextWarpper:{
flexDirection:'row',
justifyContent:'center',
paddingTop:hp('1%'),
paddingBottom:hp('1.5%')
},
helpheadText:{
  fontSize: hp('2.3%'),
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
},
whitebuttonText:{
  fontSize: hp('2.1%'),
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  color:'#FF8001'
},
helpyellowbuttonText:{
  fontSize: hp('2.1%'),
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  color:'white'
},
helpsubText:{
  fontSize: hp('2.1%'),
  color:"#6f6f6f",
  paddingLeft:'1%',
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
},helpsubjectWrapper:{
paddingTop:hp('2%'),
//marginTop:-3

},helpmessageWrapper:{
  paddingTop:hp('2%'),
},
hinput: {
  //marginTop:-4
  // margin: 15,
  // height: 40,
  // borderColor: '#7a42f4',
  // borderWidth: 1
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  fontSize: hp('2.2%'),
  textAlign: I18nManager.isRTL ? "right" : "left",
},
});

export { images, styles };
