import ServiceWrapperAwait from '../../../service/ServiceWrapperAwait';

const test = false;
const latitude = '24.773';
const longitude = '46.67';

export default class HomeScreenServices {

  getNearbyRestaurantListNoPagination = async (location, restTypesFilter) => {
    const sAsyncWrapper = new ServiceWrapperAwait();
    const latitudeUser = location.status == true ? location.location.data.latitude : '';
    const longitudeUser = location.status == true ? location.location.data.longitude : '';
    const data = {
      latitude: test == true ? latitude : latitudeUser,
      longitude: test == true ? longitude : longitudeUser,
      category: restTypesFilter,
      sortby: 'nearby',
    }
    const response = await sAsyncWrapper.post('user-app/restaurent', data);
    return {
      data: typeof response.data != 'undefined' ? response.data : [],
      langChosen: response.langSelected,
      loading: false,
    };
  }

  timeFormat = (time) => {

    time = time.split(':');
    try {
      time = (time[0] > 12 ? time[0] - 12 : time[0]) + ':' + time[1] + (time[0] <= 11 ? ' AM' : ' PM');
    } catch (error) {
      return '';
    }

    return time;
  }
}