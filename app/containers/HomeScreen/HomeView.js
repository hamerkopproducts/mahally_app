import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import {
	View,
	FlatList,
	SafeAreaView,
	Linking,
	PermissionsAndroid,
	Platform,
	TouchableOpacity,
	Text,
	Alert
} from "react-native";
import globals from "../../lib/globals";
import { styles } from "./styles";

import Header from "../../components/Header";
import HomeCard from "../../components/HomeCard";
import TopTab from "../../components/TopTab";
import appTexts from "../../lib/appTexts";
import BannerButton from "../../components/BannerButton";
import MapScreen from "./MapScreen";

import { store } from '../../../configureStore';
import Geolocation from 'react-native-geolocation-service';
import GetLocation from 'react-native-get-location';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import ServiceWrapperAwait from '../../service/ServiceWrapperAwait';
import functions from "../../lib/functions";
import InAppNotificationModal from "../NotificationPopup/InAppNotificationModal";

import SortModal from "../../components/SortModal";
import FilterModal from "../../components/FilterModal";
import Loader from '../../components/Loader';

const HomeView = (props) => {

	const {
		homeCardButtonPress,
		tabIndex,
		onemoretab,
		firstTabCount,
		secondTabCount,
		onTabChange,
		onProfileButtonPress,
		onRightButtonPress,
		onCheckoutClick,
		notifyData,
		displayNotificationPopup,
		setDisplayNotificationPopup,
		navigateTochat,
		restTypes,
		restTypesID,
		navigation,
		addedCartItems,
		is_notification_active,
		setLocationLocally,
		languageSwitchScreen,
		notification_badge
	} = props;

	const [loader, setLoader] = useState(false);
	const [descriptionVisible, setDescVisible] = React.useState(false);
	const [isFetching, setIsFetching] = useState(false);
	const localStoreLocation = store.getState().geoLocationReducer.geoLocation;
	const [location, setLocation] = React.useState(localStoreLocation);
	const [locationChanged, setLocationChange] = React.useState(false);
	const [locationAccessAllow, setLocationAccessAllow] = React.useState(false);
	const [isFilterModalVisible, setIsFilterModalVisible] = useState(false);
	const [_allRestaurants, set_allRestaurants] = useState([]);
	const [_page, setPage] = useState(1);
	const [_lastPage, setLastPage] = useState(0);
	const [_total, setTotal] = useState(0);
	const [restTypesFilter, setRestTypesFilter] = useState([]);
	const [isNearbySelected, setNaerbySelected] = useState(true);
	const [isPopularitySelected, setPopularitySelected] = useState(false);
	const [sortValue, sortClicked] = useState('nearby');

	const [isSortModalVisible, setIsSortModalVisibile] = useState(false);

	const getCollections = async (page, is_pull_down, location, sortType, _restTypesFilter=[]) => {
		const sAsyncWrapper = new ServiceWrapperAwait();

		if (is_pull_down != 'pulldown') {
			setLoader(true);
		}
		const _page = `?page=${page}`;
		const latitudeUser = location.status == true ? location.location.data.latitude : '';
		const longitudeUser = location.status == true ? location.location.data.longitude : '';

		const data = {
			latitude: latitudeUser,
			longitude: longitudeUser,
			sortby: sortType,
			category: _restTypesFilter
		}
		const response = await sAsyncWrapper.post('user-app/restaurent' + _page, data);
		try {
			const data = new Promise((resolve, reject) => {
				setLoader(false);
				setIsFetching(false);
				try {
					resolve(response);
				} catch (err) {
					reject(err);
				}
			});

			return data;
		} catch (err) {
			setLoader(false);
			setIsFetching(false);
		}
	}
	//will focus
	useEffect(() => {
		let isMounted = true;
		const unsubscribe = navigation.addListener("focus", () => {
			setDescVisible(false);
			setLocationAccessAllow(!locationAccessAllow);
			if (isMounted && location.status === true) {
				// setPage(1);
				restaurantsListFetch(1, '', location, 'nearby', restTypesFilter); 
			}
		});

		return () => {
			unsubscribe(); 
			isMounted = false 
		};
	}, [navigation]);

	const nearadioClicking = (flag) => {
		sortClicked('nearby')
		setNaerbySelected(true)
		setPopularitySelected(false)
		// setPage(1);
		restaurantsListFetch(1, '', location, 'nearby', restTypesFilter);
	}
	const popradioClicking = (flag) => {
		sortClicked('popularity')

		setNaerbySelected(false)
		setPopularitySelected(true)
		// setPage(1);
		restaurantsListFetch(1, '', location, 'popularity', restTypesFilter);
	}

	
	const applyFilter = async(types) => {
		setIsFetching(true);
		set_allRestaurants([]);
		// setPage(1);
		setLastPage(0);
		setTotal(0);
		setIsFilterModalVisible(!isFilterModalVisible)
		setRestTypesFilter([...types]);
		setLoader(true);
		await restaurantsListFetch(1, '', location, sortValue, [...types]);
		setLoader(false);
	}

	const restaurantsListFetch = async (page, is_pull_down, location, sortValue, _restTypesFilter) => {
		const data = await getCollections(page, is_pull_down, location, sortValue, _restTypesFilter);
		if (data.success == true) {
			setTotal(data.data.total);
			setLastPage(data.data.last_page);
			setPage(data.data.current_page);
			if (data.data.current_page == page) {
				let _data = [];
				if (page == 1) {
					_data = [...data.data.data];
				} else {
					_data = [..._allRestaurants, ...data.data.data];
				}
				set_allRestaurants(_data);
			}
		} else {
			if(data == 'errors') {
				functions.displayToast('error', 'top', 'Error', 'Network error');
			} else {
				functions.displayToast('error', 'top', 'Error', data?.error?.msg || '');
			}
		}
	}

	const locationData = async () => {
		try {
			const locationPermission = await hasLocationPermission();
			if (!locationPermission) {
				return { status: false, data: 'DENIED' };
			}
			if (locationPermission) {
				return RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
					interval: 10000,
					fastInterval: 5000,
				})
					.then(data => {
						return GetLocation.getCurrentPosition({
							enableHighAccuracy: false,
							timeout: 150000,
						})
							.then(location => {
								setLocation({ status: true, location: { data: location } });
								setLocationChange(!locationChanged);
								return { status: true, data: location };
							})
							.catch(ex => {
								const { code, message } = ex;
								return { status: false, data: code };
							});
					})
					.catch(err => {
						return { status: false, data: 'UNAVAILABLE' };
					});
			} else {
				return { status: false, data: 'DENIED' };
			}
		} catch (err) {
			return { status: true, data: 'WRONG' };
		}
	};

	const openSetting = () => {
		Linking.openSettings().catch(() => {
			Alert.alert('Unable to open settings');
		});
	};
	const refreshPage = () => {
		setIsFetching(true);
		set_allRestaurants([]);
		// setPage(1);
		setLastPage(0);
		setTotal(0);
		restaurantsListFetch(1, '', location, sortValue, restTypesFilter);
	};

	const openAppSetting = () => {
		Linking.openURL('app-settings:').catch(() => {
			Alert.alert('Unable to open settings');
		});
	};

	const hasLocationPermissionIOS = async () => {
		const status = await Geolocation.requestAuthorization('whenInUse');

		if (status === 'granted') {
			return true;
		}

		if (status === 'denied') {
			Alert.alert(
				`Allow Mahally App to determine your location.`,
				'',
				[
					{ text: 'Go to Settings', onPress: openAppSetting },
					{ text: "Don't Use Location", onPress: () => { } },
				],
			);
		}

		if (status === 'disabled') {
			Alert.alert(
				`Turn on Location Services to allow mahally App to determine your location.`,
				'',
				[
					{ text: 'Go to Settings', onPress: openSetting },
					{ text: "Don't Use Location", onPress: () => { } },
				],
			);
		}

		return false;
	};

	const hasLocationPermission = async () => {
		if (Platform.OS === 'ios') {
			const hasPermission = await hasLocationPermissionIOS();
			return hasPermission;
		}

		if (Platform.OS === 'android' && Platform.Version < 23) {
			return true;
		}

		const hasPermission = await PermissionsAndroid.check(
			PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
		);

		if (hasPermission) {
			return true;
		}

		const status = await PermissionsAndroid.request(
			PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
		);

		if (status === PermissionsAndroid.RESULTS.GRANTED) {
			return true;
		}

		if (status === PermissionsAndroid.RESULTS.DENIED) {
			ToastAndroid.show('Location permission denied by user.', ToastAndroid.LONG);
		} else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
			ToastAndroid.show(
				'Location permission revoked by user.',
				ToastAndroid.LONG,
			);
		}

		return false;
	};

	const locationDataFetch = async () => {
		if (Platform.OS == 'android') {
			const locdata = await locationData();
			setLocationLocally({ location: locdata, status: locdata.status });
		}
		if (Platform.OS === 'ios') {
			const locationPermission = await hasLocationPermission();

			if (!locationPermission) {
				return false;
			}
			if (locationPermission) {
				try {
					Geolocation.getCurrentPosition(
						position => {
							const loc_data = { data: position.coords, status: true };
							setLocation({ status: true, location: loc_data });
							setLocationChange(!locationChanged);
							setLocationLocally({ location: loc_data, status: loc_data.status });
						},
						error => {
							
						},
						{
							enableHighAccuracy: false,
							timeout: 15000,
							maximumAge: 10000,
							distanceFilter: 0,
							forceRequestLocation: true,
							showLocationDialog: true,
						},
					);
				} catch (err) {
					
				}
			}
		}
	};

	const onEndReached = () => {
		const page = parseInt(_page) + 1;
		
		if (page <= _lastPage) {
			// setPage(page);
			restaurantsListFetch(page, '', location, sortValue, restTypesFilter);
		}
	}

	const onRefresh = () => {
		setIsFetching(true);
		set_allRestaurants([]);
		// setPage(1);
		setLastPage(0);
		setTotal(0);
		setTimeout(() => {
			restaurantsListFetch(1, 'pulldown', location, sortValue, restTypesFilter);
		});
	}

	let locationFetched = false;
	React.useEffect(() => {
		if (location.status === false || locationFetched === false) {
			locationFetched = true;
			locationDataFetch();
			if(location.status === true) {
				// setPage(1);
				restaurantsListFetch(1, '', location, 'nearby', restTypesFilter);
			}
		}
		if (tabIndex == 1) {
			// setPage(1);
			restaurantsListFetch(1, '', location, sortValue, restTypesFilter);
		}
		setDescVisible(false);
	}, [locationAccessAllow, tabIndex]);

	const toggleFilterModal = () => {
		setIsFilterModalVisible(!isFilterModalVisible);
	  };

	  const closeModal = () => {
		setIsSortModalVisibile(!isSortModalVisible);
	  };

	const renderItem = ({ item, index }) => (
		<HomeCard mapFlag={false} homeCardButtonPress={homeCardButtonPress} data={item}  />
	);

	return (
		<View style={styles.screenMain}>
			{displayNotificationPopup &&
				<InAppNotificationModal
					hide={() => setDisplayNotificationPopup(false)}
					visible={true}
					title={notifyData.title}
					body={notifyData.body}
					redirect={() => navigateTochat(notifyData)}
				/>
			}
			<Header
			notification_badge={notification_badge}
				isProfileButtonRequired={true}
				onRightButtonPress={onRightButtonPress}
				isLanguageButtonRequired={true}
				isRightButtonRequired={true}
				headerTitle={appTexts.STRING.Mahally}
				onProfileButtonPress={onProfileButtonPress}
				customHeaderStyle={{
					height: globals.INTEGER.headerHeight,
					alignItems: "center",
					backgroundColor: globals.COLOR.headerColor,
				}}
				is_notification_active={is_notification_active}
				screen={'home'}
				languageSwitchScreen={languageSwitchScreen}
			/>

			{(loader == true) && <Loader />}

			<View style={styles.screenDesignContainer}>

				 {/* <View style={styles.screenContainerWithMargin}> */}
					
					<TopTab
						nearadioClicking={nearadioClicking}
						popradioClicking={popradioClicking}
						refreshPage={refreshPage}
						tabIndex={tabIndex}
						applyFilter={applyFilter}
						firstTabText={appTexts.STRING.today}
						secondTabText={appTexts.STRING.all}
						firstTabCount={firstTabCount}
						secondTabCount={secondTabCount}
						onTabChange={onTabChange}
						onemoretab={onemoretab}
						restTypes={restTypes}
						restTypesID={restTypesID}
						restTypesFilter={restTypesFilter}
						isFilterModalVisible={isFilterModalVisible}
						setIsFilterModalVisible={setIsFilterModalVisible}
						isNearbySelected={isNearbySelected}
						isPopularitySelected={isPopularitySelected}
						isSortModalVisible={isSortModalVisible}
						setIsSortModalVisibile={setIsSortModalVisibile}
						hasFilterApplied={restTypesFilter && restTypesFilter.length > 0}
					/>

					{location.status == false && (
						<TouchableOpacity
							onPress={() => {
								setLocationAccessAllow(!locationAccessAllow);
							}}>
							<Text
								style={styles.enableLocation}>
								{appTexts.STRING.enablelocation}
							</Text>
						</TouchableOpacity>
					)}

					{location.status == true &&
						<>
						
							{tabIndex == 0 &&
								<MapScreen
									restTypesFilter={restTypesFilter}
									descriptionVisible={descriptionVisible}
									setDescVisible={setDescVisible}
									homeCardButtonPress={homeCardButtonPress}
									location={location}
									navigation={navigation}
									homeLoader={loader}
								/>
							}

							{tabIndex != 0 &&
								<SafeAreaView style={styles.safeArea}>
									<FlatList
										ListEmptyComponent={
											<>
												{!loader && <Text style={styles.no_orders}>{'No restaurants to display'}</Text>}
											</>
										}
										style={styles.flatListStyle}
										data={_allRestaurants}
										keyExtractor={(item, index) => index.toString()}
										renderItem={renderItem}
										onRefresh={onRefresh}
										refreshing={isFetching}
										onEndReached={() => onEndReached()}
										showsVerticalScrollIndicator={false}
									/>
								</SafeAreaView>
							}
						</>
					}

				</View>
				{tabIndex === 1 &&
					<View style={styles.bannerView}>
						{Object.keys(addedCartItems).length > 0 &&
							<BannerButton
								isHome={true}
								text1={appTexts.BANNER.kitchen}
								text3={appTexts.BANNER.check}
								text5={appTexts.BANNER.view}
								onCheckoutClick={onCheckoutClick}
								addedCartItems={addedCartItems}
							/>
						}
					</View>
				}
				
				<SortModal
					nearadioClicking={nearadioClicking}
					popradioClicking={popradioClicking}
					isNearbySelected={isNearbySelected}
					isPopularitySelected={isPopularitySelected}
					isSortModalVisible={isSortModalVisible}
					closeModal={closeModal}
				/>

				<FilterModal
					applyFilter={applyFilter}
					restTypesFilter={restTypesFilter}
					restTypes={restTypes}
					restTypesID={restTypesID}
					isFilterModalVisible={isFilterModalVisible}
					toggleFilterModal={toggleFilterModal}
				/>
				
		</View>
	)
};

HomeView.propTypes = {
	homeCardButtonPress: PropTypes.func,
	tabIndex: PropTypes.number,
	onemoretab: PropTypes.number,
	listData: PropTypes.array,
	firstTabCount: PropTypes.number,
	secondTabCount: PropTypes.number,
	thirdTabCount: PropTypes.number,
	listButtonClick: PropTypes.func,
	checkBoxClick: PropTypes.func,
	onTabChange: PropTypes.func,
	popupYesClick: PropTypes.func,
	popupNoClick: PropTypes.func,
	selectAllClick: PropTypes.func,
	nearadioClicked: PropTypes.func,
	popradioClicked: PropTypes.func,
	onProfileButtonPress: PropTypes.func,
};

export default HomeView;
