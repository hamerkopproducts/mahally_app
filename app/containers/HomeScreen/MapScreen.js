import React, { useEffect } from 'react';
import { View, Animated } from 'react-native';
import MapView, { Marker } from 'react-native-maps';

import { styles } from './styles';
import { store } from '../../../configureStore';

import HomeScreenServices from './HomeScreenServices/HomeScreenServices';
import Loader from '../../components/Loader';
import HomeCard from "../../components/HomeCard";
import _ from "lodash";
let transformAnim = new Animated.Value(220);

const MapScreen = (
  { props, restTypesFilter, homeCardButtonPress, location, descriptionVisible, setDescVisible, navigation, homeLoader }) => {

  const localStoreLocation = location;
  const [restaurants_list, setData] = React.useState({ data: [] });
  const [loader, setLoader] = React.useState(false);
  const [langChosen, setLang] = React.useState('en');
  const [is_region_changed, setRegionChange] = React.useState({});
  const isMountedmapRef = React.useRef(null);
  const [serviceDetail, setDetailsData] = React.useState({});
  const [updateData, setUpdateData] = React.useState(false);

  useEffect(() => {
    return navigation.addListener("focus", () => {
      setUpdateData(!updateData);
    });
  }, [navigation]);


  const fetchRestaurants = async (_r_types) => {

    let localStoreLocation = store.getState().geoLocationReducer.geoLocation;
    if (typeof is_region_changed != 'undefined') {
      localStoreLocation = {
        status: true,
        location: {
          data: {
            latitude: is_region_changed.latitude, longitude: is_region_changed.longitude
          }
        }
      }
    }
    try {
      const HomeServices = new HomeScreenServices();
      const response = await HomeServices.getNearbyRestaurantListNoPagination(localStoreLocation, _r_types);
      const data = new Promise((resolve, reject) => {
        resolve(response);
      });

      return data;
    } catch (e) {
      
    }
  };

  React.useEffect(() => {
    isMountedmapRef.current = true;
    try {
      if (restaurants_list.data.length === 0) {
        setLoader(true);
      }
    } catch(err) {
      setLoader(true);
    }
    const rest_response = fetchRestaurants(restTypesFilter);
    rest_response.then((data) => {
      if (isMountedmapRef.current) {
        setLoader(false);
        setData(data.data);
        setLang(data.langChosen);
      }
    })
      .catch((error) => {
        if (isMountedmapRef.current) {
          setData({ data: [], langChosen: 'en', status: false });
          setLoader(false);
        }
      });
    return () => isMountedmapRef.current = false;
  }, [is_region_changed, updateData, restTypesFilter]);

  const onRegionChange = (region) => {
    // setRegionChange(region);
  }

  const marker = (item, props) => {
    if (typeof item.user_type != 'undefined') {
      return (
        <Marker
          key={item.id}
          image={require('./images/Location.png')}
          style={{ width: 10, height: 10 }}
          coordinate={{
            latitude: parseFloat(item.latitude),
            longitude: parseFloat(item.longitude),
          }}
        />
      );
    } else {
      try {
        const restaurantId = props.navigation.getParam('restaurant_id', '');
        if (restaurantId != '') {
          props.navigation.setParams({ 'restaurant_id': '' });
          if (restaurantId == item.id) {
            setDetailsData(item);
            setDescVisible(true);
            updateAnimation(0);
          }
        }
      } catch (err) { }

      return (
        <Marker
          key={item.id}
          image={require('./images/Location-pin.png')}
          style={styles.loc}
          onPress={() => {
            setTimeout(() => {
              setDetailsData(item);
            }, 300);
            setDescVisible(true);
            if (descriptionVisible == true) {
              updateAnimation(220);
              setTimeout(() => {
                updateAnimation(1);
              }, 500);
            } else {
              updateAnimation(1);
            }
          }}
          coordinate={{
            latitude: parseFloat(item.latitude),
            longitude: parseFloat(item.longitude),
          }}
        />
      );
    }
  };

  const updateAnimation = (value) => {
    Animated.timing(transformAnim, {
      toValue: value,
      duration: 300,
      useNativeDriver: true
    }).start()
  }

  const allRestaurants =
    typeof restaurants_list.data != 'undefined' ? restaurants_list.data : [];

  let latRegion = 24.773;
  let longRegion = 46.67;
  if (localStoreLocation.status == true) {
    try {
      latRegion = localStoreLocation.location.data.latitude;
      longRegion = localStoreLocation.location.data.longitude;
    } catch (err) {
    }
  }

  let rating = 0;
  let count = 0;
  try {
    rating = _.get(serviceDetail, 'rating.value', "");
    count = _.get(serviceDetail, 'rating.count', "");
  } catch (err) {
    
  }

  const includeUser = [{ id: 'user', latitude: latRegion, longitude: longRegion, user_type: 'user' }, ...allRestaurants];

  return (
    <View style={styles.mapScreen}>
      <MapView
        onRegionChangeComplete={onRegionChange}
         style={styles.map}
        initialRegion={{
          latitude: latRegion,
          longitude: longRegion,
          latitudeDelta: 0.922,
          longitudeDelta: 0.221,
                    
        }}>
        {includeUser.map(item =>
          marker(item, langChosen, setDescVisible, setDetailsData, props),
        )}
      </MapView>

      {(loader == true) && <Loader />}

      {descriptionVisible && (
        <Animated.View style={[styles.markerDetail, styles.detailPos,
        { transform: [{ translateY: transformAnim }] }
        ]} >
          <HomeCard mapFlag={true} homeCardButtonPress={homeCardButtonPress} data={serviceDetail} />
        </Animated.View>
      )}
    </View>
  );
};
export default MapScreen;
