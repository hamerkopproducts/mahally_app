import React, { useRef, useEffect, useState } from "react";
import NetInfo from "@react-native-community/netinfo";
import { connect } from "react-redux";
import HomeView from "./HomeView";
import { bindActionCreators } from "redux";
import * as homeActions from "../../actions/homeActions";
import * as geoLocationActions from "../../actions/geoLocationActions";
import functions from "../../lib/functions";
import appTexts from "../../lib/appTexts";
import messaging from "@react-native-firebase/messaging";
import Sound from "react-native-sound";
import { Notifications } from "react-native-notifications";
import ServiceWrapperAwait from "../../service/ServiceWrapperAwait";
import _ from "lodash";
import { BackHandler, AppState, I18nManager } from 'react-native';
import * as cartActions from "../../actions/cartActions";

const HomeScreen = (props) => {
  const appState = useRef(AppState.currentState);
  const [appStateVisible, setAppStateVisible] = useState(appState.current);


  useEffect(() => {
    AppState.addEventListener("change", _handleAppStateChange);

    return () => {
      AppState.removeEventListener("change", _handleAppStateChange);
    };
  }, []);

  const _handleAppStateChange = (nextAppState) => {
    if (
      appState.current.match(/inactive|background/) &&
      nextAppState === "active"
    ) {
    }

    appState.current = nextAppState;
    setAppStateVisible(appState.current);
    if(appState.current === 'active')
    {
      props.getNotificationCount(props.token);
    }
  };
  //Initialising states
  const [tabIndex, setTabIndex] = useState(0);
  const [isPopupVisible, setIsPopupVisible] = useState(false);
  const [selectedItemToCollect, setSelectedItemToCollect] = useState([]);
  const [selectedItemToReturn, setSelectedItemToReturn] = useState([]);
  const [onemoretab, setOnemoreTab] = useState(0);
  const [notifyData, setNotifyData] = useState({});
  const [displayNotificationPopup, setDisplayNotificationPopup] = useState(
    false
  );
  
  const [sortValue, setSortValue] = useState("");

  const notificationRead = async (id) => {
    const sAsyncWrapper = new ServiceWrapperAwait();
    await sAsyncWrapper.get("notification/read/" + id, {
      language_attach: false,
      is_auth_required: true,
    });
  };

  playSound = () => {
    const callback = (error, sound) => {
      if (error) {
        return;
      }
      sound.play(() => {
        sound.release();
      });
    };
    const audio = require("../../sound/salon_alarm.mp3");
    const sound = new Sound(audio, (error) => callback(error, sound));
  };

  initialNotification = async () => {
    const notification = await Notifications.getInitialNotification();
    if (typeof notification != "undefined" && notification) {
      props.getNotificationCount(props.token)
      props.notificationActive(true);
      try {
        const _data = notification.payload;
        if (_data.notification_id) {
          notificationRead(_data.notification_id);
        }
        if (_data.type == "booking") {
          props.navigation.navigate("OrderDetail", { id: _data.order_id });
        } 
        else if (_data.type == "loyality-offer") {
          props.navigation.navigate("LoyaltyScreen", {
            id: _data.order_id,
          });
        } else if (_data.type == "support") {
          props.navigation.navigate("ChatScreen", {
            caht_id: _data.request_id,
          });
        }
      } catch (err) {
        
      }
    }
  };

  //will focus
  useEffect(() => {
    return props.navigation.addListener("focus", () => {
      props.getNotificationCount(props.token)
      NetInfo.fetch().then((state) => {
        if (state.isConnected) {
          props.getRestTypes();
        } else {
          functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
        }
      });
      BackHandler.addEventListener('hardwareBackPress', backPressed = () => {
        BackHandler.exitApp();
        return true;
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    BackHandler.addEventListener('hardwareBackPress', backPressed = () => {
      props.getNotificationCount(props.token)
      BackHandler.exitApp();
      return true;
    });

    foregroundState();
    Sound.setCategory("Playback", true);
    Notifications.registerRemoteNotifications();
    initialNotification();

    Notifications.events().registerNotificationOpened(
      (notification, completion) => {
        try {
          const _data = notification.payload;
          if (_data.notification_id) {
            notificationRead(_data.notification_id);
          }
          if (_data.type == "order") {
            props.navigation.navigate("OrderDetail", {
              id: _data.order_id,
            });
          } 
          if (_data.type == "loyality-offer") {
            props.navigation.navigate("LoyaltyScreen", {
              id: _data.order_id,
            });
          } 
          else if (_data.type == "support") {
            props.navigation.navigate("ChatScreen", {
              caht_id: _data.request_id,
            });
          }
        } catch (err) {
          
        }
        completion();
      }
    );

    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        props.getRestTypes();
        // props.getOrdersToDeliver({});
        // props.getOrdersToCollect({});
        // props.getOrdersToReturn({});
      } else {
        functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
      }
    });
  };

  useEffect(() => {
    return () => {
      handleComponentUnmount();
    };
  }, []);
  const handleComponentUnmount = () => {};

  useEffect(() => handleComponentUpdated());
  const handleComponentUpdated = () => {};

  const foregroundState = () => {
    const unsubscribe = messaging().onMessage(async (remoteMessage) => {
      props.getNotificationCount(props.token)
      props.notificationActive(true);
      playSound();
      const _data = getNotificationData(remoteMessage);
      setNotifyData(_data);
      setDisplayNotificationPopup(true);
    });
    return unsubscribe;
  };

  const getNotificationData = (remoteMessage) => {
    let body = "You have a new notification";
    let title = "Notification";

    if (typeof remoteMessage.notification != "undefined") {
      if (typeof remoteMessage.notification.body != "undefined") {
        body = remoteMessage.notification.body;
      }
      if (typeof remoteMessage.notification.title != "undefined") {
        title = remoteMessage.notification.title;
      }
    } else if (typeof remoteMessage.data != "undefined") {
      if (typeof remoteMessage.data.body != "undefined") {
        body = remoteMessage.data.body;
      } else if (
        typeof remoteMessage.data.notification != "undefined" &&
        typeof remoteMessage.data.notification.body != "undefined"
      ) {
        body = remoteMessage.data.notification.body;
      }
      if (typeof remoteMessage.data.title != "undefined") {
        title = remoteMessage.data.title;
      } else if (
        typeof remoteMessage.data.notification != "undefined" &&
        typeof remoteMessage.data.notification.title != "undefined"
      ) {
        body = remoteMessage.data.notification.title;
      }
    }
    let details = null;
    if (
      remoteMessage &&
      remoteMessage.data &&
      remoteMessage.data &&
      remoteMessage.data.type
    ) 
      {
      if (
        
        remoteMessage.data.type === "order"
      
      ) {
        details = {
          type: remoteMessage.data.type,
          order_id: remoteMessage.data.order_id,
        };
      }
      else if (remoteMessage.data.type === "Loyality") {
        details = {
          type: remoteMessage.data.type,
          requestId: remoteMessage.data.request_id,
        };
      } 
      else if (remoteMessage.data.type === "support") {
        details = {
          type: remoteMessage.data.type,
          requestId: remoteMessage.data.request_id,
        };
      }
    }

    return {
      title: title,
      body: body,
      details: details,
    };
  };
  const onRightButtonPress = () => {
    props.navigation.navigate("NotificationScreen");
  };
  const onTabChange = (index) => {
    if (tabIndex !== index) {
      setTabIndex(index);
    }
  };

  const selectAllClick = () => {
    let tempListData =
      tabIndex === 1 ? props.ordersToCollect : props.ordersToReturn;
    let tempSelectedItem = [];
    tempListData.forEach((element) => {
      element.isSelected = true;
      tempSelectedItem.push(element.orderId);
    });
    if (tabIndex === 1) {
      setSelectedItemToCollect(tempSelectedItem);
      props.updateOderToCollect(tempListData);
    } else {
      setSelectedItemToReturn(tempSelectedItem);
      props.updateOderToReturn(tempListData);
    }
  };
  const nearadioClicked = (flag) => {
    setSortValue(flag);
  };
  const popradioClicked = (flag) => {
    setSortValue(flag);
  };
  const popupNoClick = () => {
    setIsPopupVisible(false);
  };
  const popupYesClick = () => {
    setIsPopupVisible(false);
  };
  const listButtonClick = (itemData) => {
    setIsPopupVisible(true);
  };
  const onProfileButtonPress = () => {         
     props.navigation.navigate("TabNavigator", { screen: "MyProfile" });

  };

  const homeCardButtonPress = async(id, latitude, longitude, preOrder) => {
    // Cant pre order msg.
    if(props.selectedRestaurant.dine_in != 'dine' && ! props.selectedRestaurant.restaurant_id && preOrder == 'disable') {
        await functions.displayAlertWithCallBack(
          '',
          I18nManager.isRTL ? 'هذا المطعم لا يقبل بالطلبات المسبقة إذا تابعت سيُطلب منك مسح الرمز عند الدفع' : 'This restaurant does not accept pre-orders, if you continue you will be asked to scan the QR code  at checkout',
          (data) => {
            if (data) {
              let _scannedData = props.selectedRestaurant;
              _scannedData.restaurant_id = id;
              _scannedData.dine_in = 'pre';
              _scannedData.scanned = false;
              _scannedData.has_preOrder = false;
              props.selectRestaurant(_scannedData);
              props.navigation.navigate("RestaurantDetailScreen", {
                restId: id,
                restLatitude: latitude,
                restLongitude: longitude,
              });
            }
          },
          appTexts.LOGIN_POP.ok,
          appTexts.LOGIN_POP.cancel
        );
        return false;
    }
    // Preorder
    if(props.selectedRestaurant.dine_in != 'take' && ! props.selectedRestaurant.restaurant_id && preOrder == 'enable') {
        let _scannedData = props.selectedRestaurant;
        _scannedData.restaurant_id = id;
        _scannedData.dine_in = 'pre';
        _scannedData.scanned = false;
        _scannedData.has_preOrder = true;
        props.selectRestaurant(_scannedData);
    }
    // take away with preorder.
    if(props.selectedRestaurant.dine_in == 'take' 
      && ! props.selectedRestaurant.restaurant_id && preOrder == 'enable') {
        let _scannedData = props.selectedRestaurant;
        _scannedData.restaurant_id = id;
        _scannedData.scanned = true;
        _scannedData.has_preOrder = true;
        props.selectRestaurant(_scannedData);
    }
    // take away without preorder - Customer will scan on checkout.
    if(props.selectedRestaurant.dine_in == 'take' 
      && ! props.selectedRestaurant.restaurant_id && preOrder == 'disable') {
        let _scannedData = props.selectedRestaurant;
        _scannedData.restaurant_id = id;
        _scannedData.scanned = false;
        _scannedData.has_preOrder = false;
        props.selectRestaurant(_scannedData);
    }
    // Go to details
    props.navigation.navigate("RestaurantDetailScreen", {
      restId: id,
      restLatitude: latitude,
      restLongitude: longitude,
    });
  };

  const onCheckoutClick = () => {
    props.navigation.navigate("CheckoutScreen");
  };

  const checkBoxClick = (itemData) => {
    let tempItemData = itemData;
    let tempOrderData =
      tabIndex === 1 ? [...props.ordersToCollect] : [...props.ordersToReturn];
    let selectedIndex = tempOrderData.indexOf(itemData);
    tempItemData.isSelected =
      tempItemData.isSelected === null ||
      tempItemData.isSelected === undefined ||
      tempItemData.isSelected === false
        ? true
        : false;
    tempOrderData[selectedIndex] = tempItemData;
    if (tabIndex === 1) {
      let tempSelectedItemToCollect = selectedItemToCollect;
      if (tempSelectedItemToCollect.indexOf(itemData.orderId) !== -1) {
        tempSelectedItemToCollect.splice(
          tempSelectedItemToCollect.indexOf(itemData.orderId),
          1
        );
      } else {
        tempSelectedItemToCollect.push(itemData.orderId);
      }
      setSelectedItemToCollect(tempSelectedItemToCollect);
      props.updateOderToCollect(tempOrderData);
    } else {
      let tempSelectedItemToReturn = selectedItemToReturn;
      if (tempSelectedItemToReturn.indexOf(itemData.orderId) !== -1) {
        tempSelectedItemToReturn.splice(
          tempSelectedItemToReturn.indexOf(itemData.orderId),
          1
        );
      } else {
        tempSelectedItemToReturn.push(itemData.orderId);
      }
      setSelectedItemToReturn(tempSelectedItemToReturn);
      props.updateOderToReturn(tempOrderData);
    }
  };

  return (
    <HomeView
      homeCardButtonPress={homeCardButtonPress}
      onProfileButtonPress={onProfileButtonPress}
      listButtonClick={listButtonClick}
      checkBoxClick={checkBoxClick}
      tabIndex={tabIndex}
      onTabChange={onTabChange}
      isPopupVisible={isPopupVisible}
      popupNoClick={popupNoClick}
      popupYesClick={popupYesClick}
      isSelectAllViewVisible={
        (tabIndex === 1 && selectedItemToCollect.length > 0) ||
        (tabIndex === 2 && selectedItemToReturn.length > 0)
      }
      selectAllClick={selectAllClick}
      onemoretab={onemoretab}
      nearadioClicked={nearadioClicked}
      popradioClicked={popradioClicked}
      restTypes={props.restTypes}
      restTypesID = {props.restTypesID}
      sortValue={sortValue}
      notifyData={notifyData}
      setDisplayNotificationPopup={setDisplayNotificationPopup}
      displayNotificationPopup={displayNotificationPopup}
      onRightButtonPress={onRightButtonPress}
      onCheckoutClick={onCheckoutClick}
      navigateTochat={(_data) => {
        setDisplayNotificationPopup(false);
        if(_data.details.type == "order") {
          props.navigation.navigate("OrderDetail", {
            id: _data.details.order_id,
          });
        } else if (_data.details.type == "Loyality") {
         
          props.navigation.navigate("LoyaltyScreen", {
            id: _data.details.order_id,
          });
        } else if (_data.details.type == "support") {
          props.navigation.navigate("ChatScreen", {
            caht_id: _data.details.requestId,
          });
        }
      }}
      navigation={props.navigation}
      addedCartItems={props.addedCartItems}
      is_notification_active={props.is_notification_active}
      setLocationLocally={props.setLocationLocally}
      languageSwitchScreen={props.languageSwitchScreen}
      notification_badge={props.notification_badge}
    />
  );
};

const mapStateToProps = (state, props) => {
  return {
    addedCartItems: state.cartReducer.cartAddedData,
    restTypes: _.get(state, 'homeReducer.restTypes', ""),
    restTypesID: _.get(state, 'homeReducer.restTypesID', ""),
    is_notification_active: state.homeReducer.is_notification_active,
    token: _.get(state, "loginReducer.userData.data.access_token", ''),
    notification_badge:  _.get(state, 'homeReducer.notificationbadge', ''),
    selectedRestaurant: state.cartReducer?.selectedRestaurant || {},
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      getRestTypes: homeActions.getRestTypes,
      notificationActive: homeActions.notificationActive,
      getNotificationCount: homeActions.getNotificationCount,
      setLocationLocally: geoLocationActions.setLocationLocally,
      languageSwitchScreen: homeActions.languageSwitchScreen,
      selectRestaurant: cartActions.selectRestaurant,
    },
    dispatch
  );
};

const homeScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeScreen);

homeScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null,
});

export default homeScreenWithRedux;
