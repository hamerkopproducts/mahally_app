import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

let headerButtonContainerWidth = globals.SCREEN_SIZE.width - (globals.MARGIN.marginTen * 2);
let headerButtonWidth = (headerButtonContainerWidth - (globals.MARGIN.marginTen * 2)) / 3;
let bottomBannerViewHeight = 100;
const images = {
  map: require('../../assets/images/HomeCard/map.png'),
  // sFlag:require('../../assets/images/chooseLanguage/Arabic.png'),
};

const styles = StyleSheet.create({
  screenMain: {
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    backgroundColor: globals.COLOR.headerColor,
    flexDirection: 'column',
  },
  screenDesignContainer: {
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    backgroundColor: globals.COLOR.greyBackground,
    
    //backgroundColor:'red'
  },
  screenContainerWithMargin: {

    width: globals.INTEGER.screenWidthWithMargin,
    //height: globals.INTEGER.screenContentHeight,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    flex:1,
    
    marginLeft: globals.INTEGER.leftRightMargin,
    
    //  marginBottom:hp("30%"),
    //paddingBottom: 20,

  },
  screenContainerWithBanner: {
    width: globals.INTEGER.screenWidthWithMargin,
    height: globals.INTEGER.screenContentHeight - bottomBannerViewHeight,
    marginLeft: globals.INTEGER.leftRightMargin,
    paddingBottom: 5
  },
  headerButtonContianer: {
    flexDirection: 'row',
    width: headerButtonContainerWidth,
    height: globals.INTEGER.heightFifty,
    alignItems: 'center'
  },
  headerButton: {
    width: headerButtonWidth,
    height: globals.INTEGER.heightThirty
  },
  headerButtonMargin: {
    marginLeft: globals.MARGIN.marginTen
  },
  categoryItemRow: {
    width: '100%',
    flexDirection: 'row'
  },
  flatListStyle: {
    width: '100%',
    //backgroundColor:'green',

  },
  bottomBannerView: {
    marginLeft: globals.INTEGER.leftRightMargin,
    width: globals.INTEGER.screenWidthWithMargin,
    height: bottomBannerViewHeight,
    backgroundColor: globals.COLOR.white
  },
  flatListStyle: {
    width: '100%',
    //backgroundColor:'green',
    flex:1,

  },
  cardView: {
    //marginVertical: hp('2%'),
    position:'relative',
    top: globals.SCREEN_SIZE.height <= 600 ?150 : (globals.SCREEN_SIZE.height <=700 ?200:350),
    width:'100%',
   
  },
  mapImage: {
    flex: 1,
  position:'absolute',
    width:'100%',
    height:'100%',
    borderTopLeftRadius:30,
    borderTopRightRadius:30,
    
  },
  safeArea:{
    marginLeft:10,
    marginRight:10,
    flex:1,
    marginBottom: 250
  },
  mapCurve:{
  
    
  },
  but:{
    position:'absolute',
    margin:0,
    bottom:80,
   // alignItems:'center',
    //justifyContent:'center'
   // width:globals.SCREEN_SIZE.width,
    
  },
  float:{
   height:60,
   width:'100%',
   width:globals.SCREEN_SIZE.width,
   alignItems:'center',
   backgroundColor:globals.COLOR.purple,
   justifyContent:'space-between',
   paddingLeft:'5%',
   paddingRight:'5%',
  backgroundColor:globals.COLOR.purple,
  height: 58,
  flexDirection:'row',
   position:'absolute',
bottom:18,
  },
  lText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:'white',  
    fontSize:12,   
    textAlign:'left' 
  },
  rText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helveticaBold,
    color:'white',  
    fontSize:12,   
    textAlign:'left' 
  },
  loc:{
    width:3,
    height:3,
  },
  bannerView:{
    bottom:60,
    width:'100%',
  },
  mapScreen: {
    flex: 1,
    backgroundColor: '#F5FCFF',
    position: 'relative',
    borderRadius: 20,
    overflow: 'hidden',
    marginTop: 12
  },
  map: {
    flex: 1,
  },
  detailPos: {
    // position: 'absolute',
    width: '95%',
    bottom: hp('30%'),
    marginLeft: hp('1%'),
  },
  no_orders: {
    marginRight: 10,
    textAlign: 'center',
    marginTop: 20,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.helveticaBold,
    fontSize:14,
  },
  enableLocation: {
    textTransform: 'uppercase',
    marginRight: 10,
    fontSize: 10,
    textAlign: 'center',
    marginTop: '50%',
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  }
});

export { images, styles };
