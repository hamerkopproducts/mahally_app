import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, 
  I18nManager, Image, TextInput } from 'react-native';
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts"

import { images, styles } from "./styles";
import CustomButton from '../../components/CustomButton';
import Header from '../../components/Header';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Loader from "../../components/Loader";

const LoginView = (props) => {
	const {
    loginButtonPress,
    guestLoginPress,
    onBackButtonClick,
    validatePhone,
    setPhone,
    isLoading,
    phone
		
  } = props;
	return (

    <View style={styles.screenMain}>
      {isLoading && <Loader />}
			<Header
        isBackButtonRequired={true}
        onBackButtonClick={onBackButtonClick}
        
				customHeaderStyle={{
					height: globals.INTEGER.headerHeight,
					alignItems: "center",
					backgroundColor: globals.COLOR.headerColor
				}}
			/>
      
		  <KeyboardAwareScrollView style={styles.screenDesignContainer}>  
        <View style={styles.logoWrapper}>
          <Image
            source={images.logoImage}
            resizeMode="cover"
            style={styles.logoImage}
          />
        </View> 
        <View style={styles.contentWrapperMain}>
        <View style={styles.contentWrapper}>
          <Text style={styles.contentHeader}>{appTexts.LOGIN.welcome}</Text>
		      {!I18nManager.isRTL && <Text style={styles.backText}>{appTexts.LOGIN.user}</Text>}
          </View>
          <View style={styles.contentWrapper}>
          <Text style={styles.contentDescription}>
          {appTexts.LOGIN.sign}
          </Text>
          </View>
          </View>
        <View style={styles.phoneSection}>
          <View style={styles.firstSection}>
            <Image
              source={require("../../assets/images/chooseLanguage/Arabic.png")}
              style={{ height: 20, width: 28,borderRadius:5, transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]}}
            />
           <View style={{marginLeft: 2}}> 
            <TextInput
              editable={false}
              style={styles.disableText}
              placeholder={"+966"}
              placeholderTextColor="#282828"
              style={{transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]}}
            />
            </View>
          </View>
          <View style={styles.secondSection}>
            <TextInput
              style={[styles.enableText, { transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }] } ]}
              value={phone}
              keyboardType="number-pad"
              onChangeText={num => {
                validatePhone(false),
                setPhone(num)
              }}
                          />
          </View>
        </View>
       

        <View style={styles.buttonWrapper}>
          <TouchableOpacity
            onPress={() => {
              loginButtonPress();
            }}
          >
            <CustomButton buttonstyle={styles.custombuttonStyle,styles.loginTouch} buttonText={appTexts.LOGIN.signin} />
          </TouchableOpacity>
        </View>
        <TouchableOpacity
        onPress={() => {
          guestLoginPress();
        }}
          >
        <View style={styles.guestView}>
        <Text style={styles.guestText}>
          {appTexts.LOGIN.guest}
        </Text>
        </View>
        </TouchableOpacity>

       
      </KeyboardAwareScrollView>  
    </View> 
  );
	

};

LoginView.propTypes = {
          loginButtonPress: PropTypes.func,
          guestLoginPress: PropTypes.func,
          validatePhone: PropTypes.func,
          // setPhone: PropTypes.func,
};


export default LoginView;