import React, { useState, useEffect } from 'react';
import { BackHandler } from 'react-native';
import { connect } from 'react-redux';
import LoginView from './LoginView';
import { bindActionCreators } from "redux";
import * as loginActions from "../../actions/loginActions";

import functions from "../../lib/functions";
import appTexts from "../../lib/appTexts";
import _ from "lodash"
import * as cartActions from "../../actions/cartActions";
import * as editUserActions from "../../actions/editUserActions";

const LoginScreen = (props) => {
  const [phoneerrorflag, setphoneerrorflag] = useState(false);
  const [phone, setPhone] = useState('');
  const [loader, setLoader] = useState(false);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    BackHandler.addEventListener('hardwareBackPress', backPressed = () => {
      BackHandler.exitApp();
      return true;
    });
  };
//will focus
useEffect(() => {
  return props.navigation.addListener("focus", () => {
    setPhone('')
  });
}, [props.navigation]);
  //updated
  useEffect(() => handleComponentUpdated());
  const handleComponentUpdated = () => {
    if(_.get(props, 'otpAPIReponse') && _.get(props, 'otpAPIReponse.success'))
    {
      setLoader(false)
      props.navigation.navigate('OtpScreen', { mobileNumber: phone})
      props.getOtpSuccess(null);
      props.guestLogingIn(false)
    }
    else if (_.get(props, 'otpAPIReponse') && _.get(props, 'otpAPIReponse.success') === false)
    {
      setLoader(false)
      functions.displayToast('error', 'top', 
      appTexts.ALERT_MESSAGES.error, 
      _.get(props, 'otpAPIReponse.error.msg'));
      props.resetOtpError(null);
    }
  };

  const loginButtonPress = () => {
   
    setLoader(true)
    if (phone.trim() === '') {
      setLoader(false)
      functions.displayToast('error', 'top', 
      appTexts.ALERT_MESSAGES.error, 
      appTexts.EDITPROFILE.Phoneerror);
    }
    else if(!functions.isValidPhone(phone.trim())){
      setLoader(false)
      functions.displayToast('error', 'top', 
      appTexts.ALERT_MESSAGES.error, 
      appTexts.EDITPROFILE.Phoneneedsixdigit);
    }
    else{
    let params = {
      "phone": phone
    }
    props.getOtp(params);
    }
  };

   const onBackButtonClick= () => {
     if(props.isGuestLoggedin == true) {
      props.navigation.navigate('TabNavigator');
     } else {
      props.navigation.navigate('LoginStackNavigator', { screen: 'AppIntrosliderloginScreen' });
     }
  };
   const guestLoginPress= () => {
     props.resetData()
     props.guestLogingIn(true)
    setTimeout(() => {
			props.navigation.navigate("TabNavigator", { screen: "Home" });
		}, 1000);
    
    props.addToCart({});
    props.selectRestaurant({});
  };
  
  const validatePhone = flag => {
    setphoneerrorflag(flag);
  };
  return (
    <LoginView 
    loginButtonPress={loginButtonPress}
    onBackButtonClick={onBackButtonClick}
    guestLoginPress={guestLoginPress}
    validatePhone={validatePhone}
    setPhone={setPhone}
    isLoading={loader}
    phone={phone}
    />
  );

};


const mapStateToProps = (state, props) => {
  return {
    otpAPIReponse: _.get(state ,'loginReducer.getotpAPIReponse', ''),
    isLoading: _.get(state ,'loginReducer.loader'),
    isGuestLoggedin: state.loginReducer.guestLogin,
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getOtp: loginActions.getOtp,
    getOtpSuccess: loginActions.getOtpSuccess,
    resetOtpError: loginActions.otpServiceError,
    guestLogingIn: loginActions.guestLoginn,
    addToCart: cartActions.cartUpdate,
    selectRestaurant: cartActions.selectRestaurant,
    guestLogin: loginActions.guestLoggedIn,
    resetData: editUserActions.resetallData,
  }, dispatch)
};

const loginWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginScreen);

loginWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default loginWithRedux;
