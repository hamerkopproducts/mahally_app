import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals"
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
const images = {
  backIcon: require("../../assets/images/chooseLanguage/backarow.png"),
  logoImage: require("../../assets/images/chooseLanguage/Bottom-ΓÇô-2.png"),
  // facebookIcon: require("../../assets/images/signup/facebook.png"),
  // googleIcon: require("../../assets/images/signup/google.png"),
};
const styles = StyleSheet.create({
  screenMain: {
    flex:1,
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    flexDirection: "column",
    backgroundColor:'white'
  },
  loginTouch:{
    width:wp('87%'),
    height:50,
    borderRadius:10,
    backgroundColor:'#612467',
    justifyContent:'center',
    alignItems:'center',
    marginHorizontal:wp('5%'),
  },
  custombuttonStyle:{
    backgroundColor:globals.COLOR.purple,
     width:150,
     height: 45,
     borderRadius:15,
     justifyContent:'center',
     alignItems:'center',
   //  marginLeft:6.5

    // backgroundColor:'#cccccc'
  },
  screenDesignContainer: {
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
  },
  arrowWrapper: {
    paddingLeft: "5%",
    marginTop: hp("6%"),
    flexDirection:'row',
    alignItems:"center"
  },
  arrowicon: {
    width: 20,
    height: 20,
  
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
  },
  verifyText: {
    //fontSize:hp('2.4%'),
    color:'#232020',
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize:18,
   
},
verifyWrapper:{
paddingLeft:'32%',
},
  logoImage: {
    alignSelf: "flex-start",
    width:globals.SCREEN_SIZE.height <=600?220:300,
    height:globals.SCREEN_SIZE.height <=600?220:300,
  },
  logoWrapper: {
    // paddingTop: hp("12%"),
    // paddingLeft: '7%',
    alignSelf:"center"
  },
  contentWrapper: {
    flexDirection: 'row',
    paddingLeft: '2.5%',
    alignItems:"center"
  //  marginTop: hp('0%'),
   // paddingTop: hp('0%'),
  },
  contentWrapperMain: {
   // paddingTop: hp('10%'),
   backgroundColor:'white',
   alignItems:'center',
   marginTop:globals.SCREEN_SIZE.height <=600? hp('0.5%'):hp('5%')
  },
  contentHeader: {
    //textAlign:'center',
    paddingTop: hp('1%'),
    paddingBottom: hp('.1%'),
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color: globals.COLOR.black,
    fontSize: 22,

  },
  backText:{
    paddingTop: hp('1%'),
    paddingBottom: hp('.1%'),
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color: 'black',
    fontSize: 22,
    paddingLeft:'2%'
  },
  phoneNo:{
    paddingLeft:'7%',
   marginTop: hp('9%'),
  },
  phoneStyle:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.semiLightTextColor,
  },
  contentDescription: {

    lineHeight: 24,
    fontSize: 14,
    color: globals.COLOR.lightTextColor,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  buttonWrapper: {
    // backgroundColor:"#612467",
   justifyContent:"center",
   alignItems:'center',
    marginTop: hp('5%'),
    // marginHorizontal:wp('5%')
    // paddingLeft:'7%'
  },

  buttonbottomWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    // paddingTop: hp('5%'),
    alignItems: 'center'
  },
  DdntreceiveText: {
    fontSize: 13,
    color: globals.COLOR.lightBlack,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  resendText: {
    fontSize: 14,
    //textDecorationLine: 'underline',
    color: globals.COLOR.lightBlack,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  modalMainContent: {
    //justifyContent: "center",
    justifyContent: 'flex-end',
    margin: 0
  },
  modalmainView: {
    backgroundColor: "white",
    //width: wp("90%"),
    padding: "4%",
    //borderRadius: 10,
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
    borderColor: "rgba(0, 0, 0, 0.1)",
  },
  formWrapper: {
    flexDirection: 'row',
    paddingLeft: '7%',
    paddingRight: '7%'
  },
  disableSection: {
    width: '20%',
    //backgroundColor:'red',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: 'red',
    borderBottomWidth: 1,
    //marginTop:8
    //justifyContent:'center'
  },
  enableSection: {
    width: '74%',
    marginLeft: 5,
    //backgroundColor:'blue'
  },
  fixedInput: {
    marginLeft: 5,
    //marginTop:5
  },
  forminWrapper: {
    flexDirection: 'row', paddingLeft: '7%',
    paddingRight: '7%'
  },
  phoneSection: {
    flexDirection: 'row',
    marginTop: hp('5%'),
   marginHorizontal:wp('17%'),
   alignItems:'center',
   justifyContent:'center',

   transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
  //marginLeft:19
  },

  

  firstSection: {
  //  width: '20%',
    flexDirection: "row",
    // justifyContent: "center",
    alignItems: "center",
    // marginHorizontal:5,

  },
  disableText: {
    marginLeft: 5,
    alignSelf: 'center',
    fontSize: 15,
    color: globals.COLOR.blackTextColor,
    width: "90%",
    flexDirection: 'row',
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
   secondSection: {

    marginLeft:7,
    // width:'80%',
   // marginTop:10,
  
    flexDirection: 'row'
  },
  enableText: {
    width: '99%',
    // marginHorizontal:'5%',
    height:48,
    borderWidth:1,
    borderColor:'lightgrey',
    borderRadius:5,
    fontSize: 15,
    // lineHeight: 24,
    alignItems: "center",
    justifyContent:'center',
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color: globals.COLOR.blackTextColor,
    textAlign:'center',
   // textAlign: I18nManager.isRTL ? "right" : "left",
  },
  signtextWrapper: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: hp('4%')
  },
  socialMedia: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    paddingLeft: '23%',
    paddingRight: '23%',
    paddingTop: hp('3%')
  },
  faceBook: {
    backgroundColor: globals.COLOR.fbButton,
    width: 90,
    height: 43,
    justifyContent: "center",
    alignItems: "center",
  },
  googlePlus: {
    backgroundColor: globals.COLOR.googleButton,
    width: 90,
    height: 43,
    justifyContent: "center",
    alignItems: "center",
  },
  appleID: {
    backgroundColor: 'black',
    width: 210,
    height: 43,
    justifyContent: "center",
    alignItems: "center",
  },

  orText: {
    color: globals.COLOR.lightBlack,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  socialicon: {
    width: 60,
    height: 72,
  },
  SignupSection: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: hp('3%'),

  },
  signText: {
    fontSize: 12,
    color: globals.COLOR.lightBlack,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  gustText: {
    color: globals.COLOR.lightBlack,
    fontSize: 11,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  coGust: {
    justifyContent: 'center',
    flexDirection: 'row',
    paddingTop: hp('1%'),
    paddingBottom: hp('3%')
  },
  goText: {
    fontSize: 11,
    color: globals.COLOR.lightBlack,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  guestView: {
    marginTop:15,
    alignItems:'center',
    justifyContent:'center'
  },
  guestText: {
    textDecorationLine: 'underline',
    textAlign:'center',
    fontSize:13,
    color:globals.COLOR.purple,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
},


});

export { images, styles };