import React from "react";
import {
  RefreshControl,
  View,
  FlatList,
  Text,
  I18nManager
} from "react-native";
import { styles } from "./styles";

import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";

import LoyaltyCard from "../../components/LoyaltyCard";
import Header from "../../components/Header";
import Loader from "../../components/Loader";
import PropTypes from "prop-types";
import _ from "lodash";
import functions from "../../lib/functions";
import BannerButton from "../../components/BannerButton";

const LoyaltyView = (props) => {
  const {
    onBackButtonClick,
    onMenuCardPress,
    restLoyaltyMealsDetails,
    isLoading,
    onRefresh,
    refreshing,
    addedCartItems,
    selectedRestaurant,
    addToCart,
    redirectToScan,
    onCheckoutClick
  } = props;

  const addItemsToCart = (item, availableFreeMeals, clear_cart) => {
    let addedItems =
      (typeof addedCartItems == "undefined" || clear_cart == true)
        ? {}
        : Object.assign({}, addedCartItems);
    if(availableFreeMeals <= 0) {
      functions.displayToast('error', 'top', appTexts.LOYALTY.notAvailable , appTexts.LOYALTY.offernotAvailable);
      return false;
    }
    if (
      Object.keys(addedItems).indexOf(
        "loyalty_" + item.item_id.toString()
      ) == -1
    ) {
      let itemToAdd = Object.assign({}, item);
      itemToAdd.addonsSelected = {};
      itemToAdd.count = 1;
      itemToAdd.id = "loyalty_" + item.item_id;
      itemToAdd.is_loyalty = true;
      itemToAdd.price = 0;
      itemToAdd.available_free = availableFreeMeals;
      itemToAdd.discount_price = 0;
      addedItems["loyalty_" + item.item_id] = itemToAdd;
      setTimeout(() => {
        functions.displayToast(
          "success",
          "top",
          appTexts.ALERT_MESSAGES.success,
          appTexts.ORDER.itemadded
        );
      }, 400);
    } else {
      let all_items = Object.assign({}, addedItems);
      if(availableFreeMeals <= all_items['loyalty_' + item.item_id].count) {
        functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, appTexts.STRING.addedmaximumoffer);
        return false;
      }
      all_items["loyalty_" + item.item_id].count += 1;
      all_items["loyalty_" + item.item_id].id = "loyalty_" + item.item_id;
      addedItems = all_items;
      setTimeout(() => {
        functions.displayToast(
          "success",
          "top",
          appTexts.ALERT_MESSAGES.success,
          appTexts.ORDER.itemupdated
        );
      }, 400);
    }
    addToCart(addedItems);
  }

  const renderItem = ({ item, index }) => (
    <LoyaltyCard
      item={item}
      onMenuCardPress={(item_id, item, type) => onMenuCardPress(item_id, item, type, item.time_status) }
      addedCartItems={addedCartItems}
      addloyalty={async(item, availableFreeMeals) => {
        if(item.time_status === 'CLOSED') {
          functions.displayToast('error', 'top', appTexts.REST.Heading_closed , appTexts.REST.closed);
          return false;
        }
        const restaurant_id = item.restaurant_id;

        if(selectedRestaurant.dine_in != 'dine' && ! selectedRestaurant.restaurant_id && item.pre_order == 'disable') {
          await functions.displayAlertWithCallBack(
            '',
            I18nManager.isRTL ? 'هذا المطعم لا يقبل بالطلبات المسبقة إذا تابعت سيُطلب منك مسح الرمز عند الدفع' : 'This restaurant does not accept pre-orders, if you continue you will be asked to scan the QR code  at checkout',
            (data) => {
              if (data) {
                redirectToScan("loyality", item.item_id, restaurant_id, item.pre_order, item.vat_includes, item.vat_percentage);
                addItemsToCart(item, availableFreeMeals, true);
              }
            },
            appTexts.LOGIN_POP.ok,
            appTexts.LOGIN_POP.cancel
          );
          return false;
        }

        if (
          typeof selectedRestaurant.restaurant_id != "undefined" &&
          selectedRestaurant.restaurant_id != restaurant_id
        ) {
          await functions.displayAlertWithCallBack(
            appTexts.REST.already_in_cart,
            appTexts.REST.cart_has_items,
            (data) => {
              if (data) {
                addToCart({});
                redirectToScan('loyality', item.item_id, restaurant_id, item.pre_order, item.vat_includes, item.vat_percentage);
                addItemsToCart(item, availableFreeMeals, true);
              }
            }
          );
        } else if (typeof selectedRestaurant.restaurant_id == "undefined") {
          redirectToScan('loyality', item.item_id, restaurant_id, item.pre_order, item.vat_includes, item.vat_percentage);
          addItemsToCart(item, availableFreeMeals, true);
        } else {
          addItemsToCart(item, availableFreeMeals, false);
        }
      }}
    />
  );

  return (
    <View style={styles.screenMain}>
      <Header
        isBackButtonRequired={true}
        onBackButtonClick={onBackButtonClick}
        isleftlogoRequired={true}
        heading={appTexts.PRIVACY.HeadingFree}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          backgroundColor: globals.COLOR.headerColor,
        }}
      />

      <View style={styles.formWrapper}>
        {isLoading && <Loader />}
        <View style={{ flex: 1}}>
          <FlatList
            ListEmptyComponent={
              <Text style={styles.no_orders}>{appTexts.OFFER.noOffer}</Text>
            }
            style={styles.flatListStyle}
            data={
              _.isEmpty(restLoyaltyMealsDetails) ? [] : restLoyaltyMealsDetails
            }
            extraData={
              _.isEmpty(restLoyaltyMealsDetails) ? [] : restLoyaltyMealsDetails
            }
            keyExtractor={(item, index) => index.toString()}
            renderItem={renderItem}
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
          />



        </View>

<View style={{position: 'absolute', bottom: 0, width: '100%'}}>
        {Object.keys(addedCartItems).length > 0 &&
        <BannerButton
          text1={appTexts.BANNER.count}
          text2={appTexts.BANNER.view}
          text3={appTexts.BANNER.check}
          text4={appTexts.BANNER.SAR}
          onCheckoutClick={onCheckoutClick}
          addedCartItems={addedCartItems}
          // openAddonModal={openAddonModal}
          // onViewClick={null}
          isCart={false}
          isDetail={true}
        />
      } 
      </View>

      </View>
    </View>
  );
};
LoyaltyView.propTypes = {
  onRefresh: PropTypes.func,
};
export default LoyaltyView;
