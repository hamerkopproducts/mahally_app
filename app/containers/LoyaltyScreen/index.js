import React, { useEffect, useState } from "react";
import NetInfo from "@react-native-community/netinfo";
import { connect } from "react-redux";
import LoyaltyView from "./LoyaltyView";
import { bindActionCreators } from "redux";

import * as loyaltyMealsActions from "../../actions/loyaltyMealsActions";
import appTexts from "../../lib/appTexts";
import functions from "../../lib/functions";
import _ from "lodash";
import * as cartActions from "../../actions/cartActions";
import { store } from '../../../configureStore';

const LoyaltyScreen = (props) => {
  //Initialising states

  const [refreshing, setRefreshing] = useState(false);
  const [params, setParams] = useState({cust_id: _.get(props, "userID", ""), latitude: 0, longitude: 0});

  //will focus
  useEffect(() => {
    return props.navigation.addListener("focus", () => {
      NetInfo.fetch().then((state) => {
        if (state.isConnected) {
        } else {
          functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
  };

	React.useEffect(() => {
    try {
      const localStoreLocation = store.getState().geoLocationReducer.geoLocation;
      const latitudeUser = localStoreLocation.status == true ? localStoreLocation.location.data.latitude : 0;
      const longitudeUser = localStoreLocation.status == true ? localStoreLocation.location.data.longitude : 0;
      let _params = params;
      _params.latitude = latitudeUser;
      _params.longitude = longitudeUser;
      props.getLoyaltyData(params);
    } catch(err) {
      props.getLoyaltyData(params);
    }
	}, []);

  //unmount
  useEffect(() => {
    return () => {
      handleComponentUnmount();
    };
  }, []);

  const handleComponentUnmount = () => {};

  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
    if (props.pullRefresh === true) {
      setRefreshing(false);
    }
  };

  const onBackButtonClick = () => {
    // props.navigation.goBack();
    props.navigation.navigate("ProfileScreen");
  };

  const onMenuCardPress = (item_id, item, type, time_status) => {
    props.navigation.navigate("ProductDetailScreen", { itemId: item_id, from: type, item: item, time_status: time_status });
  };
  
  const onRefresh = async () => {
    setRefreshing(true);
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        try {
          const localStoreLocation = store.getState().geoLocationReducer.geoLocation;
          const latitudeUser = localStoreLocation.status == true ? localStoreLocation.location.data.latitude : 0;
          const longitudeUser = localStoreLocation.status == true ? localStoreLocation.location.data.longitude : 0;
          let _params = params;
          _params.latitude = latitudeUser;
          _params.longitude = longitudeUser;
          props.getLoyaltyData(params);
        } catch(err) {
          props.getLoyaltyData(params);
        }
      } else {
        functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
      }
    });
  };

  const onCheckoutClick = () => {
    props.navigation.navigate("CheckoutScreen");
  };

  return (
    <LoyaltyView
      onMenuCardPress={onMenuCardPress}
      onBackButtonClick={onBackButtonClick}
      restLoyaltyMealsDetails={_.get(props, "restLoyaltyMealsDetails", [])}
      isLoading={_.get(props, "isLoading", [])}
      refreshing={refreshing}
      onRefresh={onRefresh}
      addedCartItems={props.addedCartItems}
      selectedRestaurant={props.selectedRestaurant}
      addToCart={props.addToCart}
      redirectToScan={async(type, item_id, restaurant_id, hasPreOrder, vat_includes, vat_percentage) => {
        // props.postScanItemToAdd({
        //   type: type,
        //   item_id: item_id
        // });

        let _scanned_data = {
          restaurant_id: restaurant_id,
          table_id: '',
          color_code: '',
          dine_in: 'take',
          vat: vat_includes == 'yes' ? vat_percentage : '',
          has_preOrder: hasPreOrder == 'enable'
        };
        props.selectRestaurant(_scanned_data);
      }}
      onCheckoutClick={onCheckoutClick}
    />
  );
};

const mapStateToProps = (state, props) => {
  return {
    userDATA: _.get(state, "loginReducer.userData.data", null),
    token: _.get(state, "loginReducer.userData.data.access_token", null),
    userID: _.get(state, "loginReducer.userData.data.user.id", null),
    restLoyaltyMealsDetails: _.get(
      state,
      "loyaltyMealsReducer.restLoyaltyMealsDetails",
      ""
    ),
    pullRefresh: _.get(state, "loyaltyMealsReducer.pullRefresh", ""),
    isLoading: _.get(state, "loyaltyMealsReducer.isLoading", ""),
    addedCartItems: state.cartReducer.cartAddedData,
    selectedRestaurant: state.cartReducer.selectedRestaurant,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      addToCart: cartActions.cartUpdate,
      getLoyaltyData: loyaltyMealsActions.getLoyaltyData,
      postScanItemToAdd: cartActions.postScanItemToAdd,
      selectRestaurant: cartActions.selectRestaurant,
    },
    dispatch
  );
};

const LoyaltyScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoyaltyScreen);

LoyaltyScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null,
});

export default LoyaltyScreenWithRedux;
