import { StyleSheet, I18nManager } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import globals from "../../lib/globals";
let headerButtonContainerWidth =
  globals.SCREEN_SIZE.width - globals.MARGIN.marginTen * 2;
let headerButtonWidth =
  (headerButtonContainerWidth - globals.MARGIN.marginTen * 2) / 3;
const images = {

};
const styles = StyleSheet.create({
  screenMain: {
    // width: globals.SCREEN_SIZE.width,
    // height: globals.SCREEN_SIZE.height,
    flexDirection: "column",
    flex: 1,
    backgroundColor: 'white'
  },

  screenContainer: {
     flex:1,
    backgroundColor: globals.COLOR.transparent,
   marginBottom: globals.INTEGER.screenBottom
  },
  headerWrapper: {
    flex: .1,
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: hp("5%"),
  },
  formWrapper: {
     flex:1,
    backgroundColor: globals.COLOR.greyBackground,
    // marginTop: hp('1.5%'),
    //alignItems:'center'
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    marginBottom: hp('1.5%'),
    // width: globals.SCREEN_SIZE.width,
    //     height: globals.SCREEN_SIZE.height

  },
  verifyText: {
    fontSize: 18,
    //fontSize:hp('2.5%'),
    color: '#232020',
    //fontFamily: globals.FONTS.avenirMedium,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  verifyWrapper: {
    //flex:0.1,
    marginLeft: '5%'
    //flexDirection:'row'

  },
  flatListStyle: {
    marginTop: "5%",
    // width:'100%',
    //flex:1,
    //  width: globals.SCREEN_SIZE.width,
    //   height: globals.SCREEN_SIZE.height
  },

  imageWrapper: {
    paddingLeft: '5%'

  },
  arrowImage: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    paddingLeft: '5%',
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]

  },

  headingText: {
    textAlign: 'left',
    //fontSize:hp('2.1%'),
    fontSize: 14,
    color: '#232020',
    //fontFamily: globals.FONTS.avenirHeavy,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  no_orders: {
    // textTransform: 'uppercase',
    marginRight: 10,
    textAlign: 'center',
    marginTop: 20,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.helveticaBold,
    fontSize:14,
  },

});

export { images, styles };
