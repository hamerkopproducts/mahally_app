import React from "react";
import { View, FlatList, Text, Animated, I18nManager } from "react-native";
import globals from "../../lib/globals";
import { styles } from "./styles";

import Header from "../../components/Header";
import OrderCard from "../../components/OrderCard";
import appTexts from "../../lib/appTexts";
import BannerButton from "../../components/BannerButton";
import Loader from "../../components/Loader";
import LoginPromptModal from "../../components/LoginPropmpt"

const MyOrdersView = (props) => {
  const {
    onRightButtonPress,
    onBackButtonClick,
    onCheckoutClick,
    onPastOrdercardClick,
    onViewPress,
    addedCartItems,
	  data,
    activeData,
    isLoading,
    onEndReached,
    onRefresh,
    isFetching,
    slideInLeft,
    showView,
    is_notification_active,
    languageSwitchScreen,
    notification_badge,
    guestLogin,
    modalVisible,
    setModalVisible,
    gotoLogin
  } = props;

  const renderItem = ({item}) => (
    <OrderCard
      item={item}
      isPastOrders={true}
      orderId={appTexts.ORDER.ID}
      onOrderCardClick={onPastOrdercardClick}
    />
  );

  return (
    <View style={styles.screenMain}>

      {isLoading && <Loader />}

      <Header
        notification_badge={notification_badge}
        navigation={props.navigation}
        iscenterLogoRequired={false}
        isRightButtonRequired={true}
        onRightButtonPress={onRightButtonPress}
        isLanguageButtonRequired={true}
        onBackButtonClick={onBackButtonClick}
        headerTitle={appTexts.PROFILELISTING.appTitle}
        customHeaderStyle={{
          alignItems: "center",
          height: globals.INTEGER.headerHeight,
          backgroundColor: globals.COLOR.headerColor,
        }}
        is_notification_active={is_notification_active}
        screen={'orders'}
        languageSwitchScreen={languageSwitchScreen}
      />
      <View style={styles.screenMains}>
        <View style={styles.listView}>

          {showView &&
            <Animated.View style={{
              transform: [
                {
                  translateY: slideInLeft.interpolate({
                    inputRange: [0, 1],
                    outputRange: [-600, 0]
                  })
                }
              ]
            }}>
                <View style={styles.listUpdatedView}>
                  <Text style={[styles.listUpdatedTxt]}>{appTexts.ORDER.list}</Text>
                </View>
            </Animated.View>
          }
         
          <FlatList
            style={styles.flatListStyle}
            ListHeaderComponent={
              <>
                <FlatList
                  style={styles.flatListStylea}
                  data={ activeData }
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={({item}) => (
                    <View style={styles.orderCardView}>
                      <OrderCard
                        item={item}
                        isPastOrders={false}
                        orderId={appTexts.ORDER.ID}
                        onOrderCardClick={onPastOrdercardClick}
                      />
                    </View>
                  )}
                />
                <View style={styles.pastScreen}>
                  <View style={styles.pastLine}>
                    <Text style={styles.pastText}>{appTexts.ORDER.past}</Text>
                  </View>
                  <View style={styles.under}></View>
                </View>
              </>
            }
            data={ data }
            keyExtractor={(item, index) => index.toString()}
            renderItem={renderItem}
            onRefresh={onRefresh}
            contentContainerStyle={{paddingBottom:'20%'}}
            refreshing={isFetching}
            onEndReached={() => onEndReached()}
            onEndReachedThreshold={0.5}
            ListEmptyComponent={
              <Text style={styles.no_orders}>
							  { guestLogin === true ? ( I18nManager.isRTL ? "الرجاء تسجيل الدخول لعرض طلبك" : "Please login to view your orders") : appTexts.ORDER.noorder}
							</Text>
            }
          />
      
        </View>
      </View>
      {Object.keys(addedCartItems).length > 0 &&
        <View style={styles.bannerView}>
          <BannerButton
            onCheckoutClick={onCheckoutClick}
            text1={appTexts.BANNER.count}
            text2={appTexts.BANNER.view}
            text3={appTexts.BANNER.check}
            text4={appTexts.BANNER.SAR}
            onViewPress={onViewPress}
            addedCartItems={addedCartItems}
            isCart={false}
            isDetail={true}
          />
        </View>
      }

      {guestLogin &&
        <LoginPromptModal
          isLoginPromptModalVisible={guestLogin && modalVisible}
          closeModal={setModalVisible}
          gotoLogin={gotoLogin}
        />
      }
    </View>
  );
};

MyOrdersView.propTypes = {};

export default MyOrdersView;
