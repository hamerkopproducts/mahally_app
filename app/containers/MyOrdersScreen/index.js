import React, { useEffect, useState } from "react";
import { Alert, Animated } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { connect } from "react-redux";
import messaging from "@react-native-firebase/messaging";

import MyOrdersView from "./MyOrdersView";
import { bindActionCreators } from "redux";
import appTexts from "../../lib/appTexts";
import functions from "../../lib/functions";
import ServiceWrapperAwait from '../../service/ServiceWrapperAwait';
import * as homeActions from "../../actions/homeActions";
import _ from "lodash";

const MyOrdersScreen = (props) => {
  const [isCartModalVisible, setIsCartModalVisible] = useState(false);

  const [loader, setLoader] = useState(false);
  const [loader2, setLoader2] = useState(false);
  const [allOrders, setAllOrders] = useState([]);
  const [activeOrders, setActiveOrders] = useState([]);
  const [page, setPage] = useState(1);
  const [lastPage, setLastPage] = useState(0);
  const [all_total, setAllTotal] = useState(0);
  const [isFetching, setIsFetching] = useState(false);

  const [showView, setShowView] = useState(false);
  const [slideInLeft, setSlideInLeft] = useState(new Animated.Value(0));
  const [modalVisible, setModalVisible] = useState(true);

  useEffect(() => {
    if(showView) {
      startAnim();
    }
  }, [showView]);

  const startAnim = () => {
    Animated.parallel([
      Animated.timing(slideInLeft, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true
      })
    ]).start();
    setTimeout(() => {
      setShowView(false);
    }, 3000)
  }

  const refreshOnMessage = () => {
    setShowView(false);
    setTimeout(() => {
      setSlideInLeft(new Animated.Value(0));
      setShowView(true);
    }, 200);
    onRefresh();
  }

  const getCollections = async(url, is_pull_down, type) => {
    const sAsyncWrapper = new ServiceWrapperAwait();
    let response = null;
    if(type == 'past') {
      if(is_pull_down != 'pulldown') {
        setLoader(true);
      }
      response = await sAsyncWrapper.get(url,
        {is_auth_required: true}
      );
    } else {
      setLoader2(true);
      response = await sAsyncWrapper.post(url, {});
      setLoader2(false);
    }
    try {
      const data = new Promise((resolve, reject) => {
        setLoader(false);
        setIsFetching(false);
        try {
          resolve(response);
        } catch (err) {
          reject(err);
        }
      });
  
      return data;
    } catch (err) {
      setLoader(false);
      setIsFetching(false);
    }
  }
  const onRightButtonPress = () => {
    props.navigation.navigate('NotificationScreen');
  };
  const ordersDataFetch = async(__page, is_pull_down) => {
    if(props.guestLogin) {
      return false;
    }

    const data = await getCollections('user-app/order/list?page=' + __page, is_pull_down, 'past');
    if(__page == 1) {
      const _active_data = await getCollections('user-app/order/active-orders', is_pull_down, 'active');
      if(_active_data.success == true) {
        setActiveOrders(_active_data.data);
      }
    }
    if(data.success == true) {
      setAllTotal(data.data.total);
      setLastPage(data.data.last_page);
      if(data.data.current_page == __page) {
        let _data = [];
        if(__page == 1) {
          _data = [...data.data.data];
        } else {
          _data = [...allOrders, ...data.data.data];
        }
        setAllOrders(_data);
      }
    } else {
      functions.displayToast('error', 'top', 'Error' , data?.error?.msg || '');
    }
  }

  const onEndReached = () => {
    const _page = parseInt(page) + 1;
      if(_page <= lastPage) {
        setPage(_page);
        ordersDataFetch(_page, '');
      }
  }

  //will focus
  useEffect(() => {
    ordersDataFetch(page, '');
  }, []);

  const onRefresh = (is_fetch=true) => {
    if(is_fetch) {
      setIsFetching(true);
    }
    setAllOrders([]);
    setPage(1);
    setLastPage(0);
    setAllTotal(0);
    setTimeout(() => {
      ordersDataFetch(1, is_fetch ? 'pulldown' : '');
    });
    
  }

  //will focus
  useEffect(() => {
    return props.navigation.addListener("focus", () => {
      setModalVisible(true);
      props.NotificationBadgeReset(props.token)
      NetInfo.fetch().then((state) => {
        if (state.isConnected) {
          try {
            if(props.route.params.from != 'details') {
              onRefresh(false);
            }
          } catch(err) {
            onRefresh(false);
          }
          props.navigation.setParams({from :'order'})
        } else {
          functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation, props.route]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    props.NotificationBadgeReset(props.token)
      return messaging().onMessage(async (remoteMessage) => {
        refreshOnMessage();
      });
  };

  //unmount
  useEffect(() => {
    return () => {
      handleComponentUnmount();
    };
  }, []);

  const handleComponentUnmount = () => {};

  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {};

  const onProfileButtonPress = () => {
    props.navigation.navigate("ProfileScreen");
  };

  const onPastOrdercardClick = (id) => {
    props.navigation.navigate("OrderDetail", {id: id, from: 'list'});
  };

  const onCheckoutClick = () => {
    props.navigation.navigate("CheckoutScreen");
  };

  const onViewPress = () => {
    setIsCartModalVisible(!isCartModalVisible);
  };

  const gotoLogin = () => {
    props.navigation.navigate('LoginStackNavigator', { screen: 'LoginScreen' });
  };

  return (
    <MyOrdersView
      onProfileButtonPress={onProfileButtonPress}
      onCheckoutClick={onCheckoutClick}
      onPastOrdercardClick={onPastOrdercardClick}
      onViewPress={onViewPress}
      isCartModalVisible={isCartModalVisible}
      addedCartItems={props.addedCartItems}
      data={allOrders}
      activeData={activeOrders}
      isLoading={loader || loader2}
      onEndReached={onEndReached}
      onRefresh={onRefresh}
      isFetching={isFetching}
      onRightButtonPress={onRightButtonPress}
      slideInLeft={slideInLeft}
      showView={showView}
      is_notification_active={props.is_notification_active}
      languageSwitchScreen={props.languageSwitchScreen}
      notification_badge={props.notification_badge}
      guestLogin={props.guestLogin}
      modalVisible={modalVisible}
      setModalVisible={setModalVisible}
      gotoLogin={gotoLogin}
    />
  );
};

const mapStateToProps = (state, props) => {
  return {
    addedCartItems: state.cartReducer.cartAddedData,
    is_notification_active: state.homeReducer.is_notification_active,
    notification_badge:  _.get(state, 'homeReducer.notificationbadge', ''),
    token: _.get(state, "loginReducer.userData.data.access_token", null),
    guestLogin: state.loginReducer.guestLogin,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      languageSwitchScreen: homeActions.languageSwitchScreen,
      NotificationBadgeReset: homeActions.NotificationBadgeReset,
    },
    dispatch
  );
};

const myOrdersScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(MyOrdersScreen);

myOrdersScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null,
});

export default myOrdersScreenWithRedux;
