import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"

let headerButtonContainerWidth = globals.SCREEN_SIZE.width - (globals.MARGIN.marginTen*2);
let headerButtonWidth = (headerButtonContainerWidth-(globals.MARGIN.marginTen*2)) / 3;

const images = {
  
};

const styles = StyleSheet.create({
  screenMain: {
    width: globals.SCREEN_SIZE.width,
    //height: globals.SCREEN_SIZE.height,
    flexDirection: 'column',
    backgroundColor:'green',
    flex:1,
  },
  screenDesignContainer: {
    width: globals.SCREEN_SIZE.width,
    flex:1,

   // height: globals.SCREEN_SIZE.height,
  },
  screenContainerWithMargin: {
    width: globals.INTEGER.screenWidthWithMargin,
   // height: globals.INTEGER.screenContentHeight,//EDITED TO REDUCE FLATLIST HEIGHT
    marginLeft: globals.INTEGER.leftRightMargin,
    flex:1,

    paddingBottom:30
  },
  screenMains: {
    backgroundColor:globals.COLOR.red,
    // borderWidth:1,
    // borderColor:'black',
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    flex:1,
  
  },
  orderCardView:{
    marginTop:'5%',
    
  },
  pastScreen:{
     backgroundColor:'white',
     borderTopRightRadius: 30,
     borderTopLeftRadius: 30,
     bottom:20
    //flex:0.3,
    //marginTop:'5%',
  },
  pastLine:{
    marginTop:'4%',
    marginLeft:'6%',

  },
  pastText:{
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize:14, 
    textAlign:'left'
  },
  under:{
    borderColor:globals.COLOR.Text,
    borderWidth:2,
    width: 50,
    marginLeft:'6%',
    borderRadius:2,
    marginTop: 5
  },
  flatListStyle:{
    //backgroundColor:'red',
 backgroundColor:'white',
  borderTopLeftRadius:60,
  borderTopRightRadius:60,
  
  
  //marginTop:30
  
  },
  listCurve:{
    backgroundColor:'green',
  },
  flatListStylea:{
    
    backgroundColor:globals.COLOR.greyBackground,
    borderTopLeftRadius:30,
    borderTopRightRadius:30,
    paddingBottom:50
    },
  listView:{
    flex:1,
    backgroundColor:globals.COLOR.headerColor,
    
  },
  bannerView:{
    position:'absolute',
    bottom:0,
    width:'100%'
  },
  no_orders: {
    // textTransform: 'uppercase',
    marginRight: 10,
    textAlign: 'center',
    marginTop: 20,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.helveticaBold,
    fontSize:14,
  },
  listUpdatedTxt: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.avenirLight,
    color: '#fff', 
    textAlign: 'center',
    fontSize: 12,
    width: 100,
  },
  listUpdatedView: {
    alignItems: 'center', 
    backgroundColor: 'black', 
    width: 100, 
    alignSelf: 'center', 
    borderRadius: 25, 
    borderWidth: 2,
    opacity: .8,
    position: 'absolute',
    top: 50
  }
});

export { images, styles };
