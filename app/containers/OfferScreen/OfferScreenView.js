import React from "react";
import { View, FlatList, RefreshControl, Text, I18nManager } from "react-native";

import { styles } from "./styles";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import PropTypes from "prop-types";

import functions from "../../lib/functions";
import OfferScreenCard from "../../components/OfferScreenCard";
import Header from "../../components/Header";
import Loader from "../../components/Loader";
import _ from "lodash";
import BannerButton from "../../components/BannerButton";

const OfferScreenView = (props) => {
  const {
    onRightButtonPress,
    onProfileButtonPress,
    onMenufoodCardPress,
    offerCardDetails,
    isLoading,
    onRefresh,
    refreshing,
    onCheckoutClick,
    addToCart,
    addedCartItems,
    selectedRestaurant,
    redirectToScan,
    is_notification_active,
    languageSwitchScreen,
    notification_badge,
  } = props;

  const addItemToCart = (item, clear_cart) => {
    let addedItems =
      (typeof addedCartItems == "undefined" || clear_cart == true)
        ? {}
        : Object.assign({}, addedCartItems);
    if (
      Object.keys(addedItems).indexOf(
        "offer_" + item.item_id.toString()
      ) == -1
    ) {
      let itemToAdd = Object.assign({}, item);
      itemToAdd.addonsSelected = {};
      itemToAdd.count = 1;
      itemToAdd.id = "offer_" + item.item_id;
      itemToAdd.is_offer = true;

      let offer_price = item.discount_price != null ? item.discount_price : item.price;
      if (item.discount_type == "Value") {
        offer_price = offer_price - item.discount_value;
      } else {
        offer_price =
          offer_price - (offer_price / 100) * item.discount_value;
      }
      itemToAdd.price = item.price;
      itemToAdd.discount_price = offer_price;

      addedItems["offer_" + item.item_id] = itemToAdd;
      setTimeout(() => {
        functions.displayToast(
          "success",
          "top",
          appTexts.ALERT_MESSAGES.success,
          appTexts.ORDER.itemadded
        );
      }, 400);
    } else {
      let all_items = Object.assign({}, addedItems);
      all_items["offer_" + item.item_id].count += 1;
      all_items["offer_" + item.item_id].id = "offer_" + item.item_id;
      addedItems = all_items;
      setTimeout(() => {
        functions.displayToast(
          "success",
          "top",
          appTexts.ALERT_MESSAGES.success,
          appTexts.ORDER.itemupdated
        );
      }, 400);
    }
    addToCart(addedItems);
  }

  const renderItem = (item, index) => (
    <OfferScreenCard
      item={item}
      onMenuCardPress={(item_id, item, type) =>
        onMenufoodCardPress(item_id, item, type, item.time_status)
      }
      openScanModal={async (item) => {
        if (item.time_status !== "OPEN") {
          functions.displayToast(
            "error",
            "top",
            appTexts.REST.Heading_closed,
            appTexts.REST.closed
          );
          return false;
        }
        const restaurant_id = item.restaurant_id;

        if(selectedRestaurant.dine_in != 'dine' && ! selectedRestaurant.restaurant_id && item.pre_order == 'disable') {
          await functions.displayAlertWithCallBack(
            '',
            I18nManager.isRTL ? 'هذا المطعم لا يقبل بالطلبات المسبقة إذا تابعت سيُطلب منك مسح الرمز عند الدفع' : 'This restaurant does not accept pre-orders, if you continue you will be asked to scan the QR code  at checkout',
            (data) => {
              if (data) {
                redirectToScan("offer", item.item_id, restaurant_id, item.pre_order, item.vat_includes, item.vat_percentage);
                addItemToCart(item, true);
              }
            },
            appTexts.LOGIN_POP.ok,
            appTexts.LOGIN_POP.cancel
          );
          return false;
        }

        if (
          typeof selectedRestaurant.restaurant_id != "undefined" &&
          selectedRestaurant.restaurant_id != restaurant_id
        ) {
          await functions.displayAlertWithCallBack(
            appTexts.REST.already_in_cart,
            appTexts.REST.cart_has_items,
            (data) => {
              if (data) {
                addToCart({});
                redirectToScan("offer", item.item_id, restaurant_id, item.pre_order, item.vat_includes, item.vat_percentage);
                addItemToCart(item, true);
              }
            }
          );
        } else if (typeof selectedRestaurant.restaurant_id == "undefined") {
          redirectToScan("offer", item.item_id, restaurant_id, item.pre_order, item.vat_includes, item.vat_percentage);
          addItemToCart(item, true);
        } else {
          addItemToCart(item, false);
        }
      }}
    />
  );

  return (
    <View style={styles.screenMain}>
      {isLoading && <Loader />}

      <Header
        notification_badge={notification_badge}
        isProfileButtonRequired={true}
        onRightButtonPress={onRightButtonPress}
        isLanguageButtonRequired={true}
        isRightButtonRequired={true}
        headerTitle={appTexts.STRING.Mahally}
        onProfileButtonPress={onProfileButtonPress}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          backgroundColor: globals.COLOR.headerColor,
        }}
        is_notification_active={is_notification_active}
        screen={"offer"}
        languageSwitchScreen={languageSwitchScreen}
      />

      <View style={styles.formWrapper}>
        <FlatList
          ListEmptyComponent={
            <Text style={styles.no_orders}>{appTexts.OFFER.noOffer}</Text>
          }
          style={styles.flatListStyle}
          contentContainerStyle={{ paddingBottom: "2%" }}
          data={_.isEmpty(offerCardDetails) ? [] : offerCardDetails}
          extraData={offerCardDetails !== null ? offerCardDetails : []}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => {
            return renderItem(item, index);
          }}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
        />

        {Object.keys(addedCartItems).length > 0 && (
          <BannerButton
            text1={appTexts.BANNER.count}
            text2={appTexts.BANNER.view}
            text3={appTexts.BANNER.check}
            text4={appTexts.BANNER.SAR}
            onCheckoutClick={onCheckoutClick}
            addedCartItems={addedCartItems}
            isCart={false}
            isDetail={true}
          />
        )}
      </View>
    </View>
  );
};

OfferScreenView.propTypes = {
  offerCardDetails: PropTypes.array,
  onRefresh: PropTypes.func,
};

export default OfferScreenView;
