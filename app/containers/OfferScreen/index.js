import React, { useEffect, useState } from 'react';
import { I18nManager, BackHandler } from 'react-native'
import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import OfferScreenView from './OfferScreenView';
import { bindActionCreators } from "redux";
import * as homeActions from "../../actions/homeActions";

import * as offersActions from "../../actions/offersActions";
import appTexts from "../../lib/appTexts";
import functions from "../../lib/functions"
import _ from "lodash"
import * as cartActions from "../../actions/cartActions";
import { store } from '../../../configureStore';

const OfferScreen = (props) => {

  //Initialising states
  const [tabIndex, setTabIndex] = useState(0);
  const [onemoretab, setOnemoreTab] = useState(1);
  const [isScanModalVisible,setIsScanModalVisible] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [params, setParams] = useState({cust_id: _.get(props, "userID", ""), latitude: '8.56', longitude: '76.87'});

  React.useEffect(() => {
    try {
      const localStoreLocation = store.getState().geoLocationReducer.geoLocation;
      const latitudeUser = localStoreLocation.status == true ? localStoreLocation.location.data.latitude : 0;
      const longitudeUser = localStoreLocation.status == true ? localStoreLocation.location.data.longitude : 0;
      let _params = params;
      _params.latitude = latitudeUser;
      _params.longitude = longitudeUser;
      props.clearMenuDetails()
      props.getOffers(_params)
    } catch(err) {
      props.clearMenuDetails()
      props.getOffers(_params)
    }
	}, []);

  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          try {
            const localStoreLocation = store.getState().geoLocationReducer.geoLocation;
            const latitudeUser = localStoreLocation.status == true ? localStoreLocation.location.data.latitude : 0;
            const longitudeUser = localStoreLocation.status == true ? localStoreLocation.location.data.longitude : 0;
            let _params = params;
            _params.latitude = latitudeUser;
            _params.longitude = longitudeUser;
            props.clearMenuDetails()
            props.getOffers(_params)
          } catch(err) {
            props.clearMenuDetails()
            props.getOffers(_params);
          }
        } else {
          functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
        }
      });
      BackHandler.addEventListener('hardwareBackPress', backPressed = () => {
        BackHandler.exitApp();
        return true;
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        props.getOffers(params)
      } else {
        functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
      }
    });
    BackHandler.addEventListener('hardwareBackPress', backPressed = () => {
      BackHandler.exitApp();
      return true;
    });
  }

  //unmount
  useEffect(() => { return () => { handleComponentUnmount(); } }, []);

  const handleComponentUnmount = () => {
  };

  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
    if(props.pullRefresh === true)
    {
      setRefreshing(false);
    }
  };
  const onRefresh = async() => {
    setRefreshing(true);
    try {
      const localStoreLocation = store.getState().geoLocationReducer.geoLocation;
      const latitudeUser = localStoreLocation.status == true ? localStoreLocation.location.data.latitude : 0;
      const longitudeUser = localStoreLocation.status == true ? localStoreLocation.location.data.longitude : 0;
      let _params = params;
      _params.latitude = latitudeUser;
      _params.longitude = longitudeUser;
      props.clearMenuDetails()
      props.getOffers(_params)
    } catch(err) {
      props.clearMenuDetails()
      props.getOffers(_params)
    }
  }
  
  const onTabChange = (index) => {
    if (tabIndex !== index) {
      setTabIndex(index);
    }
  };
  const onProfileButtonPress = () => {
    props.navigation.navigate("ProfileScreen")
  };
  const onMenufoodCardPress = (item_id, item, type, time_status) =>{
    if(item_id == '') {
      functions.displayToast('error', 'top', 'Sorry' , 'Offer item not found!');
      return false;
    }
    props.navigation.navigate('ProductDetailScreen', { itemId: item_id, from: type, item: item, time_status: time_status})
  };
  
  const onCalendarFilterPress = (index) => {
  alert('calendar click')
  };

  const openScanModal = () =>{
    setIsScanModalVisible(!isScanModalVisible)
  };
  
  const onCheckoutClick = () => {
    props.navigation.navigate("CheckoutScreen");
  };
  const onRightButtonPress = () => {
    props.navigation.navigate('NotificationScreen');
  };

  return (
    <OfferScreenView
      onProfileButtonPress={onProfileButtonPress}
      tabIndex={tabIndex}
      onemoretab={onemoretab}
      onTabChange={onTabChange}
      onCalendarFilterPress={onCalendarFilterPress} 
      isScanModalVisible={isScanModalVisible}
      openScanModal={openScanModal}
      onMenufoodCardPress={onMenufoodCardPress}
      offerCardDetails = {_.get(props, 'offerCardData', [])}
      isLoading={props.isLoading}
      refreshing={refreshing}
      onRefresh={onRefresh}
      onRightButtonPress={onRightButtonPress}
      onCheckoutClick={onCheckoutClick}
      addToCart={props.addToCart}
      addedCartItems={props.addedCartItems}
      selectedRestaurant={props.selectedRestaurant}
      redirectToScan={async(type, item_id, restaurant_id, hasPreOrder, vat_includes, vat_percentage) => {
        // props.postScanItemToAdd({
        //   type: type,
        //   item_id: item_id
        // });

        let _scanned_data = {
          restaurant_id: restaurant_id,
          table_id: '',
          color_code: '',
          dine_in: 'take',
          vat: vat_includes == 'yes' ? vat_percentage : '',
          has_preOrder: hasPreOrder == 'enable'
        };
        props.selectRestaurant(_scanned_data);
      }}
      is_notification_active={props.is_notification_active}
      notification_badge={props.notification_badge}
      languageSwitchScreen={props.languageSwitchScreen}
      />
  );
};


const mapStateToProps = (state, props) => {
  return {
    pullRefresh: _.get(state, 'offersReducer.pullRefresh', ''),
    offerCardData: _.get(state, 'offersReducer.offerCardData', []),
    isLoading: _.get(state, 'offersReducer.isLoading', ''),
    notification_badge:  _.get(state, 'homeReducer.notificationbadge', ''),

    addedCartItems: state.cartReducer.cartAddedData,
    selectedRestaurant: state.cartReducer.selectedRestaurant,
    is_notification_active: state.homeReducer.is_notification_active,

    userID: _.get(state, "loginReducer.userData.data.user.id", null),
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    getOffers: offersActions.getOffersData,
    clearMenuDetails: offersActions.clearMenuDetails,
    addToCart: cartActions.cartUpdate,
    languageSwitchScreen: homeActions.languageSwitchScreen,
    postScanItemToAdd: cartActions.postScanItemToAdd,
    selectRestaurant: cartActions.selectRestaurant,
  }, dispatch)
};

const OfferScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(OfferScreen);

OfferScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default OfferScreenWithRedux;



