import { StyleSheet, I18nManager } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import globals from "../../lib/globals";

const images = {};

const styles = StyleSheet.create({
  screenMain: {
    flexDirection: "column",
    flex: 1,
    backgroundColor: 'white'
  },
  screenContainer: {
    backgroundColor: globals.COLOR.transparent,
    marginBottom: globals.INTEGER.screenBottom
  },
  headerWrapper: {
    flex: .1,
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: hp("5%"),
  },
  formWrapper: {
    flex:1,
    backgroundColor: globals.COLOR.greyBackground,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  verifyText: {
    fontSize: 18,
    color: '#232020',
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  verifyWrapper: {
    marginLeft: '5%'
  },
  flatListStyle: {
    marginTop: "5%",
  },
  imageWrapper: {
    paddingLeft: '5%'
  },
  arrowImage: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    paddingLeft: '5%',
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
  },
  headingText: {
    textAlign: 'left',
    fontSize: 14,
    color: '#232020',
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  no_orders: {
    marginRight: 10,
    textAlign: 'center',
    marginTop: 20,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.helveticaBold,
    fontSize:14,
  },
});

export { images, styles };
