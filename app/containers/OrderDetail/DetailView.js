import React, { useState } from "react";
import PropTypes from "prop-types";
import { View, ScrollView, Text, Image, I18nManager, FlatList, RefreshControl } from "react-native";
import globals from "../../lib/globals";
import { styles, images } from "./styles";

import appTexts from "../../lib/appTexts";
import Header from "../../components/Header";
import ItemCard from "../../components/ItemCard";
import BillCard from "../../components/BillCard";
import RateModal from "../../components/RateModal";
import Loader from "../../components/Loader";
import Rating from "../../components/Rating/Rating";

const DetailView = (props) => {
  const {
    onBackButtonClick,
    data,
    isLoading,
    onTabClick,
    segments,
    onPress,
    onSubmitRating,
    isRateModalVisible,
    refreshing,
    onRefresh,
    backDropOpacity,
    is_rate_modal_visible, 
    set_is_rate_modal_visible
  } = props;

  let cooking_time = data?.cooking_time;
  if(data?.order_status_id == 5) {
    cooking_time = null;
  }

  let delivery_status = data?.delivery_status;
  if(data?.order_status_id == 4) {
    delivery_status = null;
  }


  let grant_total = parseFloat(0);
  let sub_total = parseFloat(0);
  let discount_price = parseFloat(0);
  let order_id = '';
  let created_at = '';
  try {
    grant_total = parseFloat(data.grand_total == null ? 0 : data.grand_total);
    sub_total = parseFloat(data.sub_total == null ? 0 : data.sub_total);
    order_id = data.order_id;
    created_at = data.order_date;
  } catch(err) {
    grant_total = parseFloat(0);
    sub_total = parseFloat(0);
    order_id = '';
    created_at = '';
  }

  let vat_percentage = 0;
  try {
    vat_percentage = data.vat == null ? 0 : data.vat;
  } catch(err) {
    vat_percentage = 0;
  }

  grant_total = typeof grant_total == 'undefined' ? parseFloat(0) : grant_total;

  const promo_code = data.promo_code;
  const promo_type = data.promo_type;
  const promo_value = data.promo_value == '' ? parseFloat(0) : parseFloat(data.promo_value);

  const is_accepted = data.order_status_id == 2 || 
                      data.order_status_id == 3 ||
                      data.order_status_id == 4 || 
                      data.order_status_id == 5;
  const is_in_kitchen = data.order_status_id == 3 || 
                        data.order_status_id == 4 || 
                        data.order_status_id == 5;
  const is_ready =  data.order_status_id == 4 || 
                    data.order_status_id == 5;
  const is_kitchen= data.order_status_id == 1 ||
                    data.order_status_id == 2 ||  
                     data.order_status_id == 4 || 
                     data.order_status_id == 5;
  const is_delivered = data.order_status_id == 5;

  const show_rate_modal = data.order_status_id == 5;
  if(show_rate_modal && isRateModalVisible == false && data.rating == null && is_rate_modal_visible == true) {
    onTabClick();
  }

  let rating = 0;
  let _segments = [];
  if(data.rating != null) {
    rating = data.rating.rating;
    _segments = data.rating.reviewsegment;
  }

  const total = data.order_item ? data.order_item.length : 0;
  let addon_total = 0;
  if(data.order_item) {
    for(incr=0; incr < data.order_item.length; incr++) {
      addon_total += data.order_item[incr].addon.length;
    }
  }
  const item_count_txt = `${total} ` + (total > 1 ? appTexts.BANNER.Item : appTexts.BANNER.Item) + 
                        `, ${addon_total} ` + (addon_total > 1 ? appTexts.BANNER.on_s : appTexts.BANNER.on) ;

  let logo = '';
  try {
    logo = data.restaurant.logo;
  } catch(err) {
    logo = '';
  }

  

  return (
    <View style={styles.screenMain}>

        {isLoading && <Loader />}

      <Header
      data={data}
        navigation={props.navigation}
        iscenterLogoRequired={false}
        onTabClick={onTabClick}
        isBackButtonRequired={true}
        isOrderDetailPage={true}
        orderId={order_id}
        restaurantData={data.restaurant}
        created_at={created_at}
        onBackButtonClick={onBackButtonClick}
        isDineIn={data.order_type}
        tableId={data.table_id}
        logo={logo}
        is_kitchen={is_kitchen}
        is_ready={is_ready}
        cooking_time={cooking_time}
        delivery_status={delivery_status}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          justifyContents: "center",
          backgroundColor: globals.COLOR.headerColor,
        }}
      />

      <View style={styles.screenContainer}>
        <ScrollView refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
          <View style={styles.statusBox}>
            <View style={styles.lineOne}>
              <Text style={styles.statusText}>{appTexts.ORDER.status}</Text>
            </View>
            <View style={styles.lineTwo}>
              <View style={styles.checkIcon}>
                {(is_accepted ? 
                  <Image source={images.accept} style={styles.check} />
                :
                  <Image source={images.check} style={styles.check} />
                )}
                <View style={styles.dashLine}></View>
              </View>
              <View style={styles.acceptView}>
                <Text style={styles.acceptText}>{appTexts.ORDER.accept}</Text>
                <Text style={styles.your}>{appTexts.ORDER.your}</Text>
              </View>
            </View>
            <View style={styles.lineTwo}>
              <View style={styles.checkIcon}>
              {(is_in_kitchen ? 
                  <Image source={images.accept} style={styles.check} />
                :
                  <Image source={images.check} style={styles.check} />
                )}
                <View style={styles.dashLine}></View>
              </View>
              <View style={styles.acceptView}>
                <Text style={styles.acceptText}>{appTexts.ORDER.kitchen}</Text>
                <Text style={styles.your}>{appTexts.ORDER.prepare}</Text>
              </View>
            </View>
            <View style={styles.lineTwo}>
              <View style={styles.checkIcon}>
              {(is_ready ? 
                  <Image source={images.accept} style={styles.check} />
                :
                  <Image source={images.check} style={styles.check} />
                )}
                <View style={styles.dashLine}></View>
              </View>
              <View style={styles.acceptView}>
                <Text style={styles.acceptText}>{appTexts.ORDER.ready}</Text>
                <Text style={styles.your}>{appTexts.ORDER.collect}</Text>
              </View>
            </View>
            <View style={styles.lineTwo}>
              <View style={styles.checkIcon}>
              {(is_delivered ? 
                  <Image source={images.accept} style={styles.check} />
                :
                  <Image source={images.check} style={styles.check} />
                )}
              </View>
              <View style={styles.acceptView}>
                <Text style={styles.acceptText}>{appTexts.ORDER.delivered}</Text>
                <Text style={styles.your}>{appTexts.ORDER.deliveredstatus}</Text>
              </View>
            </View>
          </View>

          <View style={styles.statusBox}>
            <View style={styles.lineOne}>
              <Text style={styles.statusText}>{appTexts.ORDER.item}</Text>
              <Text style={styles.countText}>{ item_count_txt }</Text>
            </View>

            <FlatList
                data={data.order_item}
                keyExtractor={(item, index) => index.toString()}
                renderItem={(item) => (
                    <View style={styles.itemListView}>
                    <ItemCard
                        item={item}
                        onItemCardPress={null}
                        isAddonListVisible={null}
                        isOrderDetails={true}
                    />
                    </View>
                )}
            />
          </View>
          <View style={styles.billBox}>
            <BillCard
              subtotal={sub_total}
              total_price={ grant_total } 
              discount={ discount_price }
              is_amount_promo_type={promo_type == 'Amount'}
              promo_value={promo_value}
              promo_code={promo_code}
              checkout={false}
              vat_percentage={vat_percentage}
            />
          </View>
          {data.rating != null &&
            <View style={styles.statusBox}>
              <View style={styles.rateLine}>
                <Text style={styles.rateT}>{appTexts.RATE.your}</Text>
              </View>
              <View style={styles.boxLine}>
                <View style={styles.rectangle}>
                  <Text style={styles.four}>{ rating }</Text>
                </View>
                <View style={styles.colText}>
                  <View style={styles.stars}>
                    <Rating
                      is_clickable={false} 
                      half_star={true}
                      onPress={(val) => null}
                      rating={rating}
                    />
                  </View>
                </View>
              </View>

              <View style={styles.rowText}>

              <FlatList
                data={ _segments }
                keyExtractor={(item, index) => index.toString()}
                horizontal
                showsHorizontalScrollIndicator={false}
                renderItem={({item}) => {
                  const lang = I18nManager.isRTL ? 1 : 0;
                  let name = '';
                  try {
                    name = item.segment.lang[lang].name;
                  } catch(err) {}
                  return (
                    <View style={styles.col1}>
                      <View style={styles.line1}>
                        <Text style={styles.service}>{ name }</Text>
                      </View>
                      <View style={styles.line2}>
                        <Text style={styles.service}>{item.rating}/5</Text>
                      </View>
                    </View>
                  )
                }}
              />

              </View>
            </View>
          }
        </ScrollView>
      </View>
      {isRateModalVisible&&
        <RateModal
          backDropOpacity={backDropOpacity}
          isRateModalVisible={isRateModalVisible && is_rate_modal_visible}
          openRateModal={() => {
            set_is_rate_modal_visible(false);
            onTabClick();
          }}
          segments={segments}
          onPress={onPress}
          onSubmitRating={() => {
            onSubmitRating();
          }}
        />
      }
    </View>
  );
};

DetailView.propTypes = {
  onValueChange: PropTypes.func,
  istoggle: PropTypes.bool,
};

export default DetailView;
