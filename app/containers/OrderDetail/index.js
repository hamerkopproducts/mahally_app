import React, { useEffect, useState } from "react";
import NetInfo from "@react-native-community/netinfo";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { I18nManager, BackHandler } from 'react-native'
import globals from "../../lib/globals";
import functions from "../../lib/functions";
import DetailView from "./DetailView";
import ServiceWrapperAwait from '../../service/ServiceWrapperAwait';
import messaging from "@react-native-firebase/messaging";

const OrderDetail = (props) => {
  
  const [isAddonListVisible, setIsAddonListVisible] = useState(false);
  const [isAddonListVisible2, setIsAddonListVisible2] = useState(false);
  const [isRateModalVisible, setIsRateModalVisible] = useState(false);
  const [detailsData, setData] = useState({});
  const [segments, setSegments] = useState([]);
  const [userRatedSegments, setUserRatedSegments] = useState({"review" : [], message: ""});
  const [loader, setLoader] = useState({});
  const [restaurantId, setRestaurantId] = useState("");
  const [is_rate_modal_visible, set_is_rate_modal_visible] = useState(true);

  const [backDropOpacity, setBackDropOpacity] = useState(.7);
  const [refreshing, setRefreshing] = useState(false);
  const orderId = props.route.params.id;
  
  const changeBackDropOpacity = () => {
    setTimeout(() => {
      setBackDropOpacity(0.1);
    }, 300);
    setTimeout(() => {
      setBackDropOpacity(0.7);
    }, 3000);
  };

  const getDetails = async(url) => {
    const sAsyncWrapper = new ServiceWrapperAwait();
    const response = await sAsyncWrapper.get(url,
      {is_auth_required: true}
    );
    try {
      const data = new Promise((resolve, reject) => {
        try {
          resolve(response);
        } catch (err) {
          reject(err);
        }
      });
  
      return data;
    } catch (err) {
    }
  }

  const submitRating = async(data) => {
    const sAsyncWrapper = new ServiceWrapperAwait();
    const response = await sAsyncWrapper.post('user-app/rate/order', data);
    try {
      const data = new Promise((resolve, reject) => {
        try {
          resolve(response);
        } catch (err) {
          reject(err);
        }
      });
  
      return data;
    } catch (err) {
    }
  }

  const ordersDataFetch = async(id) => {
    setLoader(true);
    data = await getDetails(`user-app/order/details/${id}`);
   
    setLoader(false);
    if(data.success == true) {
      setData(data.data);
      setRestaurantId(data.data.restaurant.id)
    } else {
      functions.displayToast('error', 'top', 'Error' , data?.error?.msg || '');
    }
  }

  const onRefresh = async() => {
    data = await getDetails(`user-app/order/details/${orderId}`);
    if(data.success == true) {
      setData(data.data);
    } else {
      functions.displayToast('error', 'top', 'Error' , data?.error?.msg || '');
    }
  }

  const ratingDataFetch = async() => {
    setRefreshing(true);
    data = await getDetails(`user-app/rating`);
    setRefreshing(false);
    if(data.success == true) {
      setSegments(data.data);
    }
  }

  //will focus
  useEffect(() => {
    return props.navigation.addListener("focus", () => {
      NetInfo.fetch().then((state) => {
        if (state.isConnected) {
        } else {
          functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
        }
      });
      BackHandler.addEventListener('hardwareBackPress', backPressed = () => {
        BackHandler.exitApp();
        return true;
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
   
   setInterval( onRefresh , 20000);

    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        ratingDataFetch();
        ordersDataFetch(orderId);
      } else {
        functions.displayAlert(null, globals.ALERT_MESSAGES.noInternet);
      }
    });

    BackHandler.addEventListener('hardwareBackPress', backPressed = () => {
      BackHandler.exitApp();
      return true;
    });
  };

  useEffect(() => {
    const messageEventUnsubscribe = messaging().onMessage(async (remoteMessage) => {
      ratingDataFetch();
      ordersDataFetch(orderId);
    });

    return () => { messageEventUnsubscribe() }
  }, []);

  //unmount
  useEffect(() => {
    return () => {
      handleComponentUnmount();
    };
  }, []);

  const handleComponentUnmount = () => {};

  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {};

  const onBackButtonClick = () => {
    let from = '';
    try {
      from = props.route.params.from;
    } catch(err) {
      from = '';
    }
    props.navigation.setParams({from : ''})
    props.navigation.navigate('MyOrders', {screen: 'MyOrdersScreen', params: { from: from == 'list' ? 'details' : ''}});
  };

  const onItemCardPress = () => {
    setIsAddonListVisible(!isAddonListVisible);
  };
  const onItemCardPress2 = () => {
    setIsAddonListVisible2(!isAddonListVisible2);
  };
  const onTabClick = () => {
    setIsRateModalVisible(!isRateModalVisible);
  };

  const onPressRate = (id, val, type) => {
    // let _rated = Object.assign({}, userRatedSegments);
    let _rated = userRatedSegments;
    if(type == 'message') {
      _rated.message = val;
    } else {
      let found = false;
      for(inc=0; inc<_rated.review.length; inc++) {
        if(_rated.review[inc].segment_id == id) {
          found = true;
          _rated.review[inc].rating = val;
          break;
        }
      }
      if(found == false) {
        _rated.review.push({
          "segment_id": id,
          "rating": val
        });
      }
    }
    setUserRatedSegments(_rated);
  }

  onSubmitRating = async() => {
    let _rated = Object.assign({}, userRatedSegments);
    _rated.restaurant = restaurantId;
    _rated.order_id = orderId;
    setLoader(true);
    const data = await submitRating(_rated);
    setLoader(false);
    if( data.success == true) {
      setIsRateModalVisible(false);
      setUserRatedSegments( {"review" : [], message: ""} );
      ordersDataFetch(orderId);
      set_is_rate_modal_visible(false);
      functions.displayToast('success', 'top', appTexts.ALERT_MESSAGES.success, data.msg);
    } else {
      changeBackDropOpacity();
      functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, data?.error?.msg || '');
    }
  }

  return (
    <DetailView
      onBackButtonClick={onBackButtonClick}
      onItemCardPress={onItemCardPress}
      isAddonListVisible={isAddonListVisible}
      onItemCardPress2={onItemCardPress2}
      isAddonListVisible2={isAddonListVisible2}
      isRateModalVisible={isRateModalVisible}
      onTabClick={onTabClick}
      data={detailsData}
      isLoading={loader}
      segments={segments}
      onPress={onPressRate}
      onSubmitRating={onSubmitRating}
      refreshing={refreshing}
      onRefresh={onRefresh}
      backDropOpacity={backDropOpacity}
      is_rate_modal_visible={is_rate_modal_visible}
      set_is_rate_modal_visible={set_is_rate_modal_visible}
    />
  );
};

const mapStateToProps = (state, props) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      // doLogout: LoginActions.doLogout,
    },
    dispatch
  );
};

const orderScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderDetail);

orderScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null,
});

export default orderScreenWithRedux;
