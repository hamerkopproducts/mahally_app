import { StyleSheet, I18nManager } from "react-native";
import globals from "../../lib/globals";

let headerButtonContainerWidth =
  globals.SCREEN_SIZE.width - globals.MARGIN.marginTen * 2;
let headerButtonWidth =
  (headerButtonContainerWidth - globals.MARGIN.marginTen * 2) / 3;

 const images = {
  settingsButton:require('../../assets/images/listCard/check-box-active.png'),
  dot:require('../../assets/images/profileicon/Dot.png'),
  noti:require('../../assets/images/listCard/Notify.png'),
  check:require('../../assets/images/OrderCard/Process-a.png'),
  accept:require('../../assets/images/OrderCard/accept.png'),
  arrow:require('../../assets/images/Pay/arrow.png'),
  star: require("../../assets/images/modal/Star.png"),
 };

const styles = StyleSheet.create({
  screenMain:{
    flex:1,
    flexDirection:'column',
    backgroundColor: 'white',
},
screenContainer:{
  marginTop: '30%',
  //width: globals.INTEGER.screenWidthWithMargin,
  width: globals.INTEGER.screenWidth,
 // height: globals.INTEGER.screenContentHeight - 5 + globals.INTEGER.footerTabBarHeight,
  height: globals.INTEGER.screenContentHeight + 650,
  //marginLeft: globals.INTEGER.leftRightMargin,
  
  //width: globals.SCREEN_SIZE.width,
  //height: globals.SCREEN_SIZE.height,
  backgroundColor:globals.COLOR.greyBackground,
  borderRadius:30,
  flex:1,
  marginBottom:'2%'
   
 },
  statusBox:{
    width:'92%',
    marginLeft:'4%',
    marginRight:'4%',
    borderWidth:0.5,
    borderRadius:20,
    borderColor:'white',
    backgroundColor:'white',
   paddingLeft:'3%',
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
    //elevation: 2,
    marginTop:'4%',
   marginBottom:'2%'
  },
  
  lineOne:{
   
    marginTop:'4%',
    flexWrap:'nowrap',
  marginLeft:'2%',
    width:'70%'
  },
  statusText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.Text,  
    textAlign:'left',
    lineHeight: Platform.OS === 'ios' ? 20 : null
  },
  lineTwo:{
      flexDirection:'row',
     // marginLeft:'5%',
      alignItems:'center',
      marginBottom:'8%'
  },
  check:{
      width:30,
      height:30,
  },
  acceptView:{
      marginLeft:'2%',
      marginTop:"2%",
  },
  acceptText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.Text,  
    fontSize:12,   
    textAlign:'left',
    lineHeight: Platform.OS === 'ios' ? 20 : null
  },
  your:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.greyText,  
    fontSize:11,
    textAlign:'left'
  },
  dashLine:{
    flexDirection: 'column',
    borderWidth: 0.5,
    height: 54,
    borderColor: globals.COLOR.greyText,
    borderStyle:'dashed',
    borderRadius:50,
    left:15,
    position:'absolute',
    top:25,
  },
  countText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.greyText,  
    fontSize:14, 
    textAlign:'left'
  },
  itemListView:{
      marginTop:'3%',
      marginBottom:'3%',
      
  },
  billBox:{
     // marginTop:'5%',
      //marginBottom:'5%',
  },
  rateButton:{
    width:'92%',
    height:60,
    borderRadius:10,
    flexDirection:'row',
    justifyContent:'space-between',
    paddingLeft:'5%',
    paddingRight:'5%',
    backgroundColor:'white',
    marginLeft:'4%',
    marginRight:'2%',
    alignItems:'center',
    marginBottom:'10%',
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  rateText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.Text,  
    fontSize:14, 
  },
  scroll:{
    marginTop:'30%'
  },
  arrowImage:{
    width:20,
    height:20,
  },
  rectangle:{
    marginTop:'5%',
    marginLeft:'2%',
    width:80,
    height:50,
    borderRadius:5,
    borderWidth:0.4,
    backgroundColor:globals.COLOR.purple,
    alignItems:'center',
    justifyContent:'center'
},
four:{
  fontSize: 13,
  color: 'white',
  fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
},
boxLine:{
    flexDirection:'row',
    alignItems:'center',
   // justifyContent:'center'
},
colText:{
    marginLeft:'6%',
    alignSelf:'center',
    justifyContent:'center'
},
stars:{
   // marginLeft:'5%',
    flexDirection:'row',
  alignSelf:'center',
  justifyContent:'center'
},
rateLine:{
  paddingTop:'5%',
  paddingLeft:'2%',
},
rateT:{
  fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  textAlign:'left',
  fontSize:14,
},
starImage:{
  height:20,
  width:20,
  resizeMode:'contain'
},
rowText:{
  flexDirection:'row',
  marginTop:'4%',
  marginBottom:'4%',
},
service:{
fontSize: 12,
color: '#2F4F4F',
fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,   
},
col1:{
  justifyContent:'flex-start',
  alignItems:'center',
  flexBasis:'32%',
  width: 80
},
col2:{
  justifyContent:'center',
  alignItems:'center',
  flexBasis:'33%'
},
col3:{
  justifyContent:'flex-end',
  alignItems:'center',
  flexBasis:'32%'
},
boxView:{
  borderWidth:0.5,
  borderRadius:10,
  backgroundColor:'white',  
  width:'90%',
},
});

export { images, styles };
