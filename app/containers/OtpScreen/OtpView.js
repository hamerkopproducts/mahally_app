import React, { useRef, useState, useEffect } from "react";
import PropTypes from "prop-types";
import {
  Button,
  Text,
  View,
  TouchableOpacity,
  Image,
  StatusBar,
  TextInput,
  ScrollView,
  Keyboard
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Modal from "react-native-modal";
import { styles } from "./styles";
import Header from '../../components/Header';
import LinearGradient from "react-native-linear-gradient";
import globals from "../../lib/globals";
import appTexts from "../../lib/appTexts";
import CustomButton from '../../components/CustomButton';
import Loader from "../../components/Loader";

const LoginView = (props) => {
  const otp1Input = useRef(null);
  const otp2Input = useRef(null);
  const otp3Input = useRef(null);
  const otp4Input = useRef(null);
  const { 
    phoneNumber,
    loginButtonPress,
    resendOtpPress,
    onBackButtonClick,
    otp1,
    otp2,
      otp3,
      otp4,
      setOtp1,
      setOtp2,
      setOtp3,
      setOtp4,
      isLoading
   } = props;
   
   const [timeLeft, setTimeLeft] = useState(60);

   useEffect(() => {
    if (!timeLeft) return;
    const intervalId = setInterval(() => {
      if(isLoading === true) {
        setTimeLeft(timeLeft);
      } else {
        setTimeLeft(timeLeft - 1);
      }
    }, 1000);

    return () => clearInterval(intervalId);
  }, [timeLeft, isLoading]);

  return (
    <View style={styles.screenMain}>
      {isLoading && <Loader />}
      <Header
        isBackButtonRequired={true}
        onBackButtonClick={onBackButtonClick}
        isleftlogoRequired={true}
        heading={appTexts.STRING.verify}
				customHeaderStyle={{
          height: globals.INTEGER.headerHeight,

					backgroundColor: globals.COLOR.headerColor
				}}
			/>
      
      <ScrollView style={styles.screenDesignContainer}> 
      <View style={styles.formWrapper}>
        <View style={styles.enterDigit}>
          <Text style={styles.digitCode}>{appTexts.OTP.Enter}</Text>
        </View>

        <View style={styles.sendPhone}>
      <Text style={styles.phoneNumber}>{phoneNumber}</Text>
          {/* <Text style={styles.editText}>{appTexts.OTP.Edit}</Text> */}
        </View>
        <View style={styles.verificationCode}>
          <Text style={styles.verificationcodeText}>
            {appTexts.OTP.Verificationcode}
          </Text>
          {/* <Text style={styles.editText}>{appTexts.OTP.Edit}</Text> */}
        </View>
        <View style={styles.otpWrapper}>
          <TextInput
            maxLength={1}
            style={styles.textInput}
            placeholderTextColor="black"
            keyboardType="number-pad"
            autoFocus={false}
            ref={otp1Input}
            value={otp1}
            onChangeText={val => {
              setOtp1(val);
              if (val != '') {
                otp2Input.current.focus();
              }
            }}
          />
          <TextInput
            maxLength={1}
            style={styles.textInput}
            placeholderTextColor="#282828"
            keyboardType="number-pad"
            ref={otp2Input}
            value={otp2}
            onChangeText={val => {
              setOtp2(val);
              if (val != '') {
                otp3Input.current.focus();
              }
            }}
            onKeyPress={({ nativeEvent }) => {
              if (nativeEvent.key === 'Backspace' && otp2 == '') {
                otp1Input.current.focus();
              }
            }}
          />
          <TextInput
            maxLength={1}
            style={styles.textInput}
            placeholderTextColor="#282828"
            keyboardType="number-pad"
            ref={otp3Input}
            value={otp3}
            onChangeText={val => {
              setOtp3(val);
              if (val != '') {
                otp4Input.current.focus();
              }
            }}
            onKeyPress={({ nativeEvent }) => {
              if (nativeEvent.key === 'Backspace' && otp3 == '') {
                otp2Input.current.focus();
              }
            }}
          />
          <TextInput
            maxLength={1}
            style={styles.textInput}
            placeholderTextColor="#282828"
            keyboardType="number-pad"
            ref={otp4Input}
            value={otp4}
            onChangeText={val => {
              setOtp4(val);
              if (val != '') {
                Keyboard.dismiss();
              }
            }}
            onKeyPress={({ nativeEvent }) => {
              if (nativeEvent.key === 'Backspace' && otp4 == '') {
                otp3Input.current.focus();
              }
            }}
          />
        </View>
        <View style={styles.reseondCode}>
          <Text style={styles.resedText}>{timeLeft > 0 ? appTexts.OTP.Resend : ''}</Text>
          <Text style={styles.resedtextOrange}>{timeLeft > 0 ? " 0:" + timeLeft + "s" : ''}</Text>
        </View>
        <View style={styles.buttonWrapper}>
          <TouchableOpacity
            onPress={() => {
              loginButtonPress();
            }}
            disabled={timeLeft<=0}
          >
            <CustomButton  buttonstyle={[
                styles.custombuttonStyle,
                styles.loginTouch,
                timeLeft <=0 && { backgroundColor: 'lightgray' }
              ]} buttonText={appTexts.OTP.Submit} />
          </TouchableOpacity>
        </View>
        <View style={styles.reseondCode}>
          <Text style={styles.resedText}>{appTexts.OTP.Didnt}</Text>
          <TouchableOpacity  onPress={() => {
              resendOtpPress();
              setTimeLeft(60);
            }} disabled={timeLeft > 0} >
          <Text style={[styles.resedtextOrange, timeLeft == 0 && {color: 'green'} ]}>{appTexts.OTP.Resends}</Text>
          </TouchableOpacity>
        </View>
      </View>
      </ScrollView>
    </View>
  );
};

LoginView.propTypes = {
  loginButtonPress: PropTypes.func,
  resendOtpPress: PropTypes.func,
  onBackButtonPress: PropTypes.func,
  otp1: PropTypes.string,
  otp2: PropTypes.string,
  otp3: PropTypes.string,
  otp4: PropTypes.string,
  setOtp1: PropTypes.func,
  setOtp2: PropTypes.func,
  setOtp3: PropTypes.func,
  setOtp4: PropTypes.func
};

export default LoginView;
