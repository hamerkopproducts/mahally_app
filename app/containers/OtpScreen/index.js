import React, { useState, useEffect } from "react";
import { BackHandler } from "react-native";
import { connect } from "react-redux";
import OtpView from "./OtpView";
import { bindActionCreators } from "redux";
import * as loginActions from "../../actions/loginActions";

import _ from "lodash";
import functions from "../../lib/functions";
import appTexts from "../../lib/appTexts";
import * as cartActions from "../../actions/cartActions";
import messaging from "@react-native-firebase/messaging";

const OtpScreen = (props) => {
  //Initialising states
  const [otp1, setOtp1] = useState("");
  const [otp2, setOtp2] = useState("");
  const [otp3, setOtp3] = useState("");
  const [otp4, setOtp4] = useState("");
  const [loader, setLoader] = useState(false);
  const [fcm_token, setFcmToken] = useState('');

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    BackHandler.addEventListener(
      "hardwareBackPress",
      (backPressed = () => {
        BackHandler.exitApp();
        return true;
      })
    );
  };

  useEffect(() => {
    let isMounted = true;
    const fetchToken = async() => {
      const _token = await requestUserPermission();
      if(_token != '' && isMounted) {
        setFcmToken(_token);
      }
    }
    fetchToken();
    return () => { isMounted = false };
  });

  const requestUserPermission = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      return await getFcmToken();
    }
    return '';
  }

  const getFcmToken = async () => {
    const fcmToken = await messaging().getToken();
    if (fcmToken) {
      return fcmToken;
    }

    return '';
  }

  //updated
  useEffect(() => handleComponentUpdated());
  const handleComponentUpdated = () => {
    if (_.get(props, "userData") && _.get(props, "userData.success")) {
      props.resetSuccessmessage(_.get(props, "userData"));
      if (props.isLogged) {
        if( Object.keys(props.addedCartItems).length > 0) {
          props.navigation.navigate('CheckoutScreen');
        } else {
          props.navigation.navigate("TabNavigator", { screen: "Home" });
        }
      }
    }

    if (_.get(props, "verifyOtpError.success") === false) {
      functions.displayToast(
        "error",
        "top",
        appTexts.ALERT_MESSAGES.error,
        _.get(props, "verifyOtpError.error.msg")
      );
      setLoader(false);
      props.verifyOtpReset(null);
    }
    if (_.get(props, "resendOtpData.success")) {
      setLoader(false);
      functions.displayToast(
        "success",
        "top",
        appTexts.EDITPROFILE.success,
        _.get(props, "resendOtpData.msg")
      );
      props.resendOtpReset(null);
    }
  };

  const resendOtpPress = () => {
    setLoader(true);
    let apiParam = {
      phone: props.route.params.mobileNumber,
    };
    props.resendOtp(apiParam);
  };
  const loginButtonPress = () => {
    setLoader(true);
    if (
      otp1.trim() === "" ||
      otp2.trim() === "" ||
      otp3.trim() === "" ||
      otp4.trim() === ""
    ) {
      setLoader(false);
      functions.displayToast(
        "error",
        "top",
        appTexts.ALERT_MESSAGES.error,
        appTexts.ALERT_MESSAGES.specifyOtp
      );
    } else {
      let otp = otp1 + otp2 + otp3 + otp4;
      let apiParam = {
        phone: props.route.params.mobileNumber,
        otp: otp,
        fcm_token: fcm_token,
      };
      props.doLogin(apiParam);
    }
  };
  const onBackButtonClick = () => {
    props.navigation.goBack();
  };
  return (
    <OtpView
      loginButtonPress={loginButtonPress}
      resendOtpPress={resendOtpPress}
      onBackButtonClick={onBackButtonClick}
      phoneNumber={props.route.params.mobileNumber}
      otp1={otp1}
      otp2={otp2}
      otp3={otp3}
      otp4={otp4}
      setOtp1={setOtp1}
      setOtp2={setOtp2}
      setOtp3={setOtp3}
      setOtp4={setOtp4}
      isLoading={loader}
    />
  );
};

const mapStateToProps = (state, props) => {
  return {
    userData: _.get(state, "loginReducer.userData", ""),
    resendOtp: _.get(state, "loginReducer.resendOtp", ""),
    isLogged: _.get(state, "loginReducer.isLogged", ""),
    resendOtpData: _.get(state, "loginReducer.resendOtp", ""),
    verifyOtpError: _.get(state, "loginReducer.loginerror", ""),
    addedCartItems: state.cartReducer.cartAddedData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      doLogin: loginActions.doLogin,
      resendOtp: loginActions.resendOtp,
      resendOtpReset: loginActions.resendOtpServiceActionSuccess,
      verifyOtpReset: loginActions.loginServiceActionError,
      resetSuccessmessage: loginActions.resetSuccessmessage
      // addToCart: cartActions.cartUpdate,
      // selectRestaurant: cartActions.selectRestaurant,
    },
    dispatch
  );
};

const otpScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(OtpScreen);

otpScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null,
});

export default otpScreenWithRedux;
