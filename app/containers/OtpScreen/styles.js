import { StyleSheet,I18nManager } from "react-native";
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
  } from "react-native-responsive-screen";
import globals from "../../lib/globals";
let headerButtonContainerWidth =
  globals.SCREEN_SIZE.width - globals.MARGIN.marginTen * 2;
let headerButtonWidth =
  (headerButtonContainerWidth - globals.MARGIN.marginTen * 2) / 3;
const images = {};
const styles = StyleSheet.create({
  screenMain: {
    flex:1,
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    flexDirection: "column",
  },
  screenDesignContainer: {
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    backgroundColor:'white'
  },
  loginTouch:{
    width:320,
    height:50,
    borderRadius:10,
    backgroundColor:'#612467',
    justifyContent:'center',
    alignItems:'center',
  },
  custombuttonStyle:{
    backgroundColor:globals.COLOR.purple,
     width:150,
     height: 45,
     borderRadius:15,
     justifyContent:'center',
     alignItems:'center',
   //  marginLeft:6.5

    // backgroundColor:'#cccccc'
  },
  imageContainer:{
    flex:0.4,
    //backgroundColor:'red',
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    alignItems: 'center',
    paddingTop:'10%'
    
  },
  languageContainer:{
    flex:1,
    //backgroundColor:'green',
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    //alignItems: 'center',
    paddingTop:'25%'
    
  },
  screenContainer:{
    flex:1,
    backgroundColor: globals.COLOR.transparent,
    marginBottom: globals.INTEGER.screenBottom
  },
  headerWrapper:{
   flex:.1,
   marginTop:hp("4%"),
   backgroundColor:'white',
   flexDirection:'row',
   alignItems:'center',
   //justifyContent:'center'

  },
  formWrapper:{
flex:1,
backgroundColor:'white',
alignItems:'center'
  },
  sendPhone:{
    flexDirection:'row',
    //paddingTop:'7%'
    paddingTop:hp('1.5%')
   },
   verificationCode:{
    paddingTop:hp('2.2%')
   },
   otpWrapper:{
    flexDirection: "row",
    justifyContent: "space-around",
    width: "80%",
    paddingTop:hp('3.4%'),
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }],
   
   },  
   engButton: {
    width: 300, 
    height:50,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor:globals.COLOR.purple,
    
},
resedText:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  fontSize:hp('2.1%'),
  //color:"#40475a"
  color:"gray"
},
resedtextOrange:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helveticaBold,
  fontSize:hp('2.1%'),
  //color:"#40475a"
  //color:"#ff8001",
  color:globals.COLOR.lightTextColor,
  paddingLeft:'3%',
  paddingRight:'3%'
},
  borderView:{ 
    width: "12%",
    height: 2,
    //borderRadius: 5,
    marginTop: 10,
    marginBottom: 5,
    backgroundColor: "#ff8001",
    alignItems:'center',justifyContent:'center'
  },
  textInput:{
    paddingLeft: 0,
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }],
    height: 60,
    //fontSize: 13,
    fontSize:hp('3.3%'),
    width: "17%",
    backgroundColor:"#F8F8F8",
    borderColor: "#c1c1c1",
    //borderBottomWidth: 1.7,
    textAlign: "center",
    fontFamily:globals.FONTS.helvetica,
    fontSize:16,
    color:globals.COLOR.textColor,
    //fontFamily: globals.FONTS.avenirLight,
  },
    verifyText: {
     // fontSize:hp('2.4%'),
      color:'#232020',
      fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
      fontSize:18,
     
},
verifyWrapper:{
 paddingLeft:'28%',
},
borderStyle:{
  borderBottomColor:globals.COLOR.borderColor,
  borderTopColor:globals.COLOR.borderColor,
  padding:'1%',
  borderWidth:2,

},
editText:{
  color:'#ff8001',
  paddingLeft:'6%',
  paddingRight:'6%',
  //fontSize:16,
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  fontSize:hp('2.2%'),
},
imageWrapper: {
   paddingLeft:'4%'

},
arrowImage:{
  width:20,
  height:20,
  resizeMode:'contain',
  paddingLeft:'5%',transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
  
},
digitCode:{
  fontSize:hp('2%'),
  color:globals.COLOR.lightTextColor,
  //fontFamily: globals.FONTS.avenirHeavy,
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
},
phoneNumber:{
  fontFamily: globals.FONTS.helvetica,
  fontSize:hp('2.1%'),
  color:globals.COLOR.lightTextColor,
},
verificationcodeText:{
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  fontSize:hp('2.1%'),
  color:'black',
  fontSize:15,
},
enterDigit:{
  //paddingTop:'25%'
  paddingTop:hp('20%')
},
reseondCode:{
  //paddingTop:'9%'
  paddingTop:hp('1%'),
  flexDirection:'row',
  marginTop:'6%'
},
buttonWrapper:{
  //paddingTop:'20%'
  paddingTop:hp('7%')
},
submitText:{
  color:'white',
  fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  fontSize:14,
}
});

export { images, styles };
