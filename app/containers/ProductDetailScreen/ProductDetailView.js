import React, { useState } from "react";
import PropTypes from "prop-types";
import {
  I18nManager,
  RefreshControl,
  View,
  FlatList,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  StatusBar,
} from "react-native";

import globals from "../../lib/globals";
import { styles, images } from "./styles";
import appTexts from "../../lib/appTexts";
import SliderCard from "../../components/SliderCard";
import _ from "lodash";

import DotedDivider from "../../components/DotedDivider";
import TopTab from "../../components/TopTab";
import MenuOfferCard from "../../components/MenuOfferCard";
import ScanButton from "../../components/ScanButton";
import CustomButton from "../../components/CustomButton";

import Loader from "../../components/Loader";
import AddonModal from "../../components/AddonModal";
import functions from "../../lib/functions";
import FastImageLoader from "../../components/FastImage/FastImage";
import ParallaxScrollView from "react-native-parallax-scroll-view";

const ProductDetailView = (props) => {
  const {
    tabIndex,
    onTabChange,
    onBackButtonPress,
    onBackButtonClick,
    menuDetails,
    addonDetails,
    ingredientsDetails,
    attributesDetails,
    relatedItemsDetails,
    isLoading,
    onRefresh,
    refreshing,
    redirectToScan,
    is_restaurant_selected,
    from,
    item,
    time_status,
    addToCart,
    addItemToCart,
    isAddonModalVisible,
    openAddonModal,
    addedCartItems,
    onCheckoutClick,
    onRealteditemPress
  } = props;

  const [altStickyHeaderHeight, setStickyHeaderHeight] = useState(1);
  const stickyHeaderHeight = 100;

  let addonsSelected = {};
  try {
    addonsSelected = addedCartItems[item.id].addonsSelected;
  } catch (err) {
  }

  const categories = _.get(menuDetails, "menu_category.lang", []);
  let category_item = '';
  const lang = I18nManager.isRTL ? 'ar' : 'en';
  if(categories && categories.length > 0) {
    categories.filter(category => category.language == lang);
  }
  category_item = categories?.[0]?.name;

  const renderItem = ({ item, index }) => (
    <MenuOfferCard items={item} isDetail={true} />
  );

  const renderRelatedItems = ({ item, index }) => <SliderCard item={item} onRealteditemPress={onRealteditemPress} />;

  const renderAttributes = ({ item, index }) => (
    <View style={styles.lineOne}>
      <View style={styles.left}>
        <View style={styles.bottle}>
          <Image source={images.bottle} style={styles.bottleImage} />
        </View>
        <View style={styles.imageText}>
          <Text style={styles.attributeNameText}>{item.attributes_name}</Text>
        </View>
      </View>
      <View style={styles.rightText}>
        <Text style={styles.attributeValueText}>{item.attributes_value}</Text>
      </View>
    </View>
  );

  const renderIngredients = ({ item, index }) => (
    <View style={styles.image1}>
      <View style={styles.incImage}>
        <Image source={{ uri: item.cover_photo }} style={styles.round} />
      </View>
      <View>
        <Text style={styles.ingredientsNameText}>{item.item_name}</Text>
      </View>
    </View>
  );

  return (
    <View style={styles.screenMain}>
      <StatusBar
        backgroundColor="transparent"
        barStyle="dark-content"
        // translucent={true}
      />

      <ParallaxScrollView
        refreshControl={
          <RefreshControl
            style={{ backgroundColor: "white" }}
            refreshing={refreshing}
            onRefresh={onRefresh}
          />
        }
        contentContainerStyle={styles.parallax}
        parallaxHeaderHeight={230}
        backgroundColor="#fff"
        stickyHeaderHeight={altStickyHeaderHeight}
        const
        onScroll={(event) => {
          const threshold = 200;
          if (
            event.nativeEvent.contentOffset.y <= threshold &&
            altStickyHeaderHeight > 1
          ) {
            setStickyHeaderHeight(1);
          } else if (
            event.nativeEvent.contentOffset.y > threshold &&
            altStickyHeaderHeight === 1
          ) {
            setStickyHeaderHeight(stickyHeaderHeight);
          }
        }}
        const
        renderForeground={() => (
          <View style={{ backgroundColor: "#fff" }}>
            <FastImageLoader
              resizeMode={"cover"}
              photoURL={menuDetails.cover_photo}
              style={styles.foodPic}
              loaderStyle={{ width: "50%", height: "50%" }}
            />

            <View style={styles.back}>
              <TouchableOpacity
                onPress={() => {
                  onBackButtonClick();
                }}
              >
                <Image source={images.arrow} style={styles.arrowImage} />
              </TouchableOpacity>
            </View>

          </View>
        )}
        const
        renderStickyHeader={() => (
         <TouchableOpacity onPress={() => {
          onBackButtonClick();
        }}>
            <View style={{flexDirection: 'row', width: '100%',alignItems:'center',justifyContent:'center',marginTop:30,paddingLeft:10}}>
              <View style={{width: '8%'}}>
                  {/* <TouchableOpacity
                    onPress={() => {
                      onBackButtonClick();
                    }}
                    style={{ 
                      alignSelf: 'center',
                      justifyContent: 'center',
                      width: 20,
                      height: 20,
                      transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }],
                    }}
                  > */}
                    <Image source={images.backImage} style={styles.bImg} />
                  {/* </TouchableOpacity> */}
              </View>

              <View style={[styles.line1, {width: '90%'}]}>
                <View style={styles.leb}>
                  <Text style={styles.lebText}>
                    {I18nManager.isRTL
                      ? _.get(menuDetails, ["lang", "ar", "name"], "")
                      : _.get(menuDetails, ["lang", "en", "name"], "")}
                  </Text>
                  
                </View>
                
              </View>
            </View>
            </TouchableOpacity>
        )}
      >
        {isLoading && <Loader />}
        <ScrollView
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
        >
          <View style={styles.wrapper}>
            {menuDetails.discount_price != null &&
            <View style={styles.line1}>
                <Text style={styles.lebText}>
                  {I18nManager.isRTL
                    ? _.get(menuDetails, ["lang", "ar", "name"], "")
                    : _.get(menuDetails, ["lang", "en", "name"], "")}
                </Text>
                <Text style={styles.texStrike}>{appTexts.STRING.sar} {menuDetails.price}</Text>
            </View>
            }
            <View style={styles.line2}>
              <View style={styles.pizz}>
                <View style={styles.pizza}>
                  {category_item != '' && <Text style={styles.pizText}>{category_item}{I18nManager.isRTL ? ' ' : ''}</Text> }
                </View>
                <View style={styles.cal}>
                  <Text style={styles.calText}>
                    {menuDetails.calory} {appTexts.PRODUCT.cal}
                  </Text>
                </View>
              </View>
              <View style={{top: 0}}>
                <Text style={styles.amt}>{appTexts.STRING.sar} {menuDetails.discount_price != null ? menuDetails.discount_price : menuDetails.price}</Text>
              </View>
            </View>

            <View style={styles.div}>
              <DotedDivider />
            </View>

            <View style={styles.des}>
              <Text style={styles.desc}>{appTexts.PRODUCT.des}</Text>
            </View>
            <View style={styles.txt}>
              <Text style={styles.par}>
                {I18nManager.isRTL
                  ? _.get(menuDetails, ["lang", "ar", "description"], "")
                  : _.get(menuDetails, ["lang", "en", "description"], "")}
              </Text>
            </View>
          </View>
          {/* </View> */}

          <View style={{ backgroundColor: globals.COLOR.greyBackground }}>
            <View style={styles.tabView}>
              <TopTab
                isProductDetail={true}
                tabIndex={tabIndex}
                firstTabText={appTexts.PRODUCT.addons}
                secondTabText={appTexts.PRODUCT.ingredients}
                thirdTabText={appTexts.PRODUCT.attributes}
                onTabChange={onTabChange}
              />
            </View>
            {tabIndex === 0 ? (
              <View>
                <View style={styles.addonBox}>
                  <FlatList
                    data={addonDetails}
                    extraData={addonDetails}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={renderItem}
                  />
                </View>
              </View>
            ) : null}
            {tabIndex === 1 ? (
              <View style={styles.inc}>
              <FlatList
                style={{ width: "100%", height: 100 }}
                horizontal
                showsHorizontalScrollIndicator={false}
                data={ingredientsDetails}
                extraData={ingredientsDetails}
                keyExtractor={(item, index) => index.toString()}
                renderItem={renderIngredients}
              /></View>
            ) : null}
            {tabIndex === 2 ? (
              <View style={styles.attBox}>
                <FlatList
                  data={attributesDetails}
                  extraData={attributesDetails}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={renderAttributes}
                />
              </View>
            ) : null}
            {relatedItemsDetails.length > 0&&
              <View style={styles.sliderBox}>
                <View style={styles.recent}>
                  <Text style={styles.items}>{appTexts.PRODUCT.related}</Text>
                </View>
                <View style={styles.border}></View>

                <View style={styles.bottom}>
                  <FlatList
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    style={{}}
                    data={relatedItemsDetails}
                    extraData={relatedItemsDetails}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={renderRelatedItems}
                  />
                </View>
              </View>
            }
          </View>
        </ScrollView>
      </ParallaxScrollView>

      <AddonModal isProductDetail={true}
        onCheckoutClick={() => {
          openAddonModal();
          onCheckoutClick();
        }}
        addAddon={(count, addon_item) => {
          let addonItems = Object.assign({}, addedCartItems);
          if (
            Object.keys(addonItems[item.id].addonsSelected).indexOf(
              addon_item.id.toString()
            ) == -1
          ) {
            addon_item.count = 1;
            addonItems[item.id].addonsSelected[addon_item.id] = addon_item;
            functions.displayToast(
              "success",
              "top",
              appTexts.ALERT_MESSAGES.success,
              appTexts.LOGIN_POP.addonadd
            );
          } else {
            addonItems[item.id].addonsSelected[addon_item.id].count += count;
            if (addonItems[item.id].addonsSelected[addon_item.id].count == 0) {
              delete addonItems[item.id].addonsSelected[addon_item.id];
              functions.displayToast(
                "error",
                "top",
                "Success",
                appTexts.LOGIN_POP.addonremove
              );
            } else {
              functions.displayToast(
                "success",
                "top",
                appTexts.ALERT_MESSAGES.success,
                appTexts.LOGIN_POP.addonupdate
              );
            }
          }
          addToCart(addonItems);
        }}
        addonsDetails={_.get(item, "item_addons", {})}
        isAddonModalVisible={isAddonModalVisible}
        openAddonModal={openAddonModal}
        addonsSelected={addonsSelected}
        addedCartItems={addedCartItems}
        onViewClick={null}
        isCart={true}
      />

      {is_restaurant_selected === false && (
        <ScanButton redirectToScan={redirectToScan} />
      )}

      {is_restaurant_selected === true &&
        <TouchableOpacity onPress={() => addItemToCart(item, from, time_status) }style={styles.buttonView} >
                <CustomButton buttonstyle={[styles.custombuttonStyle,styles.loginTouch]} buttonText={appTexts.CART.add} />
        </TouchableOpacity>
      }
    </View>
  );
};

ProductDetailView.propTypes = {
  tabIndex: PropTypes.number,
  onTabChange: PropTypes.func,
  onBackButtonClick: PropTypes.func,
  onRefresh: PropTypes.func,
};

export default ProductDetailView;
