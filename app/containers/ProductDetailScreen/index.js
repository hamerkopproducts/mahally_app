import React, { useEffect, useState } from 'react';
import { I18nManager, BackHandler } from 'react-native'
import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import ProductDetailView from './ProductDetailView';
import { bindActionCreators } from "redux";

import functions from "../../lib/functions";
import appTexts from "../../lib/appTexts";
import _ from "lodash";
import * as offersActions from "../../actions/offersActions";
import * as cartActions from "../../actions/cartActions";

const ProductDetailScreen = (props) => {

  //Initialising states
  const [tabIndex, setTabIndex] = useState(0);
  const [isPopupVisible, setIsPopupVisible] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [isAddonModalVisible, setIsAddonModalVisible] = useState(false);

  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
          props.clearMenuDetails();
          props.menuDetails(_.get(props, 'route.params.itemId'));
        } else {
          functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
        }
      });
      BackHandler.addEventListener('hardwareBackPress', backPressed = () => {
        props.navigation.goBack(null);
        return true;
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);
  const handleComponentMounted = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        props.menuDetails(_.get(props, 'route.params.itemId'))
      } else {
        functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
      }
    });
    BackHandler.addEventListener('hardwareBackPress', backPressed = () => {
      props.navigation.goBack(null);
      return true;
    });
  }

  useEffect(() => { return () => { handleComponentUnmount(); } }, []);
  const handleComponentUnmount = () => {
    // props.menuDetails(null);
    if(props.pullRefresh === true)
    {
      setRefreshing(false);
    }
  };

  //updated
  useEffect(() => handleComponentUpdated());
  const handleComponentUpdated = () => {
    if(props.pullRefresh === true)
    {
      setRefreshing(false);
    }
  };

  const onRightButtonPress = () =>{
    props.navigation.navigate('NotificationScreen')
  };

  const onTabChange = (index) => {
    if (tabIndex !== index) {
      setTabIndex(index);
    }
  };
  
  const popupNoClick = () => {
    setIsPopupVisible(false);
  };
  const popupYesClick = () => {
    setIsPopupVisible(false);
  };
  const listButtonClick = (itemData) => {
    setIsPopupVisible(true);
  };
  const onProfileButtonPress = () => {
    props.navigation.navigate("ProfileScreen")
  };
  const onBackButtonClick = () =>{
    props.navigation.goBack();
  };
  const onBackButtonPress = () =>{
    props.navigation.goBack();
    props.clearMenuDetails()
  };

  const homeCardButtonPress = () => {
    props.navigation.navigate('RestaurantDetailScreen');
  };

  const openAddonModal = () => {
    setIsAddonModalVisible(!isAddonModalVisible);
  };

  const onRefresh = async() => {
    setRefreshing(true);
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        props.menuDetails(_.get(props, 'route.params.itemId'))
      } else {
        functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
      }
    });
  }

  const addItemToCart = async(item, from, time_status) => {
    let restaurant_id = item.restuarants_id;
    if( ! restaurant_id) {
      restaurant_id = item.restaurant_id;
    }
    const selectedRestaurant = props.selectedRestaurant;
    let _item_id = '';
    if(from == 'loyality' || from == 'offer') {
      _item_id = item.item_id;
    } else {
      _item_id = item.id;
    }

    if(time_status === 'CLOSED') {
      functions.displayToast('error', 'top', appTexts.REST.Heading_closed , appTexts.REST.closed);
      return false;
    }

    if(typeof selectedRestaurant.restaurant_id != 'undefined' && selectedRestaurant.restaurant_id != restaurant_id) {
      await functions.displayAlertWithCallBack(
        appTexts.REST.already_in_cart, 
        appTexts.REST.cart_has_items, 
        (data) => {
          if(data) {
            props.addToCart({});
            setTimeout(() => {
              redirectToScan(from, _item_id, restaurant_id);
              add_item_to_cart(item, from, time_status, true);
            }, 400);
          }
        }
      );
    } 
    else if(typeof selectedRestaurant.restaurant_id == 'undefined') {
      await functions.displayAlertWithCallBack(
        appTexts.SCAN.scan,
        appTexts.SCAN.scanText,
        (data) => {
          if(data) {
            redirectToScan(from, _item_id, restaurant_id);
            add_item_to_cart(item, from, time_status, true);
          }
        }
      );
    } 
    else {
      add_item_to_cart(item, from, time_status, false);
    }
  }

  const add_item_to_cart = (item, from, time_status, clear_cart) => {
    let addedCartItems = clear_cart ? {} : props.addedCartItems;

    let addedItems = typeof addedCartItems == 'undefined' ? {} : Object.assign({}, addedCartItems);
    if(from == 'loyality') {
      const availableFreeMeals = item.no_offer_available;
      if(availableFreeMeals <= 0) {
        functions.displayToast('error', 'top', 'Not available', 'This offer currently not available for you');
        return false;
      }
      if(Object.keys(addedItems).indexOf('loyalty_' + item.item_id.toString()) == -1) {
        let itemToAdd = Object.assign({}, item);
        itemToAdd.addonsSelected = {};
        itemToAdd.count = 1;
        itemToAdd.id = 'loyalty_' + item.item_id;
        itemToAdd.is_loyalty = true;
        itemToAdd.price = 0;
        itemToAdd.available_free = availableFreeMeals;
        itemToAdd.discount_price = 0;
        addedItems['loyalty_' + item.item_id] = itemToAdd;
        setTimeout(() => {
          functions.displayToast('success', 'top', appTexts.ALERT_MESSAGES.success, appTexts.ORDER.itemadded);
        }, 400);
      } else {
        let all_items = Object.assign({}, addedItems);
        if(availableFreeMeals <= all_items['loyalty_' + item.item_id].count) {
          functions.displayToast('error', 'top', 'Error', appTexts.STRING.addedmaximumoffer);
          return false;
        }
        all_items['loyalty_' + item.item_id].count += 1;
        all_items['loyalty_' + item.item_id].id = 'loyalty_' + item.item_id;
        addedItems = all_items;
        setTimeout(() => {
          functions.displayToast('success', 'top', appTexts.ALERT_MESSAGES.success, appTexts.ORDER.itemupdated);
        }, 400);
      }
      props.addToCart(addedItems);
    } else if(from == 'offer') {
      if(Object.keys(addedItems).indexOf( 'offer_' + item.item_id.toString()) == -1) {
        let itemToAdd = Object.assign({}, item);
        itemToAdd.addonsSelected = {};
        itemToAdd.count = 1;
        itemToAdd.id = 'offer_' + item.item_id;
        itemToAdd.is_offer = true;
        let offer_price = item.discount_price != null ? item.discount_price : item.price;
        if(item.discount_type == 'Value') {
          offer_price = offer_price - item.discount_value;
        } else {
          offer_price = offer_price - (offer_price/100)*item.discount_value;
        }
        itemToAdd.price = item.price;
        itemToAdd.discount_price = offer_price;
        addedItems['offer_' + item.item_id] = itemToAdd;
        setTimeout(() => {
          functions.displayToast('success', 'top', appTexts.ALERT_MESSAGES.success, appTexts.ORDER.itemadded);
        }, 400);
      } else {
        let all_items = Object.assign({}, addedItems);
        all_items['offer_' + item.item_id].count += 1;
        all_items['offer_' + item.item_id].id = 'offer_' + item.item_id;
        addedItems = all_items;
        setTimeout(() => {
          functions.displayToast('success', 'top', appTexts.ALERT_MESSAGES.success, appTexts.ORDER.itemupdated);
        }, 400);
      }
      props.addToCart(addedItems);
    } else {
      if(Object.keys(addedItems).indexOf(item.id.toString()) == -1) {
        let itemToAdd = Object.assign({}, item);
        itemToAdd.addonsSelected = {};
        itemToAdd.count = 1;
        addedItems[item.id] = itemToAdd;
        setTimeout(() => {
          functions.displayToast('success', 'top', appTexts.ALERT_MESSAGES.success, appTexts.ORDER.itemadded);
        }, 400);
      } else {
        let all_items = Object.assign({}, addedItems);
        all_items[item.id].count += 1;
        addedItems = all_items;
        setTimeout(() => {
          functions.displayToast('success', 'top', appTexts.ALERT_MESSAGES.success, appTexts.ORDER.itemupdated);
        }, 400);
      }
      props.addToCart(addedItems);
      const hasAddons = props.addOnsDetails.length > 0;
      if(hasAddons) {
        openAddonModal();
      }
    }
  }

  const onRealteditemPress = (id) => {
    props.navigation.setParams({from: 'menu'});
    props.clearMenuDetails();
    props.menuDetails(id);
  }

  const onCheckoutClick = () => {
    props.navigation.navigate("CheckoutScreen");
  };

  const redirectToScan = (type, item_id, restaurant_id) => {
    // props.postScanItemToAdd({
    //   type: type,
    //   item_id: item_id
    // });
    // props.navigation.navigate("Scan", { screen: "ScanScreen", params:{ from: '_restuarant'} });

    const vat_includes = props?.route?.params?.vat_includes || 'no';
    const vat_percentage = props?.route?.params?.vat_percentage || '';
    const hasPreOrder = props?.route?.params?.pre_order || 'disable';
    let _scanned_data = {
      restaurant_id: restaurant_id,
      table_id: '',
      color_code: '',
      dine_in: 'take',
      vat: vat_includes == 'yes' ? (vat_percentage || 0) : '',
      has_preOrder: hasPreOrder == 'enable'
    };
    props.selectRestaurant(_scanned_data);
  }

  const from =  _.get(props, 'route.params.from', {});
  const item =  _.get(props, 'route.params.item', {});
  const time_status =  _.get(props, 'route.params.time_status', {});

  return (
    <ProductDetailView
      from={from}
      item={item}
      time_status={time_status}
      onCheckoutClick={onCheckoutClick}
      addToCart={props.addToCart}
      addItemToCart={addItemToCart}
      homeCardButtonPress={homeCardButtonPress}
      onProfileButtonPress={onProfileButtonPress}
      listButtonClick={listButtonClick}
      tabIndex={tabIndex}
      onTabChange={onTabChange}
      isPopupVisible={isPopupVisible}
      popupNoClick={popupNoClick}
      popupYesClick={popupYesClick}
      onRightButtonPress={onRightButtonPress}
      onBackButtonClick={onBackButtonClick}
      onBackButtonPress={onBackButtonPress}

      refreshing={refreshing}
      onRefresh={onRefresh}
      menuDetails = {props.menuDataDetails}
      addonDetails = {props.addOnsDetails}
      ingredientsDetails = {props.ingredientsDetails}
      attributesDetails = {props.attributesDetails}
      relatedItemsDetails = {props.relatedItemsDetails}
      isLoading = {props.isLoading}
      isAddonModalVisible={isAddonModalVisible}
      openAddonModal={openAddonModal}
      addedCartItems={props.addedCartItems}
      redirectToScan={async() => {
        let restaurant_id = props.selectedRestaurant.restaurant_id;
        if(typeof restaurant_id != 'undefined' && restaurant_id != '') {
          await functions.displayAlertWithCallBack(
            appTexts.CART_CHANGES.already_scanned,
            appTexts.CART_CHANGES.change_restaurant,
            (data) => {
              if(data) {
                props.navigation.navigate("Scan", { screen: "ScanScreen", params:{ from: '_restuarant', id: restaurant_id} });
              }
            },
            appTexts.CART_CHANGES.scan_new,
            appTexts.CART_CHANGES.exit
          );
        } else {
          props.navigation.navigate("Scan", { screen: "ScanScreen", params:{ from: '_restuarant'} });
        }
      }}
      is_restaurant_selected={ typeof props.selectedRestaurant.restaurant_id != 'undefined' && props.selectedRestaurant.restaurant_id != '' }
      onRealteditemPress={onRealteditemPress}
    />
  );

};


const mapStateToProps = (state, props) => {
  return {
    menuDataDetails: _.get(state, 'offersReducer.menuDetailsData', ''),
    addOnsDetails: _.get(state, 'offersReducer.addOnsData', ''),
    ingredientsDetails: _.get(state, 'offersReducer.ingredientsData', 'helo'),
    attributesDetails: _.get(state, 'offersReducer.attributesData', 'helo'),
    relatedItemsDetails: _.get(state, 'offersReducer.relatedItemsData', 'helo'),
    isLoading: _.get(state, 'offersReducer.isLoading', ''),
    pullRefresh: _.get(state, 'offersReducer.pullRefresh', ''),
    selectedRestaurant: state.cartReducer.selectedRestaurant,
    addedCartItems: state.cartReducer.cartAddedData
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    menuDetails: offersActions.getMenuDetailsData,
    clearMenuDetails: offersActions.clearMenuDetails,
    addToCart: cartActions.cartUpdate,
    postScanItemToAdd: cartActions.postScanItemToAdd,
    selectRestaurant: cartActions.selectRestaurant,
  }, dispatch)
};

const homeScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductDetailScreen);

homeScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default homeScreenWithRedux;
