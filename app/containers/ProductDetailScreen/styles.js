import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"


const images = {
 pic:require('../../assets/images/attributes/Burger.png'),
 arrow:require('../../assets/images/attributes/back.png'),
 img1:require('../../assets/images/attributes/Img.png'),
 img2:require('../../assets/images/attributes/image1.png'),
 img3:require('../../assets/images/attributes/image2.png'),
 img4:require('../../assets/images/attributes/image3.png'),
 bottle:require('../../assets/images/attributes/bottle.png'),
 pre:require('../../assets/images/attributes/pre.png'),
 cook:require('../../assets/images/attributes/cook.png'),
 scan:require('../../assets/images/footerTabItem/scan.png'),
 bkarrow: require("../../assets/images/chooseLanguage/backarow.png"),
 backImage: require("../../assets/images/chooseLanguage/backarow.png"),
};

const styles = StyleSheet.create({
  screenMain:{
      flex:1,
      flexDirection:'column',
      backgroundColor: globals.COLOR.greyBackground,
  },
  custombuttonStyle:{
    backgroundColor:globals.COLOR.purple,
     width:150,
     height: 45,
     borderRadius:15,
     justifyContent:'center',
     alignItems:'center',
   //  marginLeft:6.5

    // backgroundColor:'#cccccc'
  },
  screenContainer:{
    marginTop: 5,
    width: globals.INTEGER.screenWidthWithMargin,
    //width: globals.INTEGER.screenWidthWithMargin,
 //   height: globals.INTEGER.screenContentHeight - 5 + globals.INTEGER.footerTabBarHeight,
  //  height: globals.INTEGER.screenContentHeight,
    marginLeft: globals.INTEGER.leftRightMargin,
    backgroundColor: globals.COLOR.white,
    
  },
  foodImage:{
      height:'20'
  },
  attributeNameText:{
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  attributeValueText:{
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
  },
  ingredientsNameText:{
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }] 
  },
  back:{
      width:40,
      height:40,
      borderRadius:4,
      backgroundColor:'rgba(52,52,52,0.8)',
      position:'absolute',
      top:40,
      left:16,
      alignItems:'center',
      justifyContent:'center'
  },
  backSticky:{
    width:40,
    height:40,
    borderRadius:4,
    backgroundColor:'rgba(52,52,52,0.8)',
    // left:16,
    alignItems:'center',
    justifyContent:'center'
},
bacImage:{
  resizeMode:'contain',
  width:20,
  height:20,
  tintColor:'white',
  transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }],
},
  formWrapper:{
    // flex:1,
    // flexDirection:'row'
  },
  flatListStyle:{
    flex:1,
    // flexDirection:"row"
  },
  arrowImage:{
      width:20,
      height:20,
  },
  line1:{
      flexDirection:'row',
      justifyContent:'space-between',
      //marginTop:'5%',
  },
  texStrike:{
      textDecorationLine:'line-through',
      fontSize:12,
      fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
      color:globals.COLOR.greyText,
  },
  bImg:{
    alignSelf: 'center',
                      justifyContent: 'center',
                      width: 20,
                      height: 20,
                      transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }],
  },
  seconView:{
      backgroundColor:'white',
      paddingBottom:'5%',
  },
  wrapper:{
    backgroundColor:'white',
     paddingLeft:'5%',
     paddingRight:'5%',
     paddingBottom:'5%',
     paddingTop:'3%'
  },
  pizz:{
      flexDirection:'row',
      //marginTop: 5
  },
  line2:{
      flexDirection:'row',
      justifyContent:'space-between',
      alignItems:'center'
  },
  lebText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.helveticaBold,
    fontSize: I18nManager.isRTL ?14:16,
    color: globals.COLOR.Text,
   // marginBottom: 5
    
  },
  pizText:{
    fontSize:12,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.purple,
  },
  calText:{
    fontSize:12,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.greyText,
  },
  loginTouch:{
    width:320,
    height:50,
    borderRadius:10,
    backgroundColor:'#612467',
    justifyContent:'center',
    alignItems:'center',
  },
  leb:{
    //marginTop:8
  },
  amt:{
    fontSize:I18nManager.isRTL ?14:18,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helveticaBold,
    color:globals.COLOR.greyText,
  },
  cal:{
      marginLeft:'8%',
  },
  div:{
      marginVertical:'2%',
  },
  txt:{
      marginTop:'2%',
  },
  desc:{
    fontSize:16,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.Text,
    textAlign:'left',
    
  },
  par:{
    fontSize:12,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.greyText,
    //lineHeight:16,
    textAlign:'left',
    lineHeight:20
  },
  items:{
    fontSize:14,
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    color:globals.COLOR.Text,
    textAlign:'left'
  },
  tabView:{
      alignItems:'center',
      justifyContent:'center',
      //height:100,
  },

  foodPic:{
    width:'100%',
    height:250,
  },
  round:{
    borderRadius: 30,
    width:60,
    height:60,
    
  },
  addonBox:{
   borderRadius:30,
    width:'100%',
    backgroundColor:'white',
    paddingTop:'5%',
    paddingLeft:'5%',
    paddingRight:'5%',
    marginBottom:'5%',
    paddingBottom:'5%',
    marginTop: 12
  },
  button:{
   // position:'absolute',
    //marginLeft:'5%',
    //top:580,
    top:520,
    zIndex:10,
    alignItems:'center',
    justifyContent:'center',
    width: '100%',
    height: 62,
    alignItems: 'center',
    justifyContent: 'center',
    shadowOpacity: 0.1,
    shadowOffset: { x: 2, y: 0 },
    shadowRadius: 2,
    borderRadius: 30,
    position: 'absolute',
       shadowOpacity: 5.0,

  },
  button:{
  width:50,
  height:100,
  borderRadius:15,
  },
  buttonView:{
    position:'absolute',
  //  marginLeft:'5%',
   bottom: 30,
   // zIndex:10,
    alignSelf:'center',
  },
  attBox:{
    borderRadius:30,
    width:'100%',
    backgroundColor:'white',
    paddingTop:'5%',
    paddingLeft:'5%',
    paddingRight:'5%',
    marginBottom:'5%'
  },
  sliderBox:{
    borderRadius:30,
    width:'100%',
    backgroundColor:'white',
    paddingTop:'5%',
    paddingLeft:'5%',
   // paddingRight:'5%',
    marginBottom:'10%'
  },
  border:{
    borderWidth:2,
    borderRadius:5,
    borderColor:globals.COLOR.Text,
    width:30,
    marginBottom:'5%',
  },
  ingBox:{
    borderRadius:30,
   // borderTopRightRadius:30,
    // width:'100%',
    // backgroundColor:'white',
   marginBottom:'3%',
  },
  imageLine:{
    flexDirection:'row',
    justifyContent:'space-around',
    marginHorizontal:'5%',
    marginVertical:'5%'
   // marginBottom:'10%',
    
  },
  image1:{
    alignItems:'center',
    marginLeft: 10,
    marginRight: 10,
    
   
  },
  inc:{
    backgroundColor:'white',
    paddingTop:20,
    marginBottom:20,
    borderRadius:30,
    alignItems:'flex-start',
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }] 

  },
  incImage:{
   
  },
  textBorder:{
    marginTop:'8%',
    
  },
  lineOne:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
    marginVertical:'2%'
  },
  left:{
    flexDirection:'row',
    alignItems:'center',
    
  },
  bottle:{
    marginRight:10,
  },
  bottleImage:{
    width:40,
    height:40,
  },
  recent:{
    marginTop:'2%',
  },
  scrollViewStyle:{
    width: '100%',
    height: '100%',
    position: 'absolute',
    //marginVertical:'2%'
    //left:'5%'
    marginBottom:'2%'
  },
  bottom:{
  
   alignItems:"flex-start",
    justifyContent: 'space-between',
    paddingBottom:30
    
  },
  imageScan:{
    width:60,
    height:60,
    resizeMode:'contain',
  },
  but:{
    position:'absolute',
    top:'5%',
    alignSelf:'center',
    zIndex:30,
  },
  arrowImage:{
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }],
    width:20,
    height:20,
  },
  arr:{
    paddingLeft:'5%',
    alignSelf:'center',
  },
  bkarrowImage:{
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }],
    
    
  },
 
  

  
});

export { images, styles };
