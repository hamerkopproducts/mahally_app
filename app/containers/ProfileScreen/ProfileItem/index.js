import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, TouchableOpacity, Image, Text } from "react-native";
import { styles } from "./styles";
import appTexts from "../../../lib/appTexts";
import DividerLine from "../../../components/DividerLine";

const ProfileItem = (props) => {
  const { itemText, itemImage, onItemClick, arrowImage, isDivideRequired } = props;

  return (
    <View style={styles.itemConatiner}>
      <TouchableOpacity
        style={styles.listWrapper}
        onPress={() => {
          onItemClick();
        }}
      >
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <View style={styles.imagemainWrapper}>
            <Image source={itemImage} style={Platform.OS == "ios" ? styles.iosImage : styles.imageMain} />
          </View>
          <View style={styles.contentsWrapper}>
            <Text style={styles.contentHeading}>{itemText}</Text>
          </View>
        </View>
        <View style={styles.arrowWrapper}>
          <Image source={arrowImage} style={styles.arrowImage} />
        </View>
      </TouchableOpacity>
      {isDivideRequired &&
        <View style={styles.divide}>
          <DividerLine dividerStyle={styles.divider}/>
        </View>}
    </View>
  );
};
ProfileItem.propTypes = {
  itemText: PropTypes.string,
  itemImage: PropTypes.number,
  onItemClick: PropTypes.func,
  arrowImage: PropTypes.number,
  isDivideRequired: PropTypes.bool,
};

export default ProfileItem;
