import { StyleSheet, I18nManager } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import globals from "../../../lib/globals";

const styles = StyleSheet.create({
  itemConatiner: {
    width: '100%',
    //height: 60,
    //backgroundColor:'red'
  },
  listWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    //paddingTop:hp('3%'),
    // paddingBottom:hp('5%'),
    paddingLeft: '3%'
  },
  divider:{
    width:'100%',
    height: 0.5,
   backgroundColor: globals.COLOR.borderColor,
  },
  imageMain: {
    resizeMode: "contain", width: 25, height: 25,
  },
  iosImage:{
    resizeMode: "contain", width: 20, height: 20,
  },
  contentHeading: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 15,
    paddingLeft: '5%',
    paddingRight: '4%'
    //color:'#707070'
  },
  arrowWrapper: {
    flexDirection: 'row',
    paddingRight: '4.5%'
  },
  divide: {
    marginTop: '5%',
    marginBottom: '5%',
    // width:'100%'
  },
  arrowImage: {
    resizeMode: "contain", width: 16,
    height: 16, transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
  },


});

export { styles };
