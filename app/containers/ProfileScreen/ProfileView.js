import React, { useState } from "react";
import PropTypes from "prop-types";
import { View, Text, TouchableOpacity, Image, ScrollView } from "react-native";
import globals from "../../lib/globals";
import { styles, images } from "./styles";
import appTexts from "../../lib/appTexts";
import ProfileItem from "./ProfileItem";

import Header from "../../components/Header";
import HelpModal from "../../components/HelpModal";
import LogoutModal from "../../components/LogoutModal";
import EditProfileModal from "../../components/EditProfileModal";
import _ from "lodash";
import FastImageLoader from "../../components/FastImage/FastImage";
import Loader from "../../components/Loader";

const ProfileView = (props) => {
  const {
    logoutButtonPress,
    faqPress,
    aboutPress,
    loyaltyPress,
    privacyPolicyPress,
    termsAndConditionPress,
    toggleHelpModal,
    toggleLogoutModal,
    toggleModal,
    openEditModal,
    gotoLogin,
    isModalVisible,
    isHelpModalVisible,
    openHelpModal,
    isLogoutModalVisible,
    openLogoutModal,
    onBackButtonClick,
    onRightButtonPress,
    userdetails,
    isLoading,
    subject,
    message,
    setSubject,
    setMessage,
    onChatPress,
    token,
    is_notification_active,
    languageSwitchScreen,
    notification_badge,
    guestisLogged,
  } = props;

  const pro_pic = _.get(userdetails, "data.photo", null);
  return (
    <View style={styles.screenMain}>
      <Header
        notification_badge={notification_badge}
        navigation={props.navigation}
        iscenterLogoRequired={false}
        isRightButtonRequired={true}
        onRightButtonPress={onRightButtonPress}
        isLanguageButtonRequired={true}
        onBackButtonClick={onBackButtonClick}
        headerTitle={appTexts.PROFILELISTING.appTitle}
        customHeaderStyle={{
          alignItems: "center",
          height: globals.INTEGER.headerHeight,
          backgroundColor: globals.COLOR.headerColor,
        }}
        is_notification_active={is_notification_active}
        screen={"profile"}
        languageSwitchScreen={languageSwitchScreen}
      />
      <ScrollView>
        <View style={styles.screenMains}>
          {isLoading && <Loader />}
          {token === null ? (
            <View style={styles.profileContainer}>
              <View style={styles.roundImage}>
                {pro_pic == null && (
                  <Image source={images.person} style={styles.logo} />
                )}
              </View>
              <View style={styles.infoContainer}>
                <TouchableOpacity
                  onPress={() => {
                    token === null ? gotoLogin() : openEditModal();
                  }}
                  style={styles.butt}
                >
                  <View style={styles.joinoval}>
                    <Text style={styles.join}>{appTexts.PROFILE.join}</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          ) : (
            <View style={styles.profileContainer}>
              <View style={styles.roundImage}>
                {pro_pic == null && (
                  <Image source={images.person} style={styles.logo} />
                )}
                {pro_pic != null && (
                  <FastImageLoader
                    resizeMode={"cover"}
                    photoURL={pro_pic}
                    style={styles.logo}
                    loaderStyle={{}}
                  />
                )}
              </View>
              <View style={styles.infoContainer}>
                {_.get(userdetails, "data.name", null) ? (
                  <View style={styles.name}>
                    <Text style={styles.nameText}>
                      {_.get(userdetails, "data.name", "")}
                    </Text>
                  </View>
                ) : null}
                <View style={styles.phoneDetails}>
                  <View>
                    <Image source={images.phone} style={styles.phoneImage} />
                  </View>
                  <View style={styles.number}>
                    <Text style={styles.numberStyle}>
                      {_.get(userdetails, "data.phone_number", "")}
                    </Text>
                  </View>
                </View>
                {_.get(userdetails, "data.email", null) ? (
                  <View style={styles.phoneDetails2}>
                    <View style={styles.phoneImage}>
                      <Image source={images.mail} style={styles.phoneImage} />
                    </View>
                    <View style={styles.number2}>
                      <Text style={styles.numberStyle}>
                        {_.get(userdetails, "data.email", "")}
                      </Text>
                    </View>
                  </View>
                ) : null}
                <TouchableOpacity
                  onPress={() => {
                    token === null ? gotoLogin() : openEditModal();
                  }}
                  style={styles.butt}
                >
                  <View style={styles.oval}>
                    <View style={styles.editBox}>
                      <View style={styles.pencilView}>
                        <Image source={images.pencil} style={styles.pencil} />
                      </View>
                      <View style={styles.editTextView}>
                        <Text style={styles.editText}>
                          {appTexts.PROFILE.editDetails}
                        </Text>
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          )}
          <View style={styles.profileListContainer}>
            <View style={styles.mainList}>
              <ProfileItem
                itemText={appTexts.PROFILELISTING.Free}
                itemImage={images.free}
                arrowImage={images.arrowIcon}
                onItemClick={loyaltyPress}
                isDivideRequired={true}
              />
              <ProfileItem
                itemText={appTexts.PROFILELISTING.About}
                itemImage={images.about}
                arrowImage={images.arrowIcon}
                onItemClick={aboutPress}
                isDivideRequired={true}
              />
              <ProfileItem
                itemText={appTexts.PROFILELISTING.Faq}
                itemImage={images.faqIcon}
                arrowImage={images.arrowIcon}
                onItemClick={faqPress}
                isDivideRequired={true}
              />
              <ProfileItem
                itemText={appTexts.PROFILELISTING.Privacy}
                itemImage={images.privacyIcon}
                arrowImage={images.arrowIcon}
                onItemClick={privacyPolicyPress}
                isDivideRequired={true}
              />
              <ProfileItem
                itemText={appTexts.PROFILELISTING.Terms}
                itemImage={images.termsIcon}
                arrowImage={images.arrowIcon}
                onItemClick={termsAndConditionPress}
                isDivideRequired={guestisLogged === false}
              />
              {guestisLogged === false && (
                <ProfileItem
                  itemText={appTexts.PROFILELISTING.Support}
                  itemImage={images.supportIcon}
                  arrowImage={images.arrowIcon}
                  onItemClick={() => {
                    token === null ? gotoLogin() : openHelpModal();
                  }}
                />
              )}
            </View>
          </View>
          {guestisLogged === false && (
            <View style={styles.logoutContainer}>
              <ProfileItem
                itemText={appTexts.PROFILELISTING.Logout}
                itemImage={images.logoutIon}
                onItemClick={() => {
                  openLogoutModal();
                }}
              />
            </View>
          )}
        </View>
      </ScrollView>

      {isModalVisible && (
        <EditProfileModal
          isModalVisible={isModalVisible}
          toggleModal={toggleModal}
        />
      )}

      {isHelpModalVisible && (
        <HelpModal
          onChatPress={onChatPress}
          subject={subject}
          message={message}
          setSubject={setSubject}
          setMessage={setMessage}
          toggleHelpModal={toggleHelpModal}
          isHelpModalVisible={isHelpModalVisible}
        />
      )}

      {isLogoutModalVisible && (
        <LogoutModal
          isLogoutModalVisible={isLogoutModalVisible}
          logoutButtonPress={logoutButtonPress}
          toggleLogoutModal={toggleLogoutModal}
        />
      )}
    </View>
  );
};

ProfileView.propTypes = {
  isModalVisible: PropTypes.bool,
  isHelpModalVisible: PropTypes.bool,
  logoutButtonPress: PropTypes.func,
  privacyPolicyPress: PropTypes.func,
  termsAndConditionPress: PropTypes.func,
  toggleHelpModal: PropTypes.func,
  toggleLogoutModal: PropTypes.func,
  faqPress: PropTypes.func,
  aboutPress: PropTypes.func,
  loyaltyPress: PropTypes.func,
  toggleModal: PropTypes.func,
  selectLanguage: PropTypes.func,
  goBackButton: PropTypes.func,
  setSubject: PropTypes.func,
  setMessage: PropTypes.func,
  onChatPress: PropTypes.func,
  gotoLogin: PropTypes.func,
};

export default ProfileView;
