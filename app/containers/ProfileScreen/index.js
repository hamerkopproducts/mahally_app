import React, { useEffect, useState } from 'react';
import { I18nManager, BackHandler } from 'react-native'

import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import ProfileView from './ProfileView';
import { bindActionCreators } from "redux";

import * as loginActions from "../../actions/loginActions";
import * as editUserActions from "../../actions/editUserActions";
import appTexts from "../../lib/appTexts";
import functions from "../../lib/functions"
import RNRestart from 'react-native-restart';

import _ from "lodash"
import * as supportActions from "../../actions/supportActions";
import * as cartActions from "../../actions/cartActions";
import * as homeActions from "../../actions/homeActions";

const ProfileScreen = (props) => {

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isHelpModalVisible, setIsHelpModalVisible] = useState(false);
  const [isLogoutModalVisible, setIsLogoutModalVisible] = useState(false);
  const [isContactModalVisible, setIsContactModalVisible] = useState(false);
  const [isLanguageModalVisible, setIsLanguageModalVisible] = useState(false);

  useEffect(() => {
    let isMounted = true;
    const focusHandle = props.navigation.addListener("focus", () => {
      if (props.guestisLogged === false) {
        props.NotificationBadgeReset(props.token)
        props.getNotificationCount(props.token)
        NetInfo.fetch().then((state) => {
          if (state.isConnected) {
            props.editUserData(props.token);
            if (props.token != '' && isMounted) { props.editUserData(props.token); }
          } else {
            functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
          }
        });
      }
      BackHandler.addEventListener('hardwareBackPress', backPressed = () => {
        BackHandler.exitApp();
        return true;
      });
    });

    return () => { isMounted = false; focusHandle(); };
  }, [props.navigation, props.token, props.guestisLogged]);

  useEffect(() => handleComponentMounted(), []);
  const handleComponentMounted = () => {
    if (props.guestisLogged === false) {
      props.NotificationBadgeReset(props.token)
      props.getNotificationCount(props.token)
      NetInfo.fetch().then(state => {
        let isMounted = true;
        if (state.isConnected) {
          if (props.token != '' && isMounted) { props.editUserData(props.token); }
        } else {
          functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
        }
        return () => { isMounted = false };
      });
    }
    BackHandler.addEventListener('hardwareBackPress', backPressed = () => {
      BackHandler.exitApp();
      return true;
    });
  }

  useEffect(() => { return () => { handleComponentUnmount(); } }, []);
  const handleComponentUnmount = () => {
  };

  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
    NetInfo.fetch().then(state => {
      if (state.isConnected) {
        if (props.loggedoutData.success === true && props.guestisLogged === false) {
          props.resetLogoutData()
          props.navigation.navigate('LoginStackNavigator', { screen: 'LoginScreen' });
        }
      } else {
        functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
      }
    });

    if(props.apiError?.error?.msg && props.apiError?.error?.msg.indexOf('Unauthenticated') !== -1) {
      props.LogoutClearData();
      props.navigation.navigate('LoginStackNavigator', { screen: 'LoginScreen' });
    }

  };

  const onBackButtonClick = () => {
    props.navigation.goBack();
  };

  const onRightButtonPress = () => {
    props.navigation.navigate('NotificationScreen');
  };

  const logoutButtonPress = () => {
    if (props.token === null) {
      props.navigation.navigate('LoginStackNavigator', { screen: 'LoginScreen' });
      toggleLogoutModal();
    }
    else {
      props.addToCart({});
      props.selectRestaurant({});
      props.logOutPressed(props.token)
      toggleLogoutModal();
    }
  };

  const openLogoutModal = () => {
    setIsLogoutModalVisible(true)
  };

  const privacyPolicyPress = () => {
    props.navigation.navigate('PrivacyScreen');
  };

  const faqPress = () => {
    props.navigation.navigate('FaqScreen');
  };

  const aboutPress = () => {
    props.navigation.navigate('AboutScreen')
  }

  const loyaltyPress = () => {
    props.navigation.navigate('LoyaltyScreen')
  }

  const termsAndConditionPress = () => {
    props.navigation.navigate('TermsScreen');
  };

  const openHelpModal = () => {
    setIsHelpModalVisible(true)
  };

  const toggleHelpModal = () => {
    setIsHelpModalVisible(!isHelpModalVisible)
  };

  const toggleLogoutModal = () => {
    setIsLogoutModalVisible(!isLogoutModalVisible)
  };

  const openEditModal = () => {
    setIsModalVisible(true)
  };

  const toggleModal = () => {
    setIsModalVisible(false)
  };

  const openContactModal = () => {
    setIsContactModalVisible(true)
  };

  const toggleContactModal = () => {
    setIsContactModalVisible(false)
  };

  const openLanguageModal = () => {
    setIsLanguageModalVisible(true)
  };

  const toggleLanguageModal = () => {
    setIsLanguageModalVisible(false)
  };

  const gotoLogin = () => {
    props.navigation.navigate('LoginStackNavigator', { screen: 'LoginScreen' });
  };

  const selectLanguage = (selectedLanguage) => {
    if (selectedLanguage === 'EN') {
      I18nManager.forceRTL(false);
      setTimeout(() => {
        RNRestart.Restart();
      }, 500);
    } else if (selectedLanguage === 'AR') {
      I18nManager.forceRTL(true);
      setTimeout(() => {
        RNRestart.Restart();
        props.saveSelectedLanguage(selectedLanguage);
      }, 500);
    }
    props.saveSelectedLanguage(selectedLanguage);
  };

  return (
    <ProfileView
      logoutButtonPress={logoutButtonPress}
      isLogoutModalVisible={isLogoutModalVisible}
      openLogoutModal={openLogoutModal}
      selectLanguage={selectLanguage}
      onBackButtonClick={onBackButtonClick}
      onRightButtonPress={onRightButtonPress}
      privacyPolicyPress={privacyPolicyPress}
      faqPress={faqPress}
      aboutPress={aboutPress}
      loyaltyPress={loyaltyPress}
      termsAndConditionPress={termsAndConditionPress}
      toggleHelpModal={toggleHelpModal}
      toggleLogoutModal={toggleLogoutModal}
      isModalVisible={isModalVisible}
      isHelpModalVisible={isHelpModalVisible}
      openHelpModal={openHelpModal}
      toggleModal={toggleModal}
      openEditModal={openEditModal}
      isContactModalVisible={isContactModalVisible}
      openContactModal={openContactModal}
      toggleContactModal={toggleContactModal}
      openLanguageModal={openLanguageModal}
      toggleLanguageModal={toggleLanguageModal}
      token={_.get(props, "token", null)}
      gotoLogin={gotoLogin}
      isLanguageModalVisible={isLanguageModalVisible}
      userdetails={props.editUserDetails}
      isLoading={props.isLoading}
      is_notification_active={props.is_notification_active}
      languageSwitchScreen={props.languageSwitchScreen}
      notification_badge={props.notification_badge}
      guestisLogged={props.guestisLogged}
    />
  );

};


const mapStateToProps = (state, props) => {
  return {
    token: _.get(state, "loginReducer.userData.data.access_token", null),
    editUserDetails: _.get(state, "editUserReducer.editUserData", ''),
    isLoading: _.get(state, "editUserReducer.isLoading", ''),
    isLogged: _.get(state, "loginReducer.isLogged", ''),
    loggedoutData: _.get(state, "loginReducer.logoutData", ''),
    guestisLogged: _.get(state, "loginReducer.guestLogin", ''),
    notification_badge:  _.get(state, 'homeReducer.notificationbadge', ''),
    is_notification_active: state.homeReducer.is_notification_active,
    apiError: state.editUserReducer.apiError
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    saveSelectedLanguage: loginActions.saveSelectedLanguage,
    editUserData: editUserActions.doUserDetailsEdit,
    logOutPressed: editUserActions.doLogout,
    resetLogoutData: editUserActions.resetLogoutData,
    chatSupport: supportActions.RequestSupport,
    addToCart: cartActions.cartUpdate,
    selectRestaurant: cartActions.selectRestaurant,
    languageSwitchScreen: homeActions.languageSwitchScreen,
    NotificationBadgeReset: homeActions.NotificationBadgeReset,
    getNotificationCount: homeActions.getNotificationCount,
    LogoutClearData: editUserActions.LogoutClearData
  }, dispatch)
};

const profileScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileScreen);

profileScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default profileScreenWithRedux;
