import { StyleSheet, I18nManager } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import globals from "../../lib/globals";

let headerButtonContainerWidth =
  globals.SCREEN_SIZE.width - globals.MARGIN.marginTen * 2;
let headerButtonWidth =
  (headerButtonContainerWidth - globals.MARGIN.marginTen * 2) / 3;
const images = {
  free: require("../../assets/images/profileicon/free.png"),
  about: require("../../assets/images/profileicon/about.png"),
  faqIcon: require("../../assets/images/profileicon/Faqs.png"),
  privacyIcon: require("../../assets/images/profileicon/Privacy.png"),
  termsIcon: require("../../assets/images/profileicon/terms.png"),
  supportIcon: require("../../assets/images/profileicon/supp.png"),
  logoutIon: require("../../assets/images/profileicon/loog.png"),
  arrowIcon: require("../../assets/images/profileicon/arrow.png"),
  pencil: require("../../assets/images/profileicon/pen.png"),
  person:require("../../assets/images/profileicon/profile-image.jpg"),
  phone: require('../../assets/images/profileicon/call.png'),
  mail: require('../../assets/images/profileicon/mail.png'),
  close: require("../../assets/images/profileicon/cloo.png"),
  main: require("../../assets/images/temp/main.png"),
  cam: require("../../assets/images/Icons/camera.png"),



};
const styles = StyleSheet.create({
  screenMain: {
    flex: 1,
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
    flexDirection: "column",
    backgroundColor: 'white'
  },
  profileContainer: {
    width: '92%',
    backgroundColor: 'white',
    borderRadius: 20,
    marginLeft: '4%',
    marginRight: '4%',
    //flexDirection:'row',
    marginTop: '4%',
  },
  screenMains: {
    backgroundColor: globals.COLOR.greyBackground,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    paddingBottom: 25
  },
  roundImage: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: '3%',
    //paddingLeft:'38%'
  },
  infoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 8,
  },
  nameText: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helveticaBold,
    fontSize:15
  },
  phoneDetails: {
    flexDirection: 'row',
    marginTop: 5,
    alignItems:"center"
  },
  phoneDetails2: {
    flexDirection: 'row',
  },
  phoneImage: {
    width: 15,
    height: 15,
    alignSelf: 'center',
    resizeMode:'contain',
    transform:[{scaleX: I18nManager.isRTL ? -1 : 1}]
  },
  numberStyle: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize: 14,
    color: globals.COLOR.lightBlueText,
  },
  number: {
    marginLeft: '.1%'
  },
  number2: {
    marginLeft: '1%'
  },
  butt: {
    width: '38%',
    height: 30,
    marginTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  oval: {
    borderWidth: 0.5,
    borderColor: globals.COLOR.Text,
   width:'100%',
    borderRadius: 20,
    alignItems:'center',
    justifyContent:'center'
   // marginTop: '2%'
  },
  joinoval: {
    borderWidth: 0.5,
    borderColor: globals.COLOR.Text,
    width: 170,
    borderRadius: 20,
    alignItems:'center',
    justifyContent:'center',
    padding: 5
  },
  editBox: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  pencil: {
    width: 25,
    height: 25,
  },
  pencilView: {
    justifyContent: 'center',
  alignItems:'center'
  },
  editTextView: {
    justifyContent: 'center',
alignItems:'center'
    
  },
  editText: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helveticaBold,
    fontSize: 14,
  },
  join: {
    fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helveticaBold,
    fontSize: 12,
    lineHeight: 20,
    // marginLeft: 9,
    // marginRight: 9,
    
  },
  profileListContainer: {
    width: '92%',
    backgroundColor: 'white',
    borderRadius: 20,
    marginLeft: '4%',
    marginRight: '4%',
    
    marginTop: '2%',
    paddingTop:10,
    paddingBottom:10
  },
  logoutContainer: {
    width: '92%',
    height: '7.5%',
    backgroundColor: 'white',
    borderRadius: 20,
    marginLeft: '4%',
    marginRight: '4%',
    alignItems:"center",
    justifyContent:'center',
    marginTop: '5%',
    padding: 10,
  },

  logo: {
    width: 75,
    height: 72,
    resizeMode: 'cover',
    borderRadius: 50,
    alignSelf: 'center',

  },

  mainList: {
    alignItems:'center',
    justifyContent:'center',
    paddingBottom: 10,
    paddingTop: 10,
  },








});

export { images, styles };
