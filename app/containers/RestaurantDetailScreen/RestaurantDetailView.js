import React, { useState } from "react";
import {
  View,
  Text,
  FlatList,
  Image,
  SafeAreaView,
  I18nManager,
  TouchableOpacity,
  StatusBar,
  RefreshControl
} from "react-native";

import { styles, image } from "./styles";
import PropTypes from "prop-types";
import ParallaxScrollView from "react-native-parallax-scroll-view";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

import Loader from '../../components/Loader'
import appTexts from "../../lib/appTexts";
import KhaliedCard from "../../components/KhaliedCard";
import MenuOffersTab from "../../components/MenuOffersTab";
import OfferScreenCard from "../../components/OfferScreenCard";

import MenuOfferPage from "../../components/MenuOfferPage";
import CommentModal from "../../components/CommentModal";
import AddonModal from "../../components/AddonModal";
import LoyaltyCard from "../../components/LoyaltyCard";

import ScanButton from "../../components/ScanButton";
import DividerLine from "../../components/DividerLine";

import _ from "lodash";
import { SliderBox } from "react-native-image-slider-box";
import functions from "../../lib/functions";
import BannerButton from "../../components/BannerButton";
import FastImage from 'react-native-fast-image';
import CartModal from "../../components/CartModal";

const RestaurantDetailView = (props) => {

  const {
    tabIndex,
    onemoretab,
    firstTabCount,
    secondTabCount,
    onTabChange,
    openCommentModal,
    isCommentModalVisible,
    isAddonModalVisible,
    openAddonModal,
    onMenuCardPress,
    onRefresh,
    refreshing,
    isLoading,
    
    onBackButtonPress,
    openScanModal,
    restaurantDataDetails,
    restTypesDetails,
    restMenuDetails,
    restTermsDetails,
    restFacilitiesDetails,
    restDescription,
    restSliderImages,
    restOfferDetails,
    restLoyaltyMealsDetails,

    addToCart,
    addedCartItems,
    selectedRestaurant,
    onCheckoutClick,
    redirectToScan,
    restaurantName,
    restLocation,
    restaurant_id,
    all_segments,
    userReviews,
    postScanItemToAddData,
    from,
    postScanItemToAdd,
    setIsAddonModalVisible
  } = props;

  const [isCartModalVisible, setIsCartModalVisible] = useState(false);
  const [addonsSelected, setAddonsSelected] = useState([]);
  const [item, setItem] = useState({});
  
  const onViewClick = () => {
    setIsAddonModalVisible(false);
    setTimeout(() => {
      setIsCartModalVisible(!isCartModalVisible);
    }, 400);
  };

  const renderMealsCard = ({ item, index }) => (
    <LoyaltyCard
      addedCartItems={addedCartItems}
      restaurantName={restaurantName} 
      restLocation={restLocation} 
      item={item} 
      onMenuCardPress={onMenuCardPress} 
      isOffer={true}
      postScanItemToAddData={postScanItemToAddData}
      postScanItemToAdd={postScanItemToAdd}
      from={from}
      addloyalty={async (item, availableFreeMeals) => {

        if(restaurantDataDetails.time_status === 'CLOSED') {
          functions.displayToast('error', 'top', appTexts.REST.Heading_closed , appTexts.REST.closed);
          return false;
        }

        if(typeof selectedRestaurant.restaurant_id != 'undefined' && selectedRestaurant.restaurant_id != restaurant_id) {
          await functions.displayAlertWithCallBack(
            appTexts.REST.already_in_cart, 
            appTexts.REST.cart_has_items, 
            (data) => {
              if(data) {
                addToCart({});
                redirectToScan(false, 'loyality', item.item_id);
                addLoyaltyItem(item, availableFreeMeals, true);
              }
            }
          );
        } else if(typeof selectedRestaurant.restaurant_id == 'undefined') {
          redirectToScan(false, 'loyality', item.item_id);
          addLoyaltyItem(item, availableFreeMeals, true);
        } else {
          addLoyaltyItem(item, availableFreeMeals, false);
        }
      }}
    />
  );

  const addLoyaltyItem = (item, availableFreeMeals, clear_cart) => {
    let addedItems = (typeof addedCartItems == 'undefined' || clear_cart == true) ? {} : Object.assign({}, addedCartItems);
    if(availableFreeMeals <= 0) {
      functions.displayToast('error', 'top', appTexts.LOYALTY.notAvailable , appTexts.LOYALTY.offernotAvailable);
      return false;
    }
    if(Object.keys(addedItems).indexOf('loyalty_' + item.item_id.toString()) == -1) {
      let itemToAdd = Object.assign({}, item);
      itemToAdd.addonsSelected = {};
      itemToAdd.count = 1;
      itemToAdd.id = 'loyalty_' + item.item_id;
      itemToAdd.is_loyalty = true;
      itemToAdd.price = 0;
      itemToAdd.available_free = availableFreeMeals;
      itemToAdd.discount_price = 0;
      addedItems['loyalty_' + item.item_id] = itemToAdd;
      setTimeout(() => {
        functions.displayToast('success', 'top', appTexts.ALERT_MESSAGES.success, appTexts.ORDER.itemadded);
      }, 400);
    } else {
      let all_items = Object.assign({}, addedItems);
      if(availableFreeMeals <= all_items['loyalty_' + item.item_id].count) {
        functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.error, appTexts.STRING.addedmaximumoffer);
        return false;
      }
      all_items['loyalty_' + item.item_id].count += 1;
      all_items['loyalty_' + item.item_id].id = 'loyalty_' + item.item_id;
      addedItems = all_items;
      setTimeout(() => {
        functions.displayToast('success', 'top', appTexts.ALERT_MESSAGES.success, appTexts.ORDER.itemupdated);
      }, 400);
    }
    addToCart(addedItems);
  }

  const renderOffers = ({ item, index }) => (
    <View style={{ marginTop: "5%" }}>
      <OfferScreenCard
        item={item}
        onMenuCardPress={onMenuCardPress}
        postScanItemToAddData={postScanItemToAddData}
        postScanItemToAdd={postScanItemToAdd}
        from={from}
        openScanModal={async(item) => {

          if(restaurantDataDetails.time_status === 'CLOSED') {
            functions.displayToast('error', 'top', appTexts.REST.Heading_closed , appTexts.REST.closed);
            return false;
          }

          if(typeof selectedRestaurant.restaurant_id != 'undefined' && selectedRestaurant.restaurant_id != restaurant_id) {
            await functions.displayAlertWithCallBack(
              appTexts.REST.already_in_cart, 
              appTexts.REST.cart_has_items,
              (data) => {
                if(data) {
                  addToCart({});
                  redirectToScan(false, 'offer', item.item_id);
                  addOfferItem(item, true);
                }
              }
            );
          } else if(typeof selectedRestaurant.restaurant_id == 'undefined') {
            redirectToScan(false, 'offer', item.item_id);
            addOfferItem(item, true);
          } else {
            addOfferItem(item, false);
          }
        }}
      />
    </View>
  );

  const addOfferItem = (item, clear_cart) => {
    let addedItems = (typeof addedCartItems == 'undefined' || clear_cart == true) ? {} : Object.assign({}, addedCartItems);
    if(Object.keys(addedItems).indexOf( 'offer_' + item.item_id.toString()) == -1) {
      let itemToAdd = Object.assign({}, item);
      itemToAdd.addonsSelected = {};
      itemToAdd.count = 1;
      itemToAdd.id = 'offer_' + item.item_id;
      itemToAdd.is_offer = true;
      let offer_price = item.discount_price != null ? item.discount_price : item.price;;
      if(item.discount_type == 'Value') {
        offer_price = offer_price - item.discount_value;
      } else {
        offer_price = offer_price - (offer_price/100)*item.discount_value;
      }
      itemToAdd.price = item.price;
      itemToAdd.discount_price = offer_price;
      addedItems['offer_' + item.item_id] = itemToAdd;
      setTimeout(() => {
        functions.displayToast('success', 'top', appTexts.ALERT_MESSAGES.success, appTexts.ORDER.itemadded);
      }, 400);
    } else {
      let all_items = Object.assign({}, addedItems);
      all_items['offer_' + item.item_id].count += 1;
      all_items['offer_' + item.item_id].id = 'offer_' + item.item_id;
      addedItems = all_items;
      setTimeout(() => {
        functions.displayToast('success', 'top', appTexts.ALERT_MESSAGES.success, appTexts.ORDER.itemupdated);
      }, 400);
    }
    addToCart(addedItems);
  }

  const renderItemMenu = ({ item, index }) => (
    <MenuOfferPage
      addToCart={(dataCart, currentItem) => {
        setItem(currentItem);
        let _addonsSelected = {};
        try{
          _addonsSelected = addedCartItems[currentItem.id].addonsSelected;
          setAddonsSelected(_addonsSelected);
        } catch(err) {
          
        }
        addToCart(dataCart);
      }}
      addedCartItems={addedCartItems}
      item={item}
      onMenuCardPress={onMenuCardPress}
      isAddonsAvailable={true}
      selectedRestaurant={selectedRestaurant}
      onCheckoutClick={onCheckoutClick}
      restaurant_id={restaurant_id}
      redirectToScan={redirectToScan}
      postScanItemToAddData={postScanItemToAddData}
      from={from}
      postScanItemToAdd={postScanItemToAdd}
      setIsAddonModalVisible={setIsAddonModalVisible}
      isAddonModalVisible={isAddonModalVisible}
      time_status={restaurantDataDetails.time_status}
    />
  );

  const [altStickyHeaderHeight, setStickyHeaderHeight] = useState(1);
  const stickyHeaderHeight = 135;
  return (
    <View style={styles.screenMain}>
      <StatusBar
        backgroundColor="transparent"
        barStyle="dark-content"
        // translucent={true}
      />
      <ParallaxScrollView
        refreshControl={<RefreshControl style={{ backgroundColor: 'white' }} refreshing={refreshing} onRefresh={onRefresh} />}
        contentContainerStyle={styles.parallax}
        parallaxHeaderHeight={330}
        backgroundColor="#fff"
        stickyHeaderHeight={altStickyHeaderHeight}
        const onScroll={(event) => {
          const threshold = 135;
          if (
            event.nativeEvent.contentOffset.y <= threshold &&
            altStickyHeaderHeight > 1
          ) {
            setStickyHeaderHeight(1);
          } else if (
            event.nativeEvent.contentOffset.y > threshold &&
            altStickyHeaderHeight === 1
          ) {
            setStickyHeaderHeight(stickyHeaderHeight);
          }
        }}
        const
        renderForeground={() => (
          <View style={styles.wrapper}>
            <View style={{minHeight: 100}}>
               
              <SliderBox
                ImageComponent={FastImage}
                images={restSliderImages}
                sliderBoxHeight={200}
                ImageComponentStyle={{
                  width: '100%',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                dotStyle={styles.dot}
                dotColor='white'
                inactiveDotColor="white"
                imageLoadingColor="#90A4AE"
                inactiveDotStyle={styles.active}
                onCurrentImagePressed={index => null}
                autoplay={true}
                loop={true}
              >
              </SliderBox>
              
              <TouchableOpacity 
              onPress={() => {
                onBackButtonPress();
              }}
              style={{ position : 'absolute',
              alignItems: 'center',
              justifyContent: 'center',
               width: 40,
               borderRadius: 5,
                height: 40,
                backgroundColor:'rgba(52,52,52,0.8)',
                  left: 20, top: hp('5%')}} >
                    <Image source={image.backImage} style={styles.bacImage} />
                  </TouchableOpacity>
                
            </View>
            <View style={styles.black}>

              {isLoading && <Loader />}
              <KhaliedCard
                restaurantDataDetails={restaurantDataDetails}
                onReviewClick={openCommentModal}
                restTypesDetails={restTypesDetails}
                restTermsDetails={restTermsDetails}
                restDescription={restDescription}
                restFacilitiesDetails={restFacilitiesDetails}
              />
            </View>
          </View>
        )}
        const
        renderStickyHeader={() => (
          <View
            style={{
              backgroundColor: "white",
              paddingTop: "12%",
              paddingBottom: "12%",
              paddingLeft: "5%",
            }}
          >
            <View style={styles.sticky}>
              <TouchableOpacity
                onPress={() => {
                  onBackButtonPress();
                }}
              >
                <View style={styles.arr}>
                  <Image source={image.bkarrow} style={styles.bkarrowImage} />
                </View>
              </TouchableOpacity>
              <View style={styles.restName}>
                <Text style={styles.khName}>
                  {I18nManager.isRTL
                    ? _.get(restaurantDataDetails, "lang.ar.name", "")
                    : _.get(restaurantDataDetails, "lang.en.name", "")}
                </Text>
              </View>
            </View>
            <MenuOffersTab
              tabIndex={tabIndex}
              firstTabText={appTexts.MENUOFFERS.menu}
              secondTabText={appTexts.MENUOFFERS.offers}
              firstTabCount={firstTabCount}
              secondTabCount={secondTabCount}
              onTabChange={onTabChange}
              onemoretab={onemoretab}
            />
            <View
              style={{
                width: "100%",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <DividerLine />
            </View>
          </View>
        )}
      >
        <View style={{}}>
          <View style={styles.tabStyle}>
            {altStickyHeaderHeight==1&&
              <MenuOffersTab
                tabIndex={tabIndex}
                firstTabText={appTexts.MENUOFFERS.menu}
                secondTabText={appTexts.MENUOFFERS.offers}
                firstTabCount={firstTabCount}
                secondTabCount={secondTabCount}
                onTabChange={onTabChange}
                onemoretab={onemoretab}
              />
            }
          </View>
        </View>
        {tabIndex === 0 ? (
          <View style={styles.formWrapper}>
            <SafeAreaView>
              <FlatList
                ListEmptyComponent={
                  <Text style={styles.no_orders}>{I18nManager.isRTL ? "لا يوجد قوائم" : 'No menus to display'}</Text>
                }
                showsVerticalScrollIndicator={false}
                style={styles.flatListStyle}
                contentContainerStyle={{ paddingBottom: "5%" }}
                data={_.isEmpty(restMenuDetails) ? [] : restMenuDetails}
                extraData={restMenuDetails}
                keyExtractor={(item, index) => index.toString()}
                renderItem={renderItemMenu}
              />
            </SafeAreaView>
          </View>
        ) : null}

        {tabIndex === 1 ? (
          <View style={styles.offerList}>
            <SafeAreaView>
              <FlatList
                ListEmptyComponent={
                  <>
                    {_.isEmpty(restLoyaltyMealsDetails) &&
                      <Text style={styles.no_orders}>{appTexts.OFFER.noOffer}</Text>
                    }
                  </>
                }
                style={styles.flatListStyle}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{ paddingBottom: "5%" }}
                data={_.isEmpty(restOfferDetails) ? [] : restOfferDetails}
                keyExtractor={(item, index) => index.toString()}
                renderItem={renderOffers}
                ListHeaderComponent={
                  <View style={styles.MealsView}>
                    <FlatList
                      style={styles.flatListStyle}
                      contentContainerStyle={{ paddingBottom: "5%" }}
                      data={
                        _.isEmpty(restLoyaltyMealsDetails)
                          ? []
                          : restLoyaltyMealsDetails
                      }
                      extraData={restMenuDetails}
                      keyExtractor={(item, index) => index.toString()}
                      renderItem={renderMealsCard}
                    />
                  </View>
                }
              />
            </SafeAreaView>
          </View>
        ) : null}

      </ParallaxScrollView>
      <View style={styles.butto}>

      {Object.keys(addedCartItems).length >0 &&
        <BannerButton
          text1={appTexts.BANNER.count}
          text2={appTexts.BANNER.view}
          text3={appTexts.BANNER.check}
          text4={appTexts.BANNER.SAR}
          onCheckoutClick={onCheckoutClick}
          addedCartItems={addedCartItems}
          openAddonModal={openAddonModal}
          // onViewClick={null}
          isCart={false}
          isDetail={true}
        />
      }

        <ScanButton redirectToScan={redirectToScan} />
      </View>

      <CommentModal
        isCommentModalVisible={isCommentModalVisible}
        openCommentModal={openCommentModal}
        restaurantDataDetails={restaurantDataDetails}
        all_segments={all_segments}
        userReviews={userReviews}
      />
      
      <AddonModal
        onCheckoutClick={() => {
          openAddonModal();
          onCheckoutClick();
        }}
        addAddon={(count, addon_item) => {
          let addonItems = Object.assign({}, addedCartItems);
          if(Object.keys(addonItems[item.id].addonsSelected).indexOf(addon_item.id.toString()) == -1) {
            addon_item.count = 1;
            addonItems[item.id].addonsSelected[addon_item.id] = addon_item;
            functions.displayToast('success', 'top', appTexts.ALERT_MESSAGES.success, appTexts.LOGIN_POP.addonadd);
          } else {
            addonItems[item.id].addonsSelected[addon_item.id].count += count;
            if(addonItems[item.id].addonsSelected[addon_item.id].count == 0) {
              delete addonItems[item.id].addonsSelected[addon_item.id];
              functions.displayToast('error', 'top', appTexts.ALERT_MESSAGES.success, appTexts.LOGIN_POP.addonremove);
            } else {
              functions.displayToast('success', 'top', appTexts.ALERT_MESSAGES.success, appTexts.LOGIN_POP.addonupdate);
            }
          }
          addToCart(addonItems);
        }}
        addonsDetails={_.get(item, "item_addons", {})}
        isAddonModalVisible={isAddonModalVisible}
        openAddonModal={openAddonModal}
        addonsSelected={addonsSelected}
        addedCartItems={addedCartItems}
        onViewClick={onViewClick}
        isCart={true}
      />

      <CartModal
        isCartModalVisible={isCartModalVisible}
        onCheckoutClick={() => {
          setIsCartModalVisible(false);
          onCheckoutClick();
        }}
        openCartModal={onViewClick}
        setIsCartAddon={() => setIsAddonModalVisible(true)}
        addedCartItems={addedCartItems}
        addToCart={addToCart}
        openAddonModalCustomize={(item)=> {
          setItem(item);
          setTimeout(() => {
            setIsAddonModalVisible(true);
          }, 800);
        }}
      />

    </View>
  );
};

RestaurantDetailView.propTypes = {
  onCartButtonPress: PropTypes.func,
  onBackButtonPress: PropTypes.func,
  categoryClick: PropTypes.func,
  productsClick: PropTypes.func,
  mapClick: PropTypes.func,
  onRefresh: PropTypes.func,
};

export default RestaurantDetailView;
