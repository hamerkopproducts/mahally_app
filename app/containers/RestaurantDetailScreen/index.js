import React, { useEffect, useState } from "react";

import NetInfo from "@react-native-community/netinfo";
import { connect } from "react-redux";
import RestaurantDetailView from "./RestaurantDetailView";
import { bindActionCreators } from "redux";

import * as restaurantDetailsActions from "../../actions/restaurantDetailsActions";
import * as cartActions from "../../actions/cartActions";

import appTexts from "../../lib/appTexts";
import functions from "../../lib/functions";
import _ from "lodash";
import { I18nManager, BackHandler } from "react-native";
import ServiceWrapperAwait from '../../service/ServiceWrapperAwait';
import { store } from '../../../configureStore';

const RestaurantDetailScreen = (props) => {
  const [tabIndex, setTabIndex] = useState(0);
  const [onemoretab, setOnemoreTab] = useState(1);
  const [isCommentModalVisible, setIsCommentModalVisible] = useState(false);
  const [isAddonModalVisible, setIsAddonModalVisible] = useState(false);
  const [isLoadExpand, setisLoadExpand] = useState(false);
  const [isScanModalVisible, setIsScanModalVisible] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [userReviews, setUserReviews] = useState(false);

  const getUserReviews = async(restId) => {
    const sAsyncWrapper = new ServiceWrapperAwait();
    const response = await sAsyncWrapper.get(`user-app/reviews/${restId}`,
      {is_auth_required: false}
    );
    try {
      const data = new Promise((resolve, reject) => {
        try {
          resolve(response);
        } catch (err) {
          reject(err);
        }
      });
      return data;
    } catch (err) {
    }
  }

  const reviewsFetch = async() => {
    const data = await getUserReviews( _.get(props, "route.params.restId") );
    if(data.success == true) {
      setUserReviews( data.data );
    }
  }

  useEffect(() => {
    return props.navigation.addListener("focus", () => {
      NetInfo.fetch().then((state) => {
        if ( ! state.isConnected) {
          functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);
  useEffect(() => handleComponentUpdated());

  
  //mounted
  useEffect(() => handleComponentMounted(), []);
  const handleComponentMounted = () => {
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        let params = {
          // latitude: _.get(props, "route.params.restLatitude"),
          // longitude: _.get(props, "route.params.restLongitude"),
          cust_id: _.get(props, "userID"),
        };
        let _params = params;
        if( ! _params.latitude || ! _params.longitude) {
          const localStoreLocation = store.getState().geoLocationReducer.geoLocation;
          const latitudeUser = localStoreLocation.status == true ? localStoreLocation.location.data.latitude : 0;
          const longitudeUser = localStoreLocation.status == true ? localStoreLocation.location.data.longitude : 0;
          
          _params.latitude = latitudeUser;
          _params.longitude = longitudeUser;
        }
        props.clearRestDetails();
        reviewsFetch();
        props.restaurantDetails(_params, _.get(props, "route.params.restId"));
      } else {
        functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
      }
    });
    BackHandler.addEventListener('hardwareBackPress', backPressed = () => {
      props.navigation.goBack(null);
      return true;
    });
  };

  const onTabChange = (index) => {
    props.postScanItemToAdd({});
    if (tabIndex !== index) {
      setTabIndex(index);
      if (index === 0) {
      } else if (index === 1) {
      } else if (index === 2) {
        setOnemoreTab(0);
      }
    }
  };

  useEffect(() => {
    return () => {
      handleComponentUnmount();
    };
  }, []);

  const handleComponentUnmount = () => {};
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
    if (props.pullRefresh === true) {
      setRefreshing(false);
    }
  };

  const onRefresh = async () => {
    setRefreshing(true);
    NetInfo.fetch().then((state) => {
      if (state.isConnected) {
        let params = {
          // latitude: _.get(props, "route.params.restLatitude"),
          // longitude: _.get(props, "route.params.restLongitude"),
          cust_id: _.get(props, "userID"),
        };
        let _params = params;
        if( ! _params.latitude || ! _params.longitude) {
          const localStoreLocation = store.getState().geoLocationReducer.geoLocation;
          const latitudeUser = localStoreLocation.status == true ? localStoreLocation.location.data.latitude : 0;
          const longitudeUser = localStoreLocation.status == true ? localStoreLocation.location.data.longitude : 0;
          
          _params.latitude = latitudeUser;
          _params.longitude = longitudeUser;
        }
        props.restaurantDetails(params, _.get(props, "route.params.restId"));
      } else {
        functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
      }
    });
  };

  const openScanModal = () => {
    setIsScanModalVisible(!isScanModalVisible);
  };

  const onProfileButtonPress = () => {
    props.navigation.navigate("ProfileScreen");
  };

  const onArrowClick = () => {
    props.navigation.goBack();
  };

  const onBackButtonPress = () => {
    props.clearRestDetails();
    props.navigation.navigate('HomeScreen');
  };

  const openCommentModal = () => {
    setIsCommentModalVisible(!isCommentModalVisible);
  };

  const openAddonModal = () => {
    setIsAddonModalVisible(!isAddonModalVisible);
  };

  const onMenuCardPress = (item_id, item, from) => {

    let _item = item;

    const time_status = props.restaurantDataDetails.time_status;
    const vat_includes = props?.restaurantDataDetails?.vat_includes || 'no';
    const vat_percentage = props?.restaurantDataDetails?.vat_percentage || '';
    const hasPreOrder = props?.restaurantDataDetails?.pre_order || false;

    _item.vat_includes = vat_includes;
    _item.vat_percentage = vat_percentage;
    _item.has_preorder = hasPreOrder == 'enable';

    
    props.navigation.navigate("ProductDetailScreen", 
      { 
        itemId: item_id, 
        from: from, 
        item: _item,
        time_status: time_status
      });
  };

  const onLoadPress = () => {
    setisLoadExpand(!isLoadExpand);
  };

  const onCheckoutClick = () => {
    props.navigation.navigate("CheckoutScreen");
  };

  const restaurant_id = _.get(props, "route.params.restId");
  const segments_summary = props.restaurantDataDetails.segments;
  let all_segments = [];
  if(segments_summary) {
    for(let incr=0; incr<Object.keys(segments_summary).length; incr++) {
      all_segments.push( segments_summary[Object.keys(segments_summary)[incr]][0] );
    }
  }

  let _from = '';
  try {
    _from = _.get(props, "route.params.from");
    if(_from != 'home') {
      props.navigation.setParams({from: 'home'});
    }
  } catch(err) {
    _from = '';
  }

  if( (props.postScanItemToAddData.type == 'offer' || props.postScanItemToAddData.type == 'loyality') && tabIndex == 0) {
    setTabIndex(1);
  }

  return (
    <RestaurantDetailView
      onProfileButtonPress={onProfileButtonPress}
      onArrowClick={onArrowClick}
      onTabChange={onTabChange}
      refreshing={refreshing}
      onRefresh={onRefresh}
      tabIndex={tabIndex}
      onemoretab={onemoretab}
      firstTabCount={props.deliveredOrders.length}
      secondTabCount={props.returnedOrders.length}
      isCommentModalVisible={isCommentModalVisible}
      isScanModalVisible={isScanModalVisible}
      openCommentModal={openCommentModal}
      isAddonModalVisible={isAddonModalVisible}
      openAddonModal={openAddonModal}
      onMenuCardPress={onMenuCardPress}
      onLoadPress={onLoadPress}
      isLoadExpand={isLoadExpand}
      onBackButtonPress={onBackButtonPress}
      openScanModal={openScanModal}
      restLocation={_.get(props, "restaurantDataDetails.location", {})}
      restaurantName={
        I18nManager.isRTL
          ? _.get(props, "restaurantDataDetails.lang.ar.name", "")
          : _.get(props, "restaurantDataDetails.lang.en.name", "")
      }
      restaurantDataDetails={_.get(props, "restaurantDataDetails", {})}
      restTypesDetails={_.get(props, "restTypesDetails", [])}
      restMenuDetails={_.get(props, "restMenuDetails", {})}
      restTermsDetails={_.get(props, "restTermsDetails", {})}
      restFacilitiesDetails={_.get(props, "restFacilitiesDetails", {})}
      restDescription={_.get(props, "restaurantDataDetails.lang", {})}
      restSliderImages={_.get(props, "restSliderImages", {})}
      restOfferDetails={_.get(props, "restOfferDetails", {})}
      restLoyaltyMealsDetails={_.get(props, "restLoyaltyMealsDetails", {})}
      isLoading={props.isLoading}
      addToCart={props.addToCart}
      addedCartItems={props.addedCartItems}
      selectedRestaurant={props.selectedRestaurant}
      onCheckoutClick={onCheckoutClick}
      redirectToScan={async(is_scan_button_tap, type='', item_id='') => {
        let _restaurant_id = props.selectedRestaurant.restaurant_id;
        if(is_scan_button_tap && typeof _restaurant_id != 'undefined' && _restaurant_id != '') {
          await functions.displayAlertWithCallBack(
            appTexts.CART_CHANGES.already_scanned,
            appTexts.CART_CHANGES.change_restaurant,
            (data) => {
              if(data) {
                props.navigation.navigate("Scan", { screen: "ScanScreen", params:{ from: '_restuarant'} });
              }
            },
            appTexts.CART_CHANGES.scan_new,
            appTexts.CART_CHANGES.exit
          );
        } else {

          const vat_includes = props?.restaurantDataDetails?.vat_includes || 'no';
          const vat_percentage = props?.restaurantDataDetails?.vat_percentage || '';
          const hasPreOrder = props?.restaurantDataDetails?.pre_order || 'disable';

          // props.postScanItemToAdd({
          //   type: type,
          //   item_id: item_id
          // });

          let _scanned_data = {
            restaurant_id: restaurant_id,
            table_id: '',
            color_code: '',
            dine_in: 'take',
            vat: vat_includes == 'yes' ? vat_percentage : '',
            has_preOrder: hasPreOrder == 'enable'
          };
          props.selectRestaurant(_scanned_data);
          // props.navigation.navigate("Scan", { screen: "ScanScreen", params:{ from: '_restuarant'} });
        }
      }}
      restaurant_id={restaurant_id}
      all_segments={all_segments}
      userReviews={userReviews}
      postScanItemToAdd={props.postScanItemToAdd}
      postScanItemToAddData={props.postScanItemToAddData}
      from={ _from }
      setIsAddonModalVisible={setIsAddonModalVisible}
    />
  );
};

const mapStateToProps = (state, props) => {
  return {
    deliveredOrders: state.orderReducer.deliveredOrders,
    returnedOrders: state.orderReducer.returnedOrders,
    ordersToReturn: state.orderReducer.ordersToReturn,
    pullRefresh: _.get(state, "restaurantDetailsReducer.pullRefresh", ""),
    restaurantDataDetails: _.get(
      state,
      "restaurantDetailsReducer.restDetails",
      ""
    ),
    restTypesDetails: _.get(
      state,
      "restaurantDetailsReducer.restTypesDetails",
      []
    ),
    restMenuDetails: _.get(
      state,
      "restaurantDetailsReducer.restMenuDetails",
      ""
    ),
    restOfferDetails: _.get(
      state,
      "restaurantDetailsReducer.restOfferDetails",
      ""
    ),
    restTermsDetails: _.get(
      state,
      "restaurantDetailsReducer.restTermsDetails",
      ""
    ),
    restFacilitiesDetails: _.get(
      state,
      "restaurantDetailsReducer.restFacilitiesDetails",
      ""
    ),
    restSliderImages: _.get(
      state,
      "restaurantDetailsReducer.restSliderImages",
      ""
    ),
    restLoyaltyMealsDetails: _.get(
      state,
      "restaurantDetailsReducer.restLoyaltyMealsDetails",
      ""
    ),
    isLoggedin: _.get(state, "loginReducer.isLogged", ""),
    token: _.get(state, "loginReducer.userData.data.access_token", ""),
    userID: _.get(state, "loginReducer.userData.data.user.id", ""),
    isLoading: _.get(state, "restaurantDetailsReducer.isLoading", ""),
    addedCartItems: state.cartReducer.cartAddedData,
    selectedRestaurant: state.cartReducer.selectedRestaurant,
    postScanItemToAddData: state.cartReducer.postScanItemToAddData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      restaurantDetails: restaurantDetailsActions.restaurantDetails,
      clearRestDetails: restaurantDetailsActions.clearRestDetails,
      addToCart: cartActions.cartUpdate,
      postScanItemToAdd: cartActions.postScanItemToAdd,
      selectRestaurant: cartActions.selectRestaurant,
    },
    dispatch
  );
};

const RestaurantDetailScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(RestaurantDetailScreen);

RestaurantDetailScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null,
});

export default RestaurantDetailScreenWithRedux;
