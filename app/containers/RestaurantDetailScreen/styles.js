import { StyleSheet,I18nManager } from "react-native";
import globals from "../../lib/globals"
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
let headerButtonContainerWidth = globals.SCREEN_SIZE.width - (globals.MARGIN.marginTen*2);
let headerButtonWidth = (headerButtonContainerWidth-(globals.MARGIN.marginTen*2)) / 3;

const image = {
  backImage:require('../../assets/images/attributes/back.png'),
  scan:require('../../assets/images/footerTabItem/order.png'),
  load:require('../../assets/images/attributes/loadmore.png'),
  bkarrow: require("../../assets/images/chooseLanguage/backarow.png"),
  // hotel:require("../../assets/images/HomeCard/hotel-img.png"),
};

const styles = StyleSheet.create({
  screenMain: {
    
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height ,
    flexDirection: 'column',
    backgroundColor:'white',
    flex:1,
  },
  screenDesignContainer: {
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height,
  },
  parallax:{
    backgroundColor:globals.COLOR.greyBackground,
    flex:1,
  },
  
  sq:{
    width:40,
    height:40,
    borderRadius:4,
    backgroundColor:'rgba(52,52,52,0.8)',
    position:'absolute',
    top:40,
    left:16,
    alignItems:'center',
    justifyContent:'center'
    },
  screenContainerWithMargin: {
    width: globals.INTEGER.screenWidthWithMargin,
    height: globals.INTEGER.screenContentHeight,//EDITED TO REDUCE FLATLIST HEIGHT
    marginLeft: globals.INTEGER.leftRightMargin,
    paddingBottom:20
  },
  MealsView: {
    // width: '90%',
    // marginLeft: '5%',
  },
  screenContainerScrollView:{
    width: globals.SCREEN_SIZE.width,
    height: globals.SCREEN_SIZE.height
  },
  headerButtonContianer: {
    flexDirection: 'row',
    width: headerButtonContainerWidth,
    height: globals.INTEGER.heightFifty,
    alignItems: 'center'
  },
  headerButton: {
    width: headerButtonWidth,
    height: globals.INTEGER.heightThirty
  },
  headerButtonMargin: {
    marginLeft: globals.MARGIN.marginTen
  },
  categoryItemRow: {
    width: '100%',
    flexDirection: 'row'
  },
  DotView:{
    
  },
  
  flatListStyle: {
    width: '100%',

    
    alignSelf:'center',
    //flex:5,
//backgroundColor:'red'
  },
  tabStyle:{
    height:80,
    backgroundColor:globals.COLOR.greyBackground,
  },
  formWrapper: {
    // flex:1,
    backgroundColor:'white',//globals.COLOR.greyBackground,
    //  marginTop: hp('1.5%'),
    //alignItems:'center'
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
   // marginBottom: hp('10%'),
    //paddingBottom:'10%',
    // width: globals.SCREEN_SIZE.width,
    // height: globals.SCREEN_SIZE.height

  },
  khName:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize:16,
    textAlign:'left',  
  },
  bkarrowImage:{
    width:18,
    height:18,
    resizeMode:'contain',
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
  },
  arr:{
    paddingLeft:'5%',
  },
  // sticky:{
  //   alignItems:'center',
  //   justifyContent:'center',
  // },
  restName:{
    paddingLeft:'5%',
  },
  bacImage:{
      resizeMode:'contain',
      width:20,
      height:20,
      tintColor:'white',
      transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }],
  },
  
  leftTopic:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize:16,
    textAlign:'left',  
  },
  boldTopic:{
    height:"2.5%",
    marginVertical:"5%",
    marginHorizontal:18
  },
  offerList:{
   // flex:1,
    backgroundColor:'#fff'
  },
  button:{
   // position:'absolute',
    //marginLeft:'5%',
    bottom:0,
    zIndex:10,
    alignItems:'center',
    justifyContent:'center'

  },
  imageScan:{
    width:60,
    height:60,
   // resizeMode:'cover',
  },
  butto:{
    position:'relative',
    bottom:0,
    //top:10
   // width:'100%'
  },
sticky:{
  flexDirection:'row',
  alignItems:'center',
  //   justifyContent:'center',
},

  boLine:{
    marginTop:'8%',
  },
  load:{
    alignItems:'center',
    justifyContent:'center',
    paddingBottom:'15%',
  },
  loadStyle:{
    width:60,
    height:60,
    resizeMode:'cover'
  },
  loadText:{
    fontFamily:I18nManager.isRTL ? globals.FONTS.notokufiArabic : globals.FONTS.helvetica,
    fontSize:12,
    color:globals.COLOR.greyText,
    textAlign:'left',   
  },
  hotel:{
      width: wp("100%"),
       height: hp("40%"),
       resizeMode: 'cover'
   },
   dot:{
    width: 40,
    
   },
   active:{
    width: 15,
    borderRadius: 10,
    height: 15
   },
   wrapper:{
     resizeMode: 'contain',
     height:'100%',
     width: '100%',
     backgroundColor: 'white',
   },
       backTouch:{
     
          },
    square:{

      marginTop:'10%',
      marginLeft:'5%',
      backgroundColor:'rgba(52,52,52,0.8)',
      width:40,
      height:40,
      zIndex:999,
      resizeMode:"cover",
      borderRadius:4,
      alignItems:'center',
      justifyContent:'center',
      // position: 'absolute'
      },
       black:{
        //  flex: 1,
        //  bottom:30,
        // //  height:'60%',
        //  marginBottom: 20
      },
      off:{
        backgroundColor:'transparent'
      },
      no_orders: {
        marginRight: 10,
        textAlign: 'center',
        marginTop: 20,
        fontFamily: I18nManager.isRTL ? globals.FONTS.notokufiarabicBold : globals.FONTS.helveticaBold,
        fontSize:14,
      },
});

export { image, styles };