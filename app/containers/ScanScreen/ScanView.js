import React, { useState, useEffect } from "react";
import { View, Image, I18nManager, Alert } from "react-native";
import { Title } from "native-base";
import globals from "../../lib/globals";
import { styles } from "./styles";

import appTexts from "../../lib/appTexts";
import Header from "../../components/Header";

import QRCodeScanner from "react-native-qrcode-scanner";
import { RNCamera } from "react-native-camera";
import ScanModal from "../../components/ScanModal";
import functions from "../../lib/functions";
import { store } from "../../../configureStore";
import ServiceWrapperAwait from "../../service/ServiceWrapperAwait";
import Loader from "../../components/Loader";

let scanner = null;

const ScanView = (props) => {
  const {
    selectRestaurant,
    notification_badge,
    selectedRestaurant,
    _params,
    addToCart,
    is_notification_active,
    languageSwitchScreen,
  } = props;

  let _rest_already_scanned =
    typeof selectedRestaurant.restaurant_id != "undefined" &&
    selectedRestaurant.restaurant_id != "";
  const [idleScan, setIdleScan] = useState(false);
  const [isDineInModalVisible, setIsDineInModalVisible] = useState(false);
  const [scannedData, setScannedData] = useState({});
  const [is_already_scanned, set_is_already_scanned] = useState(
    _rest_already_scanned
  );
  const [loader, setLoader] = useState(false);
  const [preLoader, setPreLoader] = useState(false);
  const [hasPreOrder, setHasPreOrder] = useState(false);
  const [hideScanner, setHideScanner] = useState(false);

  useEffect(() => {
    // if(typeof selectedRestaurant.restaurant_id == 'undefined' || selectedRestaurant.restaurant_id !== 26) {
    //   onSuccess({data: 26});
    // }
    const focusListener = props.navigation.addListener("focus", () => {
      const is_from_checkout = _params.from == "_checkout";
      if(is_from_checkout) {
        setScannedData(selectedRestaurant);
        scannerReactivate();
        return false;
      }
      const skip_already_scan_popup = _params.from == "_restuarant";
      let _rest_already_scanned =
        typeof selectedRestaurant.restaurant_id != "undefined" &&
        selectedRestaurant.restaurant_id != "" &&
        skip_already_scan_popup == false;
        
      set_is_already_scanned(is_already_scanned);
      if (_rest_already_scanned) {
        checkAlreadyScanned();
      } else {
        preOrderCheck();
      }
      // props.navigation.setParams({ from: null });
    });

    return () => {
      focusListener();
    };
  }, [props.navigation, _params]);

  const checkAlreadyScanned = async () => {
    await functions.displayAlertWithCallBack(
      appTexts.CART_CHANGES.already_scanned,
      appTexts.CART_CHANGES.change_restaurant,
      (data) => {
        if (data) {
          preOrderCheck();
        } else {
          props.navigation.navigate("TabNavigator", { screen: "HomeScreen" });
        }
      },
      appTexts.CART_CHANGES.scan_new,
      appTexts.CART_CHANGES.exit
    );
  };

  const preOrderCheck = async() => {
    setPreLoader(true);
    const has_preOrder = await preOrderRestaurant();
    setPreLoader(false);
    if (has_preOrder.success === true) {
      if (has_preOrder?.data?.preorder_availability === true) {
        setHasPreOrder(true);
      } else {
        setHasPreOrder(false);
      }
    }
    setIsDineInModalVisible(true);
  };

  const scannerReactivate = () => {
    setIsDineInModalVisible(false);
    setTimeout(() => {
      scanner.reactivate();
    }, 2000);
    setIdleScan(false);
  }

  const onModalOptionSelected = async(selectedOption) => {
    let _scanned_data = {
      restaurant_id: '',
      table_id: '',
      color_code: '',
      dine_in: selectedOption,
      vat: '',
      has_preOrder: hasPreOrder
    };
    setScannedData(_scanned_data);
    setIsDineInModalVisible(false);
    if(selectedOption == 'pre' || selectedOption == 'take') {

      if(_params.from == "_restuarant" && _params.id) {
        const restId = _params.id;
        _scanned_data.restaurant_id = restId;
        _scanned_data.dine_in = selectedOption;
        selectRestaurant(_scanned_data);
        props.navigation.setParams({from: null});
        goto_restaurant_detail(restId);
      } else {
        selectRestaurant(_scanned_data);
        // setHideScanner(true);
        props.navigation.navigate("TabNavigator", { screen: "Home" });
        // await functions.displayAlertWithCallBack(
        //   '',
        //   I18nManager.isRTL ? 'الرجاء اختيار أحد المطاعم للمتابعة' : 'Please select any one of the restaurant to continue',
        //   (data) => {
        //     setHideScanner(false);
        //     if (data) {
        //       props.navigation.navigate("TabNavigator", { screen: "Home" });
        //     } else {
        //       props.navigation.goBack();
        //     }
        //   },
        //   appTexts.LOGIN_POP.ok,
        //   appTexts.LOGIN_POP.cancel
        // );
      }
    } else {
      setTimeout(() => {
        scanner.reactivate();
      }, 2000);
      setIdleScan(false);
    }
  }

  const vatdata = async (_data) => {
    const sAsyncWrapper = new ServiceWrapperAwait();
    setLoader(true);
    const response = await sAsyncWrapper.post("user-app/vat-data", _data);
    try {
      const data = new Promise((resolve, reject) => {
        setLoader(false);
        try {
          resolve(response);
        } catch (err) {
          reject(err);
        }
      });

      return data;
    } catch (err) {
      setLoader(false);
    }
  };

  const preOrderRestaurant = async () => {
    const sAsyncWrapper = new ServiceWrapperAwait();
    setLoader(true);
    const response = await sAsyncWrapper.get("user-app/preorder_availability", {});
    try {
      const data = new Promise((resolve, reject) => {
        setLoader(false);
        try {
          resolve(response);
        } catch (err) {
          reject(err);
        }
      });

      return data;
    } catch (err) {
      setLoader(false);
    }
  };

  const goto_restaurant_detail = (id) => {
    const localStoreLocation = store.getState().geoLocationReducer.geoLocation;
    const latitudeUser =
      localStoreLocation.status == true
        ? localStoreLocation.location.data.latitude
        : 0;
    const longitudeUser =
      localStoreLocation.status == true
        ? localStoreLocation.location.data.longitude
        : 0;

    props.navigation.navigate("RestaurantDetailScreen", {
      restId: id,
      restLatitude: latitudeUser,
      restLongitude: longitudeUser,
      from: "ScanView",
    });
  };

  const onSuccess = async (e) => {
    if (e.data) {
      let _scannedData = scannedData;
      let _r_id = "";
      try {
        _r_id = parseInt(e.data, 10);
      } catch (err) {
        _r_id = "";
      }

      let restaurant_id = "";
      let table_id = "";
      let color_code = "";
      if (_r_id == "" || isNaN(_r_id)) {
        try {
          restaurant_id = e.data.split("Restaurant iD :")[1].split(" ")[0];
          table_id = e.data.split("Table No. :")[1].split(" ")[0];
          color_code = e.data.split("Color : ")[1].split(" ")[0];
        } catch (err) {
          restaurant_id = "";
          table_id = "";
          color_code = "";
        }
      } else {
        restaurant_id = _r_id;
        table_id = "";
        color_code = "";
      }

      if(_params.from == "_checkout" && scannedData.restaurant_id != restaurant_id) {
        await functions.displayAlertWithCallBack(
          I18nManager.isRTL ? 'يرجى مسح نفس المطعم الذي أضفت عناصر القائمة' : 'Please scan the same restaurant which you added menu items',
          '',
          (data) => {
            if (data) {
              scannerReactivate();
            } else {
              props.navigation.goBack();
            }
          },
          appTexts.LOGIN_POP.ok,
          appTexts.LOGIN_POP.cancel
        );
        return false;
      }

      if(_scannedData.dine_in == 'dine' && table_id == '') {
        await functions.displayAlertWithCallBack(
          I18nManager.isRTL ? 'الرجاء مسح كود الطاولة للطلبات المحلية' : 'Please scan QR from table for DIne-In',
          '',
          (data) => {
            if (data) {
              scannerReactivate();
            } else {
              props.navigation.goBack();
            }
          },
          appTexts.LOGIN_POP.ok,
          appTexts.LOGIN_POP.cancel
        );
        return false;
      }

      if (restaurant_id == "") {
        scannerReactivate();
        return false;
      }

      if (
        typeof selectedRestaurant.restaurant_id != "undefined" &&
        selectedRestaurant.restaurant_id != restaurant_id
      ) {
        // Clear cart when user scan a new restaurant.
        addToCart({});
      }

      const vat_data = await vatdata({ restaurant_id: restaurant_id });
      let vat = 0;
      if (vat_data.success === true) {
        if (vat_data?.data?.vat_includes === "yes") {
          vat = vat_data.data.vat_percentage;
        }
      }
      
      _scannedData.restaurant_id = restaurant_id;
      _scannedData.table_id = table_id;
      _scannedData.color_code = color_code;
      _scannedData.vat = vat;
      _scannedData.scanned = true;

      selectRestaurant(_scannedData);
      setIdleScan(true);
      if(_params.from == "_checkout") {
        props.navigation.setParams({from: null});
        props.navigation.navigate("CheckoutScreen", {from: 'scan'});
      } else {
        goto_restaurant_detail(restaurant_id);
      }
    } else {
      scannerReactivate();
    }
  };

  return (
    <View style={styles.screenMain}>
      <Header
        notification_badge={notification_badge}
        isProfileButtonRequired={true}
        isLanguageButtonRequired={true}
        isRightButtonRequired={true}
        headerTitle={appTexts.STRING.Mahally}
        customHeaderStyle={{
          height: globals.INTEGER.headerHeight,
          alignItems: "center",
          backgroundColor: globals.COLOR.headerColor,
        }}
        is_notification_active={is_notification_active}
        screen={"scan"}
        languageSwitchScreen={languageSwitchScreen}
      />

      {loader && <Loader />}

      <View style={styles.screenContainer}>
        {(isDineInModalVisible == false && preLoader == false && hideScanner == false) &&
          <QRCodeScanner
            ref={(node) => {
              scanner = node;
            }}
            onRead={onSuccess}
            flashMode={RNCamera.Constants.FlashMode.auto}
            containerStyle={{ width: "90%", height: 100 }}
            cameraStyle={{ width: "100%", height: 300, backgroundColor: "gray" }}
            showMarker={true}
            markerStyle={{ borderColor: "#fff" }}
            notAuthorizedView={
              <View style={styles.noPermView}>
                <Title numberOfLines={1} style={styles.noPermissionText}>
                  Camera Permission Required
                </Title>
              </View>
            }
            permissionDialogTitle={"Info"}
            permissionDialogMessage={"Need camera permission"}
            cameraTimeout={10000}
            cameraTimeoutView={
              <View style={styles.noPermView}>
                { !I18nManager.isRTL &&
                  <Title numberOfLines={2} style={styles.noPermissionText}>
                    Scanner is in standby mode {"\n"}Tap here to re-activate
                  </Title>
                }
                {I18nManager.isRTL &&
                  <Title numberOfLines={2} style={styles.noPermissionText}>
                    {"الماسح في وضع السكون. اضغط هنا لتفعيله"}
                  </Title>
                }
              </View>
            }
          />
        }

        {idleScan && (
          <Image
            style={[
              styles.screenContainer,
              { position: "absolute", top: "30%" },
            ]}
            source={require("./images/mahally.png")}
          />
        )}
      </View>

      {isDineInModalVisible && (
        <ScanModal
          onMenuCardPress={onModalOptionSelected}
          isScanModalVisible={isDineInModalVisible}
          toggleScanModal={() => setIsDineInModalVisible(!isDineInModalVisible)}
          showPre={false}
        />
      )}
    </View>
  );
};

ScanView.propTypes = {};

export default ScanView;
