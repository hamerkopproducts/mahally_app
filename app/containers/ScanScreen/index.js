import React, { useEffect, useState } from 'react';

import NetInfo from "@react-native-community/netinfo";
import { connect } from 'react-redux';
import ScanView from './ScanView';
import { bindActionCreators } from "redux";

import appTexts from "../../lib/appTexts";
import functions from "../../lib/functions"
import * as cartActions from "../../actions/cartActions";
import * as homeActions from "../../actions/homeActions";
import _ from "lodash";
const ScanScreen = (props) => {
  //will focus
  useEffect(() => {
    return props.navigation.addListener('focus', () => {
      NetInfo.fetch().then(state => {
        if (state.isConnected) {
  
        } else {
          functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
        }
      });
    });
  }, [props.navigation]);

  //mounted
  useEffect(() => handleComponentMounted(), []);

  const handleComponentMounted = () => {
    NetInfo.fetch().then(state => {
      if ( ! state.isConnected) {
        functions.displayAlert(null, appTexts.ALERT_MESSAGES.noInternet);
      }
    });
  }

  //unmount
  useEffect(() => { return () => { handleComponentUnmount(); } }, []);

  const handleComponentUnmount = () => {
   
  };
 
  //updated
  useEffect(() => handleComponentUpdated());

  const handleComponentUpdated = () => {
  
  };

  let route_params =  {};
  try {
    route_params = Object.assign({}, props.route.params);
  } catch(err) {
    route_params = {};
  }
  if(typeof route_params == 'undefined') {
    route_params = {};
  }
  
  return (
    <ScanView
      navigation={props.navigation}
      selectRestaurant={props.selectRestaurant}
      selectedRestaurant={props.selectedRestaurant}
      _params={route_params}
      addToCart={props.addToCart}
      is_notification_active={props.is_notification_active}
      languageSwitchScreen={props.languageSwitchScreen}
      notification_badge={props.notification_badge}
    />
  );

};


const mapStateToProps = (state, props) => {
  return {
    selectedRestaurant: state.cartReducer.selectedRestaurant,
    is_notification_active: state.homeReducer.is_notification_active,
    notification_badge:  _.get(state, 'homeReducer.notificationbadge', ''),
  }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    selectRestaurant: cartActions.selectRestaurant,
    addToCart: cartActions.cartUpdate,
    languageSwitchScreen: homeActions.languageSwitchScreen
  }, dispatch)
};

const scanScreenWithRedux = connect(
  mapStateToProps,
  mapDispatchToProps
)(ScanScreen);

scanScreenWithRedux.navigationOptions = ({ navigation }) => ({
  header: null
});

export default scanScreenWithRedux;
