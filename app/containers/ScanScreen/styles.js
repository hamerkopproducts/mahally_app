import { StyleSheet } from "react-native";
import globals from "../../lib/globals"

let headerButtonContainerWidth = globals.SCREEN_SIZE.width - (globals.MARGIN.marginTen*2);
let headerButtonWidth = (headerButtonContainerWidth-(globals.MARGIN.marginTen*2)) / 3;

const images = {
  
};

const styles = StyleSheet.create({
  screenMain:{
      flex:1,
      flexDirection:'column',
      backgroundColor: globals.COLOR.screenBackground
  },
  screenContainer:{
    flex:1,
    marginTop:  globals.INTEGER.heightTen,
    flexDirection: 'column',
    backgroundColor: globals.COLOR.white,
    alignItems: 'center'
  },
  headerButtonContianer:{
    flexDirection: 'row',
    width: headerButtonContainerWidth,
    height: globals.INTEGER.heightFifty,
    alignItems: 'center'
  },
  headerButton:{
    width: headerButtonWidth,
    height: globals.INTEGER.heightThirty
  },
  headerButtonMargin:{
    marginLeft: globals.MARGIN.marginTen
  },
  noPermView: {
    height: '100%',
    backgroundColor: '#F6F4F4',
    alignItems: 'center',
    justifyContent: 'center'
  },
  noPermissionText: {
    fontFamily: globals.FONTS.helvetica,
    fontSize:14,
  }
});

export { images, styles };
