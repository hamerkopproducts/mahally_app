import React, { Component } from "react";
import {
  View,
  Image,
  StatusBar,
  Animated,
  Easing,
  I18nManager,
} from "react-native";

import { styles, images } from "./styles";
import globals from "../../lib/globals";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import * as homeActions from "../../actions/homeActions";
import ServiceWrapperAwait from '../../service/ServiceWrapperAwait';

class Splash extends Component {

  constructor(props) {
    super(props);
    this.state = {
      ready: true,
      fadeValue: new Animated.Value(.5),
      SlideInLeft: new Animated.Value(0),
      SlideInBottom: new Animated.Value(0),
    }
  }

  switchLang = (_data) => {
    const sAsyncWrapper = new ServiceWrapperAwait();
    sAsyncWrapper.post('user-app/lang', _data);
  }

  componentDidMount() {
    setTimeout(() => {
      if(this.props.selectedLanguage) {
        this.switchLang({lang: I18nManager.isRTL ? 'ar' : 'en'});
      }
      if(this.props.isLogged || this.props.guestLogin)
      {
        const to_screen = this.props.language_switch_screen;
        this.props.languageSwitchScreen('home');
        if(to_screen) {
          try {
            // home orders offer profile scan
            switch(to_screen) {
              case 'home' :
                this.props.navigation.navigate('TabNavigator' , { screen: 'HomeScreen' });
                break;
              case 'orders' :
                this.props.navigation.navigate('TabNavigator' , { screen: 'MyOrders' });
                break;
              case 'offer' :
                this.props.navigation.navigate('TabNavigator' , { screen: 'MyOffers' });
                break;
              case 'profile' :
                this.props.navigation.navigate('TabNavigator' , { screen: 'MyProfile' });
                break;
              case 'scan' :
                this.props.navigation.navigate('TabNavigator' , { screen: 'Scan' });
                break;
              default:
                this.props.navigation.navigate('TabNavigator' , { screen: 'HomeScreen' });
            }
          } catch(err) {
            this.props.navigation.navigate('TabNavigator' , { screen: 'HomeScreen' });
          }
        } else {
          this.props.navigation.navigate('TabNavigator' , { screen: 'HomeScreen' });
        }
      }
      else if (this.props.selectedLanguage == null) {
        this.props.navigation.navigate("LangStackNavigator");
      } else {
        this.props.navigation.navigate("LoginStackNavigator");
      }
    }, 2000);
    this._start(.2);
    setTimeout(() => {
      this._start(1);
    }, 1000);
    this.animate_top();
    this.animate_bottom();
  }

  animate_top() {
    return Animated.parallel([
      Animated.timing(this.state.SlideInLeft, {
        toValue: 1,
        duration: 1500,
        delay: 200,
        useNativeDriver: true,
      }),
    ]).start();
  }

  animate_bottom() {
    return Animated.parallel([
      Animated.timing(this.state.SlideInBottom, {
        toValue: 1,
        duration: 1300,
        delay: 700,
        useNativeDriver: true,
      }),
    ]).start();
  }

  _start = (_to) => {
    return Animated.parallel([
      Animated.timing(this.state.fadeValue, {
        toValue: _to,
        duration: 1000,
        delay: 100,
        useNativeDriver: true,
      }),
    ]).start();
  };
  

  render() {
    let { fadeValue, SlideInLeft, SlideInBottom } = this.state;
    return (
      <View style={styles.screenMain}>

        <StatusBar
          barStyle="dark-content"
          backgroundColor={globals.COLOR.screenBackground}
        />

        <View style={styles.mahally}>
          <Animated.View
            style={{
              opacity: fadeValue,
              // flex: 1,
              height: 244,
              width: 155,
              borderRadius: 12,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Image
              source={images.mahallyLogo}
              style={styles.mahallyDesign}
            ></Image>
          </Animated.View>
        </View>

        <View style={styles.imageContainer}>
          <Animated.View
            style={{
              transform: [
                {
                  translateY: SlideInLeft.interpolate({
                    inputRange: [0, 1],
                    outputRange: I18nManager.isRTL ? [-50, 130] : [-50, 130],
                  }),
                },
                {
                  translateX: SlideInLeft.interpolate({
                    inputRange: [0, 1],
                    outputRange: I18nManager.isRTL ? [250, 0] : [-250, 0],
                  }),
                },
              ],
              flex: 0.1,
              position: "relative",
              width: 300,
              height: 200,
              borderRadius: 12,
              justifyContent: "center",
            }}
          >
            <Image source={images.teaImage} style={styles.logo} />
          </Animated.View>
        </View>

        <View style={styles.design}>
          <Animated.View
            style={{
              transform: [
                {
                  translateY: SlideInBottom.interpolate({
                    inputRange: [0, 1],
                    outputRange: [260, -20],
                  }),
                },
              ],
              flex: 1,
              marginTop: hp("10%"),

              width: 250,
              height: 200,
              resizeMode: "contain",
              borderRadius: 12,
              justifyContent: "center",
            }}
          >
            <Image source={images.foodImage} style={styles.greenDesign}></Image>
          </Animated.View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state, props) => {
  return {
    isLogged: state.loginReducer.isLogged,
    selectedLanguage: state.loginReducer.selectedLanguage,
    guestLogin: state.loginReducer.guestLogin,
    language_switch_screen: state.homeReducer.language_switch_screen,
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    {
      languageSwitchScreen: homeActions.languageSwitchScreen,
    },
    dispatch
  );
};

const splashWithRedux = connect(mapStateToProps, mapDispatchToProps)(Splash);

splashWithRedux.navigationOptions = ({ navigation }) => ({
  header: null,
});

export default splashWithRedux;
