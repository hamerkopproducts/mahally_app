import { StyleSheet,I18nManager } from "react-native";
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
  } from "react-native-responsive-screen";
import globals from "../../lib/globals";
let headerButtonContainerWidth =
  globals.SCREEN_SIZE.width - globals.MARGIN.marginTen * 2;
let headerButtonWidth =
  (headerButtonContainerWidth - globals.MARGIN.marginTen * 2) / 3;
const images = {
  teaImage: require("../../assets/images/chooseLanguage/tea.png"),
  foodImage:require('../../assets/images/chooseLanguage/Bottom.png'),
  mahallyLogo:require('../../assets/images/chooseLanguage/MLogo.png'),
};
const styles = StyleSheet.create({
  screenMain:{
    flex:1,
    backgroundColor: globals.COLOR.screenBackground
},
bgImage:{
  position: 'absolute',
  top: 0,
  width: globals.SCREEN_SIZE.width,
  height: globals.SCREEN_SIZE.height
},
screenMainContainer:{
  position: 'absolute',
  top: 0,
  width: globals.SCREEN_SIZE.width,
  height: globals.SCREEN_SIZE.height,
  alignItems: 'center',
},
screenContainer:{
  flex:1,
  backgroundColor: globals.COLOR.transparent,
  marginBottom: globals.INTEGER.screenBottom
},
imageContainer: {
  flex: 1,
  position:"relative",
  alignSelf:"flex-start"
  
},
design:{
  alignItems:"flex-start",
},
greenDesign:{
  width:180,
  height:260,
  resizeMode:'contain'
},
logoi: {
  width: 170,
  height: 170,
  resizeMode: 'contain'
},
imageContainer: {
  flex: 0.4,
  // position:"relative",
  alignSelf:"flex-start"
  //  marginTop:hp('1%'),
  
  },
  logo: {
    width: 300,
    height: 200,
    resizeMode: 'cover',
    marginBottom:hp('15%'),
    transform: [{ scaleX: I18nManager.isRTL ? -1 : 1 }]
  },
design:{
  bottom:0,
  left:0,
  margin:0,
  position:"absolute",
  marginHorizontal:wp('20%'),
  //marginTop:hp('10%'),
//  Top:20
},
greenDesign:{
  
  marginTop:hp('10%'),
  width:250,
  height:200,
  resizeMode:'contain'
},
mahally:{
  justifyContent:"center",
  alignItems:"center",
    // marginVertical:hp('20%'),
    position: 'absolute',
    height: '100%',
    alignSelf: 'center'
},
mahallyDesign:{
  // marginTop:hp('7%'),
  // alignItems:"center",
  width:244,
  height:155,
  resizeMode:'contain'
},
mahally1Design:{
  marginTop:hp('3%'),
  width:300,
  height:150,
  resizeMode:'contain'
},
});

export { images, styles };
