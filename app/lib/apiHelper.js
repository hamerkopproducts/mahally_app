import globals from "./globals"
import {ERROR_SUFFIX, SUCCESS_SUFFIX} from '../actions/types';
import NetworkUtils from '../utility/NetworkUtility';
import callApi from './callApi'
let productionURL = 'https://mahallyapp.com/api/';
let developmentURL = 'https://mahally.ourdemo.online/api/';

const under_dev = false;
let baseURL = under_dev === true ? developmentURL : productionURL;

let getTermsAPI = 'user-app/terms-and-conditions';
let getPrivacyAPI='user-app/privacy-policies';
let getAboutAPI=' user-app/aboutas';
let getReadyForDeliveryAPI = 'order/redy-for-delivery';
let getfaqAPI = 'user-app/faq';

let getOtpAPI = 'user-app/login';
let loginAPI = 'user-app/verify';
let resendAPI = 'user-app/resend';
let offersAPI = 'user-app/offers';
let menuDetailsAPI = 'user-app/item/';
let restDetailsAPI = 'user-app/restaurent/';
//let getReadyForDeliveryAPI = 'order/redy-for-delivery';
let getEditUserAPI = 'user-app/profile';
let geUpdateUserDetailsAPI = 'user-app/profile/update';
let getLogoutAPI='user-app/user-logout';
let getLoyaltyMealsAPI='user-app/loyality-meal';
let requestSupportApi = 'user-app/support';
let restTypesApi = 'user-app/restaurant-type';
let howToUse = 'user-app/useapp';
let NotificationCount = 'user-app/notification/count';
let NotificationBadgeReset = 'user-app/notification/badge';
let getSmsAPI = 'user-app/settings';

const apiHelper = {

  getBaseUrl: () => {
    return baseURL;
  },

  getAPIHeaderForDelete: (params,token) => {
  	const AuthStr = 'Bearer '.concat(token);
    let headerConfig = {
      headers: {
        'Content-Type': 'application/json',
        Authorization: AuthStr
      },
      data: params
    };
    return headerConfig;
  },
  getAPIHeader: (token) => {
  	const AuthStr = 'Bearer '.concat(token);
    let headerConfig = { headers: { Authorization: AuthStr } };
    return headerConfig;
  },
  getLoginAPIHeader: () => {
    let headerConfig = { headers: { 'Content-Type': 'application/json', 'Accept': 'application/json' } };
    return headerConfig;
  },
  getOtpAPI:()=> {
    return baseURL + getOtpAPI;
  },

  getFaqsAPI: () => {
    return baseURL + getfaqAPI 

  },

  getSettingsAPI: () => {
    return getSmsAPI;
  },
  
  getTermsAndConditionAPI: () => {
    return baseURL + getTermsAPI 

  },
  getPrivacyPolicyAPI: () => {
    return baseURL + getPrivacyAPI 

  },
  getAboutUsAPI: () => {
    return baseURL + getAboutAPI 

  },
  loginAPI: () => {
    return baseURL + loginAPI;
  },
  resendOtpAPI: () => {
    return baseURL + resendAPI;
  },
  getReadyForDeliveryAPI: () => {
    return baseURL + getReadyForDeliveryAPI;
  },
  getOffersAPI: () => {
    return baseURL + offersAPI;
  },
  getMenuDetailsAPI: (id) => {
    return baseURL + menuDetailsAPI + id;  
  },
  getUserEditDetailsAPI: () => {
    return baseURL + getEditUserAPI 

  },
  getRestDetailsAPI: (id) => {
    return baseURL + restDetailsAPI + id

  },
  setupdateUserDetailsApi: () => {
    return baseURL + geUpdateUserDetailsAPI 

  },
  logoutApi: () => {
    return baseURL + getLogoutAPI 

  },
  getLoyaltyAPI: () => {
    return baseURL + getLoyaltyMealsAPI 

  },
  RequestSupportApi: ()=>{
    return baseURL + requestSupportApi;
  },
  getRestTypeAPI: ()=>{
    return baseURL + restTypesApi;
  },
  getHowToUseAPI: ()=>{
    return baseURL + howToUse;
  },
  getNotificationCount: ()=>{
    return baseURL + NotificationCount;
    
  },
  getNotificationBadgeReset: ()=>{
    return baseURL + NotificationBadgeReset;
    
  },

}

export default apiHelper;


export const executeApiAction = (
  method,
  api,
  action,
  authorize = false,
  data = {},
  onSuccess = () => {},
  onError = () => {},
) => {
  const actionSuccess = action + SUCCESS_SUFFIX;
  const actionError = action + ERROR_SUFFIX;

  return async (dispatch, getState) => {
    try {
      dispatch({type: action});

      const isConnected = await NetworkUtils.isNetworkAvailable();
      if (!isConnected) {
        throw new Error('Network Error');
      }

      const authorizationToken =
        authorize && getState().loginReducer?.userData?.data
          ? getState().loginReducer.userData.data.access_token
          : '';

      const response = await callApi(method, api, authorizationToken, data);
      if (response.status !== 200) {
        throw new Error();
      }

      dispatch({type: actionSuccess, payload: response.data});
      onSuccess(response.data);
    } catch (error) {
      // if (error && error.message && error.message === 'Network Error')
      //   dispatch({type: actionError, payload: {data: {sucess: false, error: {msg: 'No Internet Connection!'}}}});

      if (error.response) {
        dispatch({
          type: actionError,
          payload: error.response,
        });

        onError(error.response);
      }
    }
  };
};
