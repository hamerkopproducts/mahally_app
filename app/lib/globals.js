import { Platform, Dimensions } from 'react-native';
let { height, width } = Dimensions.get('window');
import { getStatusBarHeight, getBottomSpace } from 'react-native-iphone-x-helper';
import { displayName as appName } from '../../app.json';

let headerHeight= 95;
let footerTabBarHeight = 70;//80 before
let screenMargin = 15;
let screenPaddingFromFooter = 0;//5 before

module.exports = {
  DEVICE_TYPE: Platform.OS === "ios" ? 1 : 2,
  DEVICE_TYPE_STRING: Platform.OS,
  SPLASH_DELAY: 3000,
  SCREEN_SIZE: {
    width: width,
    height: height
  },
  STRING: {
    appName: appName
  },
  ALERT_MESSAGES: {
    noInternet: 'No Network Connection'
  },
  COLOR: {
    headerColor: '#ffffff',
    transparent: 'transparent',
    screenBackground:'#fcfcfc',
    tabUnderLineColor:'#ff8001',
    textColorGreen:'#259245',
    linkTextColor:'#0090E5',
    borderColor: '#cccccc',
    textColorBlack: '#000000',
    lightTextColor: '#707070',
    white: '#ffffff',
    black: '#000000',
    redTextColor:'#DB3236',
    themeGreen:'#2eb781',
    lightTextColor:'#9ea1a7',
    semiLightTextColor:'#6f6f6f',
    blackTextColor:'#000000',
    borderColor:'#cfcfcf',
    textColor:'#233249',
    themeColor:'#2db881',
    purple:'#612467',
    Text:'#000000',
    greyText:'#707070',
    lightBlueText:'#53627c',
    greyBackground:'#f4f7f7',
    peach:'#F5A389'

  },
  INTEGER: {
    headerHeight: headerHeight,
    footerTabBarHeight: footerTabBarHeight,
    screenPaddingFromFooter: screenPaddingFromFooter,
    screenWidthWithMargin: width - (2 * screenMargin),
    screenContentHeight: height - (footerTabBarHeight + headerHeight + getStatusBarHeight('safe') + getBottomSpace() + screenPaddingFromFooter),
    leftRightMargin: screenMargin,
    statusBarHeight: getStatusBarHeight('safe'),
    bottomSpace: getBottomSpace()
  },
  FONTS: {
    avenirBlack: 'Avenir-Black',
    avenirLight: 'Avenir-Light',
    avenirMedium: 'Avenir-Medium',
    avenirHeavy: 'Avenir-Heavy',
    avenirRoman: 'Avenir-Roman',
    openSansBold:'OpenSans-Bold',
    openSansLight:'OpenSans-Light',
    openSansSemiBold:'OpenSans-SemiBold',
    helvetica:'Helvetica',
    helveticaOblique:'Helvetica-Oblique',
    helveticaLight:'helvetica-light-587ebe5a59211',
    helveticaBoldOblique:'Helvetica-BoldOblique',
    helveticaBold:'Helvetica-Bold',
    notokufiArabic:'NotoKufiArabic',
    notokufiarabicBold:'NotoKufiArabic-Bold',

  },
  FONTSIZE: {
    fontSizeSeven: 7,
    fontSizeEight: 8,
    fontSizeNine: 9,
    fontSizeTen: 10,
    fontSizeEleven: 11,
    fontSizeTwelve: 12,
    fontSizeThirteen: 13,
    fontSizeFourteen: 14,
    fontSizeFifteen: 15,
    fontSizeSixteen: 16,
    fontSizeSeventeen: 17,
    fontSizeEighteen: 18,
    fontSizeTwenty: 20,
    fontSizeTwentyTwo: 22,
    fontSizeTwentyFour: 24
  },
  MARGIN: {
    marginOne: 1,
    marginTwo: 2,
    marginThree: 3,
    marginFour: 4,
    marginFive: 5,
    marginSix: 6,
    marginSeven: 7,
    marginEight: 8,
    marginNine: 9,
    marginTen: 10,
    marginTwelve: 12,
    marginThirteen: 13,
    marginFifteen: 15,
    marginEighteen: 18,
    marginTwenty: 20
  }
};
