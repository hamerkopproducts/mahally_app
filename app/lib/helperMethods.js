import _ from 'lodash'
import { I18nManager } from "react-native";
import moment from 'moment';

export const menuDetailsMapper = (data) => {
    return _.map(data, item => {
        return {
            cover_photo:  _.get(item, ['cover_photo'], '') ,

            item_name:  I18nManager.isRTL ?
            _.get(item, ['lang','ar','name'], ''):
            _.get(item, ['lang','en','name'], ''),

            description:  I18nManager.isRTL ?
            _.get(item, ['lang','ar','description'], ''):
            _.get(item, ['lang','en','description'], ''),

            discount_price:  _.get(item, ['discount_price'], null) ,
            price:  _.get(item, ['price'], '') ,
            calory:  _.get(item, ['calory'], '') ,
        }
    })
} 

export const addOnsDetailsMapper = (data) => {
    return _.map(data, item => {
        return {
            cover_photo:  _.get(item, ['image_path'], '') ,
            item_name:  I18nManager.isRTL ?
                _.get(item, ['lang','ar','name'], ''):
                _.get(item, ['lang','en','name'], ''),
            price:  _.get(item, ['price'], '') ,
            id: _.get(item, ['id'], ''),
            item_id: _.get(item, ['pivot', 'menu_item_id'], ''),
            addon_id: _.get(item, ['pivot', 'addon_id'], '')
        }
    })
} 
export const ingredientsDetailsMapper = (data) => {
    return _.map(data, item => {
        return {
            cover_photo:  _.get(item, ['image_path'], '') ,

            item_name:  I18nManager.isRTL ?
            _.get(item, ['lang','ar','name'], ''):
            _.get(item, ['lang','en','name'], '')
        }
    })
} 
export const attributesDetailsMapper = (data) => {
    return _.map(data, item => {
        return {
            attributes_value : I18nManager.isRTL ?
            _.get(item, ['attribute_val','lang','1','name'], ''):
            _.get(item, ['attribute_val','lang','0','name'], '') ,

            attributes_name: I18nManager.isRTL ?
            _.get(item, ['lang','ar','name'], ''):
            _.get(item, ['lang','en','name'], '') 

           
        }
    })
} 

export const relatedItemsDetailsMapper = (data) => {
    return _.map(data, item => {
        return {
            cover_photo:  _.get(item, ['cover_photo'], '') ,
            item_id: item.id,
            item_name:  I18nManager.isRTL ?
            _.get(item, ['lang','ar','name'], ''):
            _.get(item, ['lang','en','name'], ''),

            timing: startTime(item.timing),

            discount_price:  _.get(item, ['discount_price'], null) ,
            price:  _.get(item, ['price'], '') ,
            calory:  _.get(item, ['calory'], '') ,
            hasAddOns: _.get(item, ['item_addons'], '').length > 0 ? true : false
        }
    })
} 
export const startTime = (data) => {
    return _.map(data, item => {
        return {
            start_time: _.get(item, ['start_time'], '') ,
            end_time: _.get(item, ['end_time'], '') ,
        }
    })
} 
export const relatedItemsTimeDetailsMapper = (data) => {
    return _.map(data, item => {
        return {
            start_time: _.get(item, ['timing',0,'start_time'], '') ,
            end_time: _.get(item, ['timing',0,'end_time'], '') ,
        }
    })
} 
export const restDetailsMapper = (data) => {
    return _.map(data, item => {
        return {
            rest_type_name:  I18nManager.isRTL ?
            _.get(item, ['lang','1','name'], ''):
            _.get(item, ['lang','0','name'], ''),
        }
    })
} 

export const offersCardMapper = (data) => {
    return _.map(data, item => {
        return {
            cover_photo: I18nManager.isRTL ?
                _.get(item, ['lang', 'ar', 'image_path'], '') :
                _.get(item, ['lang', 'en', 'image_path'], ''),

            item_name: I18nManager.isRTL ?
                _.get(item, ['lang', 'ar', 'name'], '') :
                _.get(item, ['lang', 'en', 'name'], ''),

                pro_name: I18nManager.isRTL ?
                _.get(item, ['items','lang', '1', 'name'], '') :
                _.get(item, ['items','lang', '0', 'name'], ''),

                pre_order:_.get(item, ['restaurent', 'pre_order'], ''),

            price: _.get(item, ['items', 'price'], 0),
            discount_price: _.get(item, ['items', 'discount_price'], null),
            
            discount_value: _.get(item, ['discount_value'], ''),
            discount_type: _.get(item, ['discount_type'], ''),
            remaining_days: _.get(item, ['remaining_days'], ''),

            item_id: _.get(item, ['items', 'id'], ''),
            offer_id: _.get(item, ['id'], ''),
            restaurant_id: _.get(item, ['restuarants_id']),
            time_status: _.get(item, ['time_status'], ''),

            available_time: item?.items?.available_time || [],

            vat_includes:_.get(item, ['restaurent', 'vat_includes'], ''),
            vat_percentage:_.get(item, ['restaurent', 'vat_percentage'], ''),
        }
    })
} 

export const restOfferDetailsMapper = (data) => {
    
    return _.map(data, item => {
        return {
            cover_photo: I18nManager.isRTL ?
                _.get(item, ['lang', '1', 'image_path'], '') :
                _.get(item, ['lang', '0', 'image_path'], ''),

            restuarants_id: _.get(item, ['restuarants_id'], ''),
            item_name: I18nManager.isRTL ?
                _.get(item, ['lang', '1', 'name'], '') :
                _.get(item, ['lang', '0', 'name'], ''),

            pro_name: I18nManager.isRTL ?
                _.get(item, ['items','lang', '1', 'name'], '') :
                _.get(item, ['items','lang', '0', 'name'], ''),
            pre_order:_.get(item, ['restaurent', 'pre_order'], ''),
            price: _.get(item, ['items', 'price'], 0),
            discount_price: _.get(item, ['items', 'discount_price'], null),

            discount_value: _.get(item, ['discount_value'], ''),
            discount_type: _.get(item, ['discount_type'], ''),
            remaining_days: _.get(item, ['remaining_days'], ''),

            item_id: _.get(item, ['menu_item_id'], ''),
            offer_id: _.get(item, ['id'], ''),
            available_time: item?.items?.available_time || []
        }
    })
} 
export const restMealsMapper = (data) => {
    return _.map(data, item => {
        return {
            cover_photo: I18nManager.isRTL ?
                _.get(item, ['lang', '1', 'image_path'], '') :
                _.get(item, ['lang', '0', 'image_path'], ''),
                item_name: I18nManager.isRTL ?
                _.get(item, ['lang', '1', 'name'], '') :
                _.get(item, ['lang', '0', 'name'], ''),

                discount_value: _.get(item, ['discount_value'], ''),
                discount_type: _.get(item, ['discount_type'], ''),
                remaining_days: _.get(item, ['remaining_days'], ''),
    
                item_id: _.get(item, ['id'], ''),
        }
    })
} 
export const restTermsDetailsMapper = (data) => {
    
    return _.map(data, item => {
        return {
            cover_photo: I18nManager.isRTL ?
                _.get(item, ['image_path'], '') :
                _.get(item, ['image_path'], ''),

                item_name: I18nManager.isRTL ?
                _.get(item, ['lang', 'ar', 'name'], '') :
                _.get(item, ['lang', 'en', 'name'], ''),

                item_image: _.get(item, ['image_path'], ''),

        }
    })
} 
export const restFacilitiesDetailsMapper = (data) => {
    return _.map(data, item => {
        return {
            cover_photo:  _.get(item, ['image_path'], ''),

                item_name: I18nManager.isRTL ?
                _.get(item, ['lang', '1', 'name'], '') :
                _.get(item, ['lang', '0', 'name'], ''),
        }
    })
} 
export const restSliderImagesMapper = data => {
    return _.map(data, item => _.get(item, ['image_path'], ''))
  }
export const restLoyaltyMealsDetailsMapper = (data) => {
    let now = moment();
    return _.map(data, item => {

        let timeDiff = moment( _.get(item, ['expire_in'], moment().format('H:m') ) ).endOf('day') - now;
        let dur = moment.duration(timeDiff);
        let rem_txt = dur.days();

        return {
            rest_name: I18nManager.isRTL ?
            _.get(item, ['retaurant','lang', '1', 'name'], '') :
            _.get(item, ['retaurant','lang', '0', 'name'], ''),

            location: _.get(item, [ 'retaurant', 'location'], ''),
            restuarants_id: _.get(item, [ 'restuarants_id'], ''),

            item_id: _.get(item, ['item', 'id'], ''),
            loyality_id: _.get(item, ['id'], ''),
            no_offer_available: _.get(item, ['no_offer_available'], 0),

            pro_name: I18nManager.isRTL ?
                _.get(item, ['item','lang', '1', 'name'], '') :
                _.get(item, ['item','lang', '0', 'name'], ''),

            pre_order:_.get(item, ['retaurant', 'pre_order'], ''),

            location: _.get(item, ['location'], ''),
            cover_photo: _.get(item, ['item','cover_photo'], ''),
            item_name: I18nManager.isRTL ?
                _.get(item, ['lang', '1', 'name'], '') :
                _.get(item, ['lang', '0', 'name'], ''),
            order_count: _.get(item, ['already_done'], ''),
            remaing_count: _.get(item, ['order'], ''),
            remaining_message: _.get(item, ['remaining_orders'], ''),
            remaining_days: rem_txt,
            available_time: item?.item?.available_time || [],

            vat_includes:_.get(item, ['restaurent', 'vat_includes'], ''),
            vat_percentage:_.get(item, ['restaurent', 'vat_percentage'], ''),
        }
    })
} 
export const restProfileLoyaltyMealsDetailsMapper = (data) => {
    let now = moment();
    return _.map(data, item => {

        let timeDiff = moment( _.get(item, ['expire_in'], moment().format('H:m') ) ).endOf('day') - now;
        let dur = moment.duration(timeDiff);
        let rem_txt = dur.days();

        return {
            rest_name: I18nManager.isRTL ?
            _.get(item, ['retaurant','lang', '1', 'name'], '') :
            _.get(item, ['retaurant','lang', '0', 'name'], ''),
            restaurant_id: _.get(item, ['retaurant', 'id'], ''),
            restaurants_id: _.get(item, ['retaurant', 'id'], ''),
            location: _.get(item, [ 'retaurant', 'location'], ''),
            time_status: _.get(item, [ 'time_status'], ''),

            loyality_id: _.get(item, ['id'], ''),
            item_id: _.get(item, ['item', 'id'], ''),
            no_offer_available: _.get(item, ['no_offer_available'], 0),

            cover_photo: _.get(item, ['item','cover_photo'], ''),
            item_name: I18nManager.isRTL ?
                _.get(item, ['lang', 'ar', 'name'], '') :
                _.get(item, ['lang', 'en', 'name'], ''),
            pro_name: I18nManager.isRTL ?
                _.get(item, ['item','lang', '1', 'name'], '') :
                _.get(item, ['item','lang', '0', 'name'], ''),

            pre_order:_.get(item, ['retaurant', 'pre_order'], ''),
            order_count: _.get(item, ['already_done'], ''),
            remaing_count: _.get(item, ['order'], ''),
            remaining_message: _.get(item, ['remaining_orders'], ''),
            remaining_days: rem_txt,

            available_time: item?.item?.available_time || []
        }
    })
} 
export const restTypesMapper = (data) => {
    return _.map(data, item => {
        return {
            rest_type: I18nManager.isRTL ?
            _.get(item, ['lang', 'ar', 'name'], '') :
            _.get(item, ['lang', 'en', 'name'], ''),

            id: _.get(item, [ 'id'], ''),
}
    })
} 
export const howtouseDetailsMapper = (data) => {
    return _.map(data, item => {
        return {
            title: I18nManager.isRTL ?
            _.get(item, ['lang', 'ar', 'title'], '') :
            _.get(item, ['lang', 'en', 'title'], ''),

            text: I18nManager.isRTL ?
            _.get(item, ['lang', 'ar', 'content'], '') :
            _.get(item, ['lang', 'en', 'content'], ''),

            image: I18nManager.isRTL ?
            _.get(item, ['lang', 'ar', 'screen_shot'], '') :
            _.get(item, ['lang', 'en', 'screen_shot'], ''),

}
    })
} 
export const restTypesIDMapper = data => {
    return _.map(data, item => _.get(item, [ 'id'], ''))
  }