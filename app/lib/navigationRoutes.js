import React, { Component } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import globals from "./globals"
import appTexts from "./appTexts"

//Login Screens
import LoginScreen from '../containers/LoginScreen'

//Appintrologin Screens
import AppIntrosliderloginScreen from '../containers/AppIntrosliderloginScreen'
import SplashScreen from '../containers/SplashScreen'
import ChooseLanguageScreen from '../containers/ChooseLanguageScreen'
import OtpScreen from '../containers/OtpScreen'
import AboutScreen from '../containers/AboutScreen'
import LoyaltyScreen from '../containers/LoyaltyScreen'
import RestaurantDetailScreen from '../containers/RestaurantDetailScreen'
import ProductDetailScreen from '../containers/ProductDetailScreen'

import ChatScreen from '../containers/ChatScreen'
import HomeScreen from '../containers/HomeScreen'
// import MenuDetailScreen from '../containers/MenuDetailScreen';
import PrivacyScreen from '../containers/PrivacypolicyScreen';
import OrderDetail from '../containers/OrderDetail';

import OfferScreen from '../containers/OfferScreen'
import FaqScreen from '../containers/FaqScreen'
import TermsScreen from '../containers/TermsandconditionsScreen'
//MyOrdersScreen Tab Screen
import MyOrdersScreen from '../containers/MyOrdersScreen'
import NotificationScreen from '../containers/NotificationScreen'
import CheckoutScreen from '../containers/CheckoutScreen'

// ScanScreen Tab Screen
import ScanScreen from '../containers/ScanScreen'

//Profile Tab Screen
import ProfileScreen from '../containers/ProfileScreen'

//Footer Tab Item Style
import FooterTabItem from '../components/FooterTabItem'
// import ItemDetailScreen from '../containers/ItemDetailScreen'


//Splash stacks for screen navigation
const SplashStack = createStackNavigator();
const SplashStackNavigator = () => {
	return (
		<SplashStack.Navigator
			screenOptions={{
				headerShown: false
			}}
		>
			<SplashStack.Screen name="SplashScreen" component={SplashScreen} />
		</SplashStack.Navigator>
	)
}

//Language stacks for screen navigation
const LanguageStack = createStackNavigator();
const LanguageStackNavigator = () => {
	return (
		<LanguageStack.Navigator
			screenOptions={{
				headerShown: false
			}}
		>
			<LanguageStack.Screen name="ChooseLanguageScreen" component={ChooseLanguageScreen} />
		</LanguageStack.Navigator>
	)
}

//Login stacks for screen navigation
const LoginStack = createStackNavigator();
const LoginStackNavigator = () => {
	return (
		<LoginStack.Navigator
			screenOptions={{
				headerShown: false
			}}
		>
			 <LoginStack.Screen name="AppIntrosliderloginScreen" component={AppIntrosliderloginScreen} />
			 <LoginStack.Screen name="LoginScreen" component={LoginScreen} /> 
			<LoginStack.Screen name="OtpScreen" component={OtpScreen} /> 
			<LoginStack.Screen name="ProfileScreen" component={ProfileScreen} /> 
		</LoginStack.Navigator>
	)
}

// HomeTab stacks for screen navigation
const HomeStack = createStackNavigator();
const HomeTab = () => {
	return (
		<HomeStack.Navigator screenOptions={{ headerShown: false }}>
			<HomeStack.Screen name="HomeScreen" component={HomeScreen} />
		</HomeStack.Navigator>
	)
}
// MyOrdersTab stacks for screen navigation
const MyOrdersScreenStack = createStackNavigator();
const MyOrdersTab = () => {
	return (
		<MyOrdersScreenStack.Navigator screenOptions={{ headerShown: false }}>
			<MyOrdersScreenStack.Screen name="MyOrdersScreen" component={MyOrdersScreen} />
		</MyOrdersScreenStack.Navigator>
	)
}
// ScanTab stacks for screen navigation
const ScanScreenStack = createStackNavigator();
const ScanTab = () => {
	return (
		<ScanScreenStack.Navigator screenOptions={{ headerShown: false }}>
			<ScanScreenStack.Screen name="ScanScreen" component={ScanScreen} />
		</ScanScreenStack.Navigator>
	)
}
// ProfileTab stacks for screen navigation
const ProfileScreenStack = createStackNavigator();
const ProfileTab = () => {
	return (
		<ProfileScreenStack.Navigator screenOptions={{ headerShown: false }}>
			<ProfileScreenStack.Screen name="ProfileScreen" component={ProfileScreen} />
		</ProfileScreenStack.Navigator>
	)
}

 //OfferTab
const OfferScreenStack = createStackNavigator();
const OfferTab = () => {
	return (
		<OfferScreenStack.Navigator screenOptions={{ headerShown: false }}>
			<OfferScreenStack.Screen name="OfferScreen" component={OfferScreen} />
		</OfferScreenStack.Navigator>
	)
}

//Initialize bottom tab navigator
const DashboardTabRoutes = createBottomTabNavigator();

const TabNavigator = () => {
	return (
		<DashboardTabRoutes.Navigator tabBarOptions={{
			showLabel: false,
			style: {
				height: globals.INTEGER.footerTabBarHeight,
				borderWidth: 0,
				borderTopLeftRadius: 20,
				borderTopRightRadius: 20,
				borderTopWidth: 0.5,
				borderRightWidth: 0.5,
				borderLeftWidth: 0.5,
				backgroundColor: globals.COLOR.white,
				borderColor: "#ccc",
				shadowColor: "#000",
				shadowOffset: { width: 0, height: 0 },
				shadowOpacity: 0.2,
				shadowRadius: 1,
				elevation: 1,
				paddingBottom:'3%',
				
			}
		}}>
			<DashboardTabRoutes.Screen
				name="Home"
				component={HomeTab}
				options={{
					tabBarLabel: 'Home',
					tabBarIcon: ({ focused }) => (
						<FooterTabItem tabBarIndex={0} isFocused={focused} tabBarLabel={appTexts.FOOTER.home} />
					),
				}}
			/>
			
			<DashboardTabRoutes.Screen
				name="MyOffers"
				component={OfferTab}
				options={{
					tabBarLabel: 'Offers',
					
					tabBarIcon: ({ focused }) => (
						<FooterTabItem tabBarIndex={1} isFocused={focused} tabBarLabel={appTexts.FOOTER.offers}/>
					),
				}}
			/>
			<DashboardTabRoutes.Screen
				name="Scan"
				component={ScanTab}
				options={{
					tabBarLabel: '',
					tabBarIcon: ({ focused }) => (
						<FooterTabItem tabBarIndex={2} isFocused={focused} />
					),
				}}
			/>
			
			<DashboardTabRoutes.Screen
				name="MyOrders"
				component={MyOrdersTab}
				options={{
					tabBarLabel: 'Orders',
					tabBarIcon: ({ focused }) => (
						<FooterTabItem tabBarIndex={3} isFocused={focused} tabBarLabel={appTexts.FOOTER.orders} />
					),
				}}
			/> 
			
			 <DashboardTabRoutes.Screen
				name="MyProfile"
				component={ProfileTab}
				options={{
					tabBarLabel: 'Profile',
					tabBarIcon: ({ focused }) => (
						<FooterTabItem tabBarIndex={4} isFocused={focused} tabBarLabel={appTexts.FOOTER.profile}/>
					),
				}}
			/> 
		</DashboardTabRoutes.Navigator>

	);
};

//Main stacks for screen navigation
const MainScreenStack = createStackNavigator();
const MainScreenStackNavigator = () => {
	return (
		<MainScreenStack.Navigator
			screenOptions={{
				headerShown: false,
				gestureEnabled: false
			}}
		>
			<MainScreenStack.Screen name="SplashStackNavigator" component={SplashStackNavigator} />
			<MainScreenStack.Screen name="LangStackNavigator" component={LanguageStackNavigator} />
			<MainScreenStack.Screen name="LoginStackNavigator" component={LoginStackNavigator} />
			<MainScreenStack.Screen name="TabNavigator" component={TabNavigator} />
			 <MainScreenStack.Screen name="OrderDetail" component={OrderDetail} />
			<MainScreenStack.Screen name="NotificationScreen" component={NotificationScreen} />
			<MainScreenStack.Screen name="RestaurantDetailScreen" component={RestaurantDetailScreen} />
			<MainScreenStack.Screen name="CheckoutScreen" component={CheckoutScreen} />
			<MainScreenStack.Screen name="ChatScreen" component={ChatScreen} />
			 <MainScreenStack.Screen name="ProductDetailScreen" component={ProductDetailScreen} /> 
			<LoginStack.Screen name="PrivacyScreen" component={PrivacyScreen} />
			<LoginStack.Screen name="FaqScreen" component={FaqScreen} />
			<LoginStack.Screen name="TermsScreen" component={TermsScreen} />
			<LoginStack.Screen name="AboutScreen" component={AboutScreen} />
			<LoginStack.Screen name="LoyaltyScreen" component={LoyaltyScreen} />
			<LoginStack.Screen name="ProfileScreen" component={ProfileScreen} />
			<LoginStack.Screen name="LoginScreen" component={LoginScreen} /> 
			<LoginStack.Screen name="OtpScreen" component={OtpScreen} /> 

		</MainScreenStack.Navigator>
	)
}


export const LanguageContainer = () => {
	return (
		<NavigationContainer>
			<LanguageStackNavigator />
		</NavigationContainer>
	)
}


export const LoginContainer = () => {
	return (
		<NavigationContainer>
			<LoginStackNavigator />
		</NavigationContainer>
	)
}

export const HomeContainer = () => {
	return (
		<NavigationContainer>
			<MainScreenStackNavigator />
		</NavigationContainer>
	)
}