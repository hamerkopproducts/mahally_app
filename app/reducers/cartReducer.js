import * as ActionTypes from '../actions/types'

const defaultCartState = {
    cartAddedData: {},
    selectedRestaurant: {},
    coupon_code: {},
    postScanItemToAddData: {}
};

export default function cartReducer(state = defaultCartState, action) {
    switch (action.type) {
        case ActionTypes.POST_SCAN_ADD_ITEM_DATA:
            return Object.assign({}, state, {
                postScanItemToAddData: action.data
            });
        case ActionTypes.MODIFY_CART_DATA:
            return Object.assign({}, state, {
                cartAddedData: action.data
            });
        case ActionTypes.SELECT_RESTAURANT:
            return Object.assign({}, state, {
                selectedRestaurant: action.data
            });
        case ActionTypes.COUPON_CODE:
            return Object.assign({}, state, {
                coupon_code: action.data
            });
        default:
            return state;
    }
}
