import * as ActionTypes from '../actions/types'

const defaultEditUserState = {
    userData: {},
    editUserData: {},
    updateUserData: {},
    isLogged: false,
    selectedLanguage:null,
    isLoading: false,
    isLoggedOut: false,
    phone_to_verify: '',
    logoutflag: false,
    apiError: {}
};

export default function editUserReducer(state = defaultEditUserState, action) {
    switch (action.type) {
        case ActionTypes.EDIT_USER_LOADING:
            return Object.assign({}, state, {
                isLoading: true,
                isLogged: true
            });
        case ActionTypes.EDIT_USER_SUCCESS:
            return Object.assign({}, state, {
                editUserData: action.responseData,
                updateUserData: '',
                isLoading: false,
                isLogged: true
            });
        case ActionTypes.UPDATE_USER_SUCCESS:
            return Object.assign({}, state, {
                updateUserData: action.responseData,
                isLoading: false,
                isLogged: true
            });
        case ActionTypes.EDIT_USER_ERROR:
            return Object.assign({}, state, { 
                isLoading: false,
                apiError: action.error
            });
        case ActionTypes.PHONE_TO_VERIFY:
            return Object.assign({}, state, {
                phone_to_verify: action.responseData,
                isLoading: false,
            }); 
        case ActionTypes.LOGIN:
            return{
                isLoggedOut: false,
                isLoading: false
            }; 
        case ActionTypes.GUEST_LOGIN:
            return{
                isLoading: false
            }; 
        case ActionTypes.LOGOUT_SUCCESS:
            return {
                ...state,
                logoutflag: true ,
                error: undefined,
                success: undefined,
                isIntroFinished: false,
                isLoggedOut: true,

                userData: {},
                editUserData: {},
                updateUserData: {},
                isLogged: false,
                isLoading: false,
                phone_to_verify: '',
                apiError: {}
            };
        default:
            return state;
    }
}
