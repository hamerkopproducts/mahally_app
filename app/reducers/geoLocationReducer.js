import * as ActionTypes from "../actions/types";

const defaultLoginState = {
  geoLocation: { status: false, location: {} },
};

export default function geoLocationReducer(state = defaultLoginState, action) {
  switch (action.type) {

    case ActionTypes.GEO_LOCATION:
      return Object.assign({}, state, {
        geoLocation: action.data,
      });

    default:
      return state;
  }
}
