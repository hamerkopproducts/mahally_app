import * as ActionTypes from '../actions/types'
import { restTypesMapper } from '../lib/helperMethods'
import { restTypesIDMapper } from '../lib/helperMethods'

const defaultState = {
  ordersToDeliver: [],
  ordersToCollect: [],
  ordersToReturn: [],
  isLoading: false,
  restTypes: {},
  is_notification_active: false,
  language_switch_screen: 'home',
  notificationbadge: 'false'
};

export default function homeReducer(state = defaultState, action) {
  switch (action.type) {
    case ActionTypes.LANGUAGE_SWITCH_SCREEN:
      return Object.assign({}, state, {
        language_switch_screen: action.data,
      });
    case ActionTypes.NOTIFICATION_ACTIVE:
      return Object.assign({}, state, {
        is_notification_active: action.data,
      });
      case ActionTypes.NOTIFICATION_COUNT:
        return Object.assign({}, state, {
          isLoading: false,
          notificationbadge: action.responseData,
        });
    case ActionTypes.GET_ORDER_LOADING:
      return Object.assign({}, state, {
        isLoading: true,
      });
    case ActionTypes.GET_ORDER_ERROR:
      return Object.assign({}, state, {
        isLoading: false,
        error: action.error
      });
    case ActionTypes.GET_ORDER_TO_DELIVER_SUCCESS:
      return Object.assign({}, state, {
        isLoading: false,
        ordersToDeliver: action.responseData
      });
    case ActionTypes.GET_ORDER_TO_COLLECT_SUCCESS:
      return Object.assign({}, state, {
        isLoading: false,
        ordersToCollect: action.responseData
      });
    case ActionTypes.GET_ORDER_TO_RETURN_SUCCESS:
      return Object.assign({}, state, {
        isLoading: false,
        ordersToReturn: action.responseData
      });
    case ActionTypes.REST_TYPE_LOADING:
      return Object.assign({}, state, {
        isLoading: true,
        ordersToReturn: action.responseData
      });
    case ActionTypes.REST_TYPE_SUCCESS:
      return Object.assign({}, state, {
        isLoading: false,
        restTypes: restTypesMapper(action.responseData),
        restTypesID: restTypesIDMapper(action.responseData),

        // restTypes: action.responseData
      });
    case ActionTypes.REST_TYPE_ERROR:
      return Object.assign({}, state, {
        isLoading: false
      });
      case ActionTypes.LOGOUT_SUCCESS:
            return {
                ...state,
                
                ordersToDeliver: [],
  ordersToCollect: [],
  ordersToReturn: [],
  ordersToDeliver:[],
  isLoading: false,
  restTypes: {},
  restTypesID: {},
            };
    default:
      return state;
  }
}
