import { combineReducers } from 'redux';
import loginReducer from './loginReducer';
import homeReducer from './homeReducer';
import orderReducer from './orderReducer';
import termsPrivacyFaqReducer from './termsPrivacyFaqReducer';
import offersReducer from './offersReducer';
import editUserReducer from './editUserReducer';
import geoLocationReducer from "./geoLocationReducer";
import restaurantDetailsReducer from "./restaurantDetailsReducer";
import loyaltyMealsReducer from "./loyaltyMealsReducer";
import cartReducer from './cartReducer';
import supportChatReducer from './supportChatReducer';
import settingsReducer from './settingsReducer';

const rootReducer = combineReducers({
    loginReducer,
    homeReducer,
    orderReducer,
    termsPrivacyFaqReducer,
    offersReducer,
    editUserReducer,
    geoLocationReducer,
    restaurantDetailsReducer,
    loyaltyMealsReducer,
    cartReducer,
    supportChatReducer,
    settingsReducer,
});

export default rootReducer;
