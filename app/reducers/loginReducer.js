import * as ActionTypes from '../actions/types'

const defaultLoginState = {
    userData: {},
    isLogged: false,
    resetNavigation: undefined,
    isLoading: false,
    error: undefined,
    getotpAPIReponse: {},
    loginerror: {},
    resendOtp: {},
    selectedLanguage: null,
    logoutData: {},
    guestLogin: false
};

export default function loginReducer(state = defaultLoginState, action) {
    switch (action.type) {
        case ActionTypes.GUEST_LOGIN:
            return Object.assign({}, state, {
                guestLogin: action.responseData,
            });
        case ActionTypes.LOGIN_SERVICE_LOADING:
            return Object.assign({}, state, {
                isLoading: true,
            });
        case ActionTypes.LOGIN_SERVICE_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                error: action.error
            });

        case ActionTypes.GET_OTP_SERVICE_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                getotpAPIReponse: action.error
            });
        case ActionTypes.GET_OTP_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                getotpAPIReponse: action.responseData
            });
        case ActionTypes.RESEND_OTP_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                resendOtp: action.responseData
            });
        case ActionTypes.LOGIN_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                userData: action.responseData,
                isLogged: true
            });
        case ActionTypes.LOGIN_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                loginerror: action.responseData
            });
        case ActionTypes.LANGUAGE_SELECTION_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                selectedLanguage: action.responseData
            });
        case ActionTypes.RESET_LOGOUT_DATA:
            return Object.assign({}, state, {
                logoutData: {}
            });
        case ActionTypes.LOGOUT_SUCCESS:
            return Object.assign({}, state, {
                logoutData: action.responseData,
                userData: {},
                otpAPIReponse: {},
                isLogged: false,
                isLoading: false,
                error: undefined,
                success: undefined,
                resetNavigation: action.resetNavigation,
                isSessionExpired: true,
                guestLogin: false
            });
        default:
            return state;
    }
}
