import * as ActionTypes from '../actions/types'
import { restProfileLoyaltyMealsDetailsMapper } from '../lib/helperMethods'


const defaultLoyaltyDetailsState = {
    isLoading: false,
    resetNavigation: undefined,
    isLoading: false,
    error: undefined,
    selectedLanguage: null,
    restLoyaltyMealsDetails: {},

    pullRefresh: ''
};

export default function loyaltyMealsReducer(state = defaultLoyaltyDetailsState, action) {
    switch (action.type) {
        case ActionTypes.LOYALTY_SERVICE_LOADING:
            return Object.assign({}, state, {
                isLoading: true,
                pullRefresh: '',
            });
        case ActionTypes.LOYALTY_SERVICE_SUCCESS:
            return Object.assign({}, state, {
                
                pullRefresh: action.responseData.success,
                restLoyaltyMealsDetails: restProfileLoyaltyMealsDetailsMapper(action.responseData.data),
                // restDetails: action.responseData.data,
                isLoading: false,
            });
        case ActionTypes.MENU_DETAILS_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                error: action.error
            });
        case ActionTypes.CLEAR_LOYALTY_DETAILS:
            return Object.assign({}, state, {
                isLoading: false,
                resetNavigation: undefined,
                isLoading: false,
                error: undefined,
                selectedLanguage: null,
                relatedItemsData: {},
                restTypesDetails: {},
                restMenuDetails: {},
                restDetails: {},
                restSliderImages: {},
                restOfferDetails: {},
                restTermsDetails: {},
                restFacilitiesDetails: {},
                restLoyaltyMealsDetails: {},

                pullRefresh: ''
            });
        case ActionTypes.LOGOUT_SUCCESS:
            return {
                ...state,
                isLoading: false,
                resetNavigation: undefined,
                isLoading: false,
                error: undefined,
                selectedLanguage: null,
                relatedItemsData: {},
                restTypesDetails: {},
                restMenuDetails: {},
                restDetails: {},
                restSliderImages: {},
                restOfferDetails: {},
                restTermsDetails: {},
                restFacilitiesDetails: {},
                restLoyaltyMealsDetails: {},
            };
        default:
            return state;
    }
}
