import * as ActionTypes from '../actions/types'
import { offersCardMapper } from '../lib/helperMethods'
import { menuDetailsMapper } from '../lib/helperMethods'
import { addOnsDetailsMapper } from '../lib/helperMethods'
import { ingredientsDetailsMapper } from '../lib/helperMethods'
import { attributesDetailsMapper } from '../lib/helperMethods'
import { relatedItemsDetailsMapper } from '../lib/helperMethods'
import { relatedItemsTimeDetailsMapper } from '../lib/helperMethods'

const defaultOffersState = {
    isLoading: false,
    resetNavigation: undefined,
    isLoading: false,
    error: undefined,
    selectedLanguage: null,
    offerCardData: {},
    menuDetailsData: {},
    addOnsData: {},
    ingredientsData: {},
    attributesData: {},
    relatedItemsData: {},
    pullRefresh: ''
};

export default function offersReducer(state = defaultOffersState, action) {
    switch (action.type) {
        case ActionTypes.OFFERS_SERVICE_LOADING:
            return Object.assign({}, state, {
                pullRefresh: '',
                isLoading: true,
            });
        case ActionTypes.OFFERS_SERVICE_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                error: action.error
            });
        case ActionTypes.OFFERS_SERVICE_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                offerCardData: offersCardMapper(action.responseData.data),
                pullRefresh: action.responseData.success,
                isLoading: false,
                getoffersAPIReponse: action.responseData
            });
        case ActionTypes.MENU_DETAILS_SERVICE_LOADING:
            return Object.assign({}, state, {
                isLoading: true,
            });
        case ActionTypes.CLEAR_MENU_DETAILS:
            return Object.assign({}, state, {
                isLoading: false,
                menuDetailsData: {},
                addOnsData: {},
                ingredientsData: {},
                attributesData: {},
                relatedItemsData: {},
            });

        case ActionTypes.MENU_DETAILS_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                menuDetailsData: action.responseData.data.details,
                // menuDetailsData: menuDetailsMapper(action.responseData.data),
                addOnsData: addOnsDetailsMapper(action.responseData.data.details.item_addons),
                ingredientsData: ingredientsDetailsMapper(action.responseData.data.details.item_ingredients),
                attributesData: attributesDetailsMapper(action.responseData.data.details.item_attributes),
                relatedItemsData: relatedItemsDetailsMapper(action.responseData.data.related),
                // relatedItemsTimeData: relatedItemsTimeDetailsMapper(action.responseData.data.related.timing),

                isLoading: false,
            });
        case ActionTypes.MENU_DETAILS_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                error: action.error
            });
        case ActionTypes.LOGOUT_SUCCESS:
            return {
                ...state,
                isLoading: false,
                resetNavigation: undefined,
                isLoading: false,
                error: undefined,
                selectedLanguage: null,
                offerCardData: {},
                menuDetailsData: {},
                addOnsData: {},
                ingredientsData: {},
                attributesData: {},
                relatedItemsData: {},
                pullRefresh: ''
            };
        default:
            return state;
    }
}
