import * as ActionTypes from '../actions/types'

const defaultState = {
  deliveredOrders: [],
  returnedOrders: [],
  isLoading: false
};

export default function orderReducer(state = defaultState, action) {
  switch (action.type) {
    case ActionTypes.GET_COMPLETED_ORDER_LOADING:
      return Object.assign({}, state, {
        isLoading: true,
      });
    case ActionTypes.GET_COMPLETED_ORDER_ERROR:
      return Object.assign({}, state, {
        isLoading: false,
        error: action.error
      });
    case ActionTypes.GET_DELIVERED_ORDER_SUCCESS:
      return Object.assign({}, state, {
        isLoading: false,
        deliveredOrders: action.responseData
      });
    case ActionTypes.GET_RETURNED_ORDER_SUCCESS:
      return Object.assign({}, state, {
        isLoading: false,
        returnedOrders: action.responseData
      });
    default:
      return state;
  }
}
