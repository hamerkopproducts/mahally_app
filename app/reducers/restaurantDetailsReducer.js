import * as ActionTypes from '../actions/types'
import { restDetailsMapper } from '../lib/helperMethods'
import { restMenuDetailsMapper } from '../lib/helperMethods'
import { restOfferDetailsMapper } from '../lib/helperMethods'
import { restTermsDetailsMapper } from '../lib/helperMethods'
import { restFacilitiesDetailsMapper } from '../lib/helperMethods'
import { restSliderImagesMapper } from '../lib/helperMethods'
import { restLoyaltyMealsDetailsMapper } from '../lib/helperMethods'


const defaultrestaurantDetailsState = {
    isLoading: false,
    resetNavigation: undefined,
    isLoading: false,
    error: undefined,
    selectedLanguage: null,
    relatedItemsData: {},
    restTypesDetails: {},
    restMenuDetails: {},
    restDetails: {},
    restSliderImages: {},
    restOfferDetails: {},
    restTermsDetails: {},
    restFacilitiesDetails: {},
    restLoyaltyMealsDetails: {},

    pullRefresh: ''
};

export default function restaurantDetailsReducer(state = defaultrestaurantDetailsState, action) {
    switch (action.type) {
        case ActionTypes.REST_DETAILS_LOADING:
            return Object.assign({}, state, {
                isLoading: true,
                pullRefresh: '',
            });
        case ActionTypes.REST_DETAILS_SUCCESS:
            return Object.assign({}, state, {
                
                pullRefresh: action.responseData.success,
                restSliderImages: restSliderImagesMapper(action.responseData.data.details.restaurant_images),
                restTypesDetails: restDetailsMapper(action.responseData.data.details.type),
                restOfferDetails: restOfferDetailsMapper(action.responseData.data.details.offers),
                restTermsDetails: restTermsDetailsMapper(action.responseData.data.details.restaurent_terms),
                restFacilitiesDetails: restFacilitiesDetailsMapper(action.responseData.data.details.restaurent_facilities),
                restLoyaltyMealsDetails: restLoyaltyMealsDetailsMapper(action.responseData.data.details.meals),
                // restLoyaltyMealsFullDetails : restLoyaltyMealsDetails.map( item =>
                //     {
                //         location = action.responseData.data.details.location;
                //         return item;
                // }
                // ),
                restMenuDetails: Object.values(action.responseData.data.menuitems),
                restDetails: action.responseData.data.details,
                isLoading: false,
            });
        case ActionTypes.REST_DETAILS_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                error: action.error
            });
        case ActionTypes.CLEAR_REST_DETAILS:
            return Object.assign({}, state, {
                isLoading: false,
                resetNavigation: undefined,
                isLoading: false,
                error: undefined,
                selectedLanguage: null,
                relatedItemsData: {},
                restTypesDetails: [],
                restMenuDetails: {},
                restDetails: {},
                restSliderImages: {},
                restOfferDetails: {},
                restTermsDetails: {},
                restFacilitiesDetails: {},
                restLoyaltyMealsDetails: {},

                pullRefresh: ''
            });
        case ActionTypes.LOGOUT_SUCCESS:
            return {
                ...state,
                isLoading: false,
                resetNavigation: undefined,
                isLoading: false,
                error: undefined,
                selectedLanguage: null,
                relatedItemsData: {},
                restTypesDetails: {},
                restMenuDetails: {},
                restDetails: {},
                restSliderImages: {},
                restOfferDetails: {},
                restTermsDetails: {},
                restFacilitiesDetails: {},
                restLoyaltyMealsDetails: {},
            
                pullRefresh: ''
            };
        default:
            return state;
    }
}
