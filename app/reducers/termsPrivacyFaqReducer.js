import { act } from 'react-test-renderer';
import * as ActionTypes from '../actions/types'
import { howtouseDetailsMapper } from '../lib/helperMethods'
const defaultTermsState = {
    isLogged: false,
    resetNavigation: undefined,
    isLoading: false,
    error: undefined,
    success: undefined,
    isIntroFinished: false,
    languageSelected: null,
    termsAPIReponse: {},
    privacyAPIReponse: {},
    faqsAPIReponse: {},
    isLoggedOut: false
};

export default function termsPrivacyFaqReducer(state = defaultTermsState, action) {
    switch (action.type) {
        case ActionTypes.TERMS_LOADING:
            return Object.assign({}, state, {
                isLoading: true,
            });
        case ActionTypes.LANG_SWITCH:
            return Object.assign({}, state, {
                languageSelected: action.languageSelected,
            });

        case ActionTypes.TERMS_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                error: action.error
            });
        case ActionTypes.TERMS_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                isLoggedOut: false,
                termsAPIReponse: action.responseData
            });
        case ActionTypes.FAQS_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                isLoggedOut: false,
                faqsAPIReponse: action.responseData
            });
        case ActionTypes.PRIVACY_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                isLoggedOut: false,
                privacyAPIReponse: action.responseData
            });
        case ActionTypes.ABOUT_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                error: action.error
            });
        case ActionTypes.ABOUT_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                isLoggedOut: false,
                aboutAPIReponse: action.responseData
            });
        case ActionTypes.HOWTOUSE_SUCCESS:
            return Object.assign({}, state, {
                isLoading: false,
                isLoggedOut: false,
                howToUseDataReponse: howtouseDetailsMapper(action.responseData.data),
            });
        case ActionTypes.HOWTOUSE_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                isLoggedOut: false,
                howToUseDataError: action.responseData
            });
        case ActionTypes.RESET_ERROR:
            return Object.assign({}, state, {
                isLoading: false,
                error:null,
            });
            case ActionTypes.SIGNOUT_SUCCESS:
                return {
                  ...state,
          
                  resetNavigation: action.resetNavigation,
                  error: undefined,
                  success: undefined,
                  isIntroFinished: false,
                  isLoggedOut: true,
          
                  isLogged: false,
                  resetNavigation: undefined,
                  isLoading: false,
                  isIntroFinished: false,
                //   languageSelected: null,
                  termsAPIReponse: {},
                  privacyAPIReponse: {},
                  faqsAPIReponse: {},
                };
        default:
            return state;
    }
}
