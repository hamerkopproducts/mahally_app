import rootReducer from './app/reducers'

// Imports: Dependencies
import AsyncStorage from '@react-native-community/async-storage';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';

// const middleware = [thunk];

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: [
    'loginReducer',
    'homeReducer',
    'orderReducer',
    'offersReducer',
    'editUserReducer',
    'geoLocationReducer',
    'restaurantDetailsReducer',
    'loyaltyMealsReducer',
    'cartReducer',
    'supportChatReducer',
    'settingsReducer'
  ],
  blacklist: [
  ],
};

const persistedReducer = persistReducer(persistConfig, rootReducer)
const store = createStore(
  persistedReducer,
  applyMiddleware(thunk),
);
let persistor = persistStore(store);

export {
  store,
  persistor,
};