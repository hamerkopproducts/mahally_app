/**
 * @format
 */
import 'react-native-gesture-handler';
/**
 * @format
 */
import React, { Component } from 'react';
import { AppRegistry, Text } from 'react-native';
import { name as appName } from './app.json';
import App from './App';

import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/es/integration/react';
import { store, persistor } from './configureStore';

if (Text.defaultProps == null) {
    Text.defaultProps = {};
    Text.defaultProps.allowFontScaling = false;
}

export default class Emtyaz extends Component {
    render() {
        return (<Provider store={store}>
            <PersistGate loading={null} persistor={persistor} />
            <App />
        </Provider>
        );
    }
}
AppRegistry.registerComponent(appName, () => Emtyaz);
